using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTickDamage : MonoBehaviour
{

    public float onTickDamage = 1f;
    public ChangePostProcessProfile postProcess;
    private void Start()
    {
        postProcess = FindObjectOfType<ChangePostProcessProfile>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ToxicArea"))
        {
            GetComponent<Health>().SetBurnState(true, onTickDamage);
        }
        if (other.CompareTag("ZoneWall"))
        {
            
            //postProcess.ChangeProfile(PROFILE_STATE.DEFAULT);
            //GetComponent<Health>().SetBurnState(false, onTickDamage);
            GetComponent<Health>().isBurning = false;
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("ToxicArea"))
        {
            GetComponent<Health>().SetBurnState(false, onTickDamage);
        }
        if (other.CompareTag("ZoneWall"))
        {
            postProcess.ChangeProfile(PROFILE_STATE.OUTSIDE);
            //GetComponent<Health>().ReduceOnTick(onTickDamage, null, "Electric Zone");
            GetComponent<Health>().isBurning = true;
        }
    }
}
