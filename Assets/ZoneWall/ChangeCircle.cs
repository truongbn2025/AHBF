﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;
[RequireComponent(typeof(LineRenderer))]
public class ChangeCircle : NetworkBehaviour
{
	[Range(0, 360)]
	public int Segments;
	[Range(0, 5000)]
	public float XRadius;
	[Range(0, 5000)]
	public float YRadius;  //THIS IS NOT USED - SHOULD BE ELIMINATED

	[Range(10, 100)]
	public int ZoneRadiusFactor = 50; //default to 50%

	[Header("Shrinking Zones")]
	public List<int> ZoneTimes;

	#region Private Members
	private bool Shrinking;  // this can be set to PUBLIC in order to troubleshoot.  It will show a checkbox in the Inspector
	private bool CanShrink;
	private int countdownPrecall = 10;  //this MIGHT be public, but it should not need to be changed
	private int timeToShrink = 10; //seconds
	private int count = 0;
	private bool newCenterObtained = false;
	public Vector3 centerPoint = new Vector3(0, -100, 0);
	private float distanceToMoveCenter;
	private WorldCircle circle;
	private LineRenderer renderer;
	[SerializeField] private GameObject ZoneWall;
	private float[] radii = new float[2];
	private float shrinkRadius;
	private int zoneRadiusIndex = 0;
	private int zoneTimesIndex = 0;
	private float timePassed;

    #endregion
    private float elapsed_time;

    public override void OnStartNetwork()
	{

		base.OnStartNetwork();
       
        renderer = gameObject.GetComponent<LineRenderer>();
        radii[0] = XRadius; radii[1] = YRadius;
        circle = new WorldCircle(ref renderer, Segments, radii);
        //ZoneWall = GameObject.FindGameObjectWithTag("ZoneWall");

        timePassed = Time.deltaTime;
    }


    void Update()
	{
		
		if (base.IsServer)
        {
			ServerExecute();
        }
		

        ZoneWall.transform.localScale = new Vector3((XRadius * 0.01f), 1, (XRadius * 0.01f));
	}

	private void ServerExecute()
    {
		if (Shrinking)
		{
			// we need a new center point (that is within the bounds of the current zone)
			if (!newCenterObtained)
			{
				centerPoint = NewCenterPoint(transform.position, XRadius, shrinkRadius);
				distanceToMoveCenter = Vector3.Distance(transform.position, centerPoint); //this is used in the Lerp (below)
				newCenterObtained = (centerPoint != new Vector3(0, -100, 0));
			}
            
            ZoneWall.transform.localScale = new Vector3((XRadius * 0.01f), 1, (XRadius * 0.01f));
            ShrinkCircleInClients(centerPoint, shrinkRadius);

            // move the center point, over time
            //transform.position = Vector3.Lerp(transform.position, centerPoint, (distanceToMoveCenter / timeToShrink) * Time.deltaTime);
            //transform.position = centerPoint;
            // shrink the zone diameter, over time
            //ZoneWall.transform.position = Vector3.Lerp(ZoneWall.transform.position, centerPoint, (distanceToMoveCenter / timeToShrink) * Time.deltaTime);
            //ShrinkingCircle();

            //circle.Draw(Segments, XRadius, XRadius);

            // MoveTowards will continue infinitum, so we must test that we have gotten close enough to be DONE
            if (1 > (XRadius - shrinkRadius))
			{
				timePassed = Time.deltaTime;
				Shrinking = false;
				newCenterObtained = false;
			}
		}
		else
		{
			timePassed += Time.deltaTime; // increment clock time
		}

		// have we passed the next threshold for time delay?
		if (((int)timePassed) > ZoneTimes[zoneTimesIndex])
		{
			shrinkRadius = ShrinkCircle((float)(XRadius * (ZoneRadiusFactor * 0.01)))[1];  //use the ZoneRadiusFactor as a percentage
			DrawCircle((float)(XRadius * ZoneRadiusFactor * 0.01));
			Shrinking = true;
			CanShrink = true;
			ShrinkCircleState();
			timePassed = Time.deltaTime;  //reset the time so other operations are halted.
			NextZoneTime();
		}
		if (timePassed > (ZoneTimes[zoneTimesIndex] - countdownPrecall))
		{  // we need to begin counting down
			if (ZoneTimes[zoneTimesIndex] - (int)timePassed != count)
			{
				count = Mathf.Clamp(ZoneTimes[zoneTimesIndex] - (int)timePassed, 1, 1000);  // this ensures our value never falls below zero
				Debug.Log("Shrinking in " + count + " seconds.");
			}
		}
	}

	[ObserversRpc (RunLocally = true)]
	private void ShrinkCircleInClients(Vector3 centerPoint, float shrinkRadius)
    {
		ShrinkingCircle(centerPoint, shrinkRadius);
		
	}
	[ObserversRpc (RunLocally = true) ]
	private void ShrinkCircleState()
	{
		CanShrink = true;
	}
	private void ShrinkingCircle(Vector3 centerPoint, float shrinkRadius)
	{
		transform.position = centerPoint;
		if (CanShrink)
		{
			CanShrink = false;
			StartCoroutine(moveToPosition(ZoneWall.transform.position, centerPoint, timeToShrink *2.5f));
		}
        if (base.IsServer)
        {
			XRadius = Mathf.MoveTowards(XRadius, shrinkRadius, (shrinkRadius / timeToShrink) * Time.deltaTime);
			//Debug.Log("new X radius " + XRadius);
			GetNewXRadius(XRadius);
		}
     
		
	}
	[ObserversRpc]
	private void GetNewXRadius(float NewXRadius)
    {
		XRadius = NewXRadius;
    }
	[ObserversRpc (RunLocally =true)]
	private void DrawCircle(float radius)
	{
		circle.Draw(Segments, radius, radius);
	}
	public IEnumerator moveToPosition(Vector3 oldPosition, Vector3 position, float timeToMove)
	{
        //Debug.Log("coroutine" + centerPoint + position);
        ////var currentPos = transform.position;
        //for (float t = 0; t < 1; t += Time.deltaTime / timeToMove)
        //{

        //	ZoneWall.transform.position = Vector3.Lerp(oldPosition, position, t);
        //	if (Vector3.Distance(ZoneWall.transform.position, centerPoint) < 0.05f)
        //	{
        //		ZoneWall.transform.position = position;
        //		break;
        //	}
        //	yield return null;
        //}
       
        while (elapsed_time < timeToMove)
        {
            // Calculate the interpolation factor (0 to 1) based on elapsed time and duration
            float t = elapsed_time / timeToMove;

            // Interpolate the position based on the start and target positions
            Vector3 current_position = Vector3.Lerp(oldPosition, position, t);

            // Update the game object's position to the current position
            ZoneWall.transform.position = current_position;

            elapsed_time += Time.deltaTime;
			//Debug.Log(elapsed_time + "/" + timeToMove);
            yield return null;
        }
    }
	private Vector3 NewCenterPoint(Vector3 currentCenter, float currentRadius, float newRadius)
	{
		Vector3 newPoint = Vector3.zero;

		var totalCountDown = 30000; //prevent endless loop which will kill Unity
		var foundSuitable = false;
		while (!foundSuitable)
		{
			totalCountDown--;
			Vector2 randPoint = Random.insideUnitCircle * (currentRadius * 0.5f);

			newPoint = new Vector3(randPoint.x, 0, randPoint.y);
			foundSuitable = (Vector3.Distance(currentCenter, newPoint) < currentRadius / 2);
			if (totalCountDown < 1)
				return new Vector3(0, -100, 0);  //explicitly define an error has occured.  In this case we did not locate a reasonable point
		}
		Debug.Log("New Center Point is " + newPoint);
		return newPoint;
	}

	private int NextZoneTime()
	{
		//if we have exceeded the count, just start over
		if (zoneTimesIndex >= ZoneTimes.Count - 1) // Lists are zero-indexed
			zoneTimesIndex = -1;  // the fall-through (below) will increment this

		// next time to wait
		return ZoneTimes[++zoneTimesIndex];
	}

	// This is a general purpose method
	private float[] ShrinkCircle(float amount)
	{
		float newXR = circle.radii[0] - amount;
		float newYR = circle.radii[1] - amount;
		float[] retVal = new float[2];
		retVal[0] = newXR;
		retVal[1] = newYR;
		return retVal;
	}
}
