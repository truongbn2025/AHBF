using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionControll : MonoBehaviour
{
    Collider collider;
    private void Awake()
    {
        collider = GetComponent<Collider>();
    }
    
    void OnCollisionStay(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            
            Debug.DrawRay(contact.point, contact.normal, Color.red);
        }
    }
    void OnTriggerStay(Collider other)
    {
        var temp = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
        Debug.Log(temp);
    }
}
