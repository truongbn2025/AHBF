
using FishNet.Object;
using UnityEngine;


public class SpawnEnemyBot : NetworkBehaviour
{
    Vector3 spawnPostion;
    public int spawnCount = 0;
    public float maxOffsetX;
    public float maxOffsetZ;
    public GameObject spawnPrefab;

    private int spawned = 0;
    private int getOutOfLoopKey = 10000;

    public override void OnStartNetwork()
    {
        base.OnStartNetwork();
        spawnPostion = transform.position;
        SpawnBot();
    }
    public void SpawnBot()
    {
        while (spawnCount > 0) {
            getOutOfLoopKey--; 
            Vector3 newPos = new Vector3(spawnPostion.x + Random.Range(0f, maxOffsetX), spawnPostion.y, spawnPostion.z + Random.Range(0f, maxOffsetZ));
            Debug.Log("V:" + newPos);
            RaycastHit hit;
            if (Physics.Raycast(newPos, Vector3.down, out hit, Mathf.Infinity))
            {
                Debug.Log("tag: " + hit.transform.tag + hit.transform.name);
                //Debug.Log("inside raycast");
                if (hit.transform.CompareTag("Ground"))
                {
                    Debug.Log("inside raycast Ground");
                    Instantiate(spawnPrefab, hit.point, Quaternion.identity);
                    spawnCount--;
                    spawned++;
                }
            }
            if(getOutOfLoopKey<=0)
            {
                Debug.Log("Spawned:"+spawned);
                return;
            }
        }
    }


}
