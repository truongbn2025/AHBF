using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartStats : MonoBehaviour
{
    public int Strength;
    public int Agility;
    public int Intelligent;
}
