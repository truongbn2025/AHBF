using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Head", menuName = "BodyPartSystem/Head", order = 1)]
public class Heads : BodyPart
{
  
    private void Awake()
    {
        partName = BODY_PART.HEAD;
    }
}
