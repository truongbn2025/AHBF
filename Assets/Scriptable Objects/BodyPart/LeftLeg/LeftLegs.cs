using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LeftLeg", menuName = "BodyPartSystem/LeftLeg", order = 1)]
public class LeftLegs : BodyPart
{
    private void Awake()
    {
        partName = BODY_PART.LEFT_LEG;
    }
}
