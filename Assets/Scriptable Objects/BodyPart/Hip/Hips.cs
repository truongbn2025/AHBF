using UnityEngine;

[CreateAssetMenu(fileName = "Hip", menuName = "BodyPartSystem/Hip", order = 1)]
public class Hips : BodyPart
{
    private void Awake()
    {
        partName = BODY_PART.HIP;
    }
}
