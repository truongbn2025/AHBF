using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BodyPart : ScriptableObject
{
    //public GameObject prefabs;
    public BODY_PART partName;
    public BODY_STATE state;
    public int Id;
    public GameObject prefab;
    [SerializeField] Sprite icon;
    public int bodyPartIndex;
    public int level;
    public string name;
    public int rarity;
    [TextArea(15, 20)]
    public string description;

    public Sprite GetICon()
    {
        return icon;
    }

}


