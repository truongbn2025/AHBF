
using UnityEngine;

[CreateAssetMenu(fileName = "RightLeg", menuName = "BodyPartSystem/RightLeg", order = 1)]
public class RightLegs : BodyPart
{
    private void Awake()
    {
        partName = BODY_PART.RIGHT_LEG;
    }
}
