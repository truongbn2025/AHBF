using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "RightArm", menuName = "BodyPartSystem/RightArm", order = 1)]
public class RightArms : BodyPart
{
    private void Awake()
    {
        partName = BODY_PART.RIGHT_ARM;
    }
}
