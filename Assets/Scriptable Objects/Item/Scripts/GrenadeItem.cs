using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Grenade", menuName = "InventorySystem/Items/Grenade", order = 1)]
public class GrenadeItem : ItemObject
{
    public int Amount;
    private void Awake()
    {
        type = ITEM_TYPE.GRENADE;
    }
}
