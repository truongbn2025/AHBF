using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PrimaryGun", menuName = "InventorySystem/Items/Weapon/PrimaryGun", order = 1)]
public class PrimaryWeapon : ItemObject
{
    public int Amount;
    private void Awake()
    {
        type = ITEM_TYPE.WEAPON_GUN_PRIMARY;
    }
}
