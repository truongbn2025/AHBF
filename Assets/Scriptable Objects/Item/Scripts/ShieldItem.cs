using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ShieldItem", menuName = "InventorySystem/Items/Shield", order = 1)]
public class ShieldItem : ItemObject
{

    private void Awake()
    {
        type = ITEM_TYPE.SHIELD;
    }
}
