using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PrimaryAmmo", menuName = "InventorySystem/Items/Ammo/PrimaryAmmo", order = 1)]
public class PrimaryAmmoItem : ItemObject
{

    private void Awake()
    {
        type = ITEM_TYPE.AMMO_PRIMARY;
        
    }
}
