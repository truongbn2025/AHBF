using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//public enum Attributes
//{
//    Agility,
//    Intellect,
//    Stamina,
//    Strength
//}
public abstract class ItemObject : ScriptableObject
{
    public int Id;
    public GameObject prefab;
    public Sprite icon;
    public Sprite tooltipIcon;
    public ITEM_TYPE type;
    public int weaponIndex;
    public int amount;
    public string name;
    public int rarity;
    public int restoreHealthValue;
    [TextArea(15, 20)]
    public string description;

  
    //public ItemBuff[] buffs;

    //public Item CreateItem()
    //{
    //    Item newItem = new Item(this);
    //    return newItem;
    //}
}

//[System.Serializable]
//public class Item
//{
//    public string Name;
//    public int Id;
//    public ItemBuff[] buffs;
//    public Item(ItemObject item)
//    {
//        Name = item.name;
//        Id = item.Id;
//        buffs = new ItemBuff[item.buffs.Length];
//        for (int i = 0; i < buffs.Length; i++)
//        {
//            buffs[i] = new ItemBuff(item.buffs[i].min, item.buffs[i].max)
//            {
//                attribute = item.buffs[i].attribute
//            };
//        }
//    }
//}

//[System.Serializable]
//public class ItemBuff
//{
//    public Attributes attribute;
//    public int value;
//    public int min;
//    public int max;
//    public ItemBuff(int _min, int _max)
//    {
//        min = _min;
//        max = _max;
//        GenerateValue();
//    }
//    public void GenerateValue()
//    {
//        value = UnityEngine.Random.Range(min, max);
//    }
//}