using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "EnegyCore", menuName = "InventorySystem/Items/Currency", order = 1)]
public class CurrencyItem : ItemObject
{
    public int Amount;
    private void Awake()
    {
        type = ITEM_TYPE.CURRENCY;
    }
}
