using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Melee", menuName = "InventorySystem/Items/Weapon/Melee", order = 1)]
public class MeleeWeaponItem : ItemObject
{
    
    private void Awake()
    {
        type = ITEM_TYPE.WEAPON_MELEE;
        
    }
}
