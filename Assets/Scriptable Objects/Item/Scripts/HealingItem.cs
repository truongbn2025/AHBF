using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "HealingItem", menuName = "InventorySystem/Items/Healing", order = 1)]
public class HealingItem : ItemObject
{
    
    private void Awake()
    {
        type = ITEM_TYPE.HEALING;
    }
}
