using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffItem : ItemObject
{
    public float RestoreHealthValue;
    private void Awake()
    {
        type = ITEM_TYPE.OTHER;
    }
}
