using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPartManager : MonoBehaviour
{
    public List<HeadSlot> _head = new List<HeadSlot>();
    public List<BodySlot> _body = new List<BodySlot>();
    public List<HipSlot> _hip = new List<HipSlot>();
    public List<LeftArmSlot> _leftArm = new List<LeftArmSlot>();
    public List<RighttArmSlot> _rightArm = new List<RighttArmSlot>();
    public List<LeftLegSlot> _leftLeg = new List<LeftLegSlot>();
    public List<RightLegSlot> _rightLeg = new List<RightLegSlot>();

    private void Awake()
    {
        _head.Clear();
        _body.Clear();
        _hip.Clear();
        _leftArm.Clear();
        _rightArm.Clear();
        _leftLeg.Clear();
        _rightLeg.Clear();
    }

    public void ChangeHead(BodyPart _part)
    {
        _head.Clear();
        _head.Add(new HeadSlot(_part));
    }
}



[System.Serializable]
public class HeadSlot
{
    public BodyPart playerHead;
    public HeadSlot(BodyPart _playerHead)
    {
        playerHead = _playerHead;
    }
}

[System.Serializable]
public class BodySlot
{
    public BodyPart playerBody;
    public BodySlot(BodyPart _playerBody)
    {
        playerBody = _playerBody;
    }
}

[System.Serializable]
public class HipSlot
{
    public BodyPart playerBody;
    public HipSlot(BodyPart _playerHips)
    {
        playerBody = _playerHips;
    }
}

[System.Serializable]
public class LeftArmSlot
{
    public BodyPart playerLeftArm;
    public LeftArmSlot(BodyPart _playerLeftArm)
    {
        playerLeftArm = _playerLeftArm;
    }
}

[System.Serializable]
public class RighttArmSlot
{
    public BodyPart playerRightArm;
    public RighttArmSlot(BodyPart _playerRightArm)
    {
        playerRightArm = _playerRightArm;
    }
}
[System.Serializable]
public class LeftLegSlot
{
    public BodyPart playerLeftLeg;
    public LeftLegSlot(BodyPart _playerLeftLeg)
    {
        playerLeftLeg = _playerLeftLeg;
    }
}

[System.Serializable]
public class RightLegSlot
{
    public BodyPart playerRightLeg;
    public RightLegSlot(BodyPart _playerRightLeg)
    {
        playerRightLeg = _playerRightLeg;
    }
}


