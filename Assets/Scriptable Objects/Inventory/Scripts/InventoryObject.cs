using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "Inventory", menuName = "InventorySystem/Inventory", order = 2)]
public class InventoryObject 
{
    public List<InventorySlot> Container = new List<InventorySlot>();
    public List<MeleeWeaponSlot> MeleeWeapon = new List<MeleeWeaponSlot>();
    public List<PrimaryGun> PrimaryGun = new List<PrimaryGun>();
    public List<SecondaryGun> SecondaryGun = new List<SecondaryGun>();
    public List<Grenade> Grenade = new List<Grenade>();
    public List<PrimaryAmmo> PrimaryAmmo = new List<PrimaryAmmo>();
    public List<SecondaryAmmo> SecondaryAmmo = new List<SecondaryAmmo>();

    private void Awake()
    {
        Container.Clear();
        MeleeWeapon.Clear();
        PrimaryGun.Clear();
        SecondaryGun.Clear();
        
    }
    public void AddItem(ItemObject _item, int _amount)
    {
        
        bool hasItem = false;
        for (int i = 0; i < Container.Count; i++)
        {
            if (Container[i].item == _item)
            {
                Container[i].AddAmount(_amount);
                hasItem = true;
                break;
            }
        }
        if (!hasItem)
        {
            Container.Add(new InventorySlot(_item, _amount));
        }
    }
    public void AddMeleeWeapon(ItemObject _item)
    {
        MeleeWeapon.Add(new MeleeWeaponSlot(_item));
    }
    public void AddPrimaryGun(ItemObject _item)
    {
        PrimaryGun.Add(new PrimaryGun(_item));
    }
    public void AddSecondaryGun(ItemObject _item)
    {
        SecondaryGun.Add(new SecondaryGun(_item));
    }
    public void AddPrimaryAmmo(ItemObject _item, int _amount)
    {
        bool hasItem = false;
        for (int i = 0; i < PrimaryAmmo.Count; i++)
        {
            if (PrimaryAmmo[i].item == _item)
            {
                
                PrimaryAmmo[i].AddAmount(_amount);
                hasItem = true;
                break;
            }
        }
        if (!hasItem)
        {
            PrimaryAmmo.Add(new PrimaryAmmo(_item, _amount));
        }
        
    }
    public void AddSecondaryAmmo(ItemObject _item, int _amount)
    {
        bool hasItem = false;
        for (int i = 0; i < SecondaryAmmo.Count; i++)
        {
            if (SecondaryAmmo[i].item == _item)
            {

                SecondaryAmmo[i].AddAmount(_amount);
                hasItem = true;
                break;
            }
        }
        if (!hasItem)
        {
            SecondaryAmmo.Add(new SecondaryAmmo(_item, _amount));
        }
        
    }
    public void AddGrenade(ItemObject _item, int _amount)
    {
        bool hasItem = false;
        for (int i = 0; i < Grenade.Count; i++)
        {
            if (Grenade[i].item == _item)
            {
                Grenade[i].AddAmount(_amount);
                hasItem = true;
                break;
            }
        }
        if (!hasItem)
        {
            Grenade.Add(new Grenade(_item, _amount));
        }
    }
}

[System.Serializable]
public class InventorySlot{
    public ItemObject item;
    public int amount;

    public InventorySlot(ItemObject _item, int _amount)
    {
        item = _item;
        amount = _amount;
    }
    public void AddAmount(int value)
    {
        amount += value;
    }
}
[System.Serializable]
public class Grenade
{
    public ItemObject item;
    public int amount;

    public Grenade(ItemObject _item, int _amount)
    {
        item = _item;
        amount = _amount;
    }
    public void AddAmount(int value)
    {
        amount += value;
    }
}

[System.Serializable]
public class MeleeWeaponSlot
{
    public ItemObject item;
    public MeleeWeaponSlot(ItemObject _item)
    {
        item = _item;
       
    }

}

[System.Serializable]
public class PrimaryGun
{
    public ItemObject item;
    public PrimaryGun(ItemObject _item)
    {
        item = _item;

    }

}

[System.Serializable]
public class SecondaryGun
{
    public ItemObject item;
    public SecondaryGun(ItemObject _item)
    {
        item = _item;

    }

}
[System.Serializable]
public class PrimaryAmmo
{
    public ItemObject item;
    public int amount;
    public PrimaryAmmo(ItemObject _item, int _amount)
    {
        item = _item;
        amount = _amount;
    }
    public void AddAmount(int value)
    {
        amount += value;
    }

}
[System.Serializable]
public class SecondaryAmmo
{
    public ItemObject item;
    public int amount;
    public SecondaryAmmo(ItemObject _item, int _amount)
    {
        item = _item;
        amount = _amount;
    }
    public void AddAmount(int value)
    {
        amount += value;
    }

}