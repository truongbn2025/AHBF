// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "GPUInstancer/BOXOPHOBIC/The Vegetation Engine/Default/Grass Subsurface Lit"
{

	//-------------------------------------------------------------------------------------
	// BEGIN_PROPERTIES
	//-------------------------------------------------------------------------------------

	Properties
	{
		[HideInInspector] _EmissionColor("Emission Color", Color) = (1,1,1,1)
		[HideInInspector] _AlphaCutoff("Alpha Cutoff ", Range(0, 1)) = 0.5
		[StyledCategory(Render Settings, 5, 10)]_CategoryRender("[ Category Render ]", Float) = 0
		[Enum(Opaque,0,Transparent,1)]_RenderMode("Render Mode", Float) = 0
		[Enum(Off,0,On,1)]_RenderZWrite("Render ZWrite", Float) = 1
		[Enum(Both,0,Back,1,Front,2)]_RenderCull("Render Faces", Float) = 0
		[Enum(Flip,0,Mirror,1,Same,2)]_RenderNormals("Render Normals", Float) = 0
		[HideInInspector]_RenderQueue("Render Queue", Float) = 0
		[HideInInspector]_RenderPriority("Render Priority", Float) = 0
		[Enum(Off,0,On,1)]_RenderSpecular("Render Specular", Float) = 1
		[Enum(Off,0,On,1)]_RenderDecals("Render Decals", Float) = 0
		[Enum(Off,0,On,1)]_RenderSSR("Render SSR", Float) = 0
		[Space(10)]_RenderDirect("Render Direct", Range( 0 , 1)) = 1
		_RenderShadow("Render Shadow", Range( 0 , 1)) = 1
		_RenderAmbient("Render Ambient", Range( 0 , 1)) = 1
		[Enum(Off,0,On,1)][Space(10)]_RenderClip("Alpha Clipping", Float) = 1
		[Enum(Off,0,On,1)]_RenderCoverage("Alpha To Mask", Float) = 0
		_AlphaClipValue("Alpha Treshold", Range( 0 , 1)) = 0.5
		_AlphaFeatherValue("Alpha Feather", Range( 0 , 2)) = 0.5
		[StyledSpace(10)]_SpaceRenderFade("# Space Render Fade", Float) = 0
		[StyledCategory(Global Settings)]_CategoryGlobal("[ Category Global ]", Float) = 0
		[StyledEnum(TVELayers, Default 0 Layer_1 1 Layer_2 2 Layer_3 3 Layer_4 4 Layer_5 5 Layer_6 6 Layer_7 7 Layer_8 8, 0, 0)]_LayerColorsValue("Layer Colors", Float) = 0
		[StyledEnum(TVELayers, Default 0 Layer_1 1 Layer_2 2 Layer_3 3 Layer_4 4 Layer_5 5 Layer_6 6 Layer_7 7 Layer_8 8, 0, 0)]_LayerExtrasValue("Layer Extras", Float) = 0
		[StyledEnum(TVELayers, Default 0 Layer_1 1 Layer_2 2 Layer_3 3 Layer_4 4 Layer_5 5 Layer_6 6 Layer_7 7 Layer_8 8, 0, 0)]_LayerMotionValue("Layer Motion", Float) = 0
		[StyledEnum(TVELayers, Default 0 Layer_1 1 Layer_2 2 Layer_3 3 Layer_4 4 Layer_5 5 Layer_6 6 Layer_7 7 Layer_8 8, 0, 0)]_LayerVertexValue("Layer Vertex", Float) = 0
		[StyledSpace(10)]_SpaceGlobalLayers("# Space Global Layers", Float) = 0
		[StyledMessage(Info, Procedural Variation in use. The Variation might not work as expected when switching from one LOD to another., _VertexVariationMode, 1 , 0, 10)]_MessageGlobalsVariation("# Message Globals Variation", Float) = 0
		_GlobalColors("Global Color", Range( 0 , 1)) = 1
		_GlobalAlpha("Global Alpha", Range( 0 , 1)) = 1
		_GlobalOverlay("Global Overlay", Range( 0 , 1)) = 1
		_GlobalWetness("Global Wetness", Range( 0 , 1)) = 1
		_GlobalEmissive("Global Emissive", Range( 0 , 1)) = 1
		_GlobalSize("Global Size Fade", Range( 0 , 1)) = 1
		[StyledSpace(10)]_SpaceGlobalLocals("# Space Global Locals", Float) = 0
		[StyledRemapSlider(_ColorsMaskMinValue, _ColorsMaskMaxValue, 0, 1)]_ColorsMaskRemap("Color Mask", Vector) = (0,0,0,0)
		[HideInInspector]_ColorsMaskMinValue("Color Mask Min Value", Range( 0 , 1)) = 0
		[HideInInspector]_ColorsMaskMaxValue("Color Mask Max Value", Range( 0 , 1)) = 0
		_ColorsVariationValue("Color Variation", Range( 0 , 1)) = 0
		[StyledRemapSlider(_AlphaMaskMinValue, _AlphaMaskMaxValue, 0, 1, 10, 0)]_AlphaMaskRemap("Alpha Mask", Vector) = (0,0,0,0)
		_AlphaVariationValue("Alpha Variation", Range( 0 , 1)) = 0
		[StyledRemapSlider(_OverlayMaskMinValue, _OverlayMaskMaxValue, 0, 1)]_OverlayMaskRemap("Overlay Mask", Vector) = (0,0,0,0)
		[HideInInspector]_OverlayMaskMinValue("Overlay Mask Min Value", Range( 0 , 1)) = 0.45
		[HideInInspector]_OverlayMaskMaxValue("Overlay Mask Max Value", Range( 0 , 1)) = 0.55
		_OverlayVariationValue("Overlay Variation", Range( 0 , 1)) = 0
		[StyledSpace(10)]_SpaceGlobalPosition("# Space Global Position", Float) = 0
		[StyledToggle]_ColorsPositionMode("Use Pivot Position for Colors", Float) = 0
		[StyledToggle]_ExtrasPositionMode("Use Pivot Position for Extras", Float) = 0
		[StyledCategory(Main Settings)]_CategoryMain("[Category Main ]", Float) = 0
		[NoScaleOffset][StyledTextureSingleLine]_MainAlbedoTex("Main Albedo", 2D) = "white" {}
		[NoScaleOffset][StyledTextureSingleLine]_MainNormalTex("Main Normal", 2D) = "bump" {}
		[NoScaleOffset][StyledTextureSingleLine]_MainMaskTex("Main Mask", 2D) = "white" {}
		[Space(10)][StyledVector(9)]_MainUVs("Main UVs", Vector) = (1,1,0,0)
		[HDR]_MainColor("Main Color", Color) = (1,1,1,1)
		_MainNormalValue("Main Normal", Range( -8 , 8)) = 1
		_MainOcclusionValue("Main Occlusion", Range( 0 , 1)) = 0
		_MainSmoothnessValue("Main Smoothness", Range( 0 , 1)) = 0
		[StyledCategory(Detail Settings)]_CategoryDetail("[ Category Detail ]", Float) = 0
		[Enum(Off,0,On,1)]_DetailMode("Detail Mode", Float) = 0
		[Enum(Overlay,0,Replace,1)]_DetailBlendMode("Detail Blend", Float) = 1
		[Enum(Vertex Blue,0,Projection,1)]_DetailTypeMode("Detail Type", Float) = 0
		[StyledRemapSlider(_DetailBlendMinValue, _DetailBlendMaxValue,0,1)]_DetailBlendRemap("Detail Blending", Vector) = (0,0,0,0)
		[StyledCategory(Occlusion Settings)]_CategoryOcclusion("[ Category Occlusion ]", Float) = 0
		[HDR]_VertexOcclusionColor("Vertex Occlusion Color", Color) = (1,1,1,1)
		[StyledRemapSlider(_VertexOcclusionMinValue, _VertexOcclusionMaxValue, 0, 1)]_VertexOcclusionRemap("Vertex Occlusion Mask", Vector) = (0,0,0,0)
		[HideInInspector]_VertexOcclusionMinValue("Vertex Occlusion Min Value", Range( 0 , 1)) = 0
		[HideInInspector]_VertexOcclusionMaxValue("Vertex Occlusion Max Value", Range( 0 , 1)) = 1
		[StyledCategory(Subsurface Settings)]_CategorySubsurface("[ Category Subsurface ]", Float) = 0
		_SubsurfaceValue("Subsurface Intensity", Range( 0 , 1)) = 1
		[HDR]_SubsurfaceColor("Subsurface Color", Color) = (0.4,0.4,0.1,1)
		[StyledRemapSlider(_SubsurfaceMaskMinValue, _SubsurfaceMaskMaxValue,0,1)]_SubsurfaceMaskRemap("Subsurface Mask", Vector) = (0,0,0,0)
		[HideInInspector]_SubsurfaceMaskMinValue("Subsurface Mask Min Value", Range( 0 , 1)) = 0
		[HideInInspector]_SubsurfaceMaskMaxValue("Subsurface Mask Max Value", Range( 0 , 1)) = 1
		[Space(10)][DiffusionProfile]_SubsurfaceDiffusion("Subsurface Diffusion", Float) = 0
		[HideInInspector]_SubsurfaceDiffusion_Asset("Subsurface Diffusion", Vector) = (0,0,0,0)
		[HideInInspector][Space(10)][ASEDiffusionProfile(_SubsurfaceDiffusion)]_SubsurfaceDiffusion_asset("Subsurface Diffusion", Vector) = (0,0,0,0)
		[Space(10)]_SubsurfaceScatteringValue("Subsurface Scattering", Range( 0 , 16)) = 2
		_SubsurfaceAngleValue("Subsurface Angle", Range( 1 , 16)) = 8
		_SubsurfaceNormalValue("Subsurface Normal", Range( 0 , 1)) = 0
		_SubsurfaceDirectValue("Subsurface Direct", Range( 0 , 1)) = 1
		_SubsurfaceAmbientValue("Subsurface Ambient", Range( 0 , 1)) = 0.2
		_SubsurfaceShadowValue("Subsurface Shadow", Range( 0 , 1)) = 1
		[StyledCategory(Gradient Settings)]_CategoryGradient("[ Category Gradient ]", Float) = 0
		[HDR]_GradientColorOne("Gradient Color One", Color) = (1,1,1,1)
		[HDR]_GradientColorTwo("Gradient Color Two", Color) = (1,1,1,1)
		[StyledRemapSlider(_GradientMinValue, _GradientMaxValue, 0, 1)]_GradientMaskRemap("Gradient Mask", Vector) = (0,0,0,0)
		[HideInInspector]_GradientMinValue("Gradient Mask Min", Range( 0 , 1)) = 0
		[HideInInspector]_GradientMaxValue("Gradient Mask Max ", Range( 0 , 1)) = 1
		[StyledCategory(Noise Settings)]_CategoryNoise("[ Category Noise ]", Float) = 0
		[StyledRemapSlider(_NoiseMinValue, _NoiseMaxValue, 0, 1)]_NoiseMaskRemap("Noise Mask", Vector) = (0,0,0,0)
		[StyledCategory(Emissive Settings)]_CategoryEmissive("[ Category Emissive]", Float) = 0
		[NoScaleOffset][Space(10)][StyledTextureSingleLine]_EmissiveTex("Emissive Texture", 2D) = "white" {}
		[Space(10)][StyledVector(9)]_EmissiveUVs("Emissive UVs", Vector) = (1,1,0,0)
		[Enum(None,0,Any,10,Baked,20,Realtime,30)]_EmissiveFlagMode("Emissive Baking", Float) = 0
		[HDR]_EmissiveColor("Emissive Color", Color) = (0,0,0,0)
		[StyledEmissiveIntensity]_EmissiveIntensityParams("Emissive Intensity", Vector) = (1,1,1,0)
		[StyledCategory(Perspective Settings)]_CategoryPerspective("[ Category Perspective ]", Float) = 0
		_PerspectivePushValue("Perspective Push", Range( 0 , 4)) = 0
		_PerspectiveNoiseValue("Perspective Noise", Range( 0 , 4)) = 0
		_PerspectiveAngleValue("Perspective Angle", Range( 0 , 8)) = 1
		[StyledCategory(Size Fade Settings)]_CategorySizeFade("[ Category Size Fade ]", Float) = 0
		[StyledMessage(Info, The Size Fade feature is recommended to be used to fade out vegetation at a distance in combination with the LOD Groups or with a 3rd party culling system., _SizeFadeMode, 1, 0, 10)]_MessageSizeFade("# Message Size Fade", Float) = 0
		[Enum(Off,0,On,1)]_SizeFadeMode("Size Fade Mode", Float) = 0
		_SizeFadeStartValue("Size Fade Start", Range( 0 , 1000)) = 200
		_SizeFadeEndValue("Size Fade End", Range( 0 , 1000)) = 300
		[StyledCategory(Motion Settings)]_CategoryMotion("[ Category Motion ]", Float) = 0
		[StyledMessage(Info, Procedural variation in use. Use the Scale settings if the Variation is splitting the mesh., _VertexVariationMode, 1 , 0, 10)]_MessageMotionVariation("# Message Motion Variation", Float) = 0
		[HDR]_MotionHighlightColor("Motion Highlight Color", Color) = (2,2,2,1)
		_MotionNormalValue("Motion Normal Direction", Range( 0 , 1)) = 1
		[StyledSpace(10)]_SpaceMotionGlobals("# SpaceMotionGlobals", Float) = 0
		_MotionAmplitude_10("Motion Bending", Range( 0 , 2)) = 0.2
		[IntRange]_MotionSpeed_10("Motion Speed", Range( 0 , 40)) = 2
		_MotionScale_10("Motion Scale", Range( 0 , 20)) = 0
		_MotionVariation_10("Motion Variation", Range( 0 , 20)) = 0
		[Space(10)]_MotionAmplitude_32("Motion Flutter", Range( 0 , 2)) = 0.2
		[IntRange]_MotionSpeed_32("Motion Speed", Range( 0 , 40)) = 20
		_MotionScale_32("Motion Scale", Range( 0 , 20)) = 10
		_MotionVariation_32("Motion Variation", Range( 0 , 20)) = 10
		[Space(10)]_InteractionAmplitude("Interaction Amplitude", Range( 0 , 2)) = 1
		_InteractionMaskValue("Interaction Use Mask", Range( 0 , 1)) = 1
		[StyledSpace(10)]_SpaceMotionLocals("# SpaceMotionLocals", Float) = 0
		[ASEEnd][StyledToggle]_MotionValue_30("Use Flutter Motion Settings", Float) = 1
		[HideInInspector][StyledToggle]_VertexPivotMode("Enable Pre Baked Pivots", Float) = 0
		[HideInInspector][StyledToggle]_VertexDataMode("Enable Batching Support", Float) = 0
		[HideInInspector][StyledToggle]_VertexDynamicMode("Enable Dynamic Support", Float) = 0
		[HideInInspector]_render_normals("_render_normals", Vector) = (1,1,1,0)
		[HideInInspector]_Cutoff("Legacy Cutoff", Float) = 0.5
		[HideInInspector]_Color("Legacy Color", Color) = (0,0,0,0)
		[HideInInspector]_MainTex("Legacy MainTex", 2D) = "white" {}
		[HideInInspector]_BumpMap("Legacy BumpMap", 2D) = "white" {}
		[HideInInspector]_LayerReactValue("Legacy Layer React", Float) = 0
		[HideInInspector]_VertexRollingMode("Legacy Vertex Rolling", Float) = 1
		[HideInInspector]_MaxBoundsInfo("Legacy Bounds Info", Vector) = (1,1,1,1)
		[HideInInspector]_VertexVariationMode("_VertexVariationMode", Float) = 0
		[HideInInspector]_VertexMasksMode("_VertexMasksMode", Float) = 0
		[HideInInspector]_IsTVEShader("_IsTVEShader", Float) = 1
		[HideInInspector]_IsVersion("_IsVersion", Float) = 700
		[HideInInspector]_HasEmissive("_HasEmissive", Float) = 0
		[HideInInspector]_HasGradient("_HasGradient", Float) = 0
		[HideInInspector]_HasOcclusion("_HasOcclusion", Float) = 0
		[HideInInspector]_IsGrassShader("_IsGrassShader", Float) = 1
		[HideInInspector]_IsSubsurfaceShader("_IsSubsurfaceShader", Float) = 1
		[HideInInspector]_render_src("_render_src", Float) = 1
		[HideInInspector]_render_cull("_render_cull", Float) = 0
		[HideInInspector]_render_dst("_render_dst", Float) = 0
		[HideInInspector]_render_coverage("_render_coverage", Float) = 0
		[HideInInspector]_render_zw("_render_zw", Float) = 1


		[HideInInspector]_QueueOffset("_QueueOffset", Float) = 0
        [HideInInspector]_QueueControl("_QueueControl", Float) = -1

        [HideInInspector][NoScaleOffset]unity_Lightmaps("unity_Lightmaps", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_LightmapsInd("unity_LightmapsInd", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_ShadowMasks("unity_ShadowMasks", 2DArray) = "" {}

		//_TransmissionShadow( "Transmission Shadow", Range( 0, 1 ) ) = 0.5
		_TransStrength( "Strength", Range( 0, 50 ) ) = 1
		_TransNormal( "Normal Distortion", Range( 0, 1 ) ) = 0.5
		_TransScattering( "Scattering", Range( 1, 50 ) ) = 2
		_TransDirect( "Direct", Range( 0, 1 ) ) = 0.9
		_TransAmbient( "Ambient", Range( 0, 1 ) ) = 0.1
		_TransShadow( "Shadow", Range( 0, 1 ) ) = 0.5
		//_TessPhongStrength( "Tess Phong Strength", Range( 0, 1 ) ) = 0.5
		//_TessValue( "Tess Max Tessellation", Range( 1, 32 ) ) = 16
		//_TessMin( "Tess Min Distance", Float ) = 10
		//_TessMax( "Tess Max Distance", Float ) = 25
		//_TessEdgeLength ( "Tess Edge length", Range( 2, 50 ) ) = 16
		//_TessMaxDisp( "Tess Max Displacement", Float ) = 25
	}

	//-------------------------------------------------------------------------------------
	// END_PROPERTIES
	//-------------------------------------------------------------------------------------

	SubShader
	{
		LOD 0

		

	//-------------------------------------------------------------------------------------
	// BEGIN_PASS UNIVERSALPIPELINE
	//-------------------------------------------------------------------------------------
		Tags { "RenderPipeline"="UniversalPipeline" "RenderType"="Opaque" "Queue"="Geometry" }

		Cull [_render_cull]
		ZWrite [_render_zw]
		ZTest LEqual
		Offset 0,0
		AlphaToMask [_render_coverage]

		

	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

		HLSLINCLUDE

		#pragma target 4.0

		#pragma prefer_hlslcc gles
		#pragma exclude_renderers d3d11_9x 

		#ifndef ASE_TESS_FUNCS
		#define ASE_TESS_FUNCS
		float4 FixedTess( float tessValue )
		{
			return tessValue;
		}
		
		float CalcDistanceTessFactor (float4 vertex, float minDist, float maxDist, float tess, float4x4 o2w, float3 cameraPos )
		{
			float3 wpos = mul(o2w,vertex).xyz;
			float dist = distance (wpos, cameraPos);
			float f = clamp(1.0 - (dist - minDist) / (maxDist - minDist), 0.01, 1.0) * tess;
			return f;
		}

		float4 CalcTriEdgeTessFactors (float3 triVertexFactors)
		{
			float4 tess;
			tess.x = 0.5 * (triVertexFactors.y + triVertexFactors.z);
			tess.y = 0.5 * (triVertexFactors.x + triVertexFactors.z);
			tess.z = 0.5 * (triVertexFactors.x + triVertexFactors.y);
			tess.w = (triVertexFactors.x + triVertexFactors.y + triVertexFactors.z) / 3.0f;
			return tess;
		}

		float CalcEdgeTessFactor (float3 wpos0, float3 wpos1, float edgeLen, float3 cameraPos, float4 scParams )
		{
			float dist = distance (0.5 * (wpos0+wpos1), cameraPos);
			float len = distance(wpos0, wpos1);
			float f = max(len * scParams.y / (edgeLen * dist), 1.0);
			return f;
		}

		float DistanceFromPlane (float3 pos, float4 plane)
		{
			float d = dot (float4(pos,1.0f), plane);
			return d;
		}

		bool WorldViewFrustumCull (float3 wpos0, float3 wpos1, float3 wpos2, float cullEps, float4 planes[6] )
		{
			float4 planeTest;
			planeTest.x = (( DistanceFromPlane(wpos0, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
							(( DistanceFromPlane(wpos1, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
							(( DistanceFromPlane(wpos2, planes[0]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.y = (( DistanceFromPlane(wpos0, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
							(( DistanceFromPlane(wpos1, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
							(( DistanceFromPlane(wpos2, planes[1]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.z = (( DistanceFromPlane(wpos0, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
							(( DistanceFromPlane(wpos1, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
							(( DistanceFromPlane(wpos2, planes[2]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.w = (( DistanceFromPlane(wpos0, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
							(( DistanceFromPlane(wpos1, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
							(( DistanceFromPlane(wpos2, planes[3]) > -cullEps) ? 1.0f : 0.0f );
			return !all (planeTest);
		}

		float4 DistanceBasedTess( float4 v0, float4 v1, float4 v2, float tess, float minDist, float maxDist, float4x4 o2w, float3 cameraPos )
		{
			float3 f;
			f.x = CalcDistanceTessFactor (v0,minDist,maxDist,tess,o2w,cameraPos);
			f.y = CalcDistanceTessFactor (v1,minDist,maxDist,tess,o2w,cameraPos);
			f.z = CalcDistanceTessFactor (v2,minDist,maxDist,tess,o2w,cameraPos);

			return CalcTriEdgeTessFactors (f);
		}

		float4 EdgeLengthBasedTess( float4 v0, float4 v1, float4 v2, float edgeLength, float4x4 o2w, float3 cameraPos, float4 scParams )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;
			tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
			tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
			tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
			tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			return tess;
		}

		float4 EdgeLengthBasedTessCull( float4 v0, float4 v1, float4 v2, float edgeLength, float maxDisplacement, float4x4 o2w, float3 cameraPos, float4 scParams, float4 planes[6] )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;

			if (WorldViewFrustumCull(pos0, pos1, pos2, maxDisplacement, planes))
			{
				tess = 0.0f;
			}
			else
			{
				tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
				tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
				tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
				tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			}
			return tess;
		}
		#endif //ASE_TESS_FUNCS

		ENDHLSL

	//-------------------------------------------------------------------------------------
	// END_PASS UNIVERSALPIPELINE
	//-------------------------------------------------------------------------------------

		
	//-------------------------------------------------------------------------------------
	// BEGIN_PASS UNIVERSALFORWARD
	//-------------------------------------------------------------------------------------
		Pass
		{
			
			Name "Forward"
			Tags { "LightMode"="UniversalForwardOnly" }
			
			Blend [_render_src] [_render_dst], One Zero
			ZWrite On
			ZTest LEqual
			Offset 0 , 0
			ColorMask RGBA

			

	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			HLSLPROGRAM
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

#include "./../../../../../GPUInstancer/Shaders/Include/GPUInstancerInclude.cginc"
#pragma instancing_options procedural:setupGPUI
#pragma multi_compile_instancing


			#define _SPECULAR_SETUP 1
			#define _NORMAL_DROPOFF_TS 1
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#pragma multi_compile _ DOTS_INSTANCING_ON
			#define ASE_ABSOLUTE_VERTEX_POS 1
			#define _TRANSLUCENCY_ASE 1
			#define _EMISSION
			#define _NORMALMAP 1
			#define ASE_SRP_VERSION 120100


			//-----------------------------------------------------------------------------
			// DEFINES - (Universal Pipeline keywords) api 12.1.0 thru 14.0.3
			//-----------------------------------------------------------------------------
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS _MAIN_LIGHT_SHADOWS_CASCADE _MAIN_LIGHT_SHADOWS_SCREEN
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile_fragment _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile_fragment _ _REFLECTION_PROBE_BLENDING
			#pragma multi_compile_fragment _ _REFLECTION_PROBE_BOX_PROJECTION
			#pragma multi_compile_fragment _ _SHADOWS_SOFT
			#pragma multi_compile_fragment _ _SCREEN_SPACE_OCCLUSION
			#pragma multi_compile_fragment _ _DBUFFER_MRT1 _DBUFFER_MRT2 _DBUFFER_MRT3
			#pragma multi_compile_fragment _ _LIGHT_LAYERS
			#pragma multi_compile_fragment _ _LIGHT_COOKIES
			#pragma multi_compile _ _CLUSTERED_RENDERING

			//-----------------------------------------------------------------------------
			// DEFINES - (Unity defined keywords) api 12.1.0 thru 14.0.3
			//-----------------------------------------------------------------------------
			#pragma multi_compile _ LIGHTMAP_SHADOW_MIXING
			#pragma multi_compile _ SHADOWS_SHADOWMASK
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile _ DYNAMICLIGHTMAP_ON
			#pragma multi_compile_fragment _ DEBUG_DISPLAY

			#pragma vertex vert
			#pragma fragment frag

			#define SHADERPASS SHADERPASS_FORWARD

			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/DBuffer.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

			#if defined(UNITY_INSTANCING_ENABLED) && defined(_TERRAIN_INSTANCED_PERPIXEL_NORMAL)
				#define ENABLE_TERRAIN_PERPIXEL_NORMAL
			#endif

			//-----------------------------------------------------------------------------
			// DEFINES - (Set by User)
			//-----------------------------------------------------------------------------

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_COLOR
			#pragma shader_feature_local TVE_FEATURE_CLIP
			#pragma shader_feature_local TVE_FEATURE_BATCHING
			//TVE Injection Defines
			//SHADER INJECTION POINT BEGIN
			//SHADER INJECTION POINT END
			//TVE Pipeline Defines
			#define THE_VEGETATION_ENGINE
			#define TVE_IS_UNIVERSAL_PIPELINE
			//TVE Shader Type Defines
			#define TVE_IS_GRASS_SHADER


	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_tangent : TANGENT;
				float4 texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				float4 texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				float4 lightmapUVOrVertexSH : TEXCOORD0;
				half4 fogFactorAndVertexLight : TEXCOORD1;
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
					float4 shadowCoord : TEXCOORD2;
				#endif
				float4 tSpace0 : TEXCOORD3;
				float4 tSpace1 : TEXCOORD4;
				float4 tSpace2 : TEXCOORD5;
				#if defined(ASE_NEEDS_FRAG_SCREEN_POSITION)
					float4 screenPos : TEXCOORD6;
				#endif
				#if defined(DYNAMICLIGHTMAP_ON)
					float2 dynamicLightmapUV : TEXCOORD7;
				#endif
				float4 ase_texcoord8 : TEXCOORD8;
				float4 ase_texcoord9 : TEXCOORD9;
				float4 ase_texcoord10 : TEXCOORD10;
				float4 ase_texcoord11 : TEXCOORD11;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

	//-------------------------------------------------------------------------------------
	// BEGIN_CBUFFER
	//-------------------------------------------------------------------------------------

			CBUFFER_START(UnityPerMaterial)
			float4 _SubsurfaceDiffusion_asset;
			float4 _NoiseMaskRemap;
			float4 _SubsurfaceDiffusion_Asset;
			half4 _MotionHighlightColor;
			half4 _MainColor;
			half4 _MainUVs;
			half4 _VertexOcclusionColor;
			half4 _OverlayMaskRemap;
			half4 _AlphaMaskRemap;
			half4 _DetailBlendRemap;
			float4 _Color;
			half4 _GradientColorOne;
			half4 _SubsurfaceMaskRemap;
			float4 _GradientMaskRemap;
			half4 _ColorsMaskRemap;
			half4 _GradientColorTwo;
			float4 _EmissiveIntensityParams;
			half4 _VertexOcclusionRemap;
			half4 _SubsurfaceColor;
			half4 _EmissiveUVs;
			float4 _MaxBoundsInfo;
			half4 _EmissiveColor;
			half3 _render_normals;
			half _PerspectiveAngleValue;
			half _LayerMotionValue;
			half _MotionAmplitude_10;
			float _MotionScale_10;
			float _MotionSpeed_10;
			half _MotionVariation_10;
			half _PerspectiveNoiseValue;
			half _PerspectivePushValue;
			half _VertexDynamicMode;
			half _SizeFadeEndValue;
			half _SizeFadeStartValue;
			half _GlobalSize;
			half _LayerVertexValue;
			half _VertexDataMode;
			half _MotionNormalValue;
			half _MotionValue_30;
			half _InteractionAmplitude;
			half _InteractionMaskValue;
			float _MotionSpeed_32;
			float _MotionVariation_32;
			float _MotionScale_32;
			half _SizeFadeMode;
			half _MotionAmplitude_32;
			half _IsSubsurfaceShader;
			half _GradientMaxValue;
			half _SubsurfaceValue;
			half _GlobalAlpha;
			half _AlphaVariationValue;
			half _MainOcclusionValue;
			half _GlobalWetness;
			half _MainSmoothnessValue;
			half _RenderSpecular;
			half _GlobalEmissive;
			half _MainNormalValue;
			half _OverlayMaskMaxValue;
			half _OverlayMaskMinValue;
			half _GradientMinValue;
			half _OverlayVariationValue;
			half _ExtrasPositionMode;
			half _GlobalOverlay;
			half _ColorsMaskMaxValue;
			half _ColorsMaskMinValue;
			half _ColorsVariationValue;
			half _GlobalColors;
			half _LayerColorsValue;
			half _ColorsPositionMode;
			half _VertexOcclusionMaxValue;
			half _VertexOcclusionMinValue;
			half _VertexPivotMode;
			half _LayerExtrasValue;
			half _render_coverage;
			half _RenderMode;
			half _render_dst;
			half _SubsurfaceAngleValue;
			half _HasGradient;
			half _IsTVEShader;
			half _SpaceGlobalPosition;
			half _VertexVariationMode;
			half _AlphaFeatherValue;
			half _MessageGlobalsVariation;
			half _SubsurfaceShadowValue;
			half _RenderZWrite;
			half _RenderDecals;
			half _RenderDirect;
			half _CategoryNoise;
			half _SubsurfaceScatteringValue;
			half _RenderClip;
			float _SubsurfaceDiffusion;
			half _EmissiveFlagMode;
			half _RenderSSR;
			half _CategoryGlobal;
			half _SubsurfaceNormalValue;
			half _MessageMotionVariation;
			half _SubsurfaceAmbientValue;
			half _RenderCull;
			half _HasOcclusion;
			half _Cutoff;
			half _SubsurfaceDirectValue;
			half _CategoryRender;
			half _RenderQueue;
			half _DetailBlendMode;
			half _CategoryPerspective;
			half _render_cull;
			half _RenderAmbient;
			half _CategoryDetail;
			half _render_src;
			half _render_zw;
			half _IsGrassShader;
			half _RenderPriority;
			half _SubsurfaceMaskMinValue;
			half _CategoryGradient;
			half _LayerReactValue;
			half _RenderNormals;
			half _SpaceGlobalLayers;
			half _AlphaClipValue;
			half _VertexRollingMode;
			half _IsVersion;
			half _RenderCoverage;
			half _MessageSizeFade;
			half _CategoryEmissive;
			half _CategoryOcclusion;
			half _CategoryMain;
			half _CategorySizeFade;
			half _CategoryMotion;
			half _DetailMode;
			half _DetailTypeMode;
			half _HasEmissive;
			half _SpaceGlobalLocals;
			half _VertexMasksMode;
			half _RenderShadow;
			half _SpaceMotionGlobals;
			half _CategorySubsurface;
			half _SpaceRenderFade;
			half _SpaceMotionLocals;
			half _SubsurfaceMaskMaxValue;
			#ifdef _TRANSMISSION_ASE
				float _TransmissionShadow;
			#endif
			#ifdef _TRANSLUCENCY_ASE
				float _TransStrength;
				float _TransNormal;
				float _TransScattering;
				float _TransDirect;
				float _TransAmbient;
				float _TransShadow;
			#endif
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END

			// Property used by ScenePickingPass
			#ifdef SCENEPICKINGPASS
				float4 _SelectionID;
			#endif

			// Properties used by SceneSelectionPass
			#ifdef SCENESELECTIONPASS
				int _ObjectId;
				int _PassValue;
			#endif

	//-------------------------------------------------------------------------------------
	// END_CBUFFER
	//-------------------------------------------------------------------------------------

			sampler2D _MainTex;
			half TVE_Enabled;
			sampler2D _BumpMap;
			half4 TVE_MotionParams;
			TEXTURE2D_ARRAY(TVE_MotionTex);
			half4 TVE_MotionCoords;
			SAMPLER(sampler_linear_clamp);
			float TVE_MotionUsage[10];
			sampler2D TVE_NoiseTex;
			half4 TVE_NoiseParams;
			half4 TVE_FlutterParams;
			half TVE_MotionFadeEnd;
			half TVE_MotionFadeStart;
			half4 TVE_VertexParams;
			TEXTURE2D_ARRAY(TVE_VertexTex);
			half4 TVE_VertexCoords;
			float TVE_VertexUsage[10];
			half TVE_DistanceFadeBias;
			half _DisableSRPBatcher;
			sampler2D _MainAlbedoTex;
			half4 TVE_ColorsParams;
			TEXTURE2D_ARRAY(TVE_ColorsTex);
			half4 TVE_ColorsCoords;
			float TVE_ColorsUsage[10];
			half4 TVE_OverlayColor;
			half4 TVE_ExtrasParams;
			TEXTURE2D_ARRAY(TVE_ExtrasTex);
			half4 TVE_ExtrasCoords;
			float TVE_ExtrasUsage[10];
			sampler2D _MainNormalTex;
			sampler2D _EmissiveTex;
			sampler2D _MainMaskTex;
			half TVE_OverlaySmoothness;
			half TVE_SubsurfaceValue;


	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBRForwardPass.hlsl"

			//#ifdef HAVE_VFX_MODIFICATION
			//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/VisualEffectVertex.hlsl"
			//#endif

	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			float2 DecodeFloatToVector2( float enc )
			{
				float2 result ;
				result.y = enc % 2048;
				result.x = floor(enc / 2048);
				return result / (2048 - 1);
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 VertexPosition3588_g73696 = v.vertex.xyz;
				float3 appendResult60_g74015 = (float3(v.ase_texcoord3.x , v.ase_texcoord3.z , v.ase_texcoord3.y));
				half3 Mesh_PivotsData2831_g73696 = ( appendResult60_g74015 * _VertexPivotMode );
				half3 Mesh_PivotsOS2291_g73696 = Mesh_PivotsData2831_g73696;
				float3 temp_output_2283_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				half3 VertexPos40_g73968 = temp_output_2283_0_g73696;
				float3 appendResult74_g73968 = (float3(VertexPos40_g73968.x , 0.0 , 0.0));
				half3 VertexPosRotationAxis50_g73968 = appendResult74_g73968;
				float3 break84_g73968 = VertexPos40_g73968;
				float3 appendResult81_g73968 = (float3(0.0 , break84_g73968.y , break84_g73968.z));
				half3 VertexPosOtherAxis82_g73968 = appendResult81_g73968;
				float4 temp_output_91_19_g74003 = TVE_MotionCoords;
				float4x4 break19_g73996 = GetObjectToWorldMatrix();
				float3 appendResult20_g73996 = (float3(break19_g73996[ 0 ][ 3 ] , break19_g73996[ 1 ][ 3 ] , break19_g73996[ 2 ][ 3 ]));
				float3 temp_output_122_0_g73996 = Mesh_PivotsData2831_g73696;
				float3 PivotsOnly105_g73996 = (mul( GetObjectToWorldMatrix(), float4( temp_output_122_0_g73996 , 0.0 ) ).xyz).xyz;
				half3 ObjectData20_g73997 = ( appendResult20_g73996 + PivotsOnly105_g73996 );
				float3 ase_worldPos = mul(GetObjectToWorldMatrix(), v.vertex).xyz;
				half3 WorldData19_g73997 = ase_worldPos;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73997 = WorldData19_g73997;
				#else
				float3 staticSwitch14_g73997 = ObjectData20_g73997;
				#endif
				float3 temp_output_114_0_g73996 = staticSwitch14_g73997;
				float3 vertexToFrag4224_g73696 = temp_output_114_0_g73996;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				float3 vertexToFrag3890_g73696 = ase_worldPos;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				half2 UV94_g74003 = ( (temp_output_91_19_g74003).zw + ( (temp_output_91_19_g74003).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g74003 = _LayerMotionValue;
				float4 lerpResult107_g74003 = lerp( TVE_MotionParams , saturate( SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_MotionTex, sampler_linear_clamp, UV94_g74003,temp_output_84_0_g74003, 0.0 ) ) , TVE_MotionUsage[(int)temp_output_84_0_g74003]);
				half4 Global_Motion_Params3909_g73696 = lerpResult107_g74003;
				float4 break322_g73985 = Global_Motion_Params3909_g73696;
				float3 appendResult397_g73985 = (float3(break322_g73985.x , 0.0 , break322_g73985.y));
				float3 temp_output_398_0_g73985 = (appendResult397_g73985*2.0 + -1.0);
				float3 ase_parentObjectScale = ( 1.0 / float3( length( GetWorldToObjectMatrix()[ 0 ].xyz ), length( GetWorldToObjectMatrix()[ 1 ].xyz ), length( GetWorldToObjectMatrix()[ 2 ].xyz ) ) );
				half2 Global_MotionDirectionOS39_g73696 = (( mul( GetWorldToObjectMatrix(), float4( temp_output_398_0_g73985 , 0.0 ) ).xyz * ase_parentObjectScale )).xz;
				half2 Input_DirectionOS358_g73990 = Global_MotionDirectionOS39_g73696;
				half Wind_Power369_g73985 = break322_g73985.z;
				half Global_WindPower2223_g73696 = Wind_Power369_g73985;
				half3 Input_Position419_g73963 = ObjectPosition4223_g73696;
				float Input_MotionScale287_g73963 = ( _MotionScale_10 + 1.0 );
				half Global_Scale448_g73963 = TVE_NoiseParams.x;
				float2 temp_output_597_0_g73963 = (( Input_Position419_g73963 * Input_MotionScale287_g73963 * Global_Scale448_g73963 * 0.0075 )).xz;
				half2 Global_MotionDirectionWS4683_g73696 = (temp_output_398_0_g73985).xz;
				half2 Input_DirectionWS423_g73963 = Global_MotionDirectionWS4683_g73696;
				half Input_MotionSpeed62_g73963 = _MotionSpeed_10;
				half Global_Speed449_g73963 = TVE_NoiseParams.y;
				half Input_MotionVariation284_g73963 = _MotionVariation_10;
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = v.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				half Input_GlobalVariation569_g73963 = Global_MeshVariation5104_g73696;
				float temp_output_630_0_g73963 = ( ( ( _TimeParameters.x * Input_MotionSpeed62_g73963 * Global_Speed449_g73963 ) + ( Input_MotionVariation284_g73963 * Input_GlobalVariation569_g73963 ) ) * 0.03 );
				float temp_output_607_0_g73963 = frac( temp_output_630_0_g73963 );
				float4 lerpResult590_g73963 = lerp( tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * temp_output_607_0_g73963 ) ), 0, 0.0) ) , tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * frac( ( temp_output_630_0_g73963 + 0.5 ) ) ) ), 0, 0.0) ) , ( abs( ( temp_output_607_0_g73963 - 0.5 ) ) / 0.5 ));
				half Input_GlobalWind327_g73963 = Global_WindPower2223_g73696;
				float lerpResult612_g73963 = lerp( 1.4 , 0.4 , Input_GlobalWind327_g73963);
				float3 temp_cast_7 = (lerpResult612_g73963).xxx;
				float3 break638_g73963 = (pow( ( abs( (lerpResult590_g73963).rgb ) + 0.2 ) , temp_cast_7 )*1.4 + -0.2);
				half Global_NoiseTexR34_g73696 = break638_g73963.x;
				half Motion_10_Amplitude2258_g73696 = ( _MotionAmplitude_10 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 );
				half Input_BendingAmplitude376_g73990 = Motion_10_Amplitude2258_g73696;
				half Mesh_Height1524_g73696 = v.ase_color.a;
				half Input_MeshHeight388_g73990 = Mesh_Height1524_g73696;
				half ObjectData20_g73992 = ( Input_MeshHeight388_g73990 * 2.0 );
				float enc62_g73978 = v.texcoord.w;
				float2 localDecodeFloatToVector262_g73978 = DecodeFloatToVector2( enc62_g73978 );
				float2 break63_g73978 = ( localDecodeFloatToVector262_g73978 * 100.0 );
				float Bounds_Height5230_g73696 = break63_g73978.x;
				half Input_BoundsHeight390_g73990 = Bounds_Height5230_g73696;
				half WorldData19_g73992 = ( ( Input_MeshHeight388_g73990 * Input_MeshHeight388_g73990 ) * Input_BoundsHeight390_g73990 * 2.0 );
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73992 = WorldData19_g73992;
				#else
				float staticSwitch14_g73992 = ObjectData20_g73992;
				#endif
				half Mask_Motion_10321_g73990 = staticSwitch14_g73992;
				half Input_InteractionAmplitude58_g73990 = _InteractionAmplitude;
				half Input_InteractionUseMask62_g73990 = _InteractionMaskValue;
				float lerpResult371_g73990 = lerp( 2.0 , Mask_Motion_10321_g73990 , Input_InteractionUseMask62_g73990);
				half ObjectData20_g73991 = lerpResult371_g73990;
				half WorldData19_g73991 = Mask_Motion_10321_g73990;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73991 = WorldData19_g73991;
				#else
				float staticSwitch14_g73991 = ObjectData20_g73991;
				#endif
				half Mask_Interaction373_g73990 = ( Input_InteractionAmplitude58_g73990 * staticSwitch14_g73991 );
				half Global_InteractionMask66_g73696 = ( break322_g73985.w * break322_g73985.w );
				float Input_InteractionGlobalMask330_g73990 = Global_InteractionMask66_g73696;
				float lerpResult360_g73990 = lerp( ( Input_BendingAmplitude376_g73990 * Mask_Motion_10321_g73990 ) , Mask_Interaction373_g73990 , saturate( ( Input_InteractionAmplitude58_g73990 * Input_InteractionGlobalMask330_g73990 ) ));
				float2 break364_g73990 = ( Input_DirectionOS358_g73990 * lerpResult360_g73990 );
				half Motion_10_BendingZ190_g73696 = break364_g73990.y;
				half Angle44_g73968 = Motion_10_BendingZ190_g73696;
				half3 VertexPos40_g73948 = ( VertexPosRotationAxis50_g73968 + ( VertexPosOtherAxis82_g73968 * cos( Angle44_g73968 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g73968 ) * sin( Angle44_g73968 ) ) );
				float3 appendResult74_g73948 = (float3(0.0 , 0.0 , VertexPos40_g73948.z));
				half3 VertexPosRotationAxis50_g73948 = appendResult74_g73948;
				float3 break84_g73948 = VertexPos40_g73948;
				float3 appendResult81_g73948 = (float3(break84_g73948.x , break84_g73948.y , 0.0));
				half3 VertexPosOtherAxis82_g73948 = appendResult81_g73948;
				half Motion_10_BendingX216_g73696 = break364_g73990.x;
				half Angle44_g73948 = -Motion_10_BendingX216_g73696;
				half Input_MotionScale321_g73940 = _MotionScale_32;
				half Input_MotionVariation330_g73940 = _MotionVariation_32;
				half Input_GlobalVariation372_g73940 = Global_MeshVariation5104_g73696;
				half Input_MotionSpeed62_g73940 = _MotionSpeed_32;
				half Global_Speed350_g73940 = TVE_FlutterParams.y;
				half Motion_FacingMask5214_g73696 = 1.0;
				float temp_output_7_0_g73984 = TVE_MotionFadeEnd;
				half Motion_FadeOut4005_g73696 = saturate( ( ( distance( ase_worldPos , _WorldSpaceCameraPos ) - temp_output_7_0_g73984 ) / ( TVE_MotionFadeStart - temp_output_7_0_g73984 ) ) );
				half Motion_30_Amplitude4960_g73696 = ( _MotionAmplitude_32 * _MotionValue_30 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 * Motion_FacingMask5214_g73696 * Motion_FadeOut4005_g73696 );
				half Input_MotionAmplitude58_g73940 = Motion_30_Amplitude4960_g73696;
				half Global_Power354_g73940 = TVE_FlutterParams.x;
				float enc59_g73978 = v.texcoord.z;
				float2 localDecodeFloatToVector259_g73978 = DecodeFloatToVector2( enc59_g73978 );
				float2 break61_g73978 = localDecodeFloatToVector259_g73978;
				half Mesh_Motion_30144_g73696 = break61_g73978.y;
				half Input_MeshMotion_30374_g73940 = Mesh_Motion_30144_g73696;
				half Input_MotionNormal364_g73940 = _MotionNormalValue;
				float3 lerpResult370_g73940 = lerp( float3( 1,1,1 ) , v.ase_normal , Input_MotionNormal364_g73940);
				half3 Motion_30_Details263_g73696 = ( ( sin( ( ( ( ase_worldPos.x + ase_worldPos.y + ase_worldPos.z ) * Input_MotionScale321_g73940 ) + ( Input_MotionVariation330_g73940 * Input_GlobalVariation372_g73940 ) + ( _TimeParameters.x * Input_MotionSpeed62_g73940 * Global_Speed350_g73940 ) ) ) * Input_MotionAmplitude58_g73940 * Global_Power354_g73940 * Input_MeshMotion_30374_g73940 * 0.4 ) * lerpResult370_g73940 );
				float3 Vertex_Motion_Object833_g73696 = ( ( VertexPosRotationAxis50_g73948 + ( VertexPosOtherAxis82_g73948 * cos( Angle44_g73948 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g73948 ) * sin( Angle44_g73948 ) ) ) + Motion_30_Details263_g73696 );
				float3 temp_output_3474_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				float3 appendResult2043_g73696 = (float3(Motion_10_BendingX216_g73696 , 0.0 , Motion_10_BendingZ190_g73696));
				float3 Vertex_Motion_World1118_g73696 = ( ( temp_output_3474_0_g73696 + appendResult2043_g73696 ) + Motion_30_Details263_g73696 );
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch4976_g73696 = Vertex_Motion_World1118_g73696;
				#else
				float3 staticSwitch4976_g73696 = ( Vertex_Motion_Object833_g73696 + ( _VertexDataMode * 0.0 ) );
				#endif
				float4 temp_output_94_19_g73959 = TVE_VertexCoords;
				half2 UV97_g73959 = ( (temp_output_94_19_g73959).zw + ( (temp_output_94_19_g73959).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g73959 = _LayerVertexValue;
				float4 lerpResult109_g73959 = lerp( TVE_VertexParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_VertexTex, sampler_linear_clamp, UV97_g73959,temp_output_84_0_g73959, 0.0 ) , TVE_VertexUsage[(int)temp_output_84_0_g73959]);
				half4 Global_Object_Params4173_g73696 = lerpResult109_g73959;
				half Global_VertexSize174_g73696 = saturate( Global_Object_Params4173_g73696.w );
				float lerpResult346_g73696 = lerp( 1.0 , Global_VertexSize174_g73696 , _GlobalSize);
				float3 appendResult3480_g73696 = (float3(lerpResult346_g73696 , lerpResult346_g73696 , lerpResult346_g73696));
				half3 ObjectData20_g73993 = appendResult3480_g73696;
				half3 _Vector11 = half3(1,1,1);
				half3 WorldData19_g73993 = _Vector11;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73993 = WorldData19_g73993;
				#else
				float3 staticSwitch14_g73993 = ObjectData20_g73993;
				#endif
				half3 Vertex_Size1741_g73696 = staticSwitch14_g73993;
				half3 _Vector5 = half3(1,1,1);
				float temp_output_7_0_g73931 = _SizeFadeEndValue;
				float temp_output_335_0_g73696 = saturate( ( ( ( distance( _WorldSpaceCameraPos , ObjectPosition4223_g73696 ) * ( 1.0 / TVE_DistanceFadeBias ) ) - temp_output_7_0_g73931 ) / ( _SizeFadeStartValue - temp_output_7_0_g73931 ) ) );
				float3 appendResult3482_g73696 = (float3(temp_output_335_0_g73696 , temp_output_335_0_g73696 , temp_output_335_0_g73696));
				float3 lerpResult5065_g73696 = lerp( _Vector5 , appendResult3482_g73696 , _SizeFadeMode);
				half3 ObjectData20_g73958 = lerpResult5065_g73696;
				half3 WorldData19_g73958 = _Vector5;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73958 = WorldData19_g73958;
				#else
				float3 staticSwitch14_g73958 = ObjectData20_g73958;
				#endif
				float3 Vertex_SizeFade1740_g73696 = staticSwitch14_g73958;
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - ase_worldPos );
				ase_worldViewDir = normalize(ase_worldViewDir);
				float3 normalizeResult2696_g73696 = normalize( ase_worldViewDir );
				float3 break2709_g73696 = cross( normalizeResult2696_g73696 , half3(0,1,0) );
				float3 appendResult2710_g73696 = (float3(-break2709_g73696.z , 0.0 , break2709_g73696.x));
				float3 appendResult2667_g73696 = (float3(Global_MeshVariation5104_g73696 , 0.5 , Global_MeshVariation5104_g73696));
				float3 normalizeResult2169_g73696 = normalize( ase_worldViewDir );
				float3 ViewDir_Normalized3963_g73696 = normalizeResult2169_g73696;
				float dotResult2212_g73696 = dot( ViewDir_Normalized3963_g73696 , float3(0,1,0) );
				half Mask_HView2656_g73696 = dotResult2212_g73696;
				float saferPower2652_g73696 = abs( Mask_HView2656_g73696 );
				half3 Grass_Perspective2661_g73696 = ( ( ( mul( GetWorldToObjectMatrix(), float4( appendResult2710_g73696 , 0.0 ) ).xyz * _PerspectivePushValue ) + ( (appendResult2667_g73696*2.0 + -1.0) * _PerspectiveNoiseValue ) ) * Mesh_Height1524_g73696 * pow( saferPower2652_g73696 , _PerspectiveAngleValue ) );
				float3 lerpResult16_g74000 = lerp( VertexPosition3588_g73696 , ( ( staticSwitch4976_g73696 * Vertex_Size1741_g73696 * Vertex_SizeFade1740_g73696 ) + Mesh_PivotsOS2291_g73696 + Grass_Perspective2661_g73696 ) , TVE_Enabled);
				float3 Final_VertexPosition890_g73696 = ( lerpResult16_g74000 + _DisableSRPBatcher );
				
				float temp_output_7_0_g73976 = _GradientMinValue;
				half Gradient_Tint2784_g73696 = saturate( ( ( Mesh_Height1524_g73696 - temp_output_7_0_g73976 ) / ( _GradientMaxValue - temp_output_7_0_g73976 ) ) );
				float vertexToFrag11_g73981 = Gradient_Tint2784_g73696;
				o.ase_texcoord8.x = vertexToFrag11_g73981;
				half Global_NoiseTexA4860_g73696 = (lerpResult590_g73963).a;
				half Highlight_Tint3231_g73696 = ( Global_NoiseTexA4860_g73696 * Global_WindPower2223_g73696 * Motion_FadeOut4005_g73696 * Mesh_Height1524_g73696 );
				float vertexToFrag11_g73943 = Highlight_Tint3231_g73696;
				o.ase_texcoord8.y = vertexToFrag11_g73943;
				float Mesh_Occlusion318_g73696 = v.ase_color.g;
				float temp_output_7_0_g73971 = _VertexOcclusionMinValue;
				float temp_output_3377_0_g73696 = saturate( ( ( Mesh_Occlusion318_g73696 - temp_output_7_0_g73971 ) / ( _VertexOcclusionMaxValue - temp_output_7_0_g73971 ) ) );
				float vertexToFrag11_g73969 = temp_output_3377_0_g73696;
				o.ase_texcoord8.z = vertexToFrag11_g73969;
				o.ase_texcoord10.xyz = vertexToFrag3890_g73696;
				o.ase_texcoord11.xyz = vertexToFrag4224_g73696;
				
				o.ase_texcoord9 = v.texcoord;
				o.ase_color = v.ase_color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord8.w = 0;
				o.ase_texcoord10.w = 0;
				o.ase_texcoord11.w = 0;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif

				float3 vertexValue = Final_VertexPosition890_g73696;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif
				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				float3 positionVS = TransformWorldToView( positionWS );
				float4 positionCS = TransformWorldToHClip( positionWS );

				VertexNormalInputs normalInput = GetVertexNormalInputs( v.ase_normal, v.ase_tangent );

				o.tSpace0 = float4( normalInput.normalWS, positionWS.x);
				o.tSpace1 = float4( normalInput.tangentWS, positionWS.y);
				o.tSpace2 = float4( normalInput.bitangentWS, positionWS.z);

				#if defined(LIGHTMAP_ON)
					OUTPUT_LIGHTMAP_UV( v.texcoord1, unity_LightmapST, o.lightmapUVOrVertexSH.xy );
				#endif

				#if !defined(LIGHTMAP_ON)
					OUTPUT_SH( normalInput.normalWS.xyz, o.lightmapUVOrVertexSH.xyz );
				#endif

				#if defined(DYNAMICLIGHTMAP_ON)
					o.dynamicLightmapUV.xy = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
				#endif

				#if defined(ENABLE_TERRAIN_PERPIXEL_NORMAL)
					o.lightmapUVOrVertexSH.zw = v.texcoord;
					o.lightmapUVOrVertexSH.xy = v.texcoord * unity_LightmapST.xy + unity_LightmapST.zw;
				#endif

				half3 vertexLight = VertexLighting( positionWS, normalInput.normalWS );

				#ifdef ASE_FOG
					half fogFactor = ComputeFogFactor( positionCS.z );
				#else
					half fogFactor = 0;
				#endif

				o.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
				
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = positionCS;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				
				o.clipPos = positionCS;

				#if defined(ASE_NEEDS_FRAG_SCREEN_POSITION)
					o.screenPos = ComputeScreenPos(positionCS);
				#endif

				return o;
			}
			
			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_tangent : TANGENT;
				float4 texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				float4 texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_tangent = v.ase_tangent;
				o.texcoord = v.texcoord;
				o.texcoord1 = v.texcoord1;
				o.texcoord2 = v.texcoord2;
				o.texcoord = v.texcoord;
				o.ase_texcoord3 = v.ase_texcoord3;
				o.ase_color = v.ase_color;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
				return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_tangent = patch[0].ase_tangent * bary.x + patch[1].ase_tangent * bary.y + patch[2].ase_tangent * bary.z;
				o.texcoord = patch[0].texcoord * bary.x + patch[1].texcoord * bary.y + patch[2].texcoord * bary.z;
				o.texcoord1 = patch[0].texcoord1 * bary.x + patch[1].texcoord1 * bary.y + patch[2].texcoord1 * bary.z;
				o.texcoord2 = patch[0].texcoord2 * bary.x + patch[1].texcoord2 * bary.y + patch[2].texcoord2 * bary.z;
				o.texcoord = patch[0].texcoord * bary.x + patch[1].texcoord * bary.y + patch[2].texcoord * bary.z;
				o.ase_texcoord3 = patch[0].ase_texcoord3 * bary.x + patch[1].ase_texcoord3 * bary.y + patch[2].ase_texcoord3 * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			#if defined(ASE_EARLY_Z_DEPTH_OPTIMIZE)
				#define ASE_SV_DEPTH SV_DepthLessEqual  
			#else
				#define ASE_SV_DEPTH SV_Depth
			#endif

			half4 frag ( VertexOutput IN 
						#ifdef ASE_DEPTH_WRITE_ON
						,out float outputDepth : ASE_SV_DEPTH
						#endif
						, FRONT_FACE_TYPE ase_vface : FRONT_FACE_SEMANTIC ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN);

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif

				#if defined(ENABLE_TERRAIN_PERPIXEL_NORMAL)
					float2 sampleCoords = (IN.lightmapUVOrVertexSH.zw / _TerrainHeightmapRecipSize.zw + 0.5f) * _TerrainHeightmapRecipSize.xy;
					float3 WorldNormal = TransformObjectToWorldNormal(normalize(SAMPLE_TEXTURE2D(_TerrainNormalmapTexture, sampler_TerrainNormalmapTexture, sampleCoords).rgb * 2 - 1));
					float3 WorldTangent = -cross(GetObjectToWorldMatrix()._13_23_33, WorldNormal);
					float3 WorldBiTangent = cross(WorldNormal, -WorldTangent);
				#else
					float3 WorldNormal = normalize( IN.tSpace0.xyz );
					float3 WorldTangent = IN.tSpace1.xyz;
					float3 WorldBiTangent = IN.tSpace2.xyz;
				#endif

				float3 WorldPosition = float3(IN.tSpace0.w,IN.tSpace1.w,IN.tSpace2.w);
				float3 WorldViewDirection = _WorldSpaceCameraPos.xyz  - WorldPosition;
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SCREEN_POSITION)
					float4 ScreenPos = IN.screenPos;
				#endif

				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
					ShadowCoords = IN.shadowCoord;
				#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
					ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
				#endif
	
				WorldViewDirection = SafeNormalize( WorldViewDirection );

				float vertexToFrag11_g73981 = IN.ase_texcoord8.x;
				float3 lerpResult2779_g73696 = lerp( (_GradientColorTwo).rgb , (_GradientColorOne).rgb , vertexToFrag11_g73981);
				float vertexToFrag11_g73943 = IN.ase_texcoord8.y;
				half2 Main_UVs15_g73696 = ( ( IN.ase_texcoord9.xy * (_MainUVs).xy ) + (_MainUVs).zw );
				float4 tex2DNode29_g73696 = tex2D( _MainAlbedoTex, Main_UVs15_g73696 );
				half3 Main_Albedo99_g73696 = ( (_MainColor).rgb * (tex2DNode29_g73696).rgb );
				half3 Blend_Albedo265_g73696 = Main_Albedo99_g73696;
				float3 temp_cast_0 = (1.0).xxx;
				float vertexToFrag11_g73969 = IN.ase_texcoord8.z;
				float3 lerpResult2945_g73696 = lerp( (_VertexOcclusionColor).rgb , temp_cast_0 , vertexToFrag11_g73969);
				float3 Vertex_Occlusion648_g73696 = lerpResult2945_g73696;
				half3 Blend_AlbedoTinted2808_g73696 = ( ( lerpResult2779_g73696 * float3(1,1,1) * ( ( (_MotionHighlightColor).rgb * vertexToFrag11_g73943 ) + float3( 1,1,1 ) ) ) * Blend_Albedo265_g73696 * Vertex_Occlusion648_g73696 );
				float dotResult3616_g73696 = dot( Blend_AlbedoTinted2808_g73696 , float3(0.2126,0.7152,0.0722) );
				float3 temp_cast_1 = (dotResult3616_g73696).xxx;
				float4 temp_output_91_19_g73949 = TVE_ColorsCoords;
				float3 vertexToFrag3890_g73696 = IN.ase_texcoord10.xyz;
				float3 WorldPosition3905_g73696 = vertexToFrag3890_g73696;
				float3 vertexToFrag4224_g73696 = IN.ase_texcoord11.xyz;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				float3 lerpResult4822_g73696 = lerp( WorldPosition3905_g73696 , ObjectPosition4223_g73696 , _ColorsPositionMode);
				half2 UV94_g73949 = ( (temp_output_91_19_g73949).zw + ( (temp_output_91_19_g73949).xy * (lerpResult4822_g73696).xz ) );
				float temp_output_82_0_g73949 = _LayerColorsValue;
				float4 lerpResult108_g73949 = lerp( TVE_ColorsParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_ColorsTex, sampler_linear_clamp, UV94_g73949,temp_output_82_0_g73949, 0.0 ) , TVE_ColorsUsage[(int)temp_output_82_0_g73949]);
				half Global_ColorsTex_A1701_g73696 = saturate( (lerpResult108_g73949).a );
				half Global_Colors_Influence3668_g73696 = Global_ColorsTex_A1701_g73696;
				float3 lerpResult3618_g73696 = lerp( Blend_AlbedoTinted2808_g73696 , temp_cast_1 , Global_Colors_Influence3668_g73696);
				half3 Global_ColorsTex_RGB1700_g73696 = (lerpResult108_g73949).rgb;
				#ifdef UNITY_COLORSPACE_GAMMA
				float staticSwitch1_g73970 = 2.0;
				#else
				float staticSwitch1_g73970 = 4.594794;
				#endif
				half3 Global_Colors1954_g73696 = ( Global_ColorsTex_RGB1700_g73696 * staticSwitch1_g73970 );
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = IN.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				float lerpResult3870_g73696 = lerp( 1.0 , ( Global_MeshVariation5104_g73696 * Global_MeshVariation5104_g73696 ) , _ColorsVariationValue);
				half Global_Colors_Variation3650_g73696 = ( _GlobalColors * lerpResult3870_g73696 );
				half Mesh_Height1524_g73696 = IN.ase_color.a;
				float clampResult5405_g73696 = clamp( Mesh_Height1524_g73696 , 0.01 , 0.99 );
				float temp_output_7_0_g73945 = _ColorsMaskMinValue;
				half Global_Colors_Mask3692_g73696 = saturate( ( ( clampResult5405_g73696 - temp_output_7_0_g73945 ) / ( _ColorsMaskMaxValue - temp_output_7_0_g73945 ) ) );
				float lerpResult16_g73954 = lerp( 0.0 , ( Global_Colors_Variation3650_g73696 * Global_Colors_Mask3692_g73696 ) , TVE_Enabled);
				float3 lerpResult3628_g73696 = lerp( Blend_AlbedoTinted2808_g73696 , ( lerpResult3618_g73696 * Global_Colors1954_g73696 ) , lerpResult16_g73954);
				half3 Blend_AlbedoColored863_g73696 = lerpResult3628_g73696;
				half3 Blend_AlbedoAndSubsurface149_g73696 = Blend_AlbedoColored863_g73696;
				half3 Global_OverlayColor1758_g73696 = (TVE_OverlayColor).rgb;
				half Main_Albedo_G3526_g73696 = tex2DNode29_g73696.g;
				float4 temp_output_93_19_g73986 = TVE_ExtrasCoords;
				float3 lerpResult4827_g73696 = lerp( WorldPosition3905_g73696 , ObjectPosition4223_g73696 , _ExtrasPositionMode);
				half2 UV96_g73986 = ( (temp_output_93_19_g73986).zw + ( (temp_output_93_19_g73986).xy * (lerpResult4827_g73696).xz ) );
				float temp_output_84_0_g73986 = _LayerExtrasValue;
				float4 lerpResult109_g73986 = lerp( TVE_ExtrasParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_ExtrasTex, sampler_linear_clamp, UV96_g73986,temp_output_84_0_g73986, 0.0 ) , TVE_ExtrasUsage[(int)temp_output_84_0_g73986]);
				float4 break89_g73986 = lerpResult109_g73986;
				half Global_Extras_Overlay156_g73696 = break89_g73986.b;
				float lerpResult1065_g73696 = lerp( 1.0 , Global_MeshVariation5104_g73696 , _OverlayVariationValue);
				half Overlay_Variation4560_g73696 = lerpResult1065_g73696;
				half Overlay_Commons1365_g73696 = ( _GlobalOverlay * Global_Extras_Overlay156_g73696 * Overlay_Variation4560_g73696 );
				half Overlay_Mask_2D5121_g73696 = ( ( ( Mesh_Height1524_g73696 * 0.5 ) + Main_Albedo_G3526_g73696 ) * Overlay_Commons1365_g73696 );
				float temp_output_7_0_g73980 = _OverlayMaskMinValue;
				half Overlay_Mask269_g73696 = saturate( ( ( Overlay_Mask_2D5121_g73696 - temp_output_7_0_g73980 ) / ( _OverlayMaskMaxValue - temp_output_7_0_g73980 ) ) );
				float3 lerpResult336_g73696 = lerp( Blend_AlbedoAndSubsurface149_g73696 , Global_OverlayColor1758_g73696 , Overlay_Mask269_g73696);
				half3 Final_Albedo359_g73696 = lerpResult336_g73696;
				
				float3 unpack4112_g73696 = UnpackNormalScale( tex2D( _MainNormalTex, Main_UVs15_g73696 ), _MainNormalValue );
				unpack4112_g73696.z = lerp( 1, unpack4112_g73696.z, saturate(_MainNormalValue) );
				half3 Main_Normal137_g73696 = unpack4112_g73696;
				float3 temp_output_13_0_g73938 = Main_Normal137_g73696;
				float3 switchResult12_g73938 = (((ase_vface>0)?(temp_output_13_0_g73938):(( temp_output_13_0_g73938 * _render_normals ))));
				half3 Blend_Normal312_g73696 = switchResult12_g73938;
				half3 Final_Normal366_g73696 = Blend_Normal312_g73696;
				
				float4 temp_output_4214_0_g73696 = ( _EmissiveColor * _EmissiveIntensityParams.x );
				half2 Emissive_UVs2468_g73696 = ( ( IN.ase_texcoord9.xy * (_EmissiveUVs).xy ) + (_EmissiveUVs).zw );
				half Global_Extras_Emissive4203_g73696 = break89_g73986.r;
				float lerpResult4206_g73696 = lerp( 1.0 , Global_Extras_Emissive4203_g73696 , _GlobalEmissive);
				half3 Final_Emissive2476_g73696 = ( (( temp_output_4214_0_g73696 * tex2D( _EmissiveTex, Emissive_UVs2468_g73696 ) )).rgb * lerpResult4206_g73696 );
				
				half Render_Specular4861_g73696 = _RenderSpecular;
				float3 temp_cast_7 = (( 0.04 * Render_Specular4861_g73696 )).xxx;
				
				float4 tex2DNode35_g73696 = tex2D( _MainMaskTex, Main_UVs15_g73696 );
				half Main_Smoothness227_g73696 = ( tex2DNode35_g73696.a * _MainSmoothnessValue );
				half Blend_Smoothness314_g73696 = Main_Smoothness227_g73696;
				half Global_OverlaySmoothness311_g73696 = TVE_OverlaySmoothness;
				float lerpResult343_g73696 = lerp( Blend_Smoothness314_g73696 , Global_OverlaySmoothness311_g73696 , Overlay_Mask269_g73696);
				half Final_Smoothness371_g73696 = lerpResult343_g73696;
				half Global_Extras_Wetness305_g73696 = break89_g73986.g;
				float lerpResult3673_g73696 = lerp( 0.0 , Global_Extras_Wetness305_g73696 , _GlobalWetness);
				half Final_SmoothnessAndWetness4130_g73696 = saturate( ( Final_Smoothness371_g73696 + lerpResult3673_g73696 ) );
				
				float lerpResult240_g73696 = lerp( 1.0 , tex2DNode35_g73696.g , _MainOcclusionValue);
				half Main_Occlusion247_g73696 = lerpResult240_g73696;
				half Blend_Occlusion323_g73696 = Main_Occlusion247_g73696;
				
				float localCustomAlphaClip19_g74020 = ( 0.0 );
				float Main_Alpha316_g73696 = ( _MainColor.a * tex2DNode29_g73696.a );
				half Global_Extras_Alpha1033_g73696 = saturate( break89_g73986.a );
				float lerpResult5154_g73696 = lerp( 0.0 , Global_MeshVariation5104_g73696 , _AlphaVariationValue);
				half Global_Alpha_Variation5158_g73696 = lerpResult5154_g73696;
				half AlphaTreshold2132_g73696 = _AlphaClipValue;
				half Global_Alpha_Mask4546_g73696 = 1.0;
				float lerpResult5203_g73696 = lerp( 1.0 , ( ( Global_Extras_Alpha1033_g73696 - Global_Alpha_Variation5158_g73696 ) + AlphaTreshold2132_g73696 + ( Global_Extras_Alpha1033_g73696 * 0.5 ) ) , ( Global_Alpha_Mask4546_g73696 * _GlobalAlpha ));
				float lerpResult16_g74016 = lerp( 1.0 , lerpResult5203_g73696 , TVE_Enabled);
				half Global_Alpha315_g73696 = lerpResult16_g74016;
				half Fade_Alpha3727_g73696 = 1.0;
				float temp_output_661_0_g73696 = ( Main_Alpha316_g73696 * Global_Alpha315_g73696 * Fade_Alpha3727_g73696 );
				half Alpha34_g74021 = temp_output_661_0_g73696;
				half Offest27_g74021 = AlphaTreshold2132_g73696;
				half AlphaFeather5305_g73696 = _AlphaFeatherValue;
				half Feather30_g74021 = AlphaFeather5305_g73696;
				float temp_output_25_0_g74021 = ( ( ( Alpha34_g74021 - Offest27_g74021 ) / ( max( fwidth( Alpha34_g74021 ) , 0.001 ) + Feather30_g74021 ) ) + Offest27_g74021 );
				float temp_output_3_0_g74020 = temp_output_25_0_g74021;
				float Alpha19_g74020 = temp_output_3_0_g74020;
				float temp_output_15_0_g74020 = AlphaTreshold2132_g73696;
				float Treshold19_g74020 = temp_output_15_0_g74020;
				{
				#if defined (TVE_FEATURE_CLIP)
				#if defined (TVE_IS_HD_PIPELINE)
				#if !defined (SHADERPASS_FORWARD_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#if !defined (SHADERPASS_GBUFFER_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#else
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#endif
				}
				half Final_Alpha914_g73696 = saturate( Alpha19_g74020 );
				
				float3 temp_output_799_0_g73696 = (_SubsurfaceColor).rgb;
				float dotResult3930_g73696 = dot( temp_output_799_0_g73696 , float3(0.2126,0.7152,0.0722) );
				float3 temp_cast_8 = (dotResult3930_g73696).xxx;
				float3 lerpResult3932_g73696 = lerp( temp_output_799_0_g73696 , temp_cast_8 , Global_Colors_Influence3668_g73696);
				float3 lerpResult3942_g73696 = lerp( temp_output_799_0_g73696 , ( lerpResult3932_g73696 * Global_Colors1954_g73696 ) , ( Global_Colors_Variation3650_g73696 * Global_Colors_Mask3692_g73696 ));
				half3 Subsurface_Color1722_g73696 = lerpResult3942_g73696;
				half Global_Subsurface4041_g73696 = TVE_SubsurfaceValue;
				half Subsurface_Intensity1752_g73696 = ( _SubsurfaceValue * Global_Subsurface4041_g73696 );
				float temp_output_7_0_g74014 = _SubsurfaceMaskMinValue;
				half Subsurface_Mask1557_g73696 = saturate( ( ( Mesh_Height1524_g73696 - temp_output_7_0_g74014 ) / ( _SubsurfaceMaskMaxValue - temp_output_7_0_g74014 ) ) );
				half3 Subsurface_Translucency884_g73696 = ( Subsurface_Color1722_g73696 * Subsurface_Intensity1752_g73696 * Subsurface_Mask1557_g73696 );
				

				float3 BaseColor = Final_Albedo359_g73696;
				float3 Normal = Final_Normal366_g73696;
				float3 Emission = Final_Emissive2476_g73696;
				float3 Specular = temp_cast_7;
				float Metallic = 0;
				float Smoothness = Final_SmoothnessAndWetness4130_g73696;
				float Occlusion = Blend_Occlusion323_g73696;
				float Alpha = Final_Alpha914_g73696;
				float AlphaClipThreshold = 0.5;
				float AlphaClipThresholdShadow = 0.5;
				float3 BakedGI = 0;
				float3 RefractionColor = 1;
				float RefractionIndex = 1;
				float3 Transmission = 1;
				float3 Translucency = Subsurface_Translucency884_g73696;

				#ifdef ASE_DEPTH_WRITE_ON
					float DepthValue = 0;
				#endif
				
				#ifdef _CLEARCOAT
					float CoatMask = 0;
					float CoatSmoothness = 0;
				#endif

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				InputData inputData = (InputData)0;
				inputData.positionWS = WorldPosition;
				inputData.viewDirectionWS = WorldViewDirection;

				#ifdef _NORMALMAP
						#if _NORMAL_DROPOFF_TS
							inputData.normalWS = TransformTangentToWorld(Normal, half3x3(WorldTangent, WorldBiTangent, WorldNormal));
						#elif _NORMAL_DROPOFF_OS
							inputData.normalWS = TransformObjectToWorldNormal(Normal);
						#elif _NORMAL_DROPOFF_WS
							inputData.normalWS = Normal;
						#endif
					inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
				#else
					inputData.normalWS = WorldNormal;
				#endif

				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
					inputData.shadowCoord = ShadowCoords;
				#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
					inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);
				#else
					inputData.shadowCoord = float4(0, 0, 0, 0);
				#endif

				#ifdef ASE_FOG
					inputData.fogCoord = IN.fogFactorAndVertexLight.x;
				#endif
					inputData.vertexLighting = IN.fogFactorAndVertexLight.yzw;

				#if defined(ENABLE_TERRAIN_PERPIXEL_NORMAL)
					float3 SH = SampleSH(inputData.normalWS.xyz);
				#else
					float3 SH = IN.lightmapUVOrVertexSH.xyz;
				#endif

				#if defined(DYNAMICLIGHTMAP_ON)
					inputData.bakedGI = SAMPLE_GI(IN.lightmapUVOrVertexSH.xy, IN.dynamicLightmapUV.xy, SH, inputData.normalWS);
				#else
					inputData.bakedGI = SAMPLE_GI(IN.lightmapUVOrVertexSH.xy, SH, inputData.normalWS);
				#endif

				#ifdef _ASE_BAKEDGI
					inputData.bakedGI = BakedGI;
				#endif

				inputData.normalizedScreenSpaceUV = GetNormalizedScreenSpaceUV(IN.clipPos);
				inputData.shadowMask = SAMPLE_SHADOWMASK(IN.lightmapUVOrVertexSH.xy);

				#if defined(DEBUG_DISPLAY)
					#if defined(DYNAMICLIGHTMAP_ON)
						inputData.dynamicLightmapUV = input.dynamicLightmapUV.xy;
					#endif
					#if defined(LIGHTMAP_ON)
						inputData.staticLightmapUV = input.staticLightmapUV;
					#else
						inputData.vertexSH = input.sh;
					#endif
				#endif

				SurfaceData surfaceData;
				surfaceData.albedo              = BaseColor;
				surfaceData.metallic            = saturate(Metallic);
				surfaceData.specular            = Specular;
				surfaceData.smoothness          = saturate(Smoothness),
				surfaceData.occlusion           = Occlusion,
				surfaceData.emission            = Emission,
				surfaceData.alpha               = saturate(Alpha);
				surfaceData.normalTS            = Normal;
				surfaceData.clearCoatMask       = 0;
				surfaceData.clearCoatSmoothness = 1;

				#ifdef _CLEARCOAT
					surfaceData.clearCoatMask       = saturate(CoatMask);
					surfaceData.clearCoatSmoothness = saturate(CoatSmoothness);
				#endif

				#ifdef _DBUFFER
					ApplyDecalToSurfaceData(IN.clipPos, surfaceData, inputData);
				#endif

				half4 color = UniversalFragmentPBR( inputData, surfaceData);

				#ifdef _TRANSMISSION_ASE
				{
					float shadow = _TransmissionShadow;

					Light mainLight = GetMainLight( inputData.shadowCoord );
					float3 mainAtten = mainLight.color * mainLight.distanceAttenuation;
					mainAtten = lerp( mainAtten, mainAtten * mainLight.shadowAttenuation, shadow );
					half3 mainTransmission = max(0 , -dot(inputData.normalWS, mainLight.direction)) * mainAtten * Transmission;
					color.rgb += BaseColor * mainTransmission;

					#ifdef _ADDITIONAL_LIGHTS
						int transPixelLightCount = GetAdditionalLightsCount();
						for (int i = 0; i < transPixelLightCount; ++i)
						{
							Light light = GetAdditionalLight(i, inputData.positionWS);
							float3 atten = light.color * light.distanceAttenuation;
							atten = lerp( atten, atten * light.shadowAttenuation, shadow );

							half3 transmission = max(0 , -dot(inputData.normalWS, light.direction)) * atten * Transmission;
							color.rgb += BaseColor * transmission;
						}
					#endif
				}
				#endif

				#ifdef _TRANSLUCENCY_ASE
				{
					float shadow = _TransShadow;
					float normal = _TransNormal;
					float scattering = _TransScattering;
					float direct = _TransDirect;
					float ambient = _TransAmbient;
					float strength = _TransStrength;

					Light mainLight = GetMainLight( inputData.shadowCoord );
					float3 mainAtten = mainLight.color * mainLight.distanceAttenuation;
					mainAtten = lerp( mainAtten, mainAtten * mainLight.shadowAttenuation, shadow );

					half3 mainLightDir = mainLight.direction + inputData.normalWS * normal;
					half mainVdotL = pow( saturate( dot( inputData.viewDirectionWS, -mainLightDir ) ), scattering );
					half3 mainTranslucency = mainAtten * ( mainVdotL * direct + inputData.bakedGI * ambient ) * Translucency;
					color.rgb += BaseColor * mainTranslucency * strength;

					#ifdef _ADDITIONAL_LIGHTS
						int transPixelLightCount = GetAdditionalLightsCount();
						for (int i = 0; i < transPixelLightCount; ++i)
						{
							Light light = GetAdditionalLight(i, inputData.positionWS);
							float3 atten = light.color * light.distanceAttenuation;
							atten = lerp( atten, atten * light.shadowAttenuation, shadow );

							half3 lightDir = light.direction + inputData.normalWS * normal;
							half VdotL = pow( saturate( dot( inputData.viewDirectionWS, -lightDir ) ), scattering );
							half3 translucency = atten * ( VdotL * direct + inputData.bakedGI * ambient ) * Translucency;
							color.rgb += BaseColor * translucency * strength;
						}
					#endif
				}
				#endif

				#ifdef _REFRACTION_ASE
					float4 projScreenPos = ScreenPos / ScreenPos.w;
					float3 refractionOffset = ( RefractionIndex - 1.0 ) * mul( UNITY_MATRIX_V, float4( WorldNormal,0 ) ).xyz * ( 1.0 - dot( WorldNormal, WorldViewDirection ) );
					projScreenPos.xy += refractionOffset.xy;
					float3 refraction = SHADERGRAPH_SAMPLE_SCENE_COLOR( projScreenPos.xy ) * RefractionColor;
					color.rgb = lerp( refraction, color.rgb, color.a );
					color.a = 1;
				#endif

				#ifdef ASE_FINAL_COLOR_ALPHA_MULTIPLY
					color.rgb *= color.a;
				#endif

				#ifdef ASE_FOG
					#ifdef TERRAIN_SPLAT_ADDPASS
						color.rgb = MixFogColor(color.rgb, half3( 0, 0, 0 ), IN.fogFactorAndVertexLight.x );
					#else
						color.rgb = MixFog(color.rgb, IN.fogFactorAndVertexLight.x);
					#endif
				#endif

				#ifdef ASE_DEPTH_WRITE_ON
					outputDepth = DepthValue;
				#endif

				return color;
			}

			ENDHLSL
		}
	//-------------------------------------------------------------------------------------
	// END_PASS UNIVERSALFORWARD
	//-------------------------------------------------------------------------------------

		
	//-------------------------------------------------------------------------------------
	// BEGIN_PASS SHADOWCASTER
	//-------------------------------------------------------------------------------------
		Pass
		{
			
			Name "ShadowCaster"
			Tags { "LightMode"="ShadowCaster" }

			ZWrite On
			ZTest LEqual
			AlphaToMask Off
			ColorMask 0

	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			HLSLPROGRAM
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

#include "./../../../../../GPUInstancer/Shaders/Include/GPUInstancerInclude.cginc"
#pragma instancing_options procedural:setupGPUI
#pragma multi_compile_instancing

			
			#define _SPECULAR_SETUP 1
			#define _NORMAL_DROPOFF_TS 1
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#pragma multi_compile _ DOTS_INSTANCING_ON
			#define ASE_ABSOLUTE_VERTEX_POS 1
			#define _TRANSLUCENCY_ASE 1
			#define _EMISSION
			#define _NORMALMAP 1
			#define ASE_SRP_VERSION 120100

			
			#pragma vertex vert
			#pragma fragment frag

			//-----------------------------------------------------------------------------
			// DEFINES - (Unity defined keywords) api 12.1.0 thru 14.0.3
			//-----------------------------------------------------------------------------
			#pragma multi_compile_vertex _ _CASTING_PUNCTUAL_LIGHT_SHADOW

			#define SHADERPASS SHADERPASS_SHADOWCASTER

			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

			//-----------------------------------------------------------------------------
			// DEFINES - (Set by User)
			//-----------------------------------------------------------------------------

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#pragma shader_feature_local TVE_FEATURE_CLIP
			#pragma shader_feature_local TVE_FEATURE_BATCHING
			//TVE Injection Defines
			//SHADER INJECTION POINT BEGIN
			//SHADER INJECTION POINT END
			//TVE Pipeline Defines
			#define THE_VEGETATION_ENGINE
			#define TVE_IS_UNIVERSAL_PIPELINE
			//TVE Shader Type Defines
			#define TVE_IS_GRASS_SHADER


	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

	//-------------------------------------------------------------------------------------
	// BEGIN_CBUFFER
	//-------------------------------------------------------------------------------------

			CBUFFER_START(UnityPerMaterial)
			float4 _SubsurfaceDiffusion_asset;
			float4 _NoiseMaskRemap;
			float4 _SubsurfaceDiffusion_Asset;
			half4 _MotionHighlightColor;
			half4 _MainColor;
			half4 _MainUVs;
			half4 _VertexOcclusionColor;
			half4 _OverlayMaskRemap;
			half4 _AlphaMaskRemap;
			half4 _DetailBlendRemap;
			float4 _Color;
			half4 _GradientColorOne;
			half4 _SubsurfaceMaskRemap;
			float4 _GradientMaskRemap;
			half4 _ColorsMaskRemap;
			half4 _GradientColorTwo;
			float4 _EmissiveIntensityParams;
			half4 _VertexOcclusionRemap;
			half4 _SubsurfaceColor;
			half4 _EmissiveUVs;
			float4 _MaxBoundsInfo;
			half4 _EmissiveColor;
			half3 _render_normals;
			half _PerspectiveAngleValue;
			half _LayerMotionValue;
			half _MotionAmplitude_10;
			float _MotionScale_10;
			float _MotionSpeed_10;
			half _MotionVariation_10;
			half _PerspectiveNoiseValue;
			half _PerspectivePushValue;
			half _VertexDynamicMode;
			half _SizeFadeEndValue;
			half _SizeFadeStartValue;
			half _GlobalSize;
			half _LayerVertexValue;
			half _VertexDataMode;
			half _MotionNormalValue;
			half _MotionValue_30;
			half _InteractionAmplitude;
			half _InteractionMaskValue;
			float _MotionSpeed_32;
			float _MotionVariation_32;
			float _MotionScale_32;
			half _SizeFadeMode;
			half _MotionAmplitude_32;
			half _IsSubsurfaceShader;
			half _GradientMaxValue;
			half _SubsurfaceValue;
			half _GlobalAlpha;
			half _AlphaVariationValue;
			half _MainOcclusionValue;
			half _GlobalWetness;
			half _MainSmoothnessValue;
			half _RenderSpecular;
			half _GlobalEmissive;
			half _MainNormalValue;
			half _OverlayMaskMaxValue;
			half _OverlayMaskMinValue;
			half _GradientMinValue;
			half _OverlayVariationValue;
			half _ExtrasPositionMode;
			half _GlobalOverlay;
			half _ColorsMaskMaxValue;
			half _ColorsMaskMinValue;
			half _ColorsVariationValue;
			half _GlobalColors;
			half _LayerColorsValue;
			half _ColorsPositionMode;
			half _VertexOcclusionMaxValue;
			half _VertexOcclusionMinValue;
			half _VertexPivotMode;
			half _LayerExtrasValue;
			half _render_coverage;
			half _RenderMode;
			half _render_dst;
			half _SubsurfaceAngleValue;
			half _HasGradient;
			half _IsTVEShader;
			half _SpaceGlobalPosition;
			half _VertexVariationMode;
			half _AlphaFeatherValue;
			half _MessageGlobalsVariation;
			half _SubsurfaceShadowValue;
			half _RenderZWrite;
			half _RenderDecals;
			half _RenderDirect;
			half _CategoryNoise;
			half _SubsurfaceScatteringValue;
			half _RenderClip;
			float _SubsurfaceDiffusion;
			half _EmissiveFlagMode;
			half _RenderSSR;
			half _CategoryGlobal;
			half _SubsurfaceNormalValue;
			half _MessageMotionVariation;
			half _SubsurfaceAmbientValue;
			half _RenderCull;
			half _HasOcclusion;
			half _Cutoff;
			half _SubsurfaceDirectValue;
			half _CategoryRender;
			half _RenderQueue;
			half _DetailBlendMode;
			half _CategoryPerspective;
			half _render_cull;
			half _RenderAmbient;
			half _CategoryDetail;
			half _render_src;
			half _render_zw;
			half _IsGrassShader;
			half _RenderPriority;
			half _SubsurfaceMaskMinValue;
			half _CategoryGradient;
			half _LayerReactValue;
			half _RenderNormals;
			half _SpaceGlobalLayers;
			half _AlphaClipValue;
			half _VertexRollingMode;
			half _IsVersion;
			half _RenderCoverage;
			half _MessageSizeFade;
			half _CategoryEmissive;
			half _CategoryOcclusion;
			half _CategoryMain;
			half _CategorySizeFade;
			half _CategoryMotion;
			half _DetailMode;
			half _DetailTypeMode;
			half _HasEmissive;
			half _SpaceGlobalLocals;
			half _VertexMasksMode;
			half _RenderShadow;
			half _SpaceMotionGlobals;
			half _CategorySubsurface;
			half _SpaceRenderFade;
			half _SpaceMotionLocals;
			half _SubsurfaceMaskMaxValue;
			#ifdef _TRANSMISSION_ASE
				float _TransmissionShadow;
			#endif
			#ifdef _TRANSLUCENCY_ASE
				float _TransStrength;
				float _TransNormal;
				float _TransScattering;
				float _TransDirect;
				float _TransAmbient;
				float _TransShadow;
			#endif
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END

			// Property used by ScenePickingPass
			#ifdef SCENEPICKINGPASS
				float4 _SelectionID;
			#endif

			// Properties used by SceneSelectionPass
			#ifdef SCENESELECTIONPASS
				int _ObjectId;
				int _PassValue;
			#endif

	//-------------------------------------------------------------------------------------
	// END_CBUFFER
	//-------------------------------------------------------------------------------------

			sampler2D _MainTex;
			half TVE_Enabled;
			sampler2D _BumpMap;
			half4 TVE_MotionParams;
			TEXTURE2D_ARRAY(TVE_MotionTex);
			half4 TVE_MotionCoords;
			SAMPLER(sampler_linear_clamp);
			float TVE_MotionUsage[10];
			sampler2D TVE_NoiseTex;
			half4 TVE_NoiseParams;
			half4 TVE_FlutterParams;
			half TVE_MotionFadeEnd;
			half TVE_MotionFadeStart;
			half4 TVE_VertexParams;
			TEXTURE2D_ARRAY(TVE_VertexTex);
			half4 TVE_VertexCoords;
			float TVE_VertexUsage[10];
			half TVE_DistanceFadeBias;
			half _DisableSRPBatcher;
			sampler2D _MainAlbedoTex;
			half4 TVE_ExtrasParams;
			TEXTURE2D_ARRAY(TVE_ExtrasTex);
			half4 TVE_ExtrasCoords;
			float TVE_ExtrasUsage[10];


	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShadowCasterPass.hlsl"

			//#ifdef HAVE_VFX_MODIFICATION
			//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/VisualEffectVertex.hlsl"
			//#endif

	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			float2 DecodeFloatToVector2( float enc )
			{
				float2 result ;
				result.y = enc % 2048;
				result.x = floor(enc / 2048);
				return result / (2048 - 1);
			}
			

			float3 _LightDirection;
			float3 _LightPosition;

			VertexOutput VertexFunction( VertexInput v )
			{
				VertexOutput o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );

				float3 VertexPosition3588_g73696 = v.vertex.xyz;
				float3 appendResult60_g74015 = (float3(v.ase_texcoord3.x , v.ase_texcoord3.z , v.ase_texcoord3.y));
				half3 Mesh_PivotsData2831_g73696 = ( appendResult60_g74015 * _VertexPivotMode );
				half3 Mesh_PivotsOS2291_g73696 = Mesh_PivotsData2831_g73696;
				float3 temp_output_2283_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				half3 VertexPos40_g73968 = temp_output_2283_0_g73696;
				float3 appendResult74_g73968 = (float3(VertexPos40_g73968.x , 0.0 , 0.0));
				half3 VertexPosRotationAxis50_g73968 = appendResult74_g73968;
				float3 break84_g73968 = VertexPos40_g73968;
				float3 appendResult81_g73968 = (float3(0.0 , break84_g73968.y , break84_g73968.z));
				half3 VertexPosOtherAxis82_g73968 = appendResult81_g73968;
				float4 temp_output_91_19_g74003 = TVE_MotionCoords;
				float4x4 break19_g73996 = GetObjectToWorldMatrix();
				float3 appendResult20_g73996 = (float3(break19_g73996[ 0 ][ 3 ] , break19_g73996[ 1 ][ 3 ] , break19_g73996[ 2 ][ 3 ]));
				float3 temp_output_122_0_g73996 = Mesh_PivotsData2831_g73696;
				float3 PivotsOnly105_g73996 = (mul( GetObjectToWorldMatrix(), float4( temp_output_122_0_g73996 , 0.0 ) ).xyz).xyz;
				half3 ObjectData20_g73997 = ( appendResult20_g73996 + PivotsOnly105_g73996 );
				float3 ase_worldPos = mul(GetObjectToWorldMatrix(), v.vertex).xyz;
				half3 WorldData19_g73997 = ase_worldPos;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73997 = WorldData19_g73997;
				#else
				float3 staticSwitch14_g73997 = ObjectData20_g73997;
				#endif
				float3 temp_output_114_0_g73996 = staticSwitch14_g73997;
				float3 vertexToFrag4224_g73696 = temp_output_114_0_g73996;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				float3 vertexToFrag3890_g73696 = ase_worldPos;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				half2 UV94_g74003 = ( (temp_output_91_19_g74003).zw + ( (temp_output_91_19_g74003).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g74003 = _LayerMotionValue;
				float4 lerpResult107_g74003 = lerp( TVE_MotionParams , saturate( SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_MotionTex, sampler_linear_clamp, UV94_g74003,temp_output_84_0_g74003, 0.0 ) ) , TVE_MotionUsage[(int)temp_output_84_0_g74003]);
				half4 Global_Motion_Params3909_g73696 = lerpResult107_g74003;
				float4 break322_g73985 = Global_Motion_Params3909_g73696;
				float3 appendResult397_g73985 = (float3(break322_g73985.x , 0.0 , break322_g73985.y));
				float3 temp_output_398_0_g73985 = (appendResult397_g73985*2.0 + -1.0);
				float3 ase_parentObjectScale = ( 1.0 / float3( length( GetWorldToObjectMatrix()[ 0 ].xyz ), length( GetWorldToObjectMatrix()[ 1 ].xyz ), length( GetWorldToObjectMatrix()[ 2 ].xyz ) ) );
				half2 Global_MotionDirectionOS39_g73696 = (( mul( GetWorldToObjectMatrix(), float4( temp_output_398_0_g73985 , 0.0 ) ).xyz * ase_parentObjectScale )).xz;
				half2 Input_DirectionOS358_g73990 = Global_MotionDirectionOS39_g73696;
				half Wind_Power369_g73985 = break322_g73985.z;
				half Global_WindPower2223_g73696 = Wind_Power369_g73985;
				half3 Input_Position419_g73963 = ObjectPosition4223_g73696;
				float Input_MotionScale287_g73963 = ( _MotionScale_10 + 1.0 );
				half Global_Scale448_g73963 = TVE_NoiseParams.x;
				float2 temp_output_597_0_g73963 = (( Input_Position419_g73963 * Input_MotionScale287_g73963 * Global_Scale448_g73963 * 0.0075 )).xz;
				half2 Global_MotionDirectionWS4683_g73696 = (temp_output_398_0_g73985).xz;
				half2 Input_DirectionWS423_g73963 = Global_MotionDirectionWS4683_g73696;
				half Input_MotionSpeed62_g73963 = _MotionSpeed_10;
				half Global_Speed449_g73963 = TVE_NoiseParams.y;
				half Input_MotionVariation284_g73963 = _MotionVariation_10;
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = v.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				half Input_GlobalVariation569_g73963 = Global_MeshVariation5104_g73696;
				float temp_output_630_0_g73963 = ( ( ( _TimeParameters.x * Input_MotionSpeed62_g73963 * Global_Speed449_g73963 ) + ( Input_MotionVariation284_g73963 * Input_GlobalVariation569_g73963 ) ) * 0.03 );
				float temp_output_607_0_g73963 = frac( temp_output_630_0_g73963 );
				float4 lerpResult590_g73963 = lerp( tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * temp_output_607_0_g73963 ) ), 0, 0.0) ) , tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * frac( ( temp_output_630_0_g73963 + 0.5 ) ) ) ), 0, 0.0) ) , ( abs( ( temp_output_607_0_g73963 - 0.5 ) ) / 0.5 ));
				half Input_GlobalWind327_g73963 = Global_WindPower2223_g73696;
				float lerpResult612_g73963 = lerp( 1.4 , 0.4 , Input_GlobalWind327_g73963);
				float3 temp_cast_7 = (lerpResult612_g73963).xxx;
				float3 break638_g73963 = (pow( ( abs( (lerpResult590_g73963).rgb ) + 0.2 ) , temp_cast_7 )*1.4 + -0.2);
				half Global_NoiseTexR34_g73696 = break638_g73963.x;
				half Motion_10_Amplitude2258_g73696 = ( _MotionAmplitude_10 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 );
				half Input_BendingAmplitude376_g73990 = Motion_10_Amplitude2258_g73696;
				half Mesh_Height1524_g73696 = v.ase_color.a;
				half Input_MeshHeight388_g73990 = Mesh_Height1524_g73696;
				half ObjectData20_g73992 = ( Input_MeshHeight388_g73990 * 2.0 );
				float enc62_g73978 = v.ase_texcoord.w;
				float2 localDecodeFloatToVector262_g73978 = DecodeFloatToVector2( enc62_g73978 );
				float2 break63_g73978 = ( localDecodeFloatToVector262_g73978 * 100.0 );
				float Bounds_Height5230_g73696 = break63_g73978.x;
				half Input_BoundsHeight390_g73990 = Bounds_Height5230_g73696;
				half WorldData19_g73992 = ( ( Input_MeshHeight388_g73990 * Input_MeshHeight388_g73990 ) * Input_BoundsHeight390_g73990 * 2.0 );
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73992 = WorldData19_g73992;
				#else
				float staticSwitch14_g73992 = ObjectData20_g73992;
				#endif
				half Mask_Motion_10321_g73990 = staticSwitch14_g73992;
				half Input_InteractionAmplitude58_g73990 = _InteractionAmplitude;
				half Input_InteractionUseMask62_g73990 = _InteractionMaskValue;
				float lerpResult371_g73990 = lerp( 2.0 , Mask_Motion_10321_g73990 , Input_InteractionUseMask62_g73990);
				half ObjectData20_g73991 = lerpResult371_g73990;
				half WorldData19_g73991 = Mask_Motion_10321_g73990;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73991 = WorldData19_g73991;
				#else
				float staticSwitch14_g73991 = ObjectData20_g73991;
				#endif
				half Mask_Interaction373_g73990 = ( Input_InteractionAmplitude58_g73990 * staticSwitch14_g73991 );
				half Global_InteractionMask66_g73696 = ( break322_g73985.w * break322_g73985.w );
				float Input_InteractionGlobalMask330_g73990 = Global_InteractionMask66_g73696;
				float lerpResult360_g73990 = lerp( ( Input_BendingAmplitude376_g73990 * Mask_Motion_10321_g73990 ) , Mask_Interaction373_g73990 , saturate( ( Input_InteractionAmplitude58_g73990 * Input_InteractionGlobalMask330_g73990 ) ));
				float2 break364_g73990 = ( Input_DirectionOS358_g73990 * lerpResult360_g73990 );
				half Motion_10_BendingZ190_g73696 = break364_g73990.y;
				half Angle44_g73968 = Motion_10_BendingZ190_g73696;
				half3 VertexPos40_g73948 = ( VertexPosRotationAxis50_g73968 + ( VertexPosOtherAxis82_g73968 * cos( Angle44_g73968 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g73968 ) * sin( Angle44_g73968 ) ) );
				float3 appendResult74_g73948 = (float3(0.0 , 0.0 , VertexPos40_g73948.z));
				half3 VertexPosRotationAxis50_g73948 = appendResult74_g73948;
				float3 break84_g73948 = VertexPos40_g73948;
				float3 appendResult81_g73948 = (float3(break84_g73948.x , break84_g73948.y , 0.0));
				half3 VertexPosOtherAxis82_g73948 = appendResult81_g73948;
				half Motion_10_BendingX216_g73696 = break364_g73990.x;
				half Angle44_g73948 = -Motion_10_BendingX216_g73696;
				half Input_MotionScale321_g73940 = _MotionScale_32;
				half Input_MotionVariation330_g73940 = _MotionVariation_32;
				half Input_GlobalVariation372_g73940 = Global_MeshVariation5104_g73696;
				half Input_MotionSpeed62_g73940 = _MotionSpeed_32;
				half Global_Speed350_g73940 = TVE_FlutterParams.y;
				half Motion_FacingMask5214_g73696 = 1.0;
				float temp_output_7_0_g73984 = TVE_MotionFadeEnd;
				half Motion_FadeOut4005_g73696 = saturate( ( ( distance( ase_worldPos , _WorldSpaceCameraPos ) - temp_output_7_0_g73984 ) / ( TVE_MotionFadeStart - temp_output_7_0_g73984 ) ) );
				half Motion_30_Amplitude4960_g73696 = ( _MotionAmplitude_32 * _MotionValue_30 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 * Motion_FacingMask5214_g73696 * Motion_FadeOut4005_g73696 );
				half Input_MotionAmplitude58_g73940 = Motion_30_Amplitude4960_g73696;
				half Global_Power354_g73940 = TVE_FlutterParams.x;
				float enc59_g73978 = v.ase_texcoord.z;
				float2 localDecodeFloatToVector259_g73978 = DecodeFloatToVector2( enc59_g73978 );
				float2 break61_g73978 = localDecodeFloatToVector259_g73978;
				half Mesh_Motion_30144_g73696 = break61_g73978.y;
				half Input_MeshMotion_30374_g73940 = Mesh_Motion_30144_g73696;
				half Input_MotionNormal364_g73940 = _MotionNormalValue;
				float3 lerpResult370_g73940 = lerp( float3( 1,1,1 ) , v.ase_normal , Input_MotionNormal364_g73940);
				half3 Motion_30_Details263_g73696 = ( ( sin( ( ( ( ase_worldPos.x + ase_worldPos.y + ase_worldPos.z ) * Input_MotionScale321_g73940 ) + ( Input_MotionVariation330_g73940 * Input_GlobalVariation372_g73940 ) + ( _TimeParameters.x * Input_MotionSpeed62_g73940 * Global_Speed350_g73940 ) ) ) * Input_MotionAmplitude58_g73940 * Global_Power354_g73940 * Input_MeshMotion_30374_g73940 * 0.4 ) * lerpResult370_g73940 );
				float3 Vertex_Motion_Object833_g73696 = ( ( VertexPosRotationAxis50_g73948 + ( VertexPosOtherAxis82_g73948 * cos( Angle44_g73948 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g73948 ) * sin( Angle44_g73948 ) ) ) + Motion_30_Details263_g73696 );
				float3 temp_output_3474_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				float3 appendResult2043_g73696 = (float3(Motion_10_BendingX216_g73696 , 0.0 , Motion_10_BendingZ190_g73696));
				float3 Vertex_Motion_World1118_g73696 = ( ( temp_output_3474_0_g73696 + appendResult2043_g73696 ) + Motion_30_Details263_g73696 );
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch4976_g73696 = Vertex_Motion_World1118_g73696;
				#else
				float3 staticSwitch4976_g73696 = ( Vertex_Motion_Object833_g73696 + ( _VertexDataMode * 0.0 ) );
				#endif
				float4 temp_output_94_19_g73959 = TVE_VertexCoords;
				half2 UV97_g73959 = ( (temp_output_94_19_g73959).zw + ( (temp_output_94_19_g73959).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g73959 = _LayerVertexValue;
				float4 lerpResult109_g73959 = lerp( TVE_VertexParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_VertexTex, sampler_linear_clamp, UV97_g73959,temp_output_84_0_g73959, 0.0 ) , TVE_VertexUsage[(int)temp_output_84_0_g73959]);
				half4 Global_Object_Params4173_g73696 = lerpResult109_g73959;
				half Global_VertexSize174_g73696 = saturate( Global_Object_Params4173_g73696.w );
				float lerpResult346_g73696 = lerp( 1.0 , Global_VertexSize174_g73696 , _GlobalSize);
				float3 appendResult3480_g73696 = (float3(lerpResult346_g73696 , lerpResult346_g73696 , lerpResult346_g73696));
				half3 ObjectData20_g73993 = appendResult3480_g73696;
				half3 _Vector11 = half3(1,1,1);
				half3 WorldData19_g73993 = _Vector11;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73993 = WorldData19_g73993;
				#else
				float3 staticSwitch14_g73993 = ObjectData20_g73993;
				#endif
				half3 Vertex_Size1741_g73696 = staticSwitch14_g73993;
				half3 _Vector5 = half3(1,1,1);
				float temp_output_7_0_g73931 = _SizeFadeEndValue;
				float temp_output_335_0_g73696 = saturate( ( ( ( distance( _WorldSpaceCameraPos , ObjectPosition4223_g73696 ) * ( 1.0 / TVE_DistanceFadeBias ) ) - temp_output_7_0_g73931 ) / ( _SizeFadeStartValue - temp_output_7_0_g73931 ) ) );
				float3 appendResult3482_g73696 = (float3(temp_output_335_0_g73696 , temp_output_335_0_g73696 , temp_output_335_0_g73696));
				float3 lerpResult5065_g73696 = lerp( _Vector5 , appendResult3482_g73696 , _SizeFadeMode);
				half3 ObjectData20_g73958 = lerpResult5065_g73696;
				half3 WorldData19_g73958 = _Vector5;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73958 = WorldData19_g73958;
				#else
				float3 staticSwitch14_g73958 = ObjectData20_g73958;
				#endif
				float3 Vertex_SizeFade1740_g73696 = staticSwitch14_g73958;
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - ase_worldPos );
				ase_worldViewDir = normalize(ase_worldViewDir);
				float3 normalizeResult2696_g73696 = normalize( ase_worldViewDir );
				float3 break2709_g73696 = cross( normalizeResult2696_g73696 , half3(0,1,0) );
				float3 appendResult2710_g73696 = (float3(-break2709_g73696.z , 0.0 , break2709_g73696.x));
				float3 appendResult2667_g73696 = (float3(Global_MeshVariation5104_g73696 , 0.5 , Global_MeshVariation5104_g73696));
				float3 normalizeResult2169_g73696 = normalize( ase_worldViewDir );
				float3 ViewDir_Normalized3963_g73696 = normalizeResult2169_g73696;
				float dotResult2212_g73696 = dot( ViewDir_Normalized3963_g73696 , float3(0,1,0) );
				half Mask_HView2656_g73696 = dotResult2212_g73696;
				float saferPower2652_g73696 = abs( Mask_HView2656_g73696 );
				half3 Grass_Perspective2661_g73696 = ( ( ( mul( GetWorldToObjectMatrix(), float4( appendResult2710_g73696 , 0.0 ) ).xyz * _PerspectivePushValue ) + ( (appendResult2667_g73696*2.0 + -1.0) * _PerspectiveNoiseValue ) ) * Mesh_Height1524_g73696 * pow( saferPower2652_g73696 , _PerspectiveAngleValue ) );
				float3 lerpResult16_g74000 = lerp( VertexPosition3588_g73696 , ( ( staticSwitch4976_g73696 * Vertex_Size1741_g73696 * Vertex_SizeFade1740_g73696 ) + Mesh_PivotsOS2291_g73696 + Grass_Perspective2661_g73696 ) , TVE_Enabled);
				float3 Final_VertexPosition890_g73696 = ( lerpResult16_g74000 + _DisableSRPBatcher );
				
				o.ase_texcoord3.xyz = vertexToFrag3890_g73696;
				o.ase_texcoord4.xyz = vertexToFrag4224_g73696;
				
				o.ase_texcoord2 = v.ase_texcoord;
				o.ase_color = v.ase_color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.w = 0;
				o.ase_texcoord4.w = 0;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif

				float3 vertexValue = Final_VertexPosition890_g73696;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					o.worldPos = positionWS;
				#endif

				float3 normalWS = TransformObjectToWorldDir(v.ase_normal);

				#if _CASTING_PUNCTUAL_LIGHT_SHADOW
					float3 lightDirectionWS = normalize(_LightPosition - positionWS);
				#else
					float3 lightDirectionWS = _LightDirection;
				#endif

				float4 clipPos = TransformWorldToHClip(ApplyShadowBias(positionWS, normalWS, lightDirectionWS));

				#if UNITY_REVERSED_Z
					clipPos.z = min(clipPos.z, UNITY_NEAR_CLIP_VALUE);
				#else
					clipPos.z = max(clipPos.z, UNITY_NEAR_CLIP_VALUE);
				#endif

				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif

				o.clipPos = clipPos;

				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord3 = v.ase_texcoord3;
				o.ase_color = v.ase_color;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
				return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord3 = patch[0].ase_texcoord3 * bary.x + patch[1].ase_texcoord3 * bary.y + patch[2].ase_texcoord3 * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			#if defined(ASE_EARLY_Z_DEPTH_OPTIMIZE)
				#define ASE_SV_DEPTH SV_DepthLessEqual  
			#else
				#define ASE_SV_DEPTH SV_Depth
			#endif

			half4 frag(	VertexOutput IN 
						#ifdef ASE_DEPTH_WRITE_ON
						,out float outputDepth : ASE_SV_DEPTH
						#endif
						 ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );
				
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					float3 WorldPosition = IN.worldPos;
				#endif

				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float localCustomAlphaClip19_g74020 = ( 0.0 );
				half2 Main_UVs15_g73696 = ( ( IN.ase_texcoord2.xy * (_MainUVs).xy ) + (_MainUVs).zw );
				float4 tex2DNode29_g73696 = tex2D( _MainAlbedoTex, Main_UVs15_g73696 );
				float Main_Alpha316_g73696 = ( _MainColor.a * tex2DNode29_g73696.a );
				float4 temp_output_93_19_g73986 = TVE_ExtrasCoords;
				float3 vertexToFrag3890_g73696 = IN.ase_texcoord3.xyz;
				float3 WorldPosition3905_g73696 = vertexToFrag3890_g73696;
				float3 vertexToFrag4224_g73696 = IN.ase_texcoord4.xyz;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				float3 lerpResult4827_g73696 = lerp( WorldPosition3905_g73696 , ObjectPosition4223_g73696 , _ExtrasPositionMode);
				half2 UV96_g73986 = ( (temp_output_93_19_g73986).zw + ( (temp_output_93_19_g73986).xy * (lerpResult4827_g73696).xz ) );
				float temp_output_84_0_g73986 = _LayerExtrasValue;
				float4 lerpResult109_g73986 = lerp( TVE_ExtrasParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_ExtrasTex, sampler_linear_clamp, UV96_g73986,temp_output_84_0_g73986, 0.0 ) , TVE_ExtrasUsage[(int)temp_output_84_0_g73986]);
				float4 break89_g73986 = lerpResult109_g73986;
				half Global_Extras_Alpha1033_g73696 = saturate( break89_g73986.a );
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = IN.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				float lerpResult5154_g73696 = lerp( 0.0 , Global_MeshVariation5104_g73696 , _AlphaVariationValue);
				half Global_Alpha_Variation5158_g73696 = lerpResult5154_g73696;
				half AlphaTreshold2132_g73696 = _AlphaClipValue;
				half Global_Alpha_Mask4546_g73696 = 1.0;
				float lerpResult5203_g73696 = lerp( 1.0 , ( ( Global_Extras_Alpha1033_g73696 - Global_Alpha_Variation5158_g73696 ) + AlphaTreshold2132_g73696 + ( Global_Extras_Alpha1033_g73696 * 0.5 ) ) , ( Global_Alpha_Mask4546_g73696 * _GlobalAlpha ));
				float lerpResult16_g74016 = lerp( 1.0 , lerpResult5203_g73696 , TVE_Enabled);
				half Global_Alpha315_g73696 = lerpResult16_g74016;
				half Fade_Alpha3727_g73696 = 1.0;
				float temp_output_661_0_g73696 = ( Main_Alpha316_g73696 * Global_Alpha315_g73696 * Fade_Alpha3727_g73696 );
				half Alpha34_g74021 = temp_output_661_0_g73696;
				half Offest27_g74021 = AlphaTreshold2132_g73696;
				half AlphaFeather5305_g73696 = _AlphaFeatherValue;
				half Feather30_g74021 = AlphaFeather5305_g73696;
				float temp_output_25_0_g74021 = ( ( ( Alpha34_g74021 - Offest27_g74021 ) / ( max( fwidth( Alpha34_g74021 ) , 0.001 ) + Feather30_g74021 ) ) + Offest27_g74021 );
				float temp_output_3_0_g74020 = temp_output_25_0_g74021;
				float Alpha19_g74020 = temp_output_3_0_g74020;
				float temp_output_15_0_g74020 = AlphaTreshold2132_g73696;
				float Treshold19_g74020 = temp_output_15_0_g74020;
				{
				#if defined (TVE_FEATURE_CLIP)
				#if defined (TVE_IS_HD_PIPELINE)
				#if !defined (SHADERPASS_FORWARD_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#if !defined (SHADERPASS_GBUFFER_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#else
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#endif
				}
				half Final_Alpha914_g73696 = saturate( Alpha19_g74020 );
				

				float Alpha = Final_Alpha914_g73696;
				float AlphaClipThreshold = 0.5;
				float AlphaClipThresholdShadow = 0.5;

				#ifdef ASE_DEPTH_WRITE_ON
					float DepthValue = 0;
				#endif

				#ifdef _ALPHATEST_ON
					#ifdef _ALPHATEST_SHADOW_ON
						clip(Alpha - AlphaClipThresholdShadow);
					#else
						clip(Alpha - AlphaClipThreshold);
					#endif
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif

				#ifdef ASE_DEPTH_WRITE_ON
					outputDepth = DepthValue;
				#endif

				return 0;
			}
			ENDHLSL
		}
	//-------------------------------------------------------------------------------------
	// END_PASS SHADOWCASTER
	//-------------------------------------------------------------------------------------

		
	//-------------------------------------------------------------------------------------
	// BEGIN_PASS DEPTHONLY
	//-------------------------------------------------------------------------------------
		Pass
		{
			
			Name "DepthOnly"
			Tags { "LightMode"="DepthOnly" }

			ZWrite On
			ColorMask 0
			AlphaToMask Off

	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			HLSLPROGRAM
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

#include "./../../../../../GPUInstancer/Shaders/Include/GPUInstancerInclude.cginc"
#pragma instancing_options procedural:setupGPUI
#pragma multi_compile_instancing

			
			#define _SPECULAR_SETUP 1
			#define _NORMAL_DROPOFF_TS 1
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#pragma multi_compile _ DOTS_INSTANCING_ON
			#define ASE_ABSOLUTE_VERTEX_POS 1
			#define _TRANSLUCENCY_ASE 1
			#define _EMISSION
			#define _NORMALMAP 1
			#define ASE_SRP_VERSION 120100

			
			#pragma vertex vert
			#pragma fragment frag

			#define SHADERPASS SHADERPASS_DEPTHONLY

			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

			//-----------------------------------------------------------------------------
			// DEFINES - (Set by User)
			//-----------------------------------------------------------------------------

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#pragma shader_feature_local TVE_FEATURE_CLIP
			#pragma shader_feature_local TVE_FEATURE_BATCHING
			//TVE Injection Defines
			//SHADER INJECTION POINT BEGIN
			//SHADER INJECTION POINT END
			//TVE Pipeline Defines
			#define THE_VEGETATION_ENGINE
			#define TVE_IS_UNIVERSAL_PIPELINE
			//TVE Shader Type Defines
			#define TVE_IS_GRASS_SHADER


	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

	//-------------------------------------------------------------------------------------
	// BEGIN_CBUFFER
	//-------------------------------------------------------------------------------------

			CBUFFER_START(UnityPerMaterial)
			float4 _SubsurfaceDiffusion_asset;
			float4 _NoiseMaskRemap;
			float4 _SubsurfaceDiffusion_Asset;
			half4 _MotionHighlightColor;
			half4 _MainColor;
			half4 _MainUVs;
			half4 _VertexOcclusionColor;
			half4 _OverlayMaskRemap;
			half4 _AlphaMaskRemap;
			half4 _DetailBlendRemap;
			float4 _Color;
			half4 _GradientColorOne;
			half4 _SubsurfaceMaskRemap;
			float4 _GradientMaskRemap;
			half4 _ColorsMaskRemap;
			half4 _GradientColorTwo;
			float4 _EmissiveIntensityParams;
			half4 _VertexOcclusionRemap;
			half4 _SubsurfaceColor;
			half4 _EmissiveUVs;
			float4 _MaxBoundsInfo;
			half4 _EmissiveColor;
			half3 _render_normals;
			half _PerspectiveAngleValue;
			half _LayerMotionValue;
			half _MotionAmplitude_10;
			float _MotionScale_10;
			float _MotionSpeed_10;
			half _MotionVariation_10;
			half _PerspectiveNoiseValue;
			half _PerspectivePushValue;
			half _VertexDynamicMode;
			half _SizeFadeEndValue;
			half _SizeFadeStartValue;
			half _GlobalSize;
			half _LayerVertexValue;
			half _VertexDataMode;
			half _MotionNormalValue;
			half _MotionValue_30;
			half _InteractionAmplitude;
			half _InteractionMaskValue;
			float _MotionSpeed_32;
			float _MotionVariation_32;
			float _MotionScale_32;
			half _SizeFadeMode;
			half _MotionAmplitude_32;
			half _IsSubsurfaceShader;
			half _GradientMaxValue;
			half _SubsurfaceValue;
			half _GlobalAlpha;
			half _AlphaVariationValue;
			half _MainOcclusionValue;
			half _GlobalWetness;
			half _MainSmoothnessValue;
			half _RenderSpecular;
			half _GlobalEmissive;
			half _MainNormalValue;
			half _OverlayMaskMaxValue;
			half _OverlayMaskMinValue;
			half _GradientMinValue;
			half _OverlayVariationValue;
			half _ExtrasPositionMode;
			half _GlobalOverlay;
			half _ColorsMaskMaxValue;
			half _ColorsMaskMinValue;
			half _ColorsVariationValue;
			half _GlobalColors;
			half _LayerColorsValue;
			half _ColorsPositionMode;
			half _VertexOcclusionMaxValue;
			half _VertexOcclusionMinValue;
			half _VertexPivotMode;
			half _LayerExtrasValue;
			half _render_coverage;
			half _RenderMode;
			half _render_dst;
			half _SubsurfaceAngleValue;
			half _HasGradient;
			half _IsTVEShader;
			half _SpaceGlobalPosition;
			half _VertexVariationMode;
			half _AlphaFeatherValue;
			half _MessageGlobalsVariation;
			half _SubsurfaceShadowValue;
			half _RenderZWrite;
			half _RenderDecals;
			half _RenderDirect;
			half _CategoryNoise;
			half _SubsurfaceScatteringValue;
			half _RenderClip;
			float _SubsurfaceDiffusion;
			half _EmissiveFlagMode;
			half _RenderSSR;
			half _CategoryGlobal;
			half _SubsurfaceNormalValue;
			half _MessageMotionVariation;
			half _SubsurfaceAmbientValue;
			half _RenderCull;
			half _HasOcclusion;
			half _Cutoff;
			half _SubsurfaceDirectValue;
			half _CategoryRender;
			half _RenderQueue;
			half _DetailBlendMode;
			half _CategoryPerspective;
			half _render_cull;
			half _RenderAmbient;
			half _CategoryDetail;
			half _render_src;
			half _render_zw;
			half _IsGrassShader;
			half _RenderPriority;
			half _SubsurfaceMaskMinValue;
			half _CategoryGradient;
			half _LayerReactValue;
			half _RenderNormals;
			half _SpaceGlobalLayers;
			half _AlphaClipValue;
			half _VertexRollingMode;
			half _IsVersion;
			half _RenderCoverage;
			half _MessageSizeFade;
			half _CategoryEmissive;
			half _CategoryOcclusion;
			half _CategoryMain;
			half _CategorySizeFade;
			half _CategoryMotion;
			half _DetailMode;
			half _DetailTypeMode;
			half _HasEmissive;
			half _SpaceGlobalLocals;
			half _VertexMasksMode;
			half _RenderShadow;
			half _SpaceMotionGlobals;
			half _CategorySubsurface;
			half _SpaceRenderFade;
			half _SpaceMotionLocals;
			half _SubsurfaceMaskMaxValue;
			#ifdef _TRANSMISSION_ASE
				float _TransmissionShadow;
			#endif
			#ifdef _TRANSLUCENCY_ASE
				float _TransStrength;
				float _TransNormal;
				float _TransScattering;
				float _TransDirect;
				float _TransAmbient;
				float _TransShadow;
			#endif
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END

			// Property used by ScenePickingPass
			#ifdef SCENEPICKINGPASS
				float4 _SelectionID;
			#endif

			// Properties used by SceneSelectionPass
			#ifdef SCENESELECTIONPASS
				int _ObjectId;
				int _PassValue;
			#endif

	//-------------------------------------------------------------------------------------
	// END_CBUFFER
	//-------------------------------------------------------------------------------------

			sampler2D _MainTex;
			half TVE_Enabled;
			sampler2D _BumpMap;
			half4 TVE_MotionParams;
			TEXTURE2D_ARRAY(TVE_MotionTex);
			half4 TVE_MotionCoords;
			SAMPLER(sampler_linear_clamp);
			float TVE_MotionUsage[10];
			sampler2D TVE_NoiseTex;
			half4 TVE_NoiseParams;
			half4 TVE_FlutterParams;
			half TVE_MotionFadeEnd;
			half TVE_MotionFadeStart;
			half4 TVE_VertexParams;
			TEXTURE2D_ARRAY(TVE_VertexTex);
			half4 TVE_VertexCoords;
			float TVE_VertexUsage[10];
			half TVE_DistanceFadeBias;
			half _DisableSRPBatcher;
			sampler2D _MainAlbedoTex;
			half4 TVE_ExtrasParams;
			TEXTURE2D_ARRAY(TVE_ExtrasTex);
			half4 TVE_ExtrasCoords;
			float TVE_ExtrasUsage[10];


	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthOnlyPass.hlsl"

			//#ifdef HAVE_VFX_MODIFICATION
			//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/VisualEffectVertex.hlsl"
			//#endif

	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			float2 DecodeFloatToVector2( float enc )
			{
				float2 result ;
				result.y = enc % 2048;
				result.x = floor(enc / 2048);
				return result / (2048 - 1);
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 VertexPosition3588_g73696 = v.vertex.xyz;
				float3 appendResult60_g74015 = (float3(v.ase_texcoord3.x , v.ase_texcoord3.z , v.ase_texcoord3.y));
				half3 Mesh_PivotsData2831_g73696 = ( appendResult60_g74015 * _VertexPivotMode );
				half3 Mesh_PivotsOS2291_g73696 = Mesh_PivotsData2831_g73696;
				float3 temp_output_2283_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				half3 VertexPos40_g73968 = temp_output_2283_0_g73696;
				float3 appendResult74_g73968 = (float3(VertexPos40_g73968.x , 0.0 , 0.0));
				half3 VertexPosRotationAxis50_g73968 = appendResult74_g73968;
				float3 break84_g73968 = VertexPos40_g73968;
				float3 appendResult81_g73968 = (float3(0.0 , break84_g73968.y , break84_g73968.z));
				half3 VertexPosOtherAxis82_g73968 = appendResult81_g73968;
				float4 temp_output_91_19_g74003 = TVE_MotionCoords;
				float4x4 break19_g73996 = GetObjectToWorldMatrix();
				float3 appendResult20_g73996 = (float3(break19_g73996[ 0 ][ 3 ] , break19_g73996[ 1 ][ 3 ] , break19_g73996[ 2 ][ 3 ]));
				float3 temp_output_122_0_g73996 = Mesh_PivotsData2831_g73696;
				float3 PivotsOnly105_g73996 = (mul( GetObjectToWorldMatrix(), float4( temp_output_122_0_g73996 , 0.0 ) ).xyz).xyz;
				half3 ObjectData20_g73997 = ( appendResult20_g73996 + PivotsOnly105_g73996 );
				float3 ase_worldPos = mul(GetObjectToWorldMatrix(), v.vertex).xyz;
				half3 WorldData19_g73997 = ase_worldPos;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73997 = WorldData19_g73997;
				#else
				float3 staticSwitch14_g73997 = ObjectData20_g73997;
				#endif
				float3 temp_output_114_0_g73996 = staticSwitch14_g73997;
				float3 vertexToFrag4224_g73696 = temp_output_114_0_g73996;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				float3 vertexToFrag3890_g73696 = ase_worldPos;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				half2 UV94_g74003 = ( (temp_output_91_19_g74003).zw + ( (temp_output_91_19_g74003).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g74003 = _LayerMotionValue;
				float4 lerpResult107_g74003 = lerp( TVE_MotionParams , saturate( SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_MotionTex, sampler_linear_clamp, UV94_g74003,temp_output_84_0_g74003, 0.0 ) ) , TVE_MotionUsage[(int)temp_output_84_0_g74003]);
				half4 Global_Motion_Params3909_g73696 = lerpResult107_g74003;
				float4 break322_g73985 = Global_Motion_Params3909_g73696;
				float3 appendResult397_g73985 = (float3(break322_g73985.x , 0.0 , break322_g73985.y));
				float3 temp_output_398_0_g73985 = (appendResult397_g73985*2.0 + -1.0);
				float3 ase_parentObjectScale = ( 1.0 / float3( length( GetWorldToObjectMatrix()[ 0 ].xyz ), length( GetWorldToObjectMatrix()[ 1 ].xyz ), length( GetWorldToObjectMatrix()[ 2 ].xyz ) ) );
				half2 Global_MotionDirectionOS39_g73696 = (( mul( GetWorldToObjectMatrix(), float4( temp_output_398_0_g73985 , 0.0 ) ).xyz * ase_parentObjectScale )).xz;
				half2 Input_DirectionOS358_g73990 = Global_MotionDirectionOS39_g73696;
				half Wind_Power369_g73985 = break322_g73985.z;
				half Global_WindPower2223_g73696 = Wind_Power369_g73985;
				half3 Input_Position419_g73963 = ObjectPosition4223_g73696;
				float Input_MotionScale287_g73963 = ( _MotionScale_10 + 1.0 );
				half Global_Scale448_g73963 = TVE_NoiseParams.x;
				float2 temp_output_597_0_g73963 = (( Input_Position419_g73963 * Input_MotionScale287_g73963 * Global_Scale448_g73963 * 0.0075 )).xz;
				half2 Global_MotionDirectionWS4683_g73696 = (temp_output_398_0_g73985).xz;
				half2 Input_DirectionWS423_g73963 = Global_MotionDirectionWS4683_g73696;
				half Input_MotionSpeed62_g73963 = _MotionSpeed_10;
				half Global_Speed449_g73963 = TVE_NoiseParams.y;
				half Input_MotionVariation284_g73963 = _MotionVariation_10;
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = v.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				half Input_GlobalVariation569_g73963 = Global_MeshVariation5104_g73696;
				float temp_output_630_0_g73963 = ( ( ( _TimeParameters.x * Input_MotionSpeed62_g73963 * Global_Speed449_g73963 ) + ( Input_MotionVariation284_g73963 * Input_GlobalVariation569_g73963 ) ) * 0.03 );
				float temp_output_607_0_g73963 = frac( temp_output_630_0_g73963 );
				float4 lerpResult590_g73963 = lerp( tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * temp_output_607_0_g73963 ) ), 0, 0.0) ) , tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * frac( ( temp_output_630_0_g73963 + 0.5 ) ) ) ), 0, 0.0) ) , ( abs( ( temp_output_607_0_g73963 - 0.5 ) ) / 0.5 ));
				half Input_GlobalWind327_g73963 = Global_WindPower2223_g73696;
				float lerpResult612_g73963 = lerp( 1.4 , 0.4 , Input_GlobalWind327_g73963);
				float3 temp_cast_7 = (lerpResult612_g73963).xxx;
				float3 break638_g73963 = (pow( ( abs( (lerpResult590_g73963).rgb ) + 0.2 ) , temp_cast_7 )*1.4 + -0.2);
				half Global_NoiseTexR34_g73696 = break638_g73963.x;
				half Motion_10_Amplitude2258_g73696 = ( _MotionAmplitude_10 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 );
				half Input_BendingAmplitude376_g73990 = Motion_10_Amplitude2258_g73696;
				half Mesh_Height1524_g73696 = v.ase_color.a;
				half Input_MeshHeight388_g73990 = Mesh_Height1524_g73696;
				half ObjectData20_g73992 = ( Input_MeshHeight388_g73990 * 2.0 );
				float enc62_g73978 = v.ase_texcoord.w;
				float2 localDecodeFloatToVector262_g73978 = DecodeFloatToVector2( enc62_g73978 );
				float2 break63_g73978 = ( localDecodeFloatToVector262_g73978 * 100.0 );
				float Bounds_Height5230_g73696 = break63_g73978.x;
				half Input_BoundsHeight390_g73990 = Bounds_Height5230_g73696;
				half WorldData19_g73992 = ( ( Input_MeshHeight388_g73990 * Input_MeshHeight388_g73990 ) * Input_BoundsHeight390_g73990 * 2.0 );
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73992 = WorldData19_g73992;
				#else
				float staticSwitch14_g73992 = ObjectData20_g73992;
				#endif
				half Mask_Motion_10321_g73990 = staticSwitch14_g73992;
				half Input_InteractionAmplitude58_g73990 = _InteractionAmplitude;
				half Input_InteractionUseMask62_g73990 = _InteractionMaskValue;
				float lerpResult371_g73990 = lerp( 2.0 , Mask_Motion_10321_g73990 , Input_InteractionUseMask62_g73990);
				half ObjectData20_g73991 = lerpResult371_g73990;
				half WorldData19_g73991 = Mask_Motion_10321_g73990;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73991 = WorldData19_g73991;
				#else
				float staticSwitch14_g73991 = ObjectData20_g73991;
				#endif
				half Mask_Interaction373_g73990 = ( Input_InteractionAmplitude58_g73990 * staticSwitch14_g73991 );
				half Global_InteractionMask66_g73696 = ( break322_g73985.w * break322_g73985.w );
				float Input_InteractionGlobalMask330_g73990 = Global_InteractionMask66_g73696;
				float lerpResult360_g73990 = lerp( ( Input_BendingAmplitude376_g73990 * Mask_Motion_10321_g73990 ) , Mask_Interaction373_g73990 , saturate( ( Input_InteractionAmplitude58_g73990 * Input_InteractionGlobalMask330_g73990 ) ));
				float2 break364_g73990 = ( Input_DirectionOS358_g73990 * lerpResult360_g73990 );
				half Motion_10_BendingZ190_g73696 = break364_g73990.y;
				half Angle44_g73968 = Motion_10_BendingZ190_g73696;
				half3 VertexPos40_g73948 = ( VertexPosRotationAxis50_g73968 + ( VertexPosOtherAxis82_g73968 * cos( Angle44_g73968 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g73968 ) * sin( Angle44_g73968 ) ) );
				float3 appendResult74_g73948 = (float3(0.0 , 0.0 , VertexPos40_g73948.z));
				half3 VertexPosRotationAxis50_g73948 = appendResult74_g73948;
				float3 break84_g73948 = VertexPos40_g73948;
				float3 appendResult81_g73948 = (float3(break84_g73948.x , break84_g73948.y , 0.0));
				half3 VertexPosOtherAxis82_g73948 = appendResult81_g73948;
				half Motion_10_BendingX216_g73696 = break364_g73990.x;
				half Angle44_g73948 = -Motion_10_BendingX216_g73696;
				half Input_MotionScale321_g73940 = _MotionScale_32;
				half Input_MotionVariation330_g73940 = _MotionVariation_32;
				half Input_GlobalVariation372_g73940 = Global_MeshVariation5104_g73696;
				half Input_MotionSpeed62_g73940 = _MotionSpeed_32;
				half Global_Speed350_g73940 = TVE_FlutterParams.y;
				half Motion_FacingMask5214_g73696 = 1.0;
				float temp_output_7_0_g73984 = TVE_MotionFadeEnd;
				half Motion_FadeOut4005_g73696 = saturate( ( ( distance( ase_worldPos , _WorldSpaceCameraPos ) - temp_output_7_0_g73984 ) / ( TVE_MotionFadeStart - temp_output_7_0_g73984 ) ) );
				half Motion_30_Amplitude4960_g73696 = ( _MotionAmplitude_32 * _MotionValue_30 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 * Motion_FacingMask5214_g73696 * Motion_FadeOut4005_g73696 );
				half Input_MotionAmplitude58_g73940 = Motion_30_Amplitude4960_g73696;
				half Global_Power354_g73940 = TVE_FlutterParams.x;
				float enc59_g73978 = v.ase_texcoord.z;
				float2 localDecodeFloatToVector259_g73978 = DecodeFloatToVector2( enc59_g73978 );
				float2 break61_g73978 = localDecodeFloatToVector259_g73978;
				half Mesh_Motion_30144_g73696 = break61_g73978.y;
				half Input_MeshMotion_30374_g73940 = Mesh_Motion_30144_g73696;
				half Input_MotionNormal364_g73940 = _MotionNormalValue;
				float3 lerpResult370_g73940 = lerp( float3( 1,1,1 ) , v.ase_normal , Input_MotionNormal364_g73940);
				half3 Motion_30_Details263_g73696 = ( ( sin( ( ( ( ase_worldPos.x + ase_worldPos.y + ase_worldPos.z ) * Input_MotionScale321_g73940 ) + ( Input_MotionVariation330_g73940 * Input_GlobalVariation372_g73940 ) + ( _TimeParameters.x * Input_MotionSpeed62_g73940 * Global_Speed350_g73940 ) ) ) * Input_MotionAmplitude58_g73940 * Global_Power354_g73940 * Input_MeshMotion_30374_g73940 * 0.4 ) * lerpResult370_g73940 );
				float3 Vertex_Motion_Object833_g73696 = ( ( VertexPosRotationAxis50_g73948 + ( VertexPosOtherAxis82_g73948 * cos( Angle44_g73948 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g73948 ) * sin( Angle44_g73948 ) ) ) + Motion_30_Details263_g73696 );
				float3 temp_output_3474_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				float3 appendResult2043_g73696 = (float3(Motion_10_BendingX216_g73696 , 0.0 , Motion_10_BendingZ190_g73696));
				float3 Vertex_Motion_World1118_g73696 = ( ( temp_output_3474_0_g73696 + appendResult2043_g73696 ) + Motion_30_Details263_g73696 );
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch4976_g73696 = Vertex_Motion_World1118_g73696;
				#else
				float3 staticSwitch4976_g73696 = ( Vertex_Motion_Object833_g73696 + ( _VertexDataMode * 0.0 ) );
				#endif
				float4 temp_output_94_19_g73959 = TVE_VertexCoords;
				half2 UV97_g73959 = ( (temp_output_94_19_g73959).zw + ( (temp_output_94_19_g73959).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g73959 = _LayerVertexValue;
				float4 lerpResult109_g73959 = lerp( TVE_VertexParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_VertexTex, sampler_linear_clamp, UV97_g73959,temp_output_84_0_g73959, 0.0 ) , TVE_VertexUsage[(int)temp_output_84_0_g73959]);
				half4 Global_Object_Params4173_g73696 = lerpResult109_g73959;
				half Global_VertexSize174_g73696 = saturate( Global_Object_Params4173_g73696.w );
				float lerpResult346_g73696 = lerp( 1.0 , Global_VertexSize174_g73696 , _GlobalSize);
				float3 appendResult3480_g73696 = (float3(lerpResult346_g73696 , lerpResult346_g73696 , lerpResult346_g73696));
				half3 ObjectData20_g73993 = appendResult3480_g73696;
				half3 _Vector11 = half3(1,1,1);
				half3 WorldData19_g73993 = _Vector11;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73993 = WorldData19_g73993;
				#else
				float3 staticSwitch14_g73993 = ObjectData20_g73993;
				#endif
				half3 Vertex_Size1741_g73696 = staticSwitch14_g73993;
				half3 _Vector5 = half3(1,1,1);
				float temp_output_7_0_g73931 = _SizeFadeEndValue;
				float temp_output_335_0_g73696 = saturate( ( ( ( distance( _WorldSpaceCameraPos , ObjectPosition4223_g73696 ) * ( 1.0 / TVE_DistanceFadeBias ) ) - temp_output_7_0_g73931 ) / ( _SizeFadeStartValue - temp_output_7_0_g73931 ) ) );
				float3 appendResult3482_g73696 = (float3(temp_output_335_0_g73696 , temp_output_335_0_g73696 , temp_output_335_0_g73696));
				float3 lerpResult5065_g73696 = lerp( _Vector5 , appendResult3482_g73696 , _SizeFadeMode);
				half3 ObjectData20_g73958 = lerpResult5065_g73696;
				half3 WorldData19_g73958 = _Vector5;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73958 = WorldData19_g73958;
				#else
				float3 staticSwitch14_g73958 = ObjectData20_g73958;
				#endif
				float3 Vertex_SizeFade1740_g73696 = staticSwitch14_g73958;
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - ase_worldPos );
				ase_worldViewDir = normalize(ase_worldViewDir);
				float3 normalizeResult2696_g73696 = normalize( ase_worldViewDir );
				float3 break2709_g73696 = cross( normalizeResult2696_g73696 , half3(0,1,0) );
				float3 appendResult2710_g73696 = (float3(-break2709_g73696.z , 0.0 , break2709_g73696.x));
				float3 appendResult2667_g73696 = (float3(Global_MeshVariation5104_g73696 , 0.5 , Global_MeshVariation5104_g73696));
				float3 normalizeResult2169_g73696 = normalize( ase_worldViewDir );
				float3 ViewDir_Normalized3963_g73696 = normalizeResult2169_g73696;
				float dotResult2212_g73696 = dot( ViewDir_Normalized3963_g73696 , float3(0,1,0) );
				half Mask_HView2656_g73696 = dotResult2212_g73696;
				float saferPower2652_g73696 = abs( Mask_HView2656_g73696 );
				half3 Grass_Perspective2661_g73696 = ( ( ( mul( GetWorldToObjectMatrix(), float4( appendResult2710_g73696 , 0.0 ) ).xyz * _PerspectivePushValue ) + ( (appendResult2667_g73696*2.0 + -1.0) * _PerspectiveNoiseValue ) ) * Mesh_Height1524_g73696 * pow( saferPower2652_g73696 , _PerspectiveAngleValue ) );
				float3 lerpResult16_g74000 = lerp( VertexPosition3588_g73696 , ( ( staticSwitch4976_g73696 * Vertex_Size1741_g73696 * Vertex_SizeFade1740_g73696 ) + Mesh_PivotsOS2291_g73696 + Grass_Perspective2661_g73696 ) , TVE_Enabled);
				float3 Final_VertexPosition890_g73696 = ( lerpResult16_g74000 + _DisableSRPBatcher );
				
				o.ase_texcoord3.xyz = vertexToFrag3890_g73696;
				o.ase_texcoord4.xyz = vertexToFrag4224_g73696;
				
				o.ase_texcoord2 = v.ase_texcoord;
				o.ase_color = v.ase_color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.w = 0;
				o.ase_texcoord4.w = 0;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif

				float3 vertexValue = Final_VertexPosition890_g73696;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;
				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				float4 positionCS = TransformWorldToHClip( positionWS );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					o.worldPos = positionWS;
				#endif

				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = positionCS;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif

				o.clipPos = positionCS;

				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord3 = v.ase_texcoord3;
				o.ase_color = v.ase_color;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
				return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord3 = patch[0].ase_texcoord3 * bary.x + patch[1].ase_texcoord3 * bary.y + patch[2].ase_texcoord3 * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			#if defined(ASE_EARLY_Z_DEPTH_OPTIMIZE)
				#define ASE_SV_DEPTH SV_DepthLessEqual  
			#else
				#define ASE_SV_DEPTH SV_Depth
			#endif

			half4 frag(	VertexOutput IN 
						#ifdef ASE_DEPTH_WRITE_ON
						,out float outputDepth : ASE_SV_DEPTH
						#endif
						 ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif

				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float localCustomAlphaClip19_g74020 = ( 0.0 );
				half2 Main_UVs15_g73696 = ( ( IN.ase_texcoord2.xy * (_MainUVs).xy ) + (_MainUVs).zw );
				float4 tex2DNode29_g73696 = tex2D( _MainAlbedoTex, Main_UVs15_g73696 );
				float Main_Alpha316_g73696 = ( _MainColor.a * tex2DNode29_g73696.a );
				float4 temp_output_93_19_g73986 = TVE_ExtrasCoords;
				float3 vertexToFrag3890_g73696 = IN.ase_texcoord3.xyz;
				float3 WorldPosition3905_g73696 = vertexToFrag3890_g73696;
				float3 vertexToFrag4224_g73696 = IN.ase_texcoord4.xyz;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				float3 lerpResult4827_g73696 = lerp( WorldPosition3905_g73696 , ObjectPosition4223_g73696 , _ExtrasPositionMode);
				half2 UV96_g73986 = ( (temp_output_93_19_g73986).zw + ( (temp_output_93_19_g73986).xy * (lerpResult4827_g73696).xz ) );
				float temp_output_84_0_g73986 = _LayerExtrasValue;
				float4 lerpResult109_g73986 = lerp( TVE_ExtrasParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_ExtrasTex, sampler_linear_clamp, UV96_g73986,temp_output_84_0_g73986, 0.0 ) , TVE_ExtrasUsage[(int)temp_output_84_0_g73986]);
				float4 break89_g73986 = lerpResult109_g73986;
				half Global_Extras_Alpha1033_g73696 = saturate( break89_g73986.a );
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = IN.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				float lerpResult5154_g73696 = lerp( 0.0 , Global_MeshVariation5104_g73696 , _AlphaVariationValue);
				half Global_Alpha_Variation5158_g73696 = lerpResult5154_g73696;
				half AlphaTreshold2132_g73696 = _AlphaClipValue;
				half Global_Alpha_Mask4546_g73696 = 1.0;
				float lerpResult5203_g73696 = lerp( 1.0 , ( ( Global_Extras_Alpha1033_g73696 - Global_Alpha_Variation5158_g73696 ) + AlphaTreshold2132_g73696 + ( Global_Extras_Alpha1033_g73696 * 0.5 ) ) , ( Global_Alpha_Mask4546_g73696 * _GlobalAlpha ));
				float lerpResult16_g74016 = lerp( 1.0 , lerpResult5203_g73696 , TVE_Enabled);
				half Global_Alpha315_g73696 = lerpResult16_g74016;
				half Fade_Alpha3727_g73696 = 1.0;
				float temp_output_661_0_g73696 = ( Main_Alpha316_g73696 * Global_Alpha315_g73696 * Fade_Alpha3727_g73696 );
				half Alpha34_g74021 = temp_output_661_0_g73696;
				half Offest27_g74021 = AlphaTreshold2132_g73696;
				half AlphaFeather5305_g73696 = _AlphaFeatherValue;
				half Feather30_g74021 = AlphaFeather5305_g73696;
				float temp_output_25_0_g74021 = ( ( ( Alpha34_g74021 - Offest27_g74021 ) / ( max( fwidth( Alpha34_g74021 ) , 0.001 ) + Feather30_g74021 ) ) + Offest27_g74021 );
				float temp_output_3_0_g74020 = temp_output_25_0_g74021;
				float Alpha19_g74020 = temp_output_3_0_g74020;
				float temp_output_15_0_g74020 = AlphaTreshold2132_g73696;
				float Treshold19_g74020 = temp_output_15_0_g74020;
				{
				#if defined (TVE_FEATURE_CLIP)
				#if defined (TVE_IS_HD_PIPELINE)
				#if !defined (SHADERPASS_FORWARD_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#if !defined (SHADERPASS_GBUFFER_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#else
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#endif
				}
				half Final_Alpha914_g73696 = saturate( Alpha19_g74020 );
				

				float Alpha = Final_Alpha914_g73696;
				float AlphaClipThreshold = 0.5;
				#ifdef ASE_DEPTH_WRITE_ON
					float DepthValue = 0;
				#endif

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif

				#ifdef ASE_DEPTH_WRITE_ON
					outputDepth = DepthValue;
				#endif

				return 0;
			}
			ENDHLSL
		}
	//-------------------------------------------------------------------------------------
	// END_PASS DEPTHONLY
	//-------------------------------------------------------------------------------------

		
	//-------------------------------------------------------------------------------------
	// BEGIN_PASS META
	//-------------------------------------------------------------------------------------
		Pass
		{
			
			Name "Meta"
			Tags { "LightMode"="Meta" }

			Cull Off

	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			HLSLPROGRAM
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

#include "./../../../../../GPUInstancer/Shaders/Include/GPUInstancerInclude.cginc"
#pragma instancing_options procedural:setupGPUI
#pragma multi_compile_instancing


			#define _SPECULAR_SETUP 1
			#define _NORMAL_DROPOFF_TS 1
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#pragma multi_compile _ DOTS_INSTANCING_ON
			#define ASE_ABSOLUTE_VERTEX_POS 1
			#define _TRANSLUCENCY_ASE 1
			#define _EMISSION
			#define _NORMALMAP 1
			#define ASE_SRP_VERSION 120100


			#pragma vertex vert
			#pragma fragment frag

			#pragma shader_feature EDITOR_VISUALIZATION

			#define SHADERPASS SHADERPASS_META

			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

			//-----------------------------------------------------------------------------
			// DEFINES - (Set by User)
			//-----------------------------------------------------------------------------

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_COLOR
			#pragma shader_feature_local TVE_FEATURE_CLIP
			#pragma shader_feature_local TVE_FEATURE_BATCHING
			//TVE Injection Defines
			//SHADER INJECTION POINT BEGIN
			//SHADER INJECTION POINT END
			//TVE Pipeline Defines
			#define THE_VEGETATION_ENGINE
			#define TVE_IS_UNIVERSAL_PIPELINE
			//TVE Shader Type Defines
			#define TVE_IS_GRASS_SHADER


	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 texcoord0 : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				float4 texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					float4 shadowCoord : TEXCOORD1;
				#endif
				#ifdef EDITOR_VISUALIZATION
					float4 VizUV : TEXCOORD2;
					float4 LightCoord : TEXCOORD3;
				#endif
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				float4 ase_texcoord6 : TEXCOORD6;
				float4 ase_texcoord7 : TEXCOORD7;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

	//-------------------------------------------------------------------------------------
	// BEGIN_CBUFFER
	//-------------------------------------------------------------------------------------

			CBUFFER_START(UnityPerMaterial)
			float4 _SubsurfaceDiffusion_asset;
			float4 _NoiseMaskRemap;
			float4 _SubsurfaceDiffusion_Asset;
			half4 _MotionHighlightColor;
			half4 _MainColor;
			half4 _MainUVs;
			half4 _VertexOcclusionColor;
			half4 _OverlayMaskRemap;
			half4 _AlphaMaskRemap;
			half4 _DetailBlendRemap;
			float4 _Color;
			half4 _GradientColorOne;
			half4 _SubsurfaceMaskRemap;
			float4 _GradientMaskRemap;
			half4 _ColorsMaskRemap;
			half4 _GradientColorTwo;
			float4 _EmissiveIntensityParams;
			half4 _VertexOcclusionRemap;
			half4 _SubsurfaceColor;
			half4 _EmissiveUVs;
			float4 _MaxBoundsInfo;
			half4 _EmissiveColor;
			half3 _render_normals;
			half _PerspectiveAngleValue;
			half _LayerMotionValue;
			half _MotionAmplitude_10;
			float _MotionScale_10;
			float _MotionSpeed_10;
			half _MotionVariation_10;
			half _PerspectiveNoiseValue;
			half _PerspectivePushValue;
			half _VertexDynamicMode;
			half _SizeFadeEndValue;
			half _SizeFadeStartValue;
			half _GlobalSize;
			half _LayerVertexValue;
			half _VertexDataMode;
			half _MotionNormalValue;
			half _MotionValue_30;
			half _InteractionAmplitude;
			half _InteractionMaskValue;
			float _MotionSpeed_32;
			float _MotionVariation_32;
			float _MotionScale_32;
			half _SizeFadeMode;
			half _MotionAmplitude_32;
			half _IsSubsurfaceShader;
			half _GradientMaxValue;
			half _SubsurfaceValue;
			half _GlobalAlpha;
			half _AlphaVariationValue;
			half _MainOcclusionValue;
			half _GlobalWetness;
			half _MainSmoothnessValue;
			half _RenderSpecular;
			half _GlobalEmissive;
			half _MainNormalValue;
			half _OverlayMaskMaxValue;
			half _OverlayMaskMinValue;
			half _GradientMinValue;
			half _OverlayVariationValue;
			half _ExtrasPositionMode;
			half _GlobalOverlay;
			half _ColorsMaskMaxValue;
			half _ColorsMaskMinValue;
			half _ColorsVariationValue;
			half _GlobalColors;
			half _LayerColorsValue;
			half _ColorsPositionMode;
			half _VertexOcclusionMaxValue;
			half _VertexOcclusionMinValue;
			half _VertexPivotMode;
			half _LayerExtrasValue;
			half _render_coverage;
			half _RenderMode;
			half _render_dst;
			half _SubsurfaceAngleValue;
			half _HasGradient;
			half _IsTVEShader;
			half _SpaceGlobalPosition;
			half _VertexVariationMode;
			half _AlphaFeatherValue;
			half _MessageGlobalsVariation;
			half _SubsurfaceShadowValue;
			half _RenderZWrite;
			half _RenderDecals;
			half _RenderDirect;
			half _CategoryNoise;
			half _SubsurfaceScatteringValue;
			half _RenderClip;
			float _SubsurfaceDiffusion;
			half _EmissiveFlagMode;
			half _RenderSSR;
			half _CategoryGlobal;
			half _SubsurfaceNormalValue;
			half _MessageMotionVariation;
			half _SubsurfaceAmbientValue;
			half _RenderCull;
			half _HasOcclusion;
			half _Cutoff;
			half _SubsurfaceDirectValue;
			half _CategoryRender;
			half _RenderQueue;
			half _DetailBlendMode;
			half _CategoryPerspective;
			half _render_cull;
			half _RenderAmbient;
			half _CategoryDetail;
			half _render_src;
			half _render_zw;
			half _IsGrassShader;
			half _RenderPriority;
			half _SubsurfaceMaskMinValue;
			half _CategoryGradient;
			half _LayerReactValue;
			half _RenderNormals;
			half _SpaceGlobalLayers;
			half _AlphaClipValue;
			half _VertexRollingMode;
			half _IsVersion;
			half _RenderCoverage;
			half _MessageSizeFade;
			half _CategoryEmissive;
			half _CategoryOcclusion;
			half _CategoryMain;
			half _CategorySizeFade;
			half _CategoryMotion;
			half _DetailMode;
			half _DetailTypeMode;
			half _HasEmissive;
			half _SpaceGlobalLocals;
			half _VertexMasksMode;
			half _RenderShadow;
			half _SpaceMotionGlobals;
			half _CategorySubsurface;
			half _SpaceRenderFade;
			half _SpaceMotionLocals;
			half _SubsurfaceMaskMaxValue;
			#ifdef _TRANSMISSION_ASE
				float _TransmissionShadow;
			#endif
			#ifdef _TRANSLUCENCY_ASE
				float _TransStrength;
				float _TransNormal;
				float _TransScattering;
				float _TransDirect;
				float _TransAmbient;
				float _TransShadow;
			#endif
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END

			// Property used by ScenePickingPass
			#ifdef SCENEPICKINGPASS
				float4 _SelectionID;
			#endif

			// Properties used by SceneSelectionPass
			#ifdef SCENESELECTIONPASS
				int _ObjectId;
				int _PassValue;
			#endif

	//-------------------------------------------------------------------------------------
	// END_CBUFFER
	//-------------------------------------------------------------------------------------

			sampler2D _MainTex;
			half TVE_Enabled;
			sampler2D _BumpMap;
			half4 TVE_MotionParams;
			TEXTURE2D_ARRAY(TVE_MotionTex);
			half4 TVE_MotionCoords;
			SAMPLER(sampler_linear_clamp);
			float TVE_MotionUsage[10];
			sampler2D TVE_NoiseTex;
			half4 TVE_NoiseParams;
			half4 TVE_FlutterParams;
			half TVE_MotionFadeEnd;
			half TVE_MotionFadeStart;
			half4 TVE_VertexParams;
			TEXTURE2D_ARRAY(TVE_VertexTex);
			half4 TVE_VertexCoords;
			float TVE_VertexUsage[10];
			half TVE_DistanceFadeBias;
			half _DisableSRPBatcher;
			sampler2D _MainAlbedoTex;
			half4 TVE_ColorsParams;
			TEXTURE2D_ARRAY(TVE_ColorsTex);
			half4 TVE_ColorsCoords;
			float TVE_ColorsUsage[10];
			half4 TVE_OverlayColor;
			half4 TVE_ExtrasParams;
			TEXTURE2D_ARRAY(TVE_ExtrasTex);
			half4 TVE_ExtrasCoords;
			float TVE_ExtrasUsage[10];
			sampler2D _EmissiveTex;


	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/LightingMetaPass.hlsl"

			//#ifdef HAVE_VFX_MODIFICATION
			//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/VisualEffectVertex.hlsl"
			//#endif

	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			float2 DecodeFloatToVector2( float enc )
			{
				float2 result ;
				result.y = enc % 2048;
				result.x = floor(enc / 2048);
				return result / (2048 - 1);
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 VertexPosition3588_g73696 = v.vertex.xyz;
				float3 appendResult60_g74015 = (float3(v.ase_texcoord3.x , v.ase_texcoord3.z , v.ase_texcoord3.y));
				half3 Mesh_PivotsData2831_g73696 = ( appendResult60_g74015 * _VertexPivotMode );
				half3 Mesh_PivotsOS2291_g73696 = Mesh_PivotsData2831_g73696;
				float3 temp_output_2283_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				half3 VertexPos40_g73968 = temp_output_2283_0_g73696;
				float3 appendResult74_g73968 = (float3(VertexPos40_g73968.x , 0.0 , 0.0));
				half3 VertexPosRotationAxis50_g73968 = appendResult74_g73968;
				float3 break84_g73968 = VertexPos40_g73968;
				float3 appendResult81_g73968 = (float3(0.0 , break84_g73968.y , break84_g73968.z));
				half3 VertexPosOtherAxis82_g73968 = appendResult81_g73968;
				float4 temp_output_91_19_g74003 = TVE_MotionCoords;
				float4x4 break19_g73996 = GetObjectToWorldMatrix();
				float3 appendResult20_g73996 = (float3(break19_g73996[ 0 ][ 3 ] , break19_g73996[ 1 ][ 3 ] , break19_g73996[ 2 ][ 3 ]));
				float3 temp_output_122_0_g73996 = Mesh_PivotsData2831_g73696;
				float3 PivotsOnly105_g73996 = (mul( GetObjectToWorldMatrix(), float4( temp_output_122_0_g73996 , 0.0 ) ).xyz).xyz;
				half3 ObjectData20_g73997 = ( appendResult20_g73996 + PivotsOnly105_g73996 );
				float3 ase_worldPos = mul(GetObjectToWorldMatrix(), v.vertex).xyz;
				half3 WorldData19_g73997 = ase_worldPos;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73997 = WorldData19_g73997;
				#else
				float3 staticSwitch14_g73997 = ObjectData20_g73997;
				#endif
				float3 temp_output_114_0_g73996 = staticSwitch14_g73997;
				float3 vertexToFrag4224_g73696 = temp_output_114_0_g73996;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				float3 vertexToFrag3890_g73696 = ase_worldPos;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				half2 UV94_g74003 = ( (temp_output_91_19_g74003).zw + ( (temp_output_91_19_g74003).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g74003 = _LayerMotionValue;
				float4 lerpResult107_g74003 = lerp( TVE_MotionParams , saturate( SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_MotionTex, sampler_linear_clamp, UV94_g74003,temp_output_84_0_g74003, 0.0 ) ) , TVE_MotionUsage[(int)temp_output_84_0_g74003]);
				half4 Global_Motion_Params3909_g73696 = lerpResult107_g74003;
				float4 break322_g73985 = Global_Motion_Params3909_g73696;
				float3 appendResult397_g73985 = (float3(break322_g73985.x , 0.0 , break322_g73985.y));
				float3 temp_output_398_0_g73985 = (appendResult397_g73985*2.0 + -1.0);
				float3 ase_parentObjectScale = ( 1.0 / float3( length( GetWorldToObjectMatrix()[ 0 ].xyz ), length( GetWorldToObjectMatrix()[ 1 ].xyz ), length( GetWorldToObjectMatrix()[ 2 ].xyz ) ) );
				half2 Global_MotionDirectionOS39_g73696 = (( mul( GetWorldToObjectMatrix(), float4( temp_output_398_0_g73985 , 0.0 ) ).xyz * ase_parentObjectScale )).xz;
				half2 Input_DirectionOS358_g73990 = Global_MotionDirectionOS39_g73696;
				half Wind_Power369_g73985 = break322_g73985.z;
				half Global_WindPower2223_g73696 = Wind_Power369_g73985;
				half3 Input_Position419_g73963 = ObjectPosition4223_g73696;
				float Input_MotionScale287_g73963 = ( _MotionScale_10 + 1.0 );
				half Global_Scale448_g73963 = TVE_NoiseParams.x;
				float2 temp_output_597_0_g73963 = (( Input_Position419_g73963 * Input_MotionScale287_g73963 * Global_Scale448_g73963 * 0.0075 )).xz;
				half2 Global_MotionDirectionWS4683_g73696 = (temp_output_398_0_g73985).xz;
				half2 Input_DirectionWS423_g73963 = Global_MotionDirectionWS4683_g73696;
				half Input_MotionSpeed62_g73963 = _MotionSpeed_10;
				half Global_Speed449_g73963 = TVE_NoiseParams.y;
				half Input_MotionVariation284_g73963 = _MotionVariation_10;
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = v.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				half Input_GlobalVariation569_g73963 = Global_MeshVariation5104_g73696;
				float temp_output_630_0_g73963 = ( ( ( _TimeParameters.x * Input_MotionSpeed62_g73963 * Global_Speed449_g73963 ) + ( Input_MotionVariation284_g73963 * Input_GlobalVariation569_g73963 ) ) * 0.03 );
				float temp_output_607_0_g73963 = frac( temp_output_630_0_g73963 );
				float4 lerpResult590_g73963 = lerp( tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * temp_output_607_0_g73963 ) ), 0, 0.0) ) , tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * frac( ( temp_output_630_0_g73963 + 0.5 ) ) ) ), 0, 0.0) ) , ( abs( ( temp_output_607_0_g73963 - 0.5 ) ) / 0.5 ));
				half Input_GlobalWind327_g73963 = Global_WindPower2223_g73696;
				float lerpResult612_g73963 = lerp( 1.4 , 0.4 , Input_GlobalWind327_g73963);
				float3 temp_cast_7 = (lerpResult612_g73963).xxx;
				float3 break638_g73963 = (pow( ( abs( (lerpResult590_g73963).rgb ) + 0.2 ) , temp_cast_7 )*1.4 + -0.2);
				half Global_NoiseTexR34_g73696 = break638_g73963.x;
				half Motion_10_Amplitude2258_g73696 = ( _MotionAmplitude_10 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 );
				half Input_BendingAmplitude376_g73990 = Motion_10_Amplitude2258_g73696;
				half Mesh_Height1524_g73696 = v.ase_color.a;
				half Input_MeshHeight388_g73990 = Mesh_Height1524_g73696;
				half ObjectData20_g73992 = ( Input_MeshHeight388_g73990 * 2.0 );
				float enc62_g73978 = v.texcoord0.w;
				float2 localDecodeFloatToVector262_g73978 = DecodeFloatToVector2( enc62_g73978 );
				float2 break63_g73978 = ( localDecodeFloatToVector262_g73978 * 100.0 );
				float Bounds_Height5230_g73696 = break63_g73978.x;
				half Input_BoundsHeight390_g73990 = Bounds_Height5230_g73696;
				half WorldData19_g73992 = ( ( Input_MeshHeight388_g73990 * Input_MeshHeight388_g73990 ) * Input_BoundsHeight390_g73990 * 2.0 );
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73992 = WorldData19_g73992;
				#else
				float staticSwitch14_g73992 = ObjectData20_g73992;
				#endif
				half Mask_Motion_10321_g73990 = staticSwitch14_g73992;
				half Input_InteractionAmplitude58_g73990 = _InteractionAmplitude;
				half Input_InteractionUseMask62_g73990 = _InteractionMaskValue;
				float lerpResult371_g73990 = lerp( 2.0 , Mask_Motion_10321_g73990 , Input_InteractionUseMask62_g73990);
				half ObjectData20_g73991 = lerpResult371_g73990;
				half WorldData19_g73991 = Mask_Motion_10321_g73990;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73991 = WorldData19_g73991;
				#else
				float staticSwitch14_g73991 = ObjectData20_g73991;
				#endif
				half Mask_Interaction373_g73990 = ( Input_InteractionAmplitude58_g73990 * staticSwitch14_g73991 );
				half Global_InteractionMask66_g73696 = ( break322_g73985.w * break322_g73985.w );
				float Input_InteractionGlobalMask330_g73990 = Global_InteractionMask66_g73696;
				float lerpResult360_g73990 = lerp( ( Input_BendingAmplitude376_g73990 * Mask_Motion_10321_g73990 ) , Mask_Interaction373_g73990 , saturate( ( Input_InteractionAmplitude58_g73990 * Input_InteractionGlobalMask330_g73990 ) ));
				float2 break364_g73990 = ( Input_DirectionOS358_g73990 * lerpResult360_g73990 );
				half Motion_10_BendingZ190_g73696 = break364_g73990.y;
				half Angle44_g73968 = Motion_10_BendingZ190_g73696;
				half3 VertexPos40_g73948 = ( VertexPosRotationAxis50_g73968 + ( VertexPosOtherAxis82_g73968 * cos( Angle44_g73968 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g73968 ) * sin( Angle44_g73968 ) ) );
				float3 appendResult74_g73948 = (float3(0.0 , 0.0 , VertexPos40_g73948.z));
				half3 VertexPosRotationAxis50_g73948 = appendResult74_g73948;
				float3 break84_g73948 = VertexPos40_g73948;
				float3 appendResult81_g73948 = (float3(break84_g73948.x , break84_g73948.y , 0.0));
				half3 VertexPosOtherAxis82_g73948 = appendResult81_g73948;
				half Motion_10_BendingX216_g73696 = break364_g73990.x;
				half Angle44_g73948 = -Motion_10_BendingX216_g73696;
				half Input_MotionScale321_g73940 = _MotionScale_32;
				half Input_MotionVariation330_g73940 = _MotionVariation_32;
				half Input_GlobalVariation372_g73940 = Global_MeshVariation5104_g73696;
				half Input_MotionSpeed62_g73940 = _MotionSpeed_32;
				half Global_Speed350_g73940 = TVE_FlutterParams.y;
				half Motion_FacingMask5214_g73696 = 1.0;
				float temp_output_7_0_g73984 = TVE_MotionFadeEnd;
				half Motion_FadeOut4005_g73696 = saturate( ( ( distance( ase_worldPos , _WorldSpaceCameraPos ) - temp_output_7_0_g73984 ) / ( TVE_MotionFadeStart - temp_output_7_0_g73984 ) ) );
				half Motion_30_Amplitude4960_g73696 = ( _MotionAmplitude_32 * _MotionValue_30 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 * Motion_FacingMask5214_g73696 * Motion_FadeOut4005_g73696 );
				half Input_MotionAmplitude58_g73940 = Motion_30_Amplitude4960_g73696;
				half Global_Power354_g73940 = TVE_FlutterParams.x;
				float enc59_g73978 = v.texcoord0.z;
				float2 localDecodeFloatToVector259_g73978 = DecodeFloatToVector2( enc59_g73978 );
				float2 break61_g73978 = localDecodeFloatToVector259_g73978;
				half Mesh_Motion_30144_g73696 = break61_g73978.y;
				half Input_MeshMotion_30374_g73940 = Mesh_Motion_30144_g73696;
				half Input_MotionNormal364_g73940 = _MotionNormalValue;
				float3 lerpResult370_g73940 = lerp( float3( 1,1,1 ) , v.ase_normal , Input_MotionNormal364_g73940);
				half3 Motion_30_Details263_g73696 = ( ( sin( ( ( ( ase_worldPos.x + ase_worldPos.y + ase_worldPos.z ) * Input_MotionScale321_g73940 ) + ( Input_MotionVariation330_g73940 * Input_GlobalVariation372_g73940 ) + ( _TimeParameters.x * Input_MotionSpeed62_g73940 * Global_Speed350_g73940 ) ) ) * Input_MotionAmplitude58_g73940 * Global_Power354_g73940 * Input_MeshMotion_30374_g73940 * 0.4 ) * lerpResult370_g73940 );
				float3 Vertex_Motion_Object833_g73696 = ( ( VertexPosRotationAxis50_g73948 + ( VertexPosOtherAxis82_g73948 * cos( Angle44_g73948 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g73948 ) * sin( Angle44_g73948 ) ) ) + Motion_30_Details263_g73696 );
				float3 temp_output_3474_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				float3 appendResult2043_g73696 = (float3(Motion_10_BendingX216_g73696 , 0.0 , Motion_10_BendingZ190_g73696));
				float3 Vertex_Motion_World1118_g73696 = ( ( temp_output_3474_0_g73696 + appendResult2043_g73696 ) + Motion_30_Details263_g73696 );
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch4976_g73696 = Vertex_Motion_World1118_g73696;
				#else
				float3 staticSwitch4976_g73696 = ( Vertex_Motion_Object833_g73696 + ( _VertexDataMode * 0.0 ) );
				#endif
				float4 temp_output_94_19_g73959 = TVE_VertexCoords;
				half2 UV97_g73959 = ( (temp_output_94_19_g73959).zw + ( (temp_output_94_19_g73959).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g73959 = _LayerVertexValue;
				float4 lerpResult109_g73959 = lerp( TVE_VertexParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_VertexTex, sampler_linear_clamp, UV97_g73959,temp_output_84_0_g73959, 0.0 ) , TVE_VertexUsage[(int)temp_output_84_0_g73959]);
				half4 Global_Object_Params4173_g73696 = lerpResult109_g73959;
				half Global_VertexSize174_g73696 = saturate( Global_Object_Params4173_g73696.w );
				float lerpResult346_g73696 = lerp( 1.0 , Global_VertexSize174_g73696 , _GlobalSize);
				float3 appendResult3480_g73696 = (float3(lerpResult346_g73696 , lerpResult346_g73696 , lerpResult346_g73696));
				half3 ObjectData20_g73993 = appendResult3480_g73696;
				half3 _Vector11 = half3(1,1,1);
				half3 WorldData19_g73993 = _Vector11;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73993 = WorldData19_g73993;
				#else
				float3 staticSwitch14_g73993 = ObjectData20_g73993;
				#endif
				half3 Vertex_Size1741_g73696 = staticSwitch14_g73993;
				half3 _Vector5 = half3(1,1,1);
				float temp_output_7_0_g73931 = _SizeFadeEndValue;
				float temp_output_335_0_g73696 = saturate( ( ( ( distance( _WorldSpaceCameraPos , ObjectPosition4223_g73696 ) * ( 1.0 / TVE_DistanceFadeBias ) ) - temp_output_7_0_g73931 ) / ( _SizeFadeStartValue - temp_output_7_0_g73931 ) ) );
				float3 appendResult3482_g73696 = (float3(temp_output_335_0_g73696 , temp_output_335_0_g73696 , temp_output_335_0_g73696));
				float3 lerpResult5065_g73696 = lerp( _Vector5 , appendResult3482_g73696 , _SizeFadeMode);
				half3 ObjectData20_g73958 = lerpResult5065_g73696;
				half3 WorldData19_g73958 = _Vector5;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73958 = WorldData19_g73958;
				#else
				float3 staticSwitch14_g73958 = ObjectData20_g73958;
				#endif
				float3 Vertex_SizeFade1740_g73696 = staticSwitch14_g73958;
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - ase_worldPos );
				ase_worldViewDir = normalize(ase_worldViewDir);
				float3 normalizeResult2696_g73696 = normalize( ase_worldViewDir );
				float3 break2709_g73696 = cross( normalizeResult2696_g73696 , half3(0,1,0) );
				float3 appendResult2710_g73696 = (float3(-break2709_g73696.z , 0.0 , break2709_g73696.x));
				float3 appendResult2667_g73696 = (float3(Global_MeshVariation5104_g73696 , 0.5 , Global_MeshVariation5104_g73696));
				float3 normalizeResult2169_g73696 = normalize( ase_worldViewDir );
				float3 ViewDir_Normalized3963_g73696 = normalizeResult2169_g73696;
				float dotResult2212_g73696 = dot( ViewDir_Normalized3963_g73696 , float3(0,1,0) );
				half Mask_HView2656_g73696 = dotResult2212_g73696;
				float saferPower2652_g73696 = abs( Mask_HView2656_g73696 );
				half3 Grass_Perspective2661_g73696 = ( ( ( mul( GetWorldToObjectMatrix(), float4( appendResult2710_g73696 , 0.0 ) ).xyz * _PerspectivePushValue ) + ( (appendResult2667_g73696*2.0 + -1.0) * _PerspectiveNoiseValue ) ) * Mesh_Height1524_g73696 * pow( saferPower2652_g73696 , _PerspectiveAngleValue ) );
				float3 lerpResult16_g74000 = lerp( VertexPosition3588_g73696 , ( ( staticSwitch4976_g73696 * Vertex_Size1741_g73696 * Vertex_SizeFade1740_g73696 ) + Mesh_PivotsOS2291_g73696 + Grass_Perspective2661_g73696 ) , TVE_Enabled);
				float3 Final_VertexPosition890_g73696 = ( lerpResult16_g74000 + _DisableSRPBatcher );
				
				float temp_output_7_0_g73976 = _GradientMinValue;
				half Gradient_Tint2784_g73696 = saturate( ( ( Mesh_Height1524_g73696 - temp_output_7_0_g73976 ) / ( _GradientMaxValue - temp_output_7_0_g73976 ) ) );
				float vertexToFrag11_g73981 = Gradient_Tint2784_g73696;
				o.ase_texcoord4.x = vertexToFrag11_g73981;
				half Global_NoiseTexA4860_g73696 = (lerpResult590_g73963).a;
				half Highlight_Tint3231_g73696 = ( Global_NoiseTexA4860_g73696 * Global_WindPower2223_g73696 * Motion_FadeOut4005_g73696 * Mesh_Height1524_g73696 );
				float vertexToFrag11_g73943 = Highlight_Tint3231_g73696;
				o.ase_texcoord4.y = vertexToFrag11_g73943;
				float Mesh_Occlusion318_g73696 = v.ase_color.g;
				float temp_output_7_0_g73971 = _VertexOcclusionMinValue;
				float temp_output_3377_0_g73696 = saturate( ( ( Mesh_Occlusion318_g73696 - temp_output_7_0_g73971 ) / ( _VertexOcclusionMaxValue - temp_output_7_0_g73971 ) ) );
				float vertexToFrag11_g73969 = temp_output_3377_0_g73696;
				o.ase_texcoord4.z = vertexToFrag11_g73969;
				o.ase_texcoord6.xyz = vertexToFrag3890_g73696;
				o.ase_texcoord7.xyz = vertexToFrag4224_g73696;
				
				o.ase_texcoord5 = v.texcoord0;
				o.ase_color = v.ase_color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord4.w = 0;
				o.ase_texcoord6.w = 0;
				o.ase_texcoord7.w = 0;
				
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif

				float3 vertexValue = Final_VertexPosition890_g73696;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					o.worldPos = positionWS;
				#endif

				o.clipPos = MetaVertexPosition( v.vertex, v.texcoord1.xy, v.texcoord1.xy, unity_LightmapST, unity_DynamicLightmapST );

				#ifdef EDITOR_VISUALIZATION
					float2 VizUV = 0;
					float4 LightCoord = 0;
					UnityEditorVizData(v.vertex.xyz, v.texcoord0.xy, v.texcoord1.xy, v.texcoord2.xy, VizUV, LightCoord);
					o.VizUV = float4(VizUV, 0, 0);
					o.LightCoord = LightCoord;
				#endif

				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = o.clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif

				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 texcoord0 : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				float4 texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.texcoord0 = v.texcoord0;
				o.texcoord1 = v.texcoord1;
				o.texcoord2 = v.texcoord2;
				o.ase_texcoord3 = v.ase_texcoord3;
				o.ase_color = v.ase_color;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
				return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.texcoord0 = patch[0].texcoord0 * bary.x + patch[1].texcoord0 * bary.y + patch[2].texcoord0 * bary.z;
				o.texcoord1 = patch[0].texcoord1 * bary.x + patch[1].texcoord1 * bary.y + patch[2].texcoord1 * bary.z;
				o.texcoord2 = patch[0].texcoord2 * bary.x + patch[1].texcoord2 * bary.y + patch[2].texcoord2 * bary.z;
				o.ase_texcoord3 = patch[0].ase_texcoord3 * bary.x + patch[1].ase_texcoord3 * bary.y + patch[2].ase_texcoord3 * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					float3 WorldPosition = IN.worldPos;
				#endif

				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float vertexToFrag11_g73981 = IN.ase_texcoord4.x;
				float3 lerpResult2779_g73696 = lerp( (_GradientColorTwo).rgb , (_GradientColorOne).rgb , vertexToFrag11_g73981);
				float vertexToFrag11_g73943 = IN.ase_texcoord4.y;
				half2 Main_UVs15_g73696 = ( ( IN.ase_texcoord5.xy * (_MainUVs).xy ) + (_MainUVs).zw );
				float4 tex2DNode29_g73696 = tex2D( _MainAlbedoTex, Main_UVs15_g73696 );
				half3 Main_Albedo99_g73696 = ( (_MainColor).rgb * (tex2DNode29_g73696).rgb );
				half3 Blend_Albedo265_g73696 = Main_Albedo99_g73696;
				float3 temp_cast_0 = (1.0).xxx;
				float vertexToFrag11_g73969 = IN.ase_texcoord4.z;
				float3 lerpResult2945_g73696 = lerp( (_VertexOcclusionColor).rgb , temp_cast_0 , vertexToFrag11_g73969);
				float3 Vertex_Occlusion648_g73696 = lerpResult2945_g73696;
				half3 Blend_AlbedoTinted2808_g73696 = ( ( lerpResult2779_g73696 * float3(1,1,1) * ( ( (_MotionHighlightColor).rgb * vertexToFrag11_g73943 ) + float3( 1,1,1 ) ) ) * Blend_Albedo265_g73696 * Vertex_Occlusion648_g73696 );
				float dotResult3616_g73696 = dot( Blend_AlbedoTinted2808_g73696 , float3(0.2126,0.7152,0.0722) );
				float3 temp_cast_1 = (dotResult3616_g73696).xxx;
				float4 temp_output_91_19_g73949 = TVE_ColorsCoords;
				float3 vertexToFrag3890_g73696 = IN.ase_texcoord6.xyz;
				float3 WorldPosition3905_g73696 = vertexToFrag3890_g73696;
				float3 vertexToFrag4224_g73696 = IN.ase_texcoord7.xyz;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				float3 lerpResult4822_g73696 = lerp( WorldPosition3905_g73696 , ObjectPosition4223_g73696 , _ColorsPositionMode);
				half2 UV94_g73949 = ( (temp_output_91_19_g73949).zw + ( (temp_output_91_19_g73949).xy * (lerpResult4822_g73696).xz ) );
				float temp_output_82_0_g73949 = _LayerColorsValue;
				float4 lerpResult108_g73949 = lerp( TVE_ColorsParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_ColorsTex, sampler_linear_clamp, UV94_g73949,temp_output_82_0_g73949, 0.0 ) , TVE_ColorsUsage[(int)temp_output_82_0_g73949]);
				half Global_ColorsTex_A1701_g73696 = saturate( (lerpResult108_g73949).a );
				half Global_Colors_Influence3668_g73696 = Global_ColorsTex_A1701_g73696;
				float3 lerpResult3618_g73696 = lerp( Blend_AlbedoTinted2808_g73696 , temp_cast_1 , Global_Colors_Influence3668_g73696);
				half3 Global_ColorsTex_RGB1700_g73696 = (lerpResult108_g73949).rgb;
				#ifdef UNITY_COLORSPACE_GAMMA
				float staticSwitch1_g73970 = 2.0;
				#else
				float staticSwitch1_g73970 = 4.594794;
				#endif
				half3 Global_Colors1954_g73696 = ( Global_ColorsTex_RGB1700_g73696 * staticSwitch1_g73970 );
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = IN.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				float lerpResult3870_g73696 = lerp( 1.0 , ( Global_MeshVariation5104_g73696 * Global_MeshVariation5104_g73696 ) , _ColorsVariationValue);
				half Global_Colors_Variation3650_g73696 = ( _GlobalColors * lerpResult3870_g73696 );
				half Mesh_Height1524_g73696 = IN.ase_color.a;
				float clampResult5405_g73696 = clamp( Mesh_Height1524_g73696 , 0.01 , 0.99 );
				float temp_output_7_0_g73945 = _ColorsMaskMinValue;
				half Global_Colors_Mask3692_g73696 = saturate( ( ( clampResult5405_g73696 - temp_output_7_0_g73945 ) / ( _ColorsMaskMaxValue - temp_output_7_0_g73945 ) ) );
				float lerpResult16_g73954 = lerp( 0.0 , ( Global_Colors_Variation3650_g73696 * Global_Colors_Mask3692_g73696 ) , TVE_Enabled);
				float3 lerpResult3628_g73696 = lerp( Blend_AlbedoTinted2808_g73696 , ( lerpResult3618_g73696 * Global_Colors1954_g73696 ) , lerpResult16_g73954);
				half3 Blend_AlbedoColored863_g73696 = lerpResult3628_g73696;
				half3 Blend_AlbedoAndSubsurface149_g73696 = Blend_AlbedoColored863_g73696;
				half3 Global_OverlayColor1758_g73696 = (TVE_OverlayColor).rgb;
				half Main_Albedo_G3526_g73696 = tex2DNode29_g73696.g;
				float4 temp_output_93_19_g73986 = TVE_ExtrasCoords;
				float3 lerpResult4827_g73696 = lerp( WorldPosition3905_g73696 , ObjectPosition4223_g73696 , _ExtrasPositionMode);
				half2 UV96_g73986 = ( (temp_output_93_19_g73986).zw + ( (temp_output_93_19_g73986).xy * (lerpResult4827_g73696).xz ) );
				float temp_output_84_0_g73986 = _LayerExtrasValue;
				float4 lerpResult109_g73986 = lerp( TVE_ExtrasParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_ExtrasTex, sampler_linear_clamp, UV96_g73986,temp_output_84_0_g73986, 0.0 ) , TVE_ExtrasUsage[(int)temp_output_84_0_g73986]);
				float4 break89_g73986 = lerpResult109_g73986;
				half Global_Extras_Overlay156_g73696 = break89_g73986.b;
				float lerpResult1065_g73696 = lerp( 1.0 , Global_MeshVariation5104_g73696 , _OverlayVariationValue);
				half Overlay_Variation4560_g73696 = lerpResult1065_g73696;
				half Overlay_Commons1365_g73696 = ( _GlobalOverlay * Global_Extras_Overlay156_g73696 * Overlay_Variation4560_g73696 );
				half Overlay_Mask_2D5121_g73696 = ( ( ( Mesh_Height1524_g73696 * 0.5 ) + Main_Albedo_G3526_g73696 ) * Overlay_Commons1365_g73696 );
				float temp_output_7_0_g73980 = _OverlayMaskMinValue;
				half Overlay_Mask269_g73696 = saturate( ( ( Overlay_Mask_2D5121_g73696 - temp_output_7_0_g73980 ) / ( _OverlayMaskMaxValue - temp_output_7_0_g73980 ) ) );
				float3 lerpResult336_g73696 = lerp( Blend_AlbedoAndSubsurface149_g73696 , Global_OverlayColor1758_g73696 , Overlay_Mask269_g73696);
				half3 Final_Albedo359_g73696 = lerpResult336_g73696;
				
				float4 temp_output_4214_0_g73696 = ( _EmissiveColor * _EmissiveIntensityParams.x );
				half2 Emissive_UVs2468_g73696 = ( ( IN.ase_texcoord5.xy * (_EmissiveUVs).xy ) + (_EmissiveUVs).zw );
				half Global_Extras_Emissive4203_g73696 = break89_g73986.r;
				float lerpResult4206_g73696 = lerp( 1.0 , Global_Extras_Emissive4203_g73696 , _GlobalEmissive);
				half3 Final_Emissive2476_g73696 = ( (( temp_output_4214_0_g73696 * tex2D( _EmissiveTex, Emissive_UVs2468_g73696 ) )).rgb * lerpResult4206_g73696 );
				
				float localCustomAlphaClip19_g74020 = ( 0.0 );
				float Main_Alpha316_g73696 = ( _MainColor.a * tex2DNode29_g73696.a );
				half Global_Extras_Alpha1033_g73696 = saturate( break89_g73986.a );
				float lerpResult5154_g73696 = lerp( 0.0 , Global_MeshVariation5104_g73696 , _AlphaVariationValue);
				half Global_Alpha_Variation5158_g73696 = lerpResult5154_g73696;
				half AlphaTreshold2132_g73696 = _AlphaClipValue;
				half Global_Alpha_Mask4546_g73696 = 1.0;
				float lerpResult5203_g73696 = lerp( 1.0 , ( ( Global_Extras_Alpha1033_g73696 - Global_Alpha_Variation5158_g73696 ) + AlphaTreshold2132_g73696 + ( Global_Extras_Alpha1033_g73696 * 0.5 ) ) , ( Global_Alpha_Mask4546_g73696 * _GlobalAlpha ));
				float lerpResult16_g74016 = lerp( 1.0 , lerpResult5203_g73696 , TVE_Enabled);
				half Global_Alpha315_g73696 = lerpResult16_g74016;
				half Fade_Alpha3727_g73696 = 1.0;
				float temp_output_661_0_g73696 = ( Main_Alpha316_g73696 * Global_Alpha315_g73696 * Fade_Alpha3727_g73696 );
				half Alpha34_g74021 = temp_output_661_0_g73696;
				half Offest27_g74021 = AlphaTreshold2132_g73696;
				half AlphaFeather5305_g73696 = _AlphaFeatherValue;
				half Feather30_g74021 = AlphaFeather5305_g73696;
				float temp_output_25_0_g74021 = ( ( ( Alpha34_g74021 - Offest27_g74021 ) / ( max( fwidth( Alpha34_g74021 ) , 0.001 ) + Feather30_g74021 ) ) + Offest27_g74021 );
				float temp_output_3_0_g74020 = temp_output_25_0_g74021;
				float Alpha19_g74020 = temp_output_3_0_g74020;
				float temp_output_15_0_g74020 = AlphaTreshold2132_g73696;
				float Treshold19_g74020 = temp_output_15_0_g74020;
				{
				#if defined (TVE_FEATURE_CLIP)
				#if defined (TVE_IS_HD_PIPELINE)
				#if !defined (SHADERPASS_FORWARD_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#if !defined (SHADERPASS_GBUFFER_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#else
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#endif
				}
				half Final_Alpha914_g73696 = saturate( Alpha19_g74020 );
				
				
				float3 BaseColor = Final_Albedo359_g73696;
				float3 Emission = Final_Emissive2476_g73696;
				float Alpha = Final_Alpha914_g73696;
				float AlphaClipThreshold = 0.5;

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				//MetaInput metaInput = (MetaInput)0;
				//metaInput.Albedo = BaseColor;
				//metaInput.Emission = Emission;
				//#ifdef EDITOR_VISUALIZATION
				//metaInput.VizUV = IN.VizUV.xy;
				//metaInput.LightCoord = IN.LightCoord;
				//#endif
				
				MetaInput metaInput = (MetaInput)0;
				metaInput.Albedo = BaseColor;
				metaInput.Emission = Emission;

				#ifdef EDITOR_VISUALIZATION
					metaInput.VizUV = unpacked.texCoord1.xy;
					metaInput.LightCoord = unpacked.texCoord2;
				#endif

				return UnityMetaFragment(metaInput);
			}
			ENDHLSL
		}
	//-------------------------------------------------------------------------------------
	// END_PASS META
	//-------------------------------------------------------------------------------------

		
	//-------------------------------------------------------------------------------------
	// BEGIN_PASS UNIVERSAL2D
	//-------------------------------------------------------------------------------------
		Pass
		{
			
			Name "Universal2D"
			Tags { "LightMode"="Universal2D" }

			Blend [_render_src] [_render_dst], One Zero
			ZWrite On
			ZTest LEqual
			Offset 0 , 0
			ColorMask RGBA

	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			HLSLPROGRAM
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

#include "./../../../../../GPUInstancer/Shaders/Include/GPUInstancerInclude.cginc"
#pragma instancing_options procedural:setupGPUI
#pragma multi_compile_instancing

			
			#define _SPECULAR_SETUP 1
			#define _NORMAL_DROPOFF_TS 1
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#pragma multi_compile _ DOTS_INSTANCING_ON
			#define ASE_ABSOLUTE_VERTEX_POS 1
			#define _TRANSLUCENCY_ASE 1
			#define _EMISSION
			#define _NORMALMAP 1
			#define ASE_SRP_VERSION 120100

			
			#pragma vertex vert
			#pragma fragment frag

			#define SHADERPASS SHADERPASS_2D
        
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

			//-----------------------------------------------------------------------------
			// DEFINES - (Set by User)
			//-----------------------------------------------------------------------------

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_COLOR
			#pragma shader_feature_local TVE_FEATURE_CLIP
			#pragma shader_feature_local TVE_FEATURE_BATCHING
			//TVE Injection Defines
			//SHADER INJECTION POINT BEGIN
			//SHADER INJECTION POINT END
			//TVE Pipeline Defines
			#define THE_VEGETATION_ENGINE
			#define TVE_IS_UNIVERSAL_PIPELINE
			//TVE Shader Type Defines
			#define TVE_IS_GRASS_SHADER


	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

	//-------------------------------------------------------------------------------------
	// BEGIN_CBUFFER
	//-------------------------------------------------------------------------------------

			CBUFFER_START(UnityPerMaterial)
			float4 _SubsurfaceDiffusion_asset;
			float4 _NoiseMaskRemap;
			float4 _SubsurfaceDiffusion_Asset;
			half4 _MotionHighlightColor;
			half4 _MainColor;
			half4 _MainUVs;
			half4 _VertexOcclusionColor;
			half4 _OverlayMaskRemap;
			half4 _AlphaMaskRemap;
			half4 _DetailBlendRemap;
			float4 _Color;
			half4 _GradientColorOne;
			half4 _SubsurfaceMaskRemap;
			float4 _GradientMaskRemap;
			half4 _ColorsMaskRemap;
			half4 _GradientColorTwo;
			float4 _EmissiveIntensityParams;
			half4 _VertexOcclusionRemap;
			half4 _SubsurfaceColor;
			half4 _EmissiveUVs;
			float4 _MaxBoundsInfo;
			half4 _EmissiveColor;
			half3 _render_normals;
			half _PerspectiveAngleValue;
			half _LayerMotionValue;
			half _MotionAmplitude_10;
			float _MotionScale_10;
			float _MotionSpeed_10;
			half _MotionVariation_10;
			half _PerspectiveNoiseValue;
			half _PerspectivePushValue;
			half _VertexDynamicMode;
			half _SizeFadeEndValue;
			half _SizeFadeStartValue;
			half _GlobalSize;
			half _LayerVertexValue;
			half _VertexDataMode;
			half _MotionNormalValue;
			half _MotionValue_30;
			half _InteractionAmplitude;
			half _InteractionMaskValue;
			float _MotionSpeed_32;
			float _MotionVariation_32;
			float _MotionScale_32;
			half _SizeFadeMode;
			half _MotionAmplitude_32;
			half _IsSubsurfaceShader;
			half _GradientMaxValue;
			half _SubsurfaceValue;
			half _GlobalAlpha;
			half _AlphaVariationValue;
			half _MainOcclusionValue;
			half _GlobalWetness;
			half _MainSmoothnessValue;
			half _RenderSpecular;
			half _GlobalEmissive;
			half _MainNormalValue;
			half _OverlayMaskMaxValue;
			half _OverlayMaskMinValue;
			half _GradientMinValue;
			half _OverlayVariationValue;
			half _ExtrasPositionMode;
			half _GlobalOverlay;
			half _ColorsMaskMaxValue;
			half _ColorsMaskMinValue;
			half _ColorsVariationValue;
			half _GlobalColors;
			half _LayerColorsValue;
			half _ColorsPositionMode;
			half _VertexOcclusionMaxValue;
			half _VertexOcclusionMinValue;
			half _VertexPivotMode;
			half _LayerExtrasValue;
			half _render_coverage;
			half _RenderMode;
			half _render_dst;
			half _SubsurfaceAngleValue;
			half _HasGradient;
			half _IsTVEShader;
			half _SpaceGlobalPosition;
			half _VertexVariationMode;
			half _AlphaFeatherValue;
			half _MessageGlobalsVariation;
			half _SubsurfaceShadowValue;
			half _RenderZWrite;
			half _RenderDecals;
			half _RenderDirect;
			half _CategoryNoise;
			half _SubsurfaceScatteringValue;
			half _RenderClip;
			float _SubsurfaceDiffusion;
			half _EmissiveFlagMode;
			half _RenderSSR;
			half _CategoryGlobal;
			half _SubsurfaceNormalValue;
			half _MessageMotionVariation;
			half _SubsurfaceAmbientValue;
			half _RenderCull;
			half _HasOcclusion;
			half _Cutoff;
			half _SubsurfaceDirectValue;
			half _CategoryRender;
			half _RenderQueue;
			half _DetailBlendMode;
			half _CategoryPerspective;
			half _render_cull;
			half _RenderAmbient;
			half _CategoryDetail;
			half _render_src;
			half _render_zw;
			half _IsGrassShader;
			half _RenderPriority;
			half _SubsurfaceMaskMinValue;
			half _CategoryGradient;
			half _LayerReactValue;
			half _RenderNormals;
			half _SpaceGlobalLayers;
			half _AlphaClipValue;
			half _VertexRollingMode;
			half _IsVersion;
			half _RenderCoverage;
			half _MessageSizeFade;
			half _CategoryEmissive;
			half _CategoryOcclusion;
			half _CategoryMain;
			half _CategorySizeFade;
			half _CategoryMotion;
			half _DetailMode;
			half _DetailTypeMode;
			half _HasEmissive;
			half _SpaceGlobalLocals;
			half _VertexMasksMode;
			half _RenderShadow;
			half _SpaceMotionGlobals;
			half _CategorySubsurface;
			half _SpaceRenderFade;
			half _SpaceMotionLocals;
			half _SubsurfaceMaskMaxValue;
			#ifdef _TRANSMISSION_ASE
				float _TransmissionShadow;
			#endif
			#ifdef _TRANSLUCENCY_ASE
				float _TransStrength;
				float _TransNormal;
				float _TransScattering;
				float _TransDirect;
				float _TransAmbient;
				float _TransShadow;
			#endif
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END

			// Property used by ScenePickingPass
			#ifdef SCENEPICKINGPASS
				float4 _SelectionID;
			#endif

			// Properties used by SceneSelectionPass
			#ifdef SCENESELECTIONPASS
				int _ObjectId;
				int _PassValue;
			#endif

	//-------------------------------------------------------------------------------------
	// END_CBUFFER
	//-------------------------------------------------------------------------------------

			sampler2D _MainTex;
			half TVE_Enabled;
			sampler2D _BumpMap;
			half4 TVE_MotionParams;
			TEXTURE2D_ARRAY(TVE_MotionTex);
			half4 TVE_MotionCoords;
			SAMPLER(sampler_linear_clamp);
			float TVE_MotionUsage[10];
			sampler2D TVE_NoiseTex;
			half4 TVE_NoiseParams;
			half4 TVE_FlutterParams;
			half TVE_MotionFadeEnd;
			half TVE_MotionFadeStart;
			half4 TVE_VertexParams;
			TEXTURE2D_ARRAY(TVE_VertexTex);
			half4 TVE_VertexCoords;
			float TVE_VertexUsage[10];
			half TVE_DistanceFadeBias;
			half _DisableSRPBatcher;
			sampler2D _MainAlbedoTex;
			half4 TVE_ColorsParams;
			TEXTURE2D_ARRAY(TVE_ColorsTex);
			half4 TVE_ColorsCoords;
			float TVE_ColorsUsage[10];
			half4 TVE_OverlayColor;
			half4 TVE_ExtrasParams;
			TEXTURE2D_ARRAY(TVE_ExtrasTex);
			half4 TVE_ExtrasCoords;
			float TVE_ExtrasUsage[10];


	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBR2DPass.hlsl"

			//#ifdef HAVE_VFX_MODIFICATION
			//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/VisualEffectVertex.hlsl"
			//#endif

	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			float2 DecodeFloatToVector2( float enc )
			{
				float2 result ;
				result.y = enc % 2048;
				result.x = floor(enc / 2048);
				return result / (2048 - 1);
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );

				float3 VertexPosition3588_g73696 = v.vertex.xyz;
				float3 appendResult60_g74015 = (float3(v.ase_texcoord3.x , v.ase_texcoord3.z , v.ase_texcoord3.y));
				half3 Mesh_PivotsData2831_g73696 = ( appendResult60_g74015 * _VertexPivotMode );
				half3 Mesh_PivotsOS2291_g73696 = Mesh_PivotsData2831_g73696;
				float3 temp_output_2283_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				half3 VertexPos40_g73968 = temp_output_2283_0_g73696;
				float3 appendResult74_g73968 = (float3(VertexPos40_g73968.x , 0.0 , 0.0));
				half3 VertexPosRotationAxis50_g73968 = appendResult74_g73968;
				float3 break84_g73968 = VertexPos40_g73968;
				float3 appendResult81_g73968 = (float3(0.0 , break84_g73968.y , break84_g73968.z));
				half3 VertexPosOtherAxis82_g73968 = appendResult81_g73968;
				float4 temp_output_91_19_g74003 = TVE_MotionCoords;
				float4x4 break19_g73996 = GetObjectToWorldMatrix();
				float3 appendResult20_g73996 = (float3(break19_g73996[ 0 ][ 3 ] , break19_g73996[ 1 ][ 3 ] , break19_g73996[ 2 ][ 3 ]));
				float3 temp_output_122_0_g73996 = Mesh_PivotsData2831_g73696;
				float3 PivotsOnly105_g73996 = (mul( GetObjectToWorldMatrix(), float4( temp_output_122_0_g73996 , 0.0 ) ).xyz).xyz;
				half3 ObjectData20_g73997 = ( appendResult20_g73996 + PivotsOnly105_g73996 );
				float3 ase_worldPos = mul(GetObjectToWorldMatrix(), v.vertex).xyz;
				half3 WorldData19_g73997 = ase_worldPos;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73997 = WorldData19_g73997;
				#else
				float3 staticSwitch14_g73997 = ObjectData20_g73997;
				#endif
				float3 temp_output_114_0_g73996 = staticSwitch14_g73997;
				float3 vertexToFrag4224_g73696 = temp_output_114_0_g73996;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				float3 vertexToFrag3890_g73696 = ase_worldPos;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				half2 UV94_g74003 = ( (temp_output_91_19_g74003).zw + ( (temp_output_91_19_g74003).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g74003 = _LayerMotionValue;
				float4 lerpResult107_g74003 = lerp( TVE_MotionParams , saturate( SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_MotionTex, sampler_linear_clamp, UV94_g74003,temp_output_84_0_g74003, 0.0 ) ) , TVE_MotionUsage[(int)temp_output_84_0_g74003]);
				half4 Global_Motion_Params3909_g73696 = lerpResult107_g74003;
				float4 break322_g73985 = Global_Motion_Params3909_g73696;
				float3 appendResult397_g73985 = (float3(break322_g73985.x , 0.0 , break322_g73985.y));
				float3 temp_output_398_0_g73985 = (appendResult397_g73985*2.0 + -1.0);
				float3 ase_parentObjectScale = ( 1.0 / float3( length( GetWorldToObjectMatrix()[ 0 ].xyz ), length( GetWorldToObjectMatrix()[ 1 ].xyz ), length( GetWorldToObjectMatrix()[ 2 ].xyz ) ) );
				half2 Global_MotionDirectionOS39_g73696 = (( mul( GetWorldToObjectMatrix(), float4( temp_output_398_0_g73985 , 0.0 ) ).xyz * ase_parentObjectScale )).xz;
				half2 Input_DirectionOS358_g73990 = Global_MotionDirectionOS39_g73696;
				half Wind_Power369_g73985 = break322_g73985.z;
				half Global_WindPower2223_g73696 = Wind_Power369_g73985;
				half3 Input_Position419_g73963 = ObjectPosition4223_g73696;
				float Input_MotionScale287_g73963 = ( _MotionScale_10 + 1.0 );
				half Global_Scale448_g73963 = TVE_NoiseParams.x;
				float2 temp_output_597_0_g73963 = (( Input_Position419_g73963 * Input_MotionScale287_g73963 * Global_Scale448_g73963 * 0.0075 )).xz;
				half2 Global_MotionDirectionWS4683_g73696 = (temp_output_398_0_g73985).xz;
				half2 Input_DirectionWS423_g73963 = Global_MotionDirectionWS4683_g73696;
				half Input_MotionSpeed62_g73963 = _MotionSpeed_10;
				half Global_Speed449_g73963 = TVE_NoiseParams.y;
				half Input_MotionVariation284_g73963 = _MotionVariation_10;
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = v.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				half Input_GlobalVariation569_g73963 = Global_MeshVariation5104_g73696;
				float temp_output_630_0_g73963 = ( ( ( _TimeParameters.x * Input_MotionSpeed62_g73963 * Global_Speed449_g73963 ) + ( Input_MotionVariation284_g73963 * Input_GlobalVariation569_g73963 ) ) * 0.03 );
				float temp_output_607_0_g73963 = frac( temp_output_630_0_g73963 );
				float4 lerpResult590_g73963 = lerp( tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * temp_output_607_0_g73963 ) ), 0, 0.0) ) , tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * frac( ( temp_output_630_0_g73963 + 0.5 ) ) ) ), 0, 0.0) ) , ( abs( ( temp_output_607_0_g73963 - 0.5 ) ) / 0.5 ));
				half Input_GlobalWind327_g73963 = Global_WindPower2223_g73696;
				float lerpResult612_g73963 = lerp( 1.4 , 0.4 , Input_GlobalWind327_g73963);
				float3 temp_cast_7 = (lerpResult612_g73963).xxx;
				float3 break638_g73963 = (pow( ( abs( (lerpResult590_g73963).rgb ) + 0.2 ) , temp_cast_7 )*1.4 + -0.2);
				half Global_NoiseTexR34_g73696 = break638_g73963.x;
				half Motion_10_Amplitude2258_g73696 = ( _MotionAmplitude_10 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 );
				half Input_BendingAmplitude376_g73990 = Motion_10_Amplitude2258_g73696;
				half Mesh_Height1524_g73696 = v.ase_color.a;
				half Input_MeshHeight388_g73990 = Mesh_Height1524_g73696;
				half ObjectData20_g73992 = ( Input_MeshHeight388_g73990 * 2.0 );
				float enc62_g73978 = v.ase_texcoord.w;
				float2 localDecodeFloatToVector262_g73978 = DecodeFloatToVector2( enc62_g73978 );
				float2 break63_g73978 = ( localDecodeFloatToVector262_g73978 * 100.0 );
				float Bounds_Height5230_g73696 = break63_g73978.x;
				half Input_BoundsHeight390_g73990 = Bounds_Height5230_g73696;
				half WorldData19_g73992 = ( ( Input_MeshHeight388_g73990 * Input_MeshHeight388_g73990 ) * Input_BoundsHeight390_g73990 * 2.0 );
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73992 = WorldData19_g73992;
				#else
				float staticSwitch14_g73992 = ObjectData20_g73992;
				#endif
				half Mask_Motion_10321_g73990 = staticSwitch14_g73992;
				half Input_InteractionAmplitude58_g73990 = _InteractionAmplitude;
				half Input_InteractionUseMask62_g73990 = _InteractionMaskValue;
				float lerpResult371_g73990 = lerp( 2.0 , Mask_Motion_10321_g73990 , Input_InteractionUseMask62_g73990);
				half ObjectData20_g73991 = lerpResult371_g73990;
				half WorldData19_g73991 = Mask_Motion_10321_g73990;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73991 = WorldData19_g73991;
				#else
				float staticSwitch14_g73991 = ObjectData20_g73991;
				#endif
				half Mask_Interaction373_g73990 = ( Input_InteractionAmplitude58_g73990 * staticSwitch14_g73991 );
				half Global_InteractionMask66_g73696 = ( break322_g73985.w * break322_g73985.w );
				float Input_InteractionGlobalMask330_g73990 = Global_InteractionMask66_g73696;
				float lerpResult360_g73990 = lerp( ( Input_BendingAmplitude376_g73990 * Mask_Motion_10321_g73990 ) , Mask_Interaction373_g73990 , saturate( ( Input_InteractionAmplitude58_g73990 * Input_InteractionGlobalMask330_g73990 ) ));
				float2 break364_g73990 = ( Input_DirectionOS358_g73990 * lerpResult360_g73990 );
				half Motion_10_BendingZ190_g73696 = break364_g73990.y;
				half Angle44_g73968 = Motion_10_BendingZ190_g73696;
				half3 VertexPos40_g73948 = ( VertexPosRotationAxis50_g73968 + ( VertexPosOtherAxis82_g73968 * cos( Angle44_g73968 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g73968 ) * sin( Angle44_g73968 ) ) );
				float3 appendResult74_g73948 = (float3(0.0 , 0.0 , VertexPos40_g73948.z));
				half3 VertexPosRotationAxis50_g73948 = appendResult74_g73948;
				float3 break84_g73948 = VertexPos40_g73948;
				float3 appendResult81_g73948 = (float3(break84_g73948.x , break84_g73948.y , 0.0));
				half3 VertexPosOtherAxis82_g73948 = appendResult81_g73948;
				half Motion_10_BendingX216_g73696 = break364_g73990.x;
				half Angle44_g73948 = -Motion_10_BendingX216_g73696;
				half Input_MotionScale321_g73940 = _MotionScale_32;
				half Input_MotionVariation330_g73940 = _MotionVariation_32;
				half Input_GlobalVariation372_g73940 = Global_MeshVariation5104_g73696;
				half Input_MotionSpeed62_g73940 = _MotionSpeed_32;
				half Global_Speed350_g73940 = TVE_FlutterParams.y;
				half Motion_FacingMask5214_g73696 = 1.0;
				float temp_output_7_0_g73984 = TVE_MotionFadeEnd;
				half Motion_FadeOut4005_g73696 = saturate( ( ( distance( ase_worldPos , _WorldSpaceCameraPos ) - temp_output_7_0_g73984 ) / ( TVE_MotionFadeStart - temp_output_7_0_g73984 ) ) );
				half Motion_30_Amplitude4960_g73696 = ( _MotionAmplitude_32 * _MotionValue_30 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 * Motion_FacingMask5214_g73696 * Motion_FadeOut4005_g73696 );
				half Input_MotionAmplitude58_g73940 = Motion_30_Amplitude4960_g73696;
				half Global_Power354_g73940 = TVE_FlutterParams.x;
				float enc59_g73978 = v.ase_texcoord.z;
				float2 localDecodeFloatToVector259_g73978 = DecodeFloatToVector2( enc59_g73978 );
				float2 break61_g73978 = localDecodeFloatToVector259_g73978;
				half Mesh_Motion_30144_g73696 = break61_g73978.y;
				half Input_MeshMotion_30374_g73940 = Mesh_Motion_30144_g73696;
				half Input_MotionNormal364_g73940 = _MotionNormalValue;
				float3 lerpResult370_g73940 = lerp( float3( 1,1,1 ) , v.ase_normal , Input_MotionNormal364_g73940);
				half3 Motion_30_Details263_g73696 = ( ( sin( ( ( ( ase_worldPos.x + ase_worldPos.y + ase_worldPos.z ) * Input_MotionScale321_g73940 ) + ( Input_MotionVariation330_g73940 * Input_GlobalVariation372_g73940 ) + ( _TimeParameters.x * Input_MotionSpeed62_g73940 * Global_Speed350_g73940 ) ) ) * Input_MotionAmplitude58_g73940 * Global_Power354_g73940 * Input_MeshMotion_30374_g73940 * 0.4 ) * lerpResult370_g73940 );
				float3 Vertex_Motion_Object833_g73696 = ( ( VertexPosRotationAxis50_g73948 + ( VertexPosOtherAxis82_g73948 * cos( Angle44_g73948 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g73948 ) * sin( Angle44_g73948 ) ) ) + Motion_30_Details263_g73696 );
				float3 temp_output_3474_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				float3 appendResult2043_g73696 = (float3(Motion_10_BendingX216_g73696 , 0.0 , Motion_10_BendingZ190_g73696));
				float3 Vertex_Motion_World1118_g73696 = ( ( temp_output_3474_0_g73696 + appendResult2043_g73696 ) + Motion_30_Details263_g73696 );
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch4976_g73696 = Vertex_Motion_World1118_g73696;
				#else
				float3 staticSwitch4976_g73696 = ( Vertex_Motion_Object833_g73696 + ( _VertexDataMode * 0.0 ) );
				#endif
				float4 temp_output_94_19_g73959 = TVE_VertexCoords;
				half2 UV97_g73959 = ( (temp_output_94_19_g73959).zw + ( (temp_output_94_19_g73959).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g73959 = _LayerVertexValue;
				float4 lerpResult109_g73959 = lerp( TVE_VertexParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_VertexTex, sampler_linear_clamp, UV97_g73959,temp_output_84_0_g73959, 0.0 ) , TVE_VertexUsage[(int)temp_output_84_0_g73959]);
				half4 Global_Object_Params4173_g73696 = lerpResult109_g73959;
				half Global_VertexSize174_g73696 = saturate( Global_Object_Params4173_g73696.w );
				float lerpResult346_g73696 = lerp( 1.0 , Global_VertexSize174_g73696 , _GlobalSize);
				float3 appendResult3480_g73696 = (float3(lerpResult346_g73696 , lerpResult346_g73696 , lerpResult346_g73696));
				half3 ObjectData20_g73993 = appendResult3480_g73696;
				half3 _Vector11 = half3(1,1,1);
				half3 WorldData19_g73993 = _Vector11;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73993 = WorldData19_g73993;
				#else
				float3 staticSwitch14_g73993 = ObjectData20_g73993;
				#endif
				half3 Vertex_Size1741_g73696 = staticSwitch14_g73993;
				half3 _Vector5 = half3(1,1,1);
				float temp_output_7_0_g73931 = _SizeFadeEndValue;
				float temp_output_335_0_g73696 = saturate( ( ( ( distance( _WorldSpaceCameraPos , ObjectPosition4223_g73696 ) * ( 1.0 / TVE_DistanceFadeBias ) ) - temp_output_7_0_g73931 ) / ( _SizeFadeStartValue - temp_output_7_0_g73931 ) ) );
				float3 appendResult3482_g73696 = (float3(temp_output_335_0_g73696 , temp_output_335_0_g73696 , temp_output_335_0_g73696));
				float3 lerpResult5065_g73696 = lerp( _Vector5 , appendResult3482_g73696 , _SizeFadeMode);
				half3 ObjectData20_g73958 = lerpResult5065_g73696;
				half3 WorldData19_g73958 = _Vector5;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73958 = WorldData19_g73958;
				#else
				float3 staticSwitch14_g73958 = ObjectData20_g73958;
				#endif
				float3 Vertex_SizeFade1740_g73696 = staticSwitch14_g73958;
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - ase_worldPos );
				ase_worldViewDir = normalize(ase_worldViewDir);
				float3 normalizeResult2696_g73696 = normalize( ase_worldViewDir );
				float3 break2709_g73696 = cross( normalizeResult2696_g73696 , half3(0,1,0) );
				float3 appendResult2710_g73696 = (float3(-break2709_g73696.z , 0.0 , break2709_g73696.x));
				float3 appendResult2667_g73696 = (float3(Global_MeshVariation5104_g73696 , 0.5 , Global_MeshVariation5104_g73696));
				float3 normalizeResult2169_g73696 = normalize( ase_worldViewDir );
				float3 ViewDir_Normalized3963_g73696 = normalizeResult2169_g73696;
				float dotResult2212_g73696 = dot( ViewDir_Normalized3963_g73696 , float3(0,1,0) );
				half Mask_HView2656_g73696 = dotResult2212_g73696;
				float saferPower2652_g73696 = abs( Mask_HView2656_g73696 );
				half3 Grass_Perspective2661_g73696 = ( ( ( mul( GetWorldToObjectMatrix(), float4( appendResult2710_g73696 , 0.0 ) ).xyz * _PerspectivePushValue ) + ( (appendResult2667_g73696*2.0 + -1.0) * _PerspectiveNoiseValue ) ) * Mesh_Height1524_g73696 * pow( saferPower2652_g73696 , _PerspectiveAngleValue ) );
				float3 lerpResult16_g74000 = lerp( VertexPosition3588_g73696 , ( ( staticSwitch4976_g73696 * Vertex_Size1741_g73696 * Vertex_SizeFade1740_g73696 ) + Mesh_PivotsOS2291_g73696 + Grass_Perspective2661_g73696 ) , TVE_Enabled);
				float3 Final_VertexPosition890_g73696 = ( lerpResult16_g74000 + _DisableSRPBatcher );
				
				float temp_output_7_0_g73976 = _GradientMinValue;
				half Gradient_Tint2784_g73696 = saturate( ( ( Mesh_Height1524_g73696 - temp_output_7_0_g73976 ) / ( _GradientMaxValue - temp_output_7_0_g73976 ) ) );
				float vertexToFrag11_g73981 = Gradient_Tint2784_g73696;
				o.ase_texcoord2.x = vertexToFrag11_g73981;
				half Global_NoiseTexA4860_g73696 = (lerpResult590_g73963).a;
				half Highlight_Tint3231_g73696 = ( Global_NoiseTexA4860_g73696 * Global_WindPower2223_g73696 * Motion_FadeOut4005_g73696 * Mesh_Height1524_g73696 );
				float vertexToFrag11_g73943 = Highlight_Tint3231_g73696;
				o.ase_texcoord2.y = vertexToFrag11_g73943;
				float Mesh_Occlusion318_g73696 = v.ase_color.g;
				float temp_output_7_0_g73971 = _VertexOcclusionMinValue;
				float temp_output_3377_0_g73696 = saturate( ( ( Mesh_Occlusion318_g73696 - temp_output_7_0_g73971 ) / ( _VertexOcclusionMaxValue - temp_output_7_0_g73971 ) ) );
				float vertexToFrag11_g73969 = temp_output_3377_0_g73696;
				o.ase_texcoord2.z = vertexToFrag11_g73969;
				o.ase_texcoord4.xyz = vertexToFrag3890_g73696;
				o.ase_texcoord5.xyz = vertexToFrag4224_g73696;
				
				o.ase_texcoord3 = v.ase_texcoord;
				o.ase_color = v.ase_color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord2.w = 0;
				o.ase_texcoord4.w = 0;
				o.ase_texcoord5.w = 0;
				
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif

				float3 vertexValue = Final_VertexPosition890_g73696;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				float4 positionCS = TransformWorldToHClip( positionWS );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					o.worldPos = positionWS;
				#endif

				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = positionCS;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif

				o.clipPos = positionCS;

				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord3 = v.ase_texcoord3;
				o.ase_color = v.ase_color;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
				return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord3 = patch[0].ase_texcoord3 * bary.x + patch[1].ase_texcoord3 * bary.y + patch[2].ase_texcoord3 * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					float3 WorldPosition = IN.worldPos;
				#endif

				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float vertexToFrag11_g73981 = IN.ase_texcoord2.x;
				float3 lerpResult2779_g73696 = lerp( (_GradientColorTwo).rgb , (_GradientColorOne).rgb , vertexToFrag11_g73981);
				float vertexToFrag11_g73943 = IN.ase_texcoord2.y;
				half2 Main_UVs15_g73696 = ( ( IN.ase_texcoord3.xy * (_MainUVs).xy ) + (_MainUVs).zw );
				float4 tex2DNode29_g73696 = tex2D( _MainAlbedoTex, Main_UVs15_g73696 );
				half3 Main_Albedo99_g73696 = ( (_MainColor).rgb * (tex2DNode29_g73696).rgb );
				half3 Blend_Albedo265_g73696 = Main_Albedo99_g73696;
				float3 temp_cast_0 = (1.0).xxx;
				float vertexToFrag11_g73969 = IN.ase_texcoord2.z;
				float3 lerpResult2945_g73696 = lerp( (_VertexOcclusionColor).rgb , temp_cast_0 , vertexToFrag11_g73969);
				float3 Vertex_Occlusion648_g73696 = lerpResult2945_g73696;
				half3 Blend_AlbedoTinted2808_g73696 = ( ( lerpResult2779_g73696 * float3(1,1,1) * ( ( (_MotionHighlightColor).rgb * vertexToFrag11_g73943 ) + float3( 1,1,1 ) ) ) * Blend_Albedo265_g73696 * Vertex_Occlusion648_g73696 );
				float dotResult3616_g73696 = dot( Blend_AlbedoTinted2808_g73696 , float3(0.2126,0.7152,0.0722) );
				float3 temp_cast_1 = (dotResult3616_g73696).xxx;
				float4 temp_output_91_19_g73949 = TVE_ColorsCoords;
				float3 vertexToFrag3890_g73696 = IN.ase_texcoord4.xyz;
				float3 WorldPosition3905_g73696 = vertexToFrag3890_g73696;
				float3 vertexToFrag4224_g73696 = IN.ase_texcoord5.xyz;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				float3 lerpResult4822_g73696 = lerp( WorldPosition3905_g73696 , ObjectPosition4223_g73696 , _ColorsPositionMode);
				half2 UV94_g73949 = ( (temp_output_91_19_g73949).zw + ( (temp_output_91_19_g73949).xy * (lerpResult4822_g73696).xz ) );
				float temp_output_82_0_g73949 = _LayerColorsValue;
				float4 lerpResult108_g73949 = lerp( TVE_ColorsParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_ColorsTex, sampler_linear_clamp, UV94_g73949,temp_output_82_0_g73949, 0.0 ) , TVE_ColorsUsage[(int)temp_output_82_0_g73949]);
				half Global_ColorsTex_A1701_g73696 = saturate( (lerpResult108_g73949).a );
				half Global_Colors_Influence3668_g73696 = Global_ColorsTex_A1701_g73696;
				float3 lerpResult3618_g73696 = lerp( Blend_AlbedoTinted2808_g73696 , temp_cast_1 , Global_Colors_Influence3668_g73696);
				half3 Global_ColorsTex_RGB1700_g73696 = (lerpResult108_g73949).rgb;
				#ifdef UNITY_COLORSPACE_GAMMA
				float staticSwitch1_g73970 = 2.0;
				#else
				float staticSwitch1_g73970 = 4.594794;
				#endif
				half3 Global_Colors1954_g73696 = ( Global_ColorsTex_RGB1700_g73696 * staticSwitch1_g73970 );
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = IN.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				float lerpResult3870_g73696 = lerp( 1.0 , ( Global_MeshVariation5104_g73696 * Global_MeshVariation5104_g73696 ) , _ColorsVariationValue);
				half Global_Colors_Variation3650_g73696 = ( _GlobalColors * lerpResult3870_g73696 );
				half Mesh_Height1524_g73696 = IN.ase_color.a;
				float clampResult5405_g73696 = clamp( Mesh_Height1524_g73696 , 0.01 , 0.99 );
				float temp_output_7_0_g73945 = _ColorsMaskMinValue;
				half Global_Colors_Mask3692_g73696 = saturate( ( ( clampResult5405_g73696 - temp_output_7_0_g73945 ) / ( _ColorsMaskMaxValue - temp_output_7_0_g73945 ) ) );
				float lerpResult16_g73954 = lerp( 0.0 , ( Global_Colors_Variation3650_g73696 * Global_Colors_Mask3692_g73696 ) , TVE_Enabled);
				float3 lerpResult3628_g73696 = lerp( Blend_AlbedoTinted2808_g73696 , ( lerpResult3618_g73696 * Global_Colors1954_g73696 ) , lerpResult16_g73954);
				half3 Blend_AlbedoColored863_g73696 = lerpResult3628_g73696;
				half3 Blend_AlbedoAndSubsurface149_g73696 = Blend_AlbedoColored863_g73696;
				half3 Global_OverlayColor1758_g73696 = (TVE_OverlayColor).rgb;
				half Main_Albedo_G3526_g73696 = tex2DNode29_g73696.g;
				float4 temp_output_93_19_g73986 = TVE_ExtrasCoords;
				float3 lerpResult4827_g73696 = lerp( WorldPosition3905_g73696 , ObjectPosition4223_g73696 , _ExtrasPositionMode);
				half2 UV96_g73986 = ( (temp_output_93_19_g73986).zw + ( (temp_output_93_19_g73986).xy * (lerpResult4827_g73696).xz ) );
				float temp_output_84_0_g73986 = _LayerExtrasValue;
				float4 lerpResult109_g73986 = lerp( TVE_ExtrasParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_ExtrasTex, sampler_linear_clamp, UV96_g73986,temp_output_84_0_g73986, 0.0 ) , TVE_ExtrasUsage[(int)temp_output_84_0_g73986]);
				float4 break89_g73986 = lerpResult109_g73986;
				half Global_Extras_Overlay156_g73696 = break89_g73986.b;
				float lerpResult1065_g73696 = lerp( 1.0 , Global_MeshVariation5104_g73696 , _OverlayVariationValue);
				half Overlay_Variation4560_g73696 = lerpResult1065_g73696;
				half Overlay_Commons1365_g73696 = ( _GlobalOverlay * Global_Extras_Overlay156_g73696 * Overlay_Variation4560_g73696 );
				half Overlay_Mask_2D5121_g73696 = ( ( ( Mesh_Height1524_g73696 * 0.5 ) + Main_Albedo_G3526_g73696 ) * Overlay_Commons1365_g73696 );
				float temp_output_7_0_g73980 = _OverlayMaskMinValue;
				half Overlay_Mask269_g73696 = saturate( ( ( Overlay_Mask_2D5121_g73696 - temp_output_7_0_g73980 ) / ( _OverlayMaskMaxValue - temp_output_7_0_g73980 ) ) );
				float3 lerpResult336_g73696 = lerp( Blend_AlbedoAndSubsurface149_g73696 , Global_OverlayColor1758_g73696 , Overlay_Mask269_g73696);
				half3 Final_Albedo359_g73696 = lerpResult336_g73696;
				
				float localCustomAlphaClip19_g74020 = ( 0.0 );
				float Main_Alpha316_g73696 = ( _MainColor.a * tex2DNode29_g73696.a );
				half Global_Extras_Alpha1033_g73696 = saturate( break89_g73986.a );
				float lerpResult5154_g73696 = lerp( 0.0 , Global_MeshVariation5104_g73696 , _AlphaVariationValue);
				half Global_Alpha_Variation5158_g73696 = lerpResult5154_g73696;
				half AlphaTreshold2132_g73696 = _AlphaClipValue;
				half Global_Alpha_Mask4546_g73696 = 1.0;
				float lerpResult5203_g73696 = lerp( 1.0 , ( ( Global_Extras_Alpha1033_g73696 - Global_Alpha_Variation5158_g73696 ) + AlphaTreshold2132_g73696 + ( Global_Extras_Alpha1033_g73696 * 0.5 ) ) , ( Global_Alpha_Mask4546_g73696 * _GlobalAlpha ));
				float lerpResult16_g74016 = lerp( 1.0 , lerpResult5203_g73696 , TVE_Enabled);
				half Global_Alpha315_g73696 = lerpResult16_g74016;
				half Fade_Alpha3727_g73696 = 1.0;
				float temp_output_661_0_g73696 = ( Main_Alpha316_g73696 * Global_Alpha315_g73696 * Fade_Alpha3727_g73696 );
				half Alpha34_g74021 = temp_output_661_0_g73696;
				half Offest27_g74021 = AlphaTreshold2132_g73696;
				half AlphaFeather5305_g73696 = _AlphaFeatherValue;
				half Feather30_g74021 = AlphaFeather5305_g73696;
				float temp_output_25_0_g74021 = ( ( ( Alpha34_g74021 - Offest27_g74021 ) / ( max( fwidth( Alpha34_g74021 ) , 0.001 ) + Feather30_g74021 ) ) + Offest27_g74021 );
				float temp_output_3_0_g74020 = temp_output_25_0_g74021;
				float Alpha19_g74020 = temp_output_3_0_g74020;
				float temp_output_15_0_g74020 = AlphaTreshold2132_g73696;
				float Treshold19_g74020 = temp_output_15_0_g74020;
				{
				#if defined (TVE_FEATURE_CLIP)
				#if defined (TVE_IS_HD_PIPELINE)
				#if !defined (SHADERPASS_FORWARD_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#if !defined (SHADERPASS_GBUFFER_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#else
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#endif
				}
				half Final_Alpha914_g73696 = saturate( Alpha19_g74020 );
				
				
				float3 BaseColor = Final_Albedo359_g73696;
				float Alpha = Final_Alpha914_g73696;
				float AlphaClipThreshold = 0.5;

				half4 color = half4(BaseColor, Alpha );

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				return color;
			}
			ENDHLSL
		}
	//-------------------------------------------------------------------------------------
	// END_PASS UNIVERSAL2D
	//-------------------------------------------------------------------------------------

		
	//-------------------------------------------------------------------------------------
	// BEGIN_PASS DEPTHNORMALS
	//-------------------------------------------------------------------------------------
		Pass
		{
			
			Name "DepthNormals"
			Tags { "LightMode"="DepthNormalsOnly" }

			ZWrite On
			Blend One Zero
			ZTest LEqual
			ZWrite On

	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			HLSLPROGRAM
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

#include "./../../../../../GPUInstancer/Shaders/Include/GPUInstancerInclude.cginc"
#pragma instancing_options procedural:setupGPUI
#pragma multi_compile_instancing


			#define _SPECULAR_SETUP 1
			#define _NORMAL_DROPOFF_TS 1
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#pragma multi_compile _ DOTS_INSTANCING_ON
			#define ASE_ABSOLUTE_VERTEX_POS 1
			#define _TRANSLUCENCY_ASE 1
			#define _EMISSION
			#define _NORMALMAP 1
			#define ASE_SRP_VERSION 120100


			#pragma vertex vert
			#pragma fragment frag

			#define SHADERPASS SHADERPASS_DEPTHNORMALSONLY

			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

			//-----------------------------------------------------------------------------
			// DEFINES - (Set by User)
			//-----------------------------------------------------------------------------

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#pragma shader_feature_local TVE_FEATURE_CLIP
			#pragma shader_feature_local TVE_FEATURE_BATCHING
			//TVE Injection Defines
			//SHADER INJECTION POINT BEGIN
			//SHADER INJECTION POINT END
			//TVE Pipeline Defines
			#define THE_VEGETATION_ENGINE
			#define TVE_IS_UNIVERSAL_PIPELINE
			//TVE Shader Type Defines
			#define TVE_IS_GRASS_SHADER


	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_tangent : TANGENT;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					float4 shadowCoord : TEXCOORD1;
				#endif
				float3 worldNormal : TEXCOORD2;
				float4 worldTangent : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				float4 ase_texcoord6 : TEXCOORD6;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

	//-------------------------------------------------------------------------------------
	// BEGIN_CBUFFER
	//-------------------------------------------------------------------------------------

			CBUFFER_START(UnityPerMaterial)
			float4 _SubsurfaceDiffusion_asset;
			float4 _NoiseMaskRemap;
			float4 _SubsurfaceDiffusion_Asset;
			half4 _MotionHighlightColor;
			half4 _MainColor;
			half4 _MainUVs;
			half4 _VertexOcclusionColor;
			half4 _OverlayMaskRemap;
			half4 _AlphaMaskRemap;
			half4 _DetailBlendRemap;
			float4 _Color;
			half4 _GradientColorOne;
			half4 _SubsurfaceMaskRemap;
			float4 _GradientMaskRemap;
			half4 _ColorsMaskRemap;
			half4 _GradientColorTwo;
			float4 _EmissiveIntensityParams;
			half4 _VertexOcclusionRemap;
			half4 _SubsurfaceColor;
			half4 _EmissiveUVs;
			float4 _MaxBoundsInfo;
			half4 _EmissiveColor;
			half3 _render_normals;
			half _PerspectiveAngleValue;
			half _LayerMotionValue;
			half _MotionAmplitude_10;
			float _MotionScale_10;
			float _MotionSpeed_10;
			half _MotionVariation_10;
			half _PerspectiveNoiseValue;
			half _PerspectivePushValue;
			half _VertexDynamicMode;
			half _SizeFadeEndValue;
			half _SizeFadeStartValue;
			half _GlobalSize;
			half _LayerVertexValue;
			half _VertexDataMode;
			half _MotionNormalValue;
			half _MotionValue_30;
			half _InteractionAmplitude;
			half _InteractionMaskValue;
			float _MotionSpeed_32;
			float _MotionVariation_32;
			float _MotionScale_32;
			half _SizeFadeMode;
			half _MotionAmplitude_32;
			half _IsSubsurfaceShader;
			half _GradientMaxValue;
			half _SubsurfaceValue;
			half _GlobalAlpha;
			half _AlphaVariationValue;
			half _MainOcclusionValue;
			half _GlobalWetness;
			half _MainSmoothnessValue;
			half _RenderSpecular;
			half _GlobalEmissive;
			half _MainNormalValue;
			half _OverlayMaskMaxValue;
			half _OverlayMaskMinValue;
			half _GradientMinValue;
			half _OverlayVariationValue;
			half _ExtrasPositionMode;
			half _GlobalOverlay;
			half _ColorsMaskMaxValue;
			half _ColorsMaskMinValue;
			half _ColorsVariationValue;
			half _GlobalColors;
			half _LayerColorsValue;
			half _ColorsPositionMode;
			half _VertexOcclusionMaxValue;
			half _VertexOcclusionMinValue;
			half _VertexPivotMode;
			half _LayerExtrasValue;
			half _render_coverage;
			half _RenderMode;
			half _render_dst;
			half _SubsurfaceAngleValue;
			half _HasGradient;
			half _IsTVEShader;
			half _SpaceGlobalPosition;
			half _VertexVariationMode;
			half _AlphaFeatherValue;
			half _MessageGlobalsVariation;
			half _SubsurfaceShadowValue;
			half _RenderZWrite;
			half _RenderDecals;
			half _RenderDirect;
			half _CategoryNoise;
			half _SubsurfaceScatteringValue;
			half _RenderClip;
			float _SubsurfaceDiffusion;
			half _EmissiveFlagMode;
			half _RenderSSR;
			half _CategoryGlobal;
			half _SubsurfaceNormalValue;
			half _MessageMotionVariation;
			half _SubsurfaceAmbientValue;
			half _RenderCull;
			half _HasOcclusion;
			half _Cutoff;
			half _SubsurfaceDirectValue;
			half _CategoryRender;
			half _RenderQueue;
			half _DetailBlendMode;
			half _CategoryPerspective;
			half _render_cull;
			half _RenderAmbient;
			half _CategoryDetail;
			half _render_src;
			half _render_zw;
			half _IsGrassShader;
			half _RenderPriority;
			half _SubsurfaceMaskMinValue;
			half _CategoryGradient;
			half _LayerReactValue;
			half _RenderNormals;
			half _SpaceGlobalLayers;
			half _AlphaClipValue;
			half _VertexRollingMode;
			half _IsVersion;
			half _RenderCoverage;
			half _MessageSizeFade;
			half _CategoryEmissive;
			half _CategoryOcclusion;
			half _CategoryMain;
			half _CategorySizeFade;
			half _CategoryMotion;
			half _DetailMode;
			half _DetailTypeMode;
			half _HasEmissive;
			half _SpaceGlobalLocals;
			half _VertexMasksMode;
			half _RenderShadow;
			half _SpaceMotionGlobals;
			half _CategorySubsurface;
			half _SpaceRenderFade;
			half _SpaceMotionLocals;
			half _SubsurfaceMaskMaxValue;
			#ifdef _TRANSMISSION_ASE
				float _TransmissionShadow;
			#endif
			#ifdef _TRANSLUCENCY_ASE
				float _TransStrength;
				float _TransNormal;
				float _TransScattering;
				float _TransDirect;
				float _TransAmbient;
				float _TransShadow;
			#endif
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END

			// Property used by ScenePickingPass
			#ifdef SCENEPICKINGPASS
				float4 _SelectionID;
			#endif

			// Properties used by SceneSelectionPass
			#ifdef SCENESELECTIONPASS
				int _ObjectId;
				int _PassValue;
			#endif

	//-------------------------------------------------------------------------------------
	// END_CBUFFER
	//-------------------------------------------------------------------------------------

			sampler2D _MainTex;
			half TVE_Enabled;
			sampler2D _BumpMap;
			half4 TVE_MotionParams;
			TEXTURE2D_ARRAY(TVE_MotionTex);
			half4 TVE_MotionCoords;
			SAMPLER(sampler_linear_clamp);
			float TVE_MotionUsage[10];
			sampler2D TVE_NoiseTex;
			half4 TVE_NoiseParams;
			half4 TVE_FlutterParams;
			half TVE_MotionFadeEnd;
			half TVE_MotionFadeStart;
			half4 TVE_VertexParams;
			TEXTURE2D_ARRAY(TVE_VertexTex);
			half4 TVE_VertexCoords;
			float TVE_VertexUsage[10];
			half TVE_DistanceFadeBias;
			half _DisableSRPBatcher;
			sampler2D _MainNormalTex;
			sampler2D _MainAlbedoTex;
			half4 TVE_ExtrasParams;
			TEXTURE2D_ARRAY(TVE_ExtrasTex);
			half4 TVE_ExtrasCoords;
			float TVE_ExtrasUsage[10];


	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthNormalsOnlyPass.hlsl"

			//#ifdef HAVE_VFX_MODIFICATION
			//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/VisualEffectVertex.hlsl"
			//#endif

	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			float2 DecodeFloatToVector2( float enc )
			{
				float2 result ;
				result.y = enc % 2048;
				result.x = floor(enc / 2048);
				return result / (2048 - 1);
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 VertexPosition3588_g73696 = v.vertex.xyz;
				float3 appendResult60_g74015 = (float3(v.ase_texcoord3.x , v.ase_texcoord3.z , v.ase_texcoord3.y));
				half3 Mesh_PivotsData2831_g73696 = ( appendResult60_g74015 * _VertexPivotMode );
				half3 Mesh_PivotsOS2291_g73696 = Mesh_PivotsData2831_g73696;
				float3 temp_output_2283_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				half3 VertexPos40_g73968 = temp_output_2283_0_g73696;
				float3 appendResult74_g73968 = (float3(VertexPos40_g73968.x , 0.0 , 0.0));
				half3 VertexPosRotationAxis50_g73968 = appendResult74_g73968;
				float3 break84_g73968 = VertexPos40_g73968;
				float3 appendResult81_g73968 = (float3(0.0 , break84_g73968.y , break84_g73968.z));
				half3 VertexPosOtherAxis82_g73968 = appendResult81_g73968;
				float4 temp_output_91_19_g74003 = TVE_MotionCoords;
				float4x4 break19_g73996 = GetObjectToWorldMatrix();
				float3 appendResult20_g73996 = (float3(break19_g73996[ 0 ][ 3 ] , break19_g73996[ 1 ][ 3 ] , break19_g73996[ 2 ][ 3 ]));
				float3 temp_output_122_0_g73996 = Mesh_PivotsData2831_g73696;
				float3 PivotsOnly105_g73996 = (mul( GetObjectToWorldMatrix(), float4( temp_output_122_0_g73996 , 0.0 ) ).xyz).xyz;
				half3 ObjectData20_g73997 = ( appendResult20_g73996 + PivotsOnly105_g73996 );
				float3 ase_worldPos = mul(GetObjectToWorldMatrix(), v.vertex).xyz;
				half3 WorldData19_g73997 = ase_worldPos;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73997 = WorldData19_g73997;
				#else
				float3 staticSwitch14_g73997 = ObjectData20_g73997;
				#endif
				float3 temp_output_114_0_g73996 = staticSwitch14_g73997;
				float3 vertexToFrag4224_g73696 = temp_output_114_0_g73996;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				float3 vertexToFrag3890_g73696 = ase_worldPos;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				half2 UV94_g74003 = ( (temp_output_91_19_g74003).zw + ( (temp_output_91_19_g74003).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g74003 = _LayerMotionValue;
				float4 lerpResult107_g74003 = lerp( TVE_MotionParams , saturate( SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_MotionTex, sampler_linear_clamp, UV94_g74003,temp_output_84_0_g74003, 0.0 ) ) , TVE_MotionUsage[(int)temp_output_84_0_g74003]);
				half4 Global_Motion_Params3909_g73696 = lerpResult107_g74003;
				float4 break322_g73985 = Global_Motion_Params3909_g73696;
				float3 appendResult397_g73985 = (float3(break322_g73985.x , 0.0 , break322_g73985.y));
				float3 temp_output_398_0_g73985 = (appendResult397_g73985*2.0 + -1.0);
				float3 ase_parentObjectScale = ( 1.0 / float3( length( GetWorldToObjectMatrix()[ 0 ].xyz ), length( GetWorldToObjectMatrix()[ 1 ].xyz ), length( GetWorldToObjectMatrix()[ 2 ].xyz ) ) );
				half2 Global_MotionDirectionOS39_g73696 = (( mul( GetWorldToObjectMatrix(), float4( temp_output_398_0_g73985 , 0.0 ) ).xyz * ase_parentObjectScale )).xz;
				half2 Input_DirectionOS358_g73990 = Global_MotionDirectionOS39_g73696;
				half Wind_Power369_g73985 = break322_g73985.z;
				half Global_WindPower2223_g73696 = Wind_Power369_g73985;
				half3 Input_Position419_g73963 = ObjectPosition4223_g73696;
				float Input_MotionScale287_g73963 = ( _MotionScale_10 + 1.0 );
				half Global_Scale448_g73963 = TVE_NoiseParams.x;
				float2 temp_output_597_0_g73963 = (( Input_Position419_g73963 * Input_MotionScale287_g73963 * Global_Scale448_g73963 * 0.0075 )).xz;
				half2 Global_MotionDirectionWS4683_g73696 = (temp_output_398_0_g73985).xz;
				half2 Input_DirectionWS423_g73963 = Global_MotionDirectionWS4683_g73696;
				half Input_MotionSpeed62_g73963 = _MotionSpeed_10;
				half Global_Speed449_g73963 = TVE_NoiseParams.y;
				half Input_MotionVariation284_g73963 = _MotionVariation_10;
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = v.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				half Input_GlobalVariation569_g73963 = Global_MeshVariation5104_g73696;
				float temp_output_630_0_g73963 = ( ( ( _TimeParameters.x * Input_MotionSpeed62_g73963 * Global_Speed449_g73963 ) + ( Input_MotionVariation284_g73963 * Input_GlobalVariation569_g73963 ) ) * 0.03 );
				float temp_output_607_0_g73963 = frac( temp_output_630_0_g73963 );
				float4 lerpResult590_g73963 = lerp( tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * temp_output_607_0_g73963 ) ), 0, 0.0) ) , tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * frac( ( temp_output_630_0_g73963 + 0.5 ) ) ) ), 0, 0.0) ) , ( abs( ( temp_output_607_0_g73963 - 0.5 ) ) / 0.5 ));
				half Input_GlobalWind327_g73963 = Global_WindPower2223_g73696;
				float lerpResult612_g73963 = lerp( 1.4 , 0.4 , Input_GlobalWind327_g73963);
				float3 temp_cast_7 = (lerpResult612_g73963).xxx;
				float3 break638_g73963 = (pow( ( abs( (lerpResult590_g73963).rgb ) + 0.2 ) , temp_cast_7 )*1.4 + -0.2);
				half Global_NoiseTexR34_g73696 = break638_g73963.x;
				half Motion_10_Amplitude2258_g73696 = ( _MotionAmplitude_10 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 );
				half Input_BendingAmplitude376_g73990 = Motion_10_Amplitude2258_g73696;
				half Mesh_Height1524_g73696 = v.ase_color.a;
				half Input_MeshHeight388_g73990 = Mesh_Height1524_g73696;
				half ObjectData20_g73992 = ( Input_MeshHeight388_g73990 * 2.0 );
				float enc62_g73978 = v.ase_texcoord.w;
				float2 localDecodeFloatToVector262_g73978 = DecodeFloatToVector2( enc62_g73978 );
				float2 break63_g73978 = ( localDecodeFloatToVector262_g73978 * 100.0 );
				float Bounds_Height5230_g73696 = break63_g73978.x;
				half Input_BoundsHeight390_g73990 = Bounds_Height5230_g73696;
				half WorldData19_g73992 = ( ( Input_MeshHeight388_g73990 * Input_MeshHeight388_g73990 ) * Input_BoundsHeight390_g73990 * 2.0 );
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73992 = WorldData19_g73992;
				#else
				float staticSwitch14_g73992 = ObjectData20_g73992;
				#endif
				half Mask_Motion_10321_g73990 = staticSwitch14_g73992;
				half Input_InteractionAmplitude58_g73990 = _InteractionAmplitude;
				half Input_InteractionUseMask62_g73990 = _InteractionMaskValue;
				float lerpResult371_g73990 = lerp( 2.0 , Mask_Motion_10321_g73990 , Input_InteractionUseMask62_g73990);
				half ObjectData20_g73991 = lerpResult371_g73990;
				half WorldData19_g73991 = Mask_Motion_10321_g73990;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73991 = WorldData19_g73991;
				#else
				float staticSwitch14_g73991 = ObjectData20_g73991;
				#endif
				half Mask_Interaction373_g73990 = ( Input_InteractionAmplitude58_g73990 * staticSwitch14_g73991 );
				half Global_InteractionMask66_g73696 = ( break322_g73985.w * break322_g73985.w );
				float Input_InteractionGlobalMask330_g73990 = Global_InteractionMask66_g73696;
				float lerpResult360_g73990 = lerp( ( Input_BendingAmplitude376_g73990 * Mask_Motion_10321_g73990 ) , Mask_Interaction373_g73990 , saturate( ( Input_InteractionAmplitude58_g73990 * Input_InteractionGlobalMask330_g73990 ) ));
				float2 break364_g73990 = ( Input_DirectionOS358_g73990 * lerpResult360_g73990 );
				half Motion_10_BendingZ190_g73696 = break364_g73990.y;
				half Angle44_g73968 = Motion_10_BendingZ190_g73696;
				half3 VertexPos40_g73948 = ( VertexPosRotationAxis50_g73968 + ( VertexPosOtherAxis82_g73968 * cos( Angle44_g73968 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g73968 ) * sin( Angle44_g73968 ) ) );
				float3 appendResult74_g73948 = (float3(0.0 , 0.0 , VertexPos40_g73948.z));
				half3 VertexPosRotationAxis50_g73948 = appendResult74_g73948;
				float3 break84_g73948 = VertexPos40_g73948;
				float3 appendResult81_g73948 = (float3(break84_g73948.x , break84_g73948.y , 0.0));
				half3 VertexPosOtherAxis82_g73948 = appendResult81_g73948;
				half Motion_10_BendingX216_g73696 = break364_g73990.x;
				half Angle44_g73948 = -Motion_10_BendingX216_g73696;
				half Input_MotionScale321_g73940 = _MotionScale_32;
				half Input_MotionVariation330_g73940 = _MotionVariation_32;
				half Input_GlobalVariation372_g73940 = Global_MeshVariation5104_g73696;
				half Input_MotionSpeed62_g73940 = _MotionSpeed_32;
				half Global_Speed350_g73940 = TVE_FlutterParams.y;
				half Motion_FacingMask5214_g73696 = 1.0;
				float temp_output_7_0_g73984 = TVE_MotionFadeEnd;
				half Motion_FadeOut4005_g73696 = saturate( ( ( distance( ase_worldPos , _WorldSpaceCameraPos ) - temp_output_7_0_g73984 ) / ( TVE_MotionFadeStart - temp_output_7_0_g73984 ) ) );
				half Motion_30_Amplitude4960_g73696 = ( _MotionAmplitude_32 * _MotionValue_30 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 * Motion_FacingMask5214_g73696 * Motion_FadeOut4005_g73696 );
				half Input_MotionAmplitude58_g73940 = Motion_30_Amplitude4960_g73696;
				half Global_Power354_g73940 = TVE_FlutterParams.x;
				float enc59_g73978 = v.ase_texcoord.z;
				float2 localDecodeFloatToVector259_g73978 = DecodeFloatToVector2( enc59_g73978 );
				float2 break61_g73978 = localDecodeFloatToVector259_g73978;
				half Mesh_Motion_30144_g73696 = break61_g73978.y;
				half Input_MeshMotion_30374_g73940 = Mesh_Motion_30144_g73696;
				half Input_MotionNormal364_g73940 = _MotionNormalValue;
				float3 lerpResult370_g73940 = lerp( float3( 1,1,1 ) , v.ase_normal , Input_MotionNormal364_g73940);
				half3 Motion_30_Details263_g73696 = ( ( sin( ( ( ( ase_worldPos.x + ase_worldPos.y + ase_worldPos.z ) * Input_MotionScale321_g73940 ) + ( Input_MotionVariation330_g73940 * Input_GlobalVariation372_g73940 ) + ( _TimeParameters.x * Input_MotionSpeed62_g73940 * Global_Speed350_g73940 ) ) ) * Input_MotionAmplitude58_g73940 * Global_Power354_g73940 * Input_MeshMotion_30374_g73940 * 0.4 ) * lerpResult370_g73940 );
				float3 Vertex_Motion_Object833_g73696 = ( ( VertexPosRotationAxis50_g73948 + ( VertexPosOtherAxis82_g73948 * cos( Angle44_g73948 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g73948 ) * sin( Angle44_g73948 ) ) ) + Motion_30_Details263_g73696 );
				float3 temp_output_3474_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				float3 appendResult2043_g73696 = (float3(Motion_10_BendingX216_g73696 , 0.0 , Motion_10_BendingZ190_g73696));
				float3 Vertex_Motion_World1118_g73696 = ( ( temp_output_3474_0_g73696 + appendResult2043_g73696 ) + Motion_30_Details263_g73696 );
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch4976_g73696 = Vertex_Motion_World1118_g73696;
				#else
				float3 staticSwitch4976_g73696 = ( Vertex_Motion_Object833_g73696 + ( _VertexDataMode * 0.0 ) );
				#endif
				float4 temp_output_94_19_g73959 = TVE_VertexCoords;
				half2 UV97_g73959 = ( (temp_output_94_19_g73959).zw + ( (temp_output_94_19_g73959).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g73959 = _LayerVertexValue;
				float4 lerpResult109_g73959 = lerp( TVE_VertexParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_VertexTex, sampler_linear_clamp, UV97_g73959,temp_output_84_0_g73959, 0.0 ) , TVE_VertexUsage[(int)temp_output_84_0_g73959]);
				half4 Global_Object_Params4173_g73696 = lerpResult109_g73959;
				half Global_VertexSize174_g73696 = saturate( Global_Object_Params4173_g73696.w );
				float lerpResult346_g73696 = lerp( 1.0 , Global_VertexSize174_g73696 , _GlobalSize);
				float3 appendResult3480_g73696 = (float3(lerpResult346_g73696 , lerpResult346_g73696 , lerpResult346_g73696));
				half3 ObjectData20_g73993 = appendResult3480_g73696;
				half3 _Vector11 = half3(1,1,1);
				half3 WorldData19_g73993 = _Vector11;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73993 = WorldData19_g73993;
				#else
				float3 staticSwitch14_g73993 = ObjectData20_g73993;
				#endif
				half3 Vertex_Size1741_g73696 = staticSwitch14_g73993;
				half3 _Vector5 = half3(1,1,1);
				float temp_output_7_0_g73931 = _SizeFadeEndValue;
				float temp_output_335_0_g73696 = saturate( ( ( ( distance( _WorldSpaceCameraPos , ObjectPosition4223_g73696 ) * ( 1.0 / TVE_DistanceFadeBias ) ) - temp_output_7_0_g73931 ) / ( _SizeFadeStartValue - temp_output_7_0_g73931 ) ) );
				float3 appendResult3482_g73696 = (float3(temp_output_335_0_g73696 , temp_output_335_0_g73696 , temp_output_335_0_g73696));
				float3 lerpResult5065_g73696 = lerp( _Vector5 , appendResult3482_g73696 , _SizeFadeMode);
				half3 ObjectData20_g73958 = lerpResult5065_g73696;
				half3 WorldData19_g73958 = _Vector5;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73958 = WorldData19_g73958;
				#else
				float3 staticSwitch14_g73958 = ObjectData20_g73958;
				#endif
				float3 Vertex_SizeFade1740_g73696 = staticSwitch14_g73958;
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - ase_worldPos );
				ase_worldViewDir = normalize(ase_worldViewDir);
				float3 normalizeResult2696_g73696 = normalize( ase_worldViewDir );
				float3 break2709_g73696 = cross( normalizeResult2696_g73696 , half3(0,1,0) );
				float3 appendResult2710_g73696 = (float3(-break2709_g73696.z , 0.0 , break2709_g73696.x));
				float3 appendResult2667_g73696 = (float3(Global_MeshVariation5104_g73696 , 0.5 , Global_MeshVariation5104_g73696));
				float3 normalizeResult2169_g73696 = normalize( ase_worldViewDir );
				float3 ViewDir_Normalized3963_g73696 = normalizeResult2169_g73696;
				float dotResult2212_g73696 = dot( ViewDir_Normalized3963_g73696 , float3(0,1,0) );
				half Mask_HView2656_g73696 = dotResult2212_g73696;
				float saferPower2652_g73696 = abs( Mask_HView2656_g73696 );
				half3 Grass_Perspective2661_g73696 = ( ( ( mul( GetWorldToObjectMatrix(), float4( appendResult2710_g73696 , 0.0 ) ).xyz * _PerspectivePushValue ) + ( (appendResult2667_g73696*2.0 + -1.0) * _PerspectiveNoiseValue ) ) * Mesh_Height1524_g73696 * pow( saferPower2652_g73696 , _PerspectiveAngleValue ) );
				float3 lerpResult16_g74000 = lerp( VertexPosition3588_g73696 , ( ( staticSwitch4976_g73696 * Vertex_Size1741_g73696 * Vertex_SizeFade1740_g73696 ) + Mesh_PivotsOS2291_g73696 + Grass_Perspective2661_g73696 ) , TVE_Enabled);
				float3 Final_VertexPosition890_g73696 = ( lerpResult16_g74000 + _DisableSRPBatcher );
				
				o.ase_texcoord5.xyz = vertexToFrag3890_g73696;
				o.ase_texcoord6.xyz = vertexToFrag4224_g73696;
				
				o.ase_texcoord4 = v.ase_texcoord;
				o.ase_color = v.ase_color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord5.w = 0;
				o.ase_texcoord6.w = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif

				float3 vertexValue = Final_VertexPosition890_g73696;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;
				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				float3 normalWS = TransformObjectToWorldNormal( v.ase_normal );
				float4 tangentWS = float4(TransformObjectToWorldDir( v.ase_tangent.xyz), v.ase_tangent.w);
				float4 positionCS = TransformWorldToHClip( positionWS );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					o.worldPos = positionWS;
				#endif

				o.worldNormal = normalWS;
				o.worldTangent = tangentWS;

				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = positionCS;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif

				o.clipPos = positionCS;

				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_tangent : TANGENT;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_tangent = v.ase_tangent;
				o.ase_texcoord3 = v.ase_texcoord3;
				o.ase_color = v.ase_color;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
				return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_tangent = patch[0].ase_tangent * bary.x + patch[1].ase_tangent * bary.y + patch[2].ase_tangent * bary.z;
				o.ase_texcoord3 = patch[0].ase_texcoord3 * bary.x + patch[1].ase_texcoord3 * bary.y + patch[2].ase_texcoord3 * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			#if defined(ASE_EARLY_Z_DEPTH_OPTIMIZE)
				#define ASE_SV_DEPTH SV_DepthLessEqual  
			#else
				#define ASE_SV_DEPTH SV_Depth
			#endif

			half4 frag(	VertexOutput IN 
						#ifdef ASE_DEPTH_WRITE_ON
						,out float outputDepth : ASE_SV_DEPTH
						#endif
						, FRONT_FACE_TYPE ase_vface : FRONT_FACE_SEMANTIC ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
					float3 WorldPosition = IN.worldPos;
				#endif

				float4 ShadowCoords = float4( 0, 0, 0, 0 );
				float3 WorldNormal = IN.worldNormal;
				float4 WorldTangent = IN.worldTangent;

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				half2 Main_UVs15_g73696 = ( ( IN.ase_texcoord4.xy * (_MainUVs).xy ) + (_MainUVs).zw );
				float3 unpack4112_g73696 = UnpackNormalScale( tex2D( _MainNormalTex, Main_UVs15_g73696 ), _MainNormalValue );
				unpack4112_g73696.z = lerp( 1, unpack4112_g73696.z, saturate(_MainNormalValue) );
				half3 Main_Normal137_g73696 = unpack4112_g73696;
				float3 temp_output_13_0_g73938 = Main_Normal137_g73696;
				float3 switchResult12_g73938 = (((ase_vface>0)?(temp_output_13_0_g73938):(( temp_output_13_0_g73938 * _render_normals ))));
				half3 Blend_Normal312_g73696 = switchResult12_g73938;
				half3 Final_Normal366_g73696 = Blend_Normal312_g73696;
				
				float localCustomAlphaClip19_g74020 = ( 0.0 );
				float4 tex2DNode29_g73696 = tex2D( _MainAlbedoTex, Main_UVs15_g73696 );
				float Main_Alpha316_g73696 = ( _MainColor.a * tex2DNode29_g73696.a );
				float4 temp_output_93_19_g73986 = TVE_ExtrasCoords;
				float3 vertexToFrag3890_g73696 = IN.ase_texcoord5.xyz;
				float3 WorldPosition3905_g73696 = vertexToFrag3890_g73696;
				float3 vertexToFrag4224_g73696 = IN.ase_texcoord6.xyz;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				float3 lerpResult4827_g73696 = lerp( WorldPosition3905_g73696 , ObjectPosition4223_g73696 , _ExtrasPositionMode);
				half2 UV96_g73986 = ( (temp_output_93_19_g73986).zw + ( (temp_output_93_19_g73986).xy * (lerpResult4827_g73696).xz ) );
				float temp_output_84_0_g73986 = _LayerExtrasValue;
				float4 lerpResult109_g73986 = lerp( TVE_ExtrasParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_ExtrasTex, sampler_linear_clamp, UV96_g73986,temp_output_84_0_g73986, 0.0 ) , TVE_ExtrasUsage[(int)temp_output_84_0_g73986]);
				float4 break89_g73986 = lerpResult109_g73986;
				half Global_Extras_Alpha1033_g73696 = saturate( break89_g73986.a );
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = IN.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				float lerpResult5154_g73696 = lerp( 0.0 , Global_MeshVariation5104_g73696 , _AlphaVariationValue);
				half Global_Alpha_Variation5158_g73696 = lerpResult5154_g73696;
				half AlphaTreshold2132_g73696 = _AlphaClipValue;
				half Global_Alpha_Mask4546_g73696 = 1.0;
				float lerpResult5203_g73696 = lerp( 1.0 , ( ( Global_Extras_Alpha1033_g73696 - Global_Alpha_Variation5158_g73696 ) + AlphaTreshold2132_g73696 + ( Global_Extras_Alpha1033_g73696 * 0.5 ) ) , ( Global_Alpha_Mask4546_g73696 * _GlobalAlpha ));
				float lerpResult16_g74016 = lerp( 1.0 , lerpResult5203_g73696 , TVE_Enabled);
				half Global_Alpha315_g73696 = lerpResult16_g74016;
				half Fade_Alpha3727_g73696 = 1.0;
				float temp_output_661_0_g73696 = ( Main_Alpha316_g73696 * Global_Alpha315_g73696 * Fade_Alpha3727_g73696 );
				half Alpha34_g74021 = temp_output_661_0_g73696;
				half Offest27_g74021 = AlphaTreshold2132_g73696;
				half AlphaFeather5305_g73696 = _AlphaFeatherValue;
				half Feather30_g74021 = AlphaFeather5305_g73696;
				float temp_output_25_0_g74021 = ( ( ( Alpha34_g74021 - Offest27_g74021 ) / ( max( fwidth( Alpha34_g74021 ) , 0.001 ) + Feather30_g74021 ) ) + Offest27_g74021 );
				float temp_output_3_0_g74020 = temp_output_25_0_g74021;
				float Alpha19_g74020 = temp_output_3_0_g74020;
				float temp_output_15_0_g74020 = AlphaTreshold2132_g73696;
				float Treshold19_g74020 = temp_output_15_0_g74020;
				{
				#if defined (TVE_FEATURE_CLIP)
				#if defined (TVE_IS_HD_PIPELINE)
				#if !defined (SHADERPASS_FORWARD_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#if !defined (SHADERPASS_GBUFFER_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#else
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#endif
				}
				half Final_Alpha914_g73696 = saturate( Alpha19_g74020 );
				

				float3 Normal = Final_Normal366_g73696;
				float Alpha = Final_Alpha914_g73696;
				float AlphaClipThreshold = 0.5;
				#ifdef ASE_DEPTH_WRITE_ON
					float DepthValue = 0;
				#endif

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				
				#ifdef ASE_DEPTH_WRITE_ON
					outputDepth = DepthValue;
				#endif
				
				#if defined(_GBUFFER_NORMALS_OCT)
					float2 octNormalWS = PackNormalOctQuadEncode(WorldNormal);
					float2 remappedOctNormalWS = saturate(octNormalWS * 0.5 + 0.5);
					half3 packedNormalWS = PackFloat2To888(remappedOctNormalWS);
					return half4(packedNormalWS, 0.0);
				#else					
					#if defined(_NORMALMAP)
						#if _NORMAL_DROPOFF_TS
							float crossSign = (WorldTangent.w > 0.0 ? 1.0 : -1.0) * GetOddNegativeScale();
							float3 bitangent = crossSign * cross(WorldNormal.xyz, WorldTangent.xyz);
							float3 normalWS = TransformTangentToWorld(Normal, half3x3(WorldTangent.xyz, bitangent, WorldNormal.xyz));
						#elif _NORMAL_DROPOFF_OS
							float3 normalWS = TransformObjectToWorldNormal(Normal);
						#elif _NORMAL_DROPOFF_WS
							float3 normalWS = Normal;
						#endif
					#else
						float3 normalWS = WorldNormal;
					#endif
					return half4(NormalizeNormalPerPixel(normalWS), 0.0);
				#endif
			}
			ENDHLSL
		}
	//-------------------------------------------------------------------------------------
	// END_PASS DEPTHNORMALS
	//-------------------------------------------------------------------------------------

		
	//-------------------------------------------------------------------------------------
	// BEGIN_PASS SCENESELECTIONPASS
	//-------------------------------------------------------------------------------------
		Pass
		{
			
			Name "SceneSelectionPass"
			Tags { "LightMode"="SceneSelectionPass" }
        
			Cull Off

	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			HLSLPROGRAM
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

#include "./../../../../../GPUInstancer/Shaders/Include/GPUInstancerInclude.cginc"
#pragma instancing_options procedural:setupGPUI
#pragma multi_compile_instancing

        
			#define _SPECULAR_SETUP 1
			#define _NORMAL_DROPOFF_TS 1
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#pragma multi_compile _ DOTS_INSTANCING_ON
			#define ASE_ABSOLUTE_VERTEX_POS 1
			#define _TRANSLUCENCY_ASE 1
			#define _EMISSION
			#define _NORMALMAP 1
			#define ASE_SRP_VERSION 120100

        
			#pragma only_renderers d3d11 glcore gles gles3 
			#pragma vertex vert
			#pragma fragment frag

			#define SCENESELECTIONPASS 1

			#define ATTRIBUTES_NEED_NORMAL
			#define ATTRIBUTES_NEED_TANGENT
			#define SHADERPASS SHADERPASS_DEPTHONLY

			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

			//-----------------------------------------------------------------------------
			// DEFINES - (Set by User)
			//-----------------------------------------------------------------------------

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#pragma shader_feature_local TVE_FEATURE_CLIP
			#pragma shader_feature_local TVE_FEATURE_BATCHING
			//TVE Injection Defines
			//SHADER INJECTION POINT BEGIN
			//SHADER INJECTION POINT END
			//TVE Pipeline Defines
			#define THE_VEGETATION_ENGINE
			#define TVE_IS_UNIVERSAL_PIPELINE
			//TVE Shader Type Defines
			#define TVE_IS_GRASS_SHADER


	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
        
	//-------------------------------------------------------------------------------------
	// BEGIN_CBUFFER
	//-------------------------------------------------------------------------------------

			CBUFFER_START(UnityPerMaterial)
			float4 _SubsurfaceDiffusion_asset;
			float4 _NoiseMaskRemap;
			float4 _SubsurfaceDiffusion_Asset;
			half4 _MotionHighlightColor;
			half4 _MainColor;
			half4 _MainUVs;
			half4 _VertexOcclusionColor;
			half4 _OverlayMaskRemap;
			half4 _AlphaMaskRemap;
			half4 _DetailBlendRemap;
			float4 _Color;
			half4 _GradientColorOne;
			half4 _SubsurfaceMaskRemap;
			float4 _GradientMaskRemap;
			half4 _ColorsMaskRemap;
			half4 _GradientColorTwo;
			float4 _EmissiveIntensityParams;
			half4 _VertexOcclusionRemap;
			half4 _SubsurfaceColor;
			half4 _EmissiveUVs;
			float4 _MaxBoundsInfo;
			half4 _EmissiveColor;
			half3 _render_normals;
			half _PerspectiveAngleValue;
			half _LayerMotionValue;
			half _MotionAmplitude_10;
			float _MotionScale_10;
			float _MotionSpeed_10;
			half _MotionVariation_10;
			half _PerspectiveNoiseValue;
			half _PerspectivePushValue;
			half _VertexDynamicMode;
			half _SizeFadeEndValue;
			half _SizeFadeStartValue;
			half _GlobalSize;
			half _LayerVertexValue;
			half _VertexDataMode;
			half _MotionNormalValue;
			half _MotionValue_30;
			half _InteractionAmplitude;
			half _InteractionMaskValue;
			float _MotionSpeed_32;
			float _MotionVariation_32;
			float _MotionScale_32;
			half _SizeFadeMode;
			half _MotionAmplitude_32;
			half _IsSubsurfaceShader;
			half _GradientMaxValue;
			half _SubsurfaceValue;
			half _GlobalAlpha;
			half _AlphaVariationValue;
			half _MainOcclusionValue;
			half _GlobalWetness;
			half _MainSmoothnessValue;
			half _RenderSpecular;
			half _GlobalEmissive;
			half _MainNormalValue;
			half _OverlayMaskMaxValue;
			half _OverlayMaskMinValue;
			half _GradientMinValue;
			half _OverlayVariationValue;
			half _ExtrasPositionMode;
			half _GlobalOverlay;
			half _ColorsMaskMaxValue;
			half _ColorsMaskMinValue;
			half _ColorsVariationValue;
			half _GlobalColors;
			half _LayerColorsValue;
			half _ColorsPositionMode;
			half _VertexOcclusionMaxValue;
			half _VertexOcclusionMinValue;
			half _VertexPivotMode;
			half _LayerExtrasValue;
			half _render_coverage;
			half _RenderMode;
			half _render_dst;
			half _SubsurfaceAngleValue;
			half _HasGradient;
			half _IsTVEShader;
			half _SpaceGlobalPosition;
			half _VertexVariationMode;
			half _AlphaFeatherValue;
			half _MessageGlobalsVariation;
			half _SubsurfaceShadowValue;
			half _RenderZWrite;
			half _RenderDecals;
			half _RenderDirect;
			half _CategoryNoise;
			half _SubsurfaceScatteringValue;
			half _RenderClip;
			float _SubsurfaceDiffusion;
			half _EmissiveFlagMode;
			half _RenderSSR;
			half _CategoryGlobal;
			half _SubsurfaceNormalValue;
			half _MessageMotionVariation;
			half _SubsurfaceAmbientValue;
			half _RenderCull;
			half _HasOcclusion;
			half _Cutoff;
			half _SubsurfaceDirectValue;
			half _CategoryRender;
			half _RenderQueue;
			half _DetailBlendMode;
			half _CategoryPerspective;
			half _render_cull;
			half _RenderAmbient;
			half _CategoryDetail;
			half _render_src;
			half _render_zw;
			half _IsGrassShader;
			half _RenderPriority;
			half _SubsurfaceMaskMinValue;
			half _CategoryGradient;
			half _LayerReactValue;
			half _RenderNormals;
			half _SpaceGlobalLayers;
			half _AlphaClipValue;
			half _VertexRollingMode;
			half _IsVersion;
			half _RenderCoverage;
			half _MessageSizeFade;
			half _CategoryEmissive;
			half _CategoryOcclusion;
			half _CategoryMain;
			half _CategorySizeFade;
			half _CategoryMotion;
			half _DetailMode;
			half _DetailTypeMode;
			half _HasEmissive;
			half _SpaceGlobalLocals;
			half _VertexMasksMode;
			half _RenderShadow;
			half _SpaceMotionGlobals;
			half _CategorySubsurface;
			half _SpaceRenderFade;
			half _SpaceMotionLocals;
			half _SubsurfaceMaskMaxValue;
			#ifdef _TRANSMISSION_ASE
				float _TransmissionShadow;
			#endif
			#ifdef _TRANSLUCENCY_ASE
				float _TransStrength;
				float _TransNormal;
				float _TransScattering;
				float _TransDirect;
				float _TransAmbient;
				float _TransShadow;
			#endif
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END

			// Property used by ScenePickingPass
			#ifdef SCENEPICKINGPASS
				float4 _SelectionID;
			#endif

			// Properties used by SceneSelectionPass
			#ifdef SCENESELECTIONPASS
				int _ObjectId;
				int _PassValue;
			#endif

	//-------------------------------------------------------------------------------------
	// END_CBUFFER
	//-------------------------------------------------------------------------------------

			sampler2D _MainTex;
			half TVE_Enabled;
			sampler2D _BumpMap;
			half4 TVE_MotionParams;
			TEXTURE2D_ARRAY(TVE_MotionTex);
			half4 TVE_MotionCoords;
			SAMPLER(sampler_linear_clamp);
			float TVE_MotionUsage[10];
			sampler2D TVE_NoiseTex;
			half4 TVE_NoiseParams;
			half4 TVE_FlutterParams;
			half TVE_MotionFadeEnd;
			half TVE_MotionFadeStart;
			half4 TVE_VertexParams;
			TEXTURE2D_ARRAY(TVE_VertexTex);
			half4 TVE_VertexCoords;
			float TVE_VertexUsage[10];
			half TVE_DistanceFadeBias;
			half _DisableSRPBatcher;
			sampler2D _MainAlbedoTex;
			half4 TVE_ExtrasParams;
			TEXTURE2D_ARRAY(TVE_ExtrasTex);
			half4 TVE_ExtrasCoords;
			float TVE_ExtrasUsage[10];


	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/SelectionPickingPass.hlsl"

			//#ifdef HAVE_VFX_MODIFICATION
			//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/VisualEffectVertex.hlsl"
			//#endif

	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			float2 DecodeFloatToVector2( float enc )
			{
				float2 result ;
				result.y = enc % 2048;
				result.x = floor(enc / 2048);
				return result / (2048 - 1);
			}
			

			struct SurfaceDescription
			{
				float Alpha;
				float AlphaClipThreshold;
			};
        
			VertexOutput VertexFunction(VertexInput v  )
			{
				VertexOutput o;
				ZERO_INITIALIZE(VertexOutput, o);

				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 VertexPosition3588_g73696 = v.vertex.xyz;
				float3 appendResult60_g74015 = (float3(v.ase_texcoord3.x , v.ase_texcoord3.z , v.ase_texcoord3.y));
				half3 Mesh_PivotsData2831_g73696 = ( appendResult60_g74015 * _VertexPivotMode );
				half3 Mesh_PivotsOS2291_g73696 = Mesh_PivotsData2831_g73696;
				float3 temp_output_2283_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				half3 VertexPos40_g73968 = temp_output_2283_0_g73696;
				float3 appendResult74_g73968 = (float3(VertexPos40_g73968.x , 0.0 , 0.0));
				half3 VertexPosRotationAxis50_g73968 = appendResult74_g73968;
				float3 break84_g73968 = VertexPos40_g73968;
				float3 appendResult81_g73968 = (float3(0.0 , break84_g73968.y , break84_g73968.z));
				half3 VertexPosOtherAxis82_g73968 = appendResult81_g73968;
				float4 temp_output_91_19_g74003 = TVE_MotionCoords;
				float4x4 break19_g73996 = GetObjectToWorldMatrix();
				float3 appendResult20_g73996 = (float3(break19_g73996[ 0 ][ 3 ] , break19_g73996[ 1 ][ 3 ] , break19_g73996[ 2 ][ 3 ]));
				float3 temp_output_122_0_g73996 = Mesh_PivotsData2831_g73696;
				float3 PivotsOnly105_g73996 = (mul( GetObjectToWorldMatrix(), float4( temp_output_122_0_g73996 , 0.0 ) ).xyz).xyz;
				half3 ObjectData20_g73997 = ( appendResult20_g73996 + PivotsOnly105_g73996 );
				float3 ase_worldPos = mul(GetObjectToWorldMatrix(), v.vertex).xyz;
				half3 WorldData19_g73997 = ase_worldPos;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73997 = WorldData19_g73997;
				#else
				float3 staticSwitch14_g73997 = ObjectData20_g73997;
				#endif
				float3 temp_output_114_0_g73996 = staticSwitch14_g73997;
				float3 vertexToFrag4224_g73696 = temp_output_114_0_g73996;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				float3 vertexToFrag3890_g73696 = ase_worldPos;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				half2 UV94_g74003 = ( (temp_output_91_19_g74003).zw + ( (temp_output_91_19_g74003).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g74003 = _LayerMotionValue;
				float4 lerpResult107_g74003 = lerp( TVE_MotionParams , saturate( SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_MotionTex, sampler_linear_clamp, UV94_g74003,temp_output_84_0_g74003, 0.0 ) ) , TVE_MotionUsage[(int)temp_output_84_0_g74003]);
				half4 Global_Motion_Params3909_g73696 = lerpResult107_g74003;
				float4 break322_g73985 = Global_Motion_Params3909_g73696;
				float3 appendResult397_g73985 = (float3(break322_g73985.x , 0.0 , break322_g73985.y));
				float3 temp_output_398_0_g73985 = (appendResult397_g73985*2.0 + -1.0);
				float3 ase_parentObjectScale = ( 1.0 / float3( length( GetWorldToObjectMatrix()[ 0 ].xyz ), length( GetWorldToObjectMatrix()[ 1 ].xyz ), length( GetWorldToObjectMatrix()[ 2 ].xyz ) ) );
				half2 Global_MotionDirectionOS39_g73696 = (( mul( GetWorldToObjectMatrix(), float4( temp_output_398_0_g73985 , 0.0 ) ).xyz * ase_parentObjectScale )).xz;
				half2 Input_DirectionOS358_g73990 = Global_MotionDirectionOS39_g73696;
				half Wind_Power369_g73985 = break322_g73985.z;
				half Global_WindPower2223_g73696 = Wind_Power369_g73985;
				half3 Input_Position419_g73963 = ObjectPosition4223_g73696;
				float Input_MotionScale287_g73963 = ( _MotionScale_10 + 1.0 );
				half Global_Scale448_g73963 = TVE_NoiseParams.x;
				float2 temp_output_597_0_g73963 = (( Input_Position419_g73963 * Input_MotionScale287_g73963 * Global_Scale448_g73963 * 0.0075 )).xz;
				half2 Global_MotionDirectionWS4683_g73696 = (temp_output_398_0_g73985).xz;
				half2 Input_DirectionWS423_g73963 = Global_MotionDirectionWS4683_g73696;
				half Input_MotionSpeed62_g73963 = _MotionSpeed_10;
				half Global_Speed449_g73963 = TVE_NoiseParams.y;
				half Input_MotionVariation284_g73963 = _MotionVariation_10;
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = v.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				half Input_GlobalVariation569_g73963 = Global_MeshVariation5104_g73696;
				float temp_output_630_0_g73963 = ( ( ( _TimeParameters.x * Input_MotionSpeed62_g73963 * Global_Speed449_g73963 ) + ( Input_MotionVariation284_g73963 * Input_GlobalVariation569_g73963 ) ) * 0.03 );
				float temp_output_607_0_g73963 = frac( temp_output_630_0_g73963 );
				float4 lerpResult590_g73963 = lerp( tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * temp_output_607_0_g73963 ) ), 0, 0.0) ) , tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * frac( ( temp_output_630_0_g73963 + 0.5 ) ) ) ), 0, 0.0) ) , ( abs( ( temp_output_607_0_g73963 - 0.5 ) ) / 0.5 ));
				half Input_GlobalWind327_g73963 = Global_WindPower2223_g73696;
				float lerpResult612_g73963 = lerp( 1.4 , 0.4 , Input_GlobalWind327_g73963);
				float3 temp_cast_7 = (lerpResult612_g73963).xxx;
				float3 break638_g73963 = (pow( ( abs( (lerpResult590_g73963).rgb ) + 0.2 ) , temp_cast_7 )*1.4 + -0.2);
				half Global_NoiseTexR34_g73696 = break638_g73963.x;
				half Motion_10_Amplitude2258_g73696 = ( _MotionAmplitude_10 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 );
				half Input_BendingAmplitude376_g73990 = Motion_10_Amplitude2258_g73696;
				half Mesh_Height1524_g73696 = v.ase_color.a;
				half Input_MeshHeight388_g73990 = Mesh_Height1524_g73696;
				half ObjectData20_g73992 = ( Input_MeshHeight388_g73990 * 2.0 );
				float enc62_g73978 = v.ase_texcoord.w;
				float2 localDecodeFloatToVector262_g73978 = DecodeFloatToVector2( enc62_g73978 );
				float2 break63_g73978 = ( localDecodeFloatToVector262_g73978 * 100.0 );
				float Bounds_Height5230_g73696 = break63_g73978.x;
				half Input_BoundsHeight390_g73990 = Bounds_Height5230_g73696;
				half WorldData19_g73992 = ( ( Input_MeshHeight388_g73990 * Input_MeshHeight388_g73990 ) * Input_BoundsHeight390_g73990 * 2.0 );
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73992 = WorldData19_g73992;
				#else
				float staticSwitch14_g73992 = ObjectData20_g73992;
				#endif
				half Mask_Motion_10321_g73990 = staticSwitch14_g73992;
				half Input_InteractionAmplitude58_g73990 = _InteractionAmplitude;
				half Input_InteractionUseMask62_g73990 = _InteractionMaskValue;
				float lerpResult371_g73990 = lerp( 2.0 , Mask_Motion_10321_g73990 , Input_InteractionUseMask62_g73990);
				half ObjectData20_g73991 = lerpResult371_g73990;
				half WorldData19_g73991 = Mask_Motion_10321_g73990;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73991 = WorldData19_g73991;
				#else
				float staticSwitch14_g73991 = ObjectData20_g73991;
				#endif
				half Mask_Interaction373_g73990 = ( Input_InteractionAmplitude58_g73990 * staticSwitch14_g73991 );
				half Global_InteractionMask66_g73696 = ( break322_g73985.w * break322_g73985.w );
				float Input_InteractionGlobalMask330_g73990 = Global_InteractionMask66_g73696;
				float lerpResult360_g73990 = lerp( ( Input_BendingAmplitude376_g73990 * Mask_Motion_10321_g73990 ) , Mask_Interaction373_g73990 , saturate( ( Input_InteractionAmplitude58_g73990 * Input_InteractionGlobalMask330_g73990 ) ));
				float2 break364_g73990 = ( Input_DirectionOS358_g73990 * lerpResult360_g73990 );
				half Motion_10_BendingZ190_g73696 = break364_g73990.y;
				half Angle44_g73968 = Motion_10_BendingZ190_g73696;
				half3 VertexPos40_g73948 = ( VertexPosRotationAxis50_g73968 + ( VertexPosOtherAxis82_g73968 * cos( Angle44_g73968 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g73968 ) * sin( Angle44_g73968 ) ) );
				float3 appendResult74_g73948 = (float3(0.0 , 0.0 , VertexPos40_g73948.z));
				half3 VertexPosRotationAxis50_g73948 = appendResult74_g73948;
				float3 break84_g73948 = VertexPos40_g73948;
				float3 appendResult81_g73948 = (float3(break84_g73948.x , break84_g73948.y , 0.0));
				half3 VertexPosOtherAxis82_g73948 = appendResult81_g73948;
				half Motion_10_BendingX216_g73696 = break364_g73990.x;
				half Angle44_g73948 = -Motion_10_BendingX216_g73696;
				half Input_MotionScale321_g73940 = _MotionScale_32;
				half Input_MotionVariation330_g73940 = _MotionVariation_32;
				half Input_GlobalVariation372_g73940 = Global_MeshVariation5104_g73696;
				half Input_MotionSpeed62_g73940 = _MotionSpeed_32;
				half Global_Speed350_g73940 = TVE_FlutterParams.y;
				half Motion_FacingMask5214_g73696 = 1.0;
				float temp_output_7_0_g73984 = TVE_MotionFadeEnd;
				half Motion_FadeOut4005_g73696 = saturate( ( ( distance( ase_worldPos , _WorldSpaceCameraPos ) - temp_output_7_0_g73984 ) / ( TVE_MotionFadeStart - temp_output_7_0_g73984 ) ) );
				half Motion_30_Amplitude4960_g73696 = ( _MotionAmplitude_32 * _MotionValue_30 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 * Motion_FacingMask5214_g73696 * Motion_FadeOut4005_g73696 );
				half Input_MotionAmplitude58_g73940 = Motion_30_Amplitude4960_g73696;
				half Global_Power354_g73940 = TVE_FlutterParams.x;
				float enc59_g73978 = v.ase_texcoord.z;
				float2 localDecodeFloatToVector259_g73978 = DecodeFloatToVector2( enc59_g73978 );
				float2 break61_g73978 = localDecodeFloatToVector259_g73978;
				half Mesh_Motion_30144_g73696 = break61_g73978.y;
				half Input_MeshMotion_30374_g73940 = Mesh_Motion_30144_g73696;
				half Input_MotionNormal364_g73940 = _MotionNormalValue;
				float3 lerpResult370_g73940 = lerp( float3( 1,1,1 ) , v.ase_normal , Input_MotionNormal364_g73940);
				half3 Motion_30_Details263_g73696 = ( ( sin( ( ( ( ase_worldPos.x + ase_worldPos.y + ase_worldPos.z ) * Input_MotionScale321_g73940 ) + ( Input_MotionVariation330_g73940 * Input_GlobalVariation372_g73940 ) + ( _TimeParameters.x * Input_MotionSpeed62_g73940 * Global_Speed350_g73940 ) ) ) * Input_MotionAmplitude58_g73940 * Global_Power354_g73940 * Input_MeshMotion_30374_g73940 * 0.4 ) * lerpResult370_g73940 );
				float3 Vertex_Motion_Object833_g73696 = ( ( VertexPosRotationAxis50_g73948 + ( VertexPosOtherAxis82_g73948 * cos( Angle44_g73948 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g73948 ) * sin( Angle44_g73948 ) ) ) + Motion_30_Details263_g73696 );
				float3 temp_output_3474_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				float3 appendResult2043_g73696 = (float3(Motion_10_BendingX216_g73696 , 0.0 , Motion_10_BendingZ190_g73696));
				float3 Vertex_Motion_World1118_g73696 = ( ( temp_output_3474_0_g73696 + appendResult2043_g73696 ) + Motion_30_Details263_g73696 );
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch4976_g73696 = Vertex_Motion_World1118_g73696;
				#else
				float3 staticSwitch4976_g73696 = ( Vertex_Motion_Object833_g73696 + ( _VertexDataMode * 0.0 ) );
				#endif
				float4 temp_output_94_19_g73959 = TVE_VertexCoords;
				half2 UV97_g73959 = ( (temp_output_94_19_g73959).zw + ( (temp_output_94_19_g73959).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g73959 = _LayerVertexValue;
				float4 lerpResult109_g73959 = lerp( TVE_VertexParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_VertexTex, sampler_linear_clamp, UV97_g73959,temp_output_84_0_g73959, 0.0 ) , TVE_VertexUsage[(int)temp_output_84_0_g73959]);
				half4 Global_Object_Params4173_g73696 = lerpResult109_g73959;
				half Global_VertexSize174_g73696 = saturate( Global_Object_Params4173_g73696.w );
				float lerpResult346_g73696 = lerp( 1.0 , Global_VertexSize174_g73696 , _GlobalSize);
				float3 appendResult3480_g73696 = (float3(lerpResult346_g73696 , lerpResult346_g73696 , lerpResult346_g73696));
				half3 ObjectData20_g73993 = appendResult3480_g73696;
				half3 _Vector11 = half3(1,1,1);
				half3 WorldData19_g73993 = _Vector11;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73993 = WorldData19_g73993;
				#else
				float3 staticSwitch14_g73993 = ObjectData20_g73993;
				#endif
				half3 Vertex_Size1741_g73696 = staticSwitch14_g73993;
				half3 _Vector5 = half3(1,1,1);
				float temp_output_7_0_g73931 = _SizeFadeEndValue;
				float temp_output_335_0_g73696 = saturate( ( ( ( distance( _WorldSpaceCameraPos , ObjectPosition4223_g73696 ) * ( 1.0 / TVE_DistanceFadeBias ) ) - temp_output_7_0_g73931 ) / ( _SizeFadeStartValue - temp_output_7_0_g73931 ) ) );
				float3 appendResult3482_g73696 = (float3(temp_output_335_0_g73696 , temp_output_335_0_g73696 , temp_output_335_0_g73696));
				float3 lerpResult5065_g73696 = lerp( _Vector5 , appendResult3482_g73696 , _SizeFadeMode);
				half3 ObjectData20_g73958 = lerpResult5065_g73696;
				half3 WorldData19_g73958 = _Vector5;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73958 = WorldData19_g73958;
				#else
				float3 staticSwitch14_g73958 = ObjectData20_g73958;
				#endif
				float3 Vertex_SizeFade1740_g73696 = staticSwitch14_g73958;
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - ase_worldPos );
				ase_worldViewDir = normalize(ase_worldViewDir);
				float3 normalizeResult2696_g73696 = normalize( ase_worldViewDir );
				float3 break2709_g73696 = cross( normalizeResult2696_g73696 , half3(0,1,0) );
				float3 appendResult2710_g73696 = (float3(-break2709_g73696.z , 0.0 , break2709_g73696.x));
				float3 appendResult2667_g73696 = (float3(Global_MeshVariation5104_g73696 , 0.5 , Global_MeshVariation5104_g73696));
				float3 normalizeResult2169_g73696 = normalize( ase_worldViewDir );
				float3 ViewDir_Normalized3963_g73696 = normalizeResult2169_g73696;
				float dotResult2212_g73696 = dot( ViewDir_Normalized3963_g73696 , float3(0,1,0) );
				half Mask_HView2656_g73696 = dotResult2212_g73696;
				float saferPower2652_g73696 = abs( Mask_HView2656_g73696 );
				half3 Grass_Perspective2661_g73696 = ( ( ( mul( GetWorldToObjectMatrix(), float4( appendResult2710_g73696 , 0.0 ) ).xyz * _PerspectivePushValue ) + ( (appendResult2667_g73696*2.0 + -1.0) * _PerspectiveNoiseValue ) ) * Mesh_Height1524_g73696 * pow( saferPower2652_g73696 , _PerspectiveAngleValue ) );
				float3 lerpResult16_g74000 = lerp( VertexPosition3588_g73696 , ( ( staticSwitch4976_g73696 * Vertex_Size1741_g73696 * Vertex_SizeFade1740_g73696 ) + Mesh_PivotsOS2291_g73696 + Grass_Perspective2661_g73696 ) , TVE_Enabled);
				float3 Final_VertexPosition890_g73696 = ( lerpResult16_g74000 + _DisableSRPBatcher );
				
				o.ase_texcoord1.xyz = vertexToFrag3890_g73696;
				o.ase_texcoord2.xyz = vertexToFrag4224_g73696;
				
				o.ase_texcoord = v.ase_texcoord;
				o.ase_color = v.ase_color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.w = 0;
				o.ase_texcoord2.w = 0;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif

				float3 vertexValue = Final_VertexPosition890_g73696;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				o.clipPos = TransformWorldToHClip(positionWS);

				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord3 = v.ase_texcoord3;
				o.ase_color = v.ase_color;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
				return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord3 = patch[0].ase_texcoord3 * bary.x + patch[1].ase_texcoord3 * bary.y + patch[2].ase_texcoord3 * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif
			
			half4 frag(VertexOutput IN ) : SV_TARGET
			{
				SurfaceDescription surfaceDescription = (SurfaceDescription)0;

				float localCustomAlphaClip19_g74020 = ( 0.0 );
				half2 Main_UVs15_g73696 = ( ( IN.ase_texcoord.xy * (_MainUVs).xy ) + (_MainUVs).zw );
				float4 tex2DNode29_g73696 = tex2D( _MainAlbedoTex, Main_UVs15_g73696 );
				float Main_Alpha316_g73696 = ( _MainColor.a * tex2DNode29_g73696.a );
				float4 temp_output_93_19_g73986 = TVE_ExtrasCoords;
				float3 vertexToFrag3890_g73696 = IN.ase_texcoord1.xyz;
				float3 WorldPosition3905_g73696 = vertexToFrag3890_g73696;
				float3 vertexToFrag4224_g73696 = IN.ase_texcoord2.xyz;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				float3 lerpResult4827_g73696 = lerp( WorldPosition3905_g73696 , ObjectPosition4223_g73696 , _ExtrasPositionMode);
				half2 UV96_g73986 = ( (temp_output_93_19_g73986).zw + ( (temp_output_93_19_g73986).xy * (lerpResult4827_g73696).xz ) );
				float temp_output_84_0_g73986 = _LayerExtrasValue;
				float4 lerpResult109_g73986 = lerp( TVE_ExtrasParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_ExtrasTex, sampler_linear_clamp, UV96_g73986,temp_output_84_0_g73986, 0.0 ) , TVE_ExtrasUsage[(int)temp_output_84_0_g73986]);
				float4 break89_g73986 = lerpResult109_g73986;
				half Global_Extras_Alpha1033_g73696 = saturate( break89_g73986.a );
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = IN.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				float lerpResult5154_g73696 = lerp( 0.0 , Global_MeshVariation5104_g73696 , _AlphaVariationValue);
				half Global_Alpha_Variation5158_g73696 = lerpResult5154_g73696;
				half AlphaTreshold2132_g73696 = _AlphaClipValue;
				half Global_Alpha_Mask4546_g73696 = 1.0;
				float lerpResult5203_g73696 = lerp( 1.0 , ( ( Global_Extras_Alpha1033_g73696 - Global_Alpha_Variation5158_g73696 ) + AlphaTreshold2132_g73696 + ( Global_Extras_Alpha1033_g73696 * 0.5 ) ) , ( Global_Alpha_Mask4546_g73696 * _GlobalAlpha ));
				float lerpResult16_g74016 = lerp( 1.0 , lerpResult5203_g73696 , TVE_Enabled);
				half Global_Alpha315_g73696 = lerpResult16_g74016;
				half Fade_Alpha3727_g73696 = 1.0;
				float temp_output_661_0_g73696 = ( Main_Alpha316_g73696 * Global_Alpha315_g73696 * Fade_Alpha3727_g73696 );
				half Alpha34_g74021 = temp_output_661_0_g73696;
				half Offest27_g74021 = AlphaTreshold2132_g73696;
				half AlphaFeather5305_g73696 = _AlphaFeatherValue;
				half Feather30_g74021 = AlphaFeather5305_g73696;
				float temp_output_25_0_g74021 = ( ( ( Alpha34_g74021 - Offest27_g74021 ) / ( max( fwidth( Alpha34_g74021 ) , 0.001 ) + Feather30_g74021 ) ) + Offest27_g74021 );
				float temp_output_3_0_g74020 = temp_output_25_0_g74021;
				float Alpha19_g74020 = temp_output_3_0_g74020;
				float temp_output_15_0_g74020 = AlphaTreshold2132_g73696;
				float Treshold19_g74020 = temp_output_15_0_g74020;
				{
				#if defined (TVE_FEATURE_CLIP)
				#if defined (TVE_IS_HD_PIPELINE)
				#if !defined (SHADERPASS_FORWARD_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#if !defined (SHADERPASS_GBUFFER_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#else
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#endif
				}
				half Final_Alpha914_g73696 = saturate( Alpha19_g74020 );
				

				surfaceDescription.Alpha = Final_Alpha914_g73696;
				surfaceDescription.AlphaClipThreshold = 0.5;

				#if _ALPHATEST_ON
					float alphaClipThreshold = 0.01f;
					#if ALPHA_CLIP_THRESHOLD
						alphaClipThreshold = surfaceDescription.AlphaClipThreshold;
					#endif
					clip(surfaceDescription.Alpha - alphaClipThreshold);
				#endif

				half4 outColor = 0;

				#ifdef SCENESELECTIONPASS
					outColor = half4(_ObjectId, _PassValue, 1.0, 1.0);
				#elif defined(SCENEPICKINGPASS)
					outColor = _SelectionID;
				#endif

				return outColor;
			}

			ENDHLSL
		}
	//-------------------------------------------------------------------------------------
	// END_PASS SCENESELECTIONPASS
	//-------------------------------------------------------------------------------------

		
	//-------------------------------------------------------------------------------------
	// BEGIN_PASS SCENEPICKINGPASS
	//-------------------------------------------------------------------------------------
		Pass
		{
			
			Name "ScenePickingPass"
			Tags { "LightMode"="Picking" }
        
	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			HLSLPROGRAM
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

#include "./../../../../../GPUInstancer/Shaders/Include/GPUInstancerInclude.cginc"
#pragma instancing_options procedural:setupGPUI
#pragma multi_compile_instancing


			#define _SPECULAR_SETUP 1
			#define _NORMAL_DROPOFF_TS 1
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#pragma multi_compile _ DOTS_INSTANCING_ON
			#define ASE_ABSOLUTE_VERTEX_POS 1
			#define _TRANSLUCENCY_ASE 1
			#define _EMISSION
			#define _NORMALMAP 1
			#define ASE_SRP_VERSION 120100


			#pragma only_renderers d3d11 glcore gles gles3 
			#pragma vertex vert
			#pragma fragment frag

			#define SCENEPICKINGPASS 1
        
			#define ATTRIBUTES_NEED_NORMAL
			#define ATTRIBUTES_NEED_TANGENT
			#define SHADERPASS SHADERPASS_DEPTHONLY
			       
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Input.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

			//-----------------------------------------------------------------------------
			// DEFINES - (Set by User)
			//-----------------------------------------------------------------------------

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#pragma shader_feature_local TVE_FEATURE_CLIP
			#pragma shader_feature_local TVE_FEATURE_BATCHING
			//TVE Injection Defines
			//SHADER INJECTION POINT BEGIN
			//SHADER INJECTION POINT END
			//TVE Pipeline Defines
			#define THE_VEGETATION_ENGINE
			#define TVE_IS_UNIVERSAL_PIPELINE
			//TVE Shader Type Defines
			#define TVE_IS_GRASS_SHADER


	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
        
	//-------------------------------------------------------------------------------------
	// BEGIN_CBUFFER
	//-------------------------------------------------------------------------------------

			CBUFFER_START(UnityPerMaterial)
			float4 _SubsurfaceDiffusion_asset;
			float4 _NoiseMaskRemap;
			float4 _SubsurfaceDiffusion_Asset;
			half4 _MotionHighlightColor;
			half4 _MainColor;
			half4 _MainUVs;
			half4 _VertexOcclusionColor;
			half4 _OverlayMaskRemap;
			half4 _AlphaMaskRemap;
			half4 _DetailBlendRemap;
			float4 _Color;
			half4 _GradientColorOne;
			half4 _SubsurfaceMaskRemap;
			float4 _GradientMaskRemap;
			half4 _ColorsMaskRemap;
			half4 _GradientColorTwo;
			float4 _EmissiveIntensityParams;
			half4 _VertexOcclusionRemap;
			half4 _SubsurfaceColor;
			half4 _EmissiveUVs;
			float4 _MaxBoundsInfo;
			half4 _EmissiveColor;
			half3 _render_normals;
			half _PerspectiveAngleValue;
			half _LayerMotionValue;
			half _MotionAmplitude_10;
			float _MotionScale_10;
			float _MotionSpeed_10;
			half _MotionVariation_10;
			half _PerspectiveNoiseValue;
			half _PerspectivePushValue;
			half _VertexDynamicMode;
			half _SizeFadeEndValue;
			half _SizeFadeStartValue;
			half _GlobalSize;
			half _LayerVertexValue;
			half _VertexDataMode;
			half _MotionNormalValue;
			half _MotionValue_30;
			half _InteractionAmplitude;
			half _InteractionMaskValue;
			float _MotionSpeed_32;
			float _MotionVariation_32;
			float _MotionScale_32;
			half _SizeFadeMode;
			half _MotionAmplitude_32;
			half _IsSubsurfaceShader;
			half _GradientMaxValue;
			half _SubsurfaceValue;
			half _GlobalAlpha;
			half _AlphaVariationValue;
			half _MainOcclusionValue;
			half _GlobalWetness;
			half _MainSmoothnessValue;
			half _RenderSpecular;
			half _GlobalEmissive;
			half _MainNormalValue;
			half _OverlayMaskMaxValue;
			half _OverlayMaskMinValue;
			half _GradientMinValue;
			half _OverlayVariationValue;
			half _ExtrasPositionMode;
			half _GlobalOverlay;
			half _ColorsMaskMaxValue;
			half _ColorsMaskMinValue;
			half _ColorsVariationValue;
			half _GlobalColors;
			half _LayerColorsValue;
			half _ColorsPositionMode;
			half _VertexOcclusionMaxValue;
			half _VertexOcclusionMinValue;
			half _VertexPivotMode;
			half _LayerExtrasValue;
			half _render_coverage;
			half _RenderMode;
			half _render_dst;
			half _SubsurfaceAngleValue;
			half _HasGradient;
			half _IsTVEShader;
			half _SpaceGlobalPosition;
			half _VertexVariationMode;
			half _AlphaFeatherValue;
			half _MessageGlobalsVariation;
			half _SubsurfaceShadowValue;
			half _RenderZWrite;
			half _RenderDecals;
			half _RenderDirect;
			half _CategoryNoise;
			half _SubsurfaceScatteringValue;
			half _RenderClip;
			float _SubsurfaceDiffusion;
			half _EmissiveFlagMode;
			half _RenderSSR;
			half _CategoryGlobal;
			half _SubsurfaceNormalValue;
			half _MessageMotionVariation;
			half _SubsurfaceAmbientValue;
			half _RenderCull;
			half _HasOcclusion;
			half _Cutoff;
			half _SubsurfaceDirectValue;
			half _CategoryRender;
			half _RenderQueue;
			half _DetailBlendMode;
			half _CategoryPerspective;
			half _render_cull;
			half _RenderAmbient;
			half _CategoryDetail;
			half _render_src;
			half _render_zw;
			half _IsGrassShader;
			half _RenderPriority;
			half _SubsurfaceMaskMinValue;
			half _CategoryGradient;
			half _LayerReactValue;
			half _RenderNormals;
			half _SpaceGlobalLayers;
			half _AlphaClipValue;
			half _VertexRollingMode;
			half _IsVersion;
			half _RenderCoverage;
			half _MessageSizeFade;
			half _CategoryEmissive;
			half _CategoryOcclusion;
			half _CategoryMain;
			half _CategorySizeFade;
			half _CategoryMotion;
			half _DetailMode;
			half _DetailTypeMode;
			half _HasEmissive;
			half _SpaceGlobalLocals;
			half _VertexMasksMode;
			half _RenderShadow;
			half _SpaceMotionGlobals;
			half _CategorySubsurface;
			half _SpaceRenderFade;
			half _SpaceMotionLocals;
			half _SubsurfaceMaskMaxValue;
			#ifdef _TRANSMISSION_ASE
				float _TransmissionShadow;
			#endif
			#ifdef _TRANSLUCENCY_ASE
				float _TransStrength;
				float _TransNormal;
				float _TransScattering;
				float _TransDirect;
				float _TransAmbient;
				float _TransShadow;
			#endif
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END

			// Property used by ScenePickingPass
			#ifdef SCENEPICKINGPASS
				float4 _SelectionID;
			#endif

			// Properties used by SceneSelectionPass
			#ifdef SCENESELECTIONPASS
				int _ObjectId;
				int _PassValue;
			#endif

	//-------------------------------------------------------------------------------------
	// END_CBUFFER
	//-------------------------------------------------------------------------------------

			sampler2D _MainTex;
			half TVE_Enabled;
			sampler2D _BumpMap;
			half4 TVE_MotionParams;
			TEXTURE2D_ARRAY(TVE_MotionTex);
			half4 TVE_MotionCoords;
			SAMPLER(sampler_linear_clamp);
			float TVE_MotionUsage[10];
			sampler2D TVE_NoiseTex;
			half4 TVE_NoiseParams;
			half4 TVE_FlutterParams;
			half TVE_MotionFadeEnd;
			half TVE_MotionFadeStart;
			half4 TVE_VertexParams;
			TEXTURE2D_ARRAY(TVE_VertexTex);
			half4 TVE_VertexCoords;
			float TVE_VertexUsage[10];
			half TVE_DistanceFadeBias;
			half _DisableSRPBatcher;
			sampler2D _MainAlbedoTex;
			half4 TVE_ExtrasParams;
			TEXTURE2D_ARRAY(TVE_ExtrasTex);
			half4 TVE_ExtrasCoords;
			float TVE_ExtrasUsage[10];


	//-------------------------------------------------------------------------------------
	// BEGIN_DEFINES
	//-------------------------------------------------------------------------------------

			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
			//#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/SelectionPickingPass.hlsl"

			//#ifdef HAVE_VFX_MODIFICATION
			//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/VisualEffectVertex.hlsl"
			//#endif

	//-------------------------------------------------------------------------------------
	// END_DEFINES
	//-------------------------------------------------------------------------------------

			float2 DecodeFloatToVector2( float enc )
			{
				float2 result ;
				result.y = enc % 2048;
				result.x = floor(enc / 2048);
				return result / (2048 - 1);
			}
			

			struct SurfaceDescription
			{
				float Alpha;
				float AlphaClipThreshold;
			};
        
			VertexOutput VertexFunction(VertexInput v  )
			{
				VertexOutput o;
				ZERO_INITIALIZE(VertexOutput, o);

				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 VertexPosition3588_g73696 = v.vertex.xyz;
				float3 appendResult60_g74015 = (float3(v.ase_texcoord3.x , v.ase_texcoord3.z , v.ase_texcoord3.y));
				half3 Mesh_PivotsData2831_g73696 = ( appendResult60_g74015 * _VertexPivotMode );
				half3 Mesh_PivotsOS2291_g73696 = Mesh_PivotsData2831_g73696;
				float3 temp_output_2283_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				half3 VertexPos40_g73968 = temp_output_2283_0_g73696;
				float3 appendResult74_g73968 = (float3(VertexPos40_g73968.x , 0.0 , 0.0));
				half3 VertexPosRotationAxis50_g73968 = appendResult74_g73968;
				float3 break84_g73968 = VertexPos40_g73968;
				float3 appendResult81_g73968 = (float3(0.0 , break84_g73968.y , break84_g73968.z));
				half3 VertexPosOtherAxis82_g73968 = appendResult81_g73968;
				float4 temp_output_91_19_g74003 = TVE_MotionCoords;
				float4x4 break19_g73996 = GetObjectToWorldMatrix();
				float3 appendResult20_g73996 = (float3(break19_g73996[ 0 ][ 3 ] , break19_g73996[ 1 ][ 3 ] , break19_g73996[ 2 ][ 3 ]));
				float3 temp_output_122_0_g73996 = Mesh_PivotsData2831_g73696;
				float3 PivotsOnly105_g73996 = (mul( GetObjectToWorldMatrix(), float4( temp_output_122_0_g73996 , 0.0 ) ).xyz).xyz;
				half3 ObjectData20_g73997 = ( appendResult20_g73996 + PivotsOnly105_g73996 );
				float3 ase_worldPos = mul(GetObjectToWorldMatrix(), v.vertex).xyz;
				half3 WorldData19_g73997 = ase_worldPos;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73997 = WorldData19_g73997;
				#else
				float3 staticSwitch14_g73997 = ObjectData20_g73997;
				#endif
				float3 temp_output_114_0_g73996 = staticSwitch14_g73997;
				float3 vertexToFrag4224_g73696 = temp_output_114_0_g73996;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				float3 vertexToFrag3890_g73696 = ase_worldPos;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				half2 UV94_g74003 = ( (temp_output_91_19_g74003).zw + ( (temp_output_91_19_g74003).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g74003 = _LayerMotionValue;
				float4 lerpResult107_g74003 = lerp( TVE_MotionParams , saturate( SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_MotionTex, sampler_linear_clamp, UV94_g74003,temp_output_84_0_g74003, 0.0 ) ) , TVE_MotionUsage[(int)temp_output_84_0_g74003]);
				half4 Global_Motion_Params3909_g73696 = lerpResult107_g74003;
				float4 break322_g73985 = Global_Motion_Params3909_g73696;
				float3 appendResult397_g73985 = (float3(break322_g73985.x , 0.0 , break322_g73985.y));
				float3 temp_output_398_0_g73985 = (appendResult397_g73985*2.0 + -1.0);
				float3 ase_parentObjectScale = ( 1.0 / float3( length( GetWorldToObjectMatrix()[ 0 ].xyz ), length( GetWorldToObjectMatrix()[ 1 ].xyz ), length( GetWorldToObjectMatrix()[ 2 ].xyz ) ) );
				half2 Global_MotionDirectionOS39_g73696 = (( mul( GetWorldToObjectMatrix(), float4( temp_output_398_0_g73985 , 0.0 ) ).xyz * ase_parentObjectScale )).xz;
				half2 Input_DirectionOS358_g73990 = Global_MotionDirectionOS39_g73696;
				half Wind_Power369_g73985 = break322_g73985.z;
				half Global_WindPower2223_g73696 = Wind_Power369_g73985;
				half3 Input_Position419_g73963 = ObjectPosition4223_g73696;
				float Input_MotionScale287_g73963 = ( _MotionScale_10 + 1.0 );
				half Global_Scale448_g73963 = TVE_NoiseParams.x;
				float2 temp_output_597_0_g73963 = (( Input_Position419_g73963 * Input_MotionScale287_g73963 * Global_Scale448_g73963 * 0.0075 )).xz;
				half2 Global_MotionDirectionWS4683_g73696 = (temp_output_398_0_g73985).xz;
				half2 Input_DirectionWS423_g73963 = Global_MotionDirectionWS4683_g73696;
				half Input_MotionSpeed62_g73963 = _MotionSpeed_10;
				half Global_Speed449_g73963 = TVE_NoiseParams.y;
				half Input_MotionVariation284_g73963 = _MotionVariation_10;
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = v.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				half Input_GlobalVariation569_g73963 = Global_MeshVariation5104_g73696;
				float temp_output_630_0_g73963 = ( ( ( _TimeParameters.x * Input_MotionSpeed62_g73963 * Global_Speed449_g73963 ) + ( Input_MotionVariation284_g73963 * Input_GlobalVariation569_g73963 ) ) * 0.03 );
				float temp_output_607_0_g73963 = frac( temp_output_630_0_g73963 );
				float4 lerpResult590_g73963 = lerp( tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * temp_output_607_0_g73963 ) ), 0, 0.0) ) , tex2Dlod( TVE_NoiseTex, float4( ( temp_output_597_0_g73963 + ( -Input_DirectionWS423_g73963 * frac( ( temp_output_630_0_g73963 + 0.5 ) ) ) ), 0, 0.0) ) , ( abs( ( temp_output_607_0_g73963 - 0.5 ) ) / 0.5 ));
				half Input_GlobalWind327_g73963 = Global_WindPower2223_g73696;
				float lerpResult612_g73963 = lerp( 1.4 , 0.4 , Input_GlobalWind327_g73963);
				float3 temp_cast_7 = (lerpResult612_g73963).xxx;
				float3 break638_g73963 = (pow( ( abs( (lerpResult590_g73963).rgb ) + 0.2 ) , temp_cast_7 )*1.4 + -0.2);
				half Global_NoiseTexR34_g73696 = break638_g73963.x;
				half Motion_10_Amplitude2258_g73696 = ( _MotionAmplitude_10 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 );
				half Input_BendingAmplitude376_g73990 = Motion_10_Amplitude2258_g73696;
				half Mesh_Height1524_g73696 = v.ase_color.a;
				half Input_MeshHeight388_g73990 = Mesh_Height1524_g73696;
				half ObjectData20_g73992 = ( Input_MeshHeight388_g73990 * 2.0 );
				float enc62_g73978 = v.ase_texcoord.w;
				float2 localDecodeFloatToVector262_g73978 = DecodeFloatToVector2( enc62_g73978 );
				float2 break63_g73978 = ( localDecodeFloatToVector262_g73978 * 100.0 );
				float Bounds_Height5230_g73696 = break63_g73978.x;
				half Input_BoundsHeight390_g73990 = Bounds_Height5230_g73696;
				half WorldData19_g73992 = ( ( Input_MeshHeight388_g73990 * Input_MeshHeight388_g73990 ) * Input_BoundsHeight390_g73990 * 2.0 );
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73992 = WorldData19_g73992;
				#else
				float staticSwitch14_g73992 = ObjectData20_g73992;
				#endif
				half Mask_Motion_10321_g73990 = staticSwitch14_g73992;
				half Input_InteractionAmplitude58_g73990 = _InteractionAmplitude;
				half Input_InteractionUseMask62_g73990 = _InteractionMaskValue;
				float lerpResult371_g73990 = lerp( 2.0 , Mask_Motion_10321_g73990 , Input_InteractionUseMask62_g73990);
				half ObjectData20_g73991 = lerpResult371_g73990;
				half WorldData19_g73991 = Mask_Motion_10321_g73990;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73991 = WorldData19_g73991;
				#else
				float staticSwitch14_g73991 = ObjectData20_g73991;
				#endif
				half Mask_Interaction373_g73990 = ( Input_InteractionAmplitude58_g73990 * staticSwitch14_g73991 );
				half Global_InteractionMask66_g73696 = ( break322_g73985.w * break322_g73985.w );
				float Input_InteractionGlobalMask330_g73990 = Global_InteractionMask66_g73696;
				float lerpResult360_g73990 = lerp( ( Input_BendingAmplitude376_g73990 * Mask_Motion_10321_g73990 ) , Mask_Interaction373_g73990 , saturate( ( Input_InteractionAmplitude58_g73990 * Input_InteractionGlobalMask330_g73990 ) ));
				float2 break364_g73990 = ( Input_DirectionOS358_g73990 * lerpResult360_g73990 );
				half Motion_10_BendingZ190_g73696 = break364_g73990.y;
				half Angle44_g73968 = Motion_10_BendingZ190_g73696;
				half3 VertexPos40_g73948 = ( VertexPosRotationAxis50_g73968 + ( VertexPosOtherAxis82_g73968 * cos( Angle44_g73968 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g73968 ) * sin( Angle44_g73968 ) ) );
				float3 appendResult74_g73948 = (float3(0.0 , 0.0 , VertexPos40_g73948.z));
				half3 VertexPosRotationAxis50_g73948 = appendResult74_g73948;
				float3 break84_g73948 = VertexPos40_g73948;
				float3 appendResult81_g73948 = (float3(break84_g73948.x , break84_g73948.y , 0.0));
				half3 VertexPosOtherAxis82_g73948 = appendResult81_g73948;
				half Motion_10_BendingX216_g73696 = break364_g73990.x;
				half Angle44_g73948 = -Motion_10_BendingX216_g73696;
				half Input_MotionScale321_g73940 = _MotionScale_32;
				half Input_MotionVariation330_g73940 = _MotionVariation_32;
				half Input_GlobalVariation372_g73940 = Global_MeshVariation5104_g73696;
				half Input_MotionSpeed62_g73940 = _MotionSpeed_32;
				half Global_Speed350_g73940 = TVE_FlutterParams.y;
				half Motion_FacingMask5214_g73696 = 1.0;
				float temp_output_7_0_g73984 = TVE_MotionFadeEnd;
				half Motion_FadeOut4005_g73696 = saturate( ( ( distance( ase_worldPos , _WorldSpaceCameraPos ) - temp_output_7_0_g73984 ) / ( TVE_MotionFadeStart - temp_output_7_0_g73984 ) ) );
				half Motion_30_Amplitude4960_g73696 = ( _MotionAmplitude_32 * _MotionValue_30 * Global_WindPower2223_g73696 * Global_NoiseTexR34_g73696 * Motion_FacingMask5214_g73696 * Motion_FadeOut4005_g73696 );
				half Input_MotionAmplitude58_g73940 = Motion_30_Amplitude4960_g73696;
				half Global_Power354_g73940 = TVE_FlutterParams.x;
				float enc59_g73978 = v.ase_texcoord.z;
				float2 localDecodeFloatToVector259_g73978 = DecodeFloatToVector2( enc59_g73978 );
				float2 break61_g73978 = localDecodeFloatToVector259_g73978;
				half Mesh_Motion_30144_g73696 = break61_g73978.y;
				half Input_MeshMotion_30374_g73940 = Mesh_Motion_30144_g73696;
				half Input_MotionNormal364_g73940 = _MotionNormalValue;
				float3 lerpResult370_g73940 = lerp( float3( 1,1,1 ) , v.ase_normal , Input_MotionNormal364_g73940);
				half3 Motion_30_Details263_g73696 = ( ( sin( ( ( ( ase_worldPos.x + ase_worldPos.y + ase_worldPos.z ) * Input_MotionScale321_g73940 ) + ( Input_MotionVariation330_g73940 * Input_GlobalVariation372_g73940 ) + ( _TimeParameters.x * Input_MotionSpeed62_g73940 * Global_Speed350_g73940 ) ) ) * Input_MotionAmplitude58_g73940 * Global_Power354_g73940 * Input_MeshMotion_30374_g73940 * 0.4 ) * lerpResult370_g73940 );
				float3 Vertex_Motion_Object833_g73696 = ( ( VertexPosRotationAxis50_g73948 + ( VertexPosOtherAxis82_g73948 * cos( Angle44_g73948 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g73948 ) * sin( Angle44_g73948 ) ) ) + Motion_30_Details263_g73696 );
				float3 temp_output_3474_0_g73696 = ( VertexPosition3588_g73696 - Mesh_PivotsOS2291_g73696 );
				float3 appendResult2043_g73696 = (float3(Motion_10_BendingX216_g73696 , 0.0 , Motion_10_BendingZ190_g73696));
				float3 Vertex_Motion_World1118_g73696 = ( ( temp_output_3474_0_g73696 + appendResult2043_g73696 ) + Motion_30_Details263_g73696 );
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch4976_g73696 = Vertex_Motion_World1118_g73696;
				#else
				float3 staticSwitch4976_g73696 = ( Vertex_Motion_Object833_g73696 + ( _VertexDataMode * 0.0 ) );
				#endif
				float4 temp_output_94_19_g73959 = TVE_VertexCoords;
				half2 UV97_g73959 = ( (temp_output_94_19_g73959).zw + ( (temp_output_94_19_g73959).xy * (ObjectPosition4223_g73696).xz ) );
				float temp_output_84_0_g73959 = _LayerVertexValue;
				float4 lerpResult109_g73959 = lerp( TVE_VertexParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_VertexTex, sampler_linear_clamp, UV97_g73959,temp_output_84_0_g73959, 0.0 ) , TVE_VertexUsage[(int)temp_output_84_0_g73959]);
				half4 Global_Object_Params4173_g73696 = lerpResult109_g73959;
				half Global_VertexSize174_g73696 = saturate( Global_Object_Params4173_g73696.w );
				float lerpResult346_g73696 = lerp( 1.0 , Global_VertexSize174_g73696 , _GlobalSize);
				float3 appendResult3480_g73696 = (float3(lerpResult346_g73696 , lerpResult346_g73696 , lerpResult346_g73696));
				half3 ObjectData20_g73993 = appendResult3480_g73696;
				half3 _Vector11 = half3(1,1,1);
				half3 WorldData19_g73993 = _Vector11;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73993 = WorldData19_g73993;
				#else
				float3 staticSwitch14_g73993 = ObjectData20_g73993;
				#endif
				half3 Vertex_Size1741_g73696 = staticSwitch14_g73993;
				half3 _Vector5 = half3(1,1,1);
				float temp_output_7_0_g73931 = _SizeFadeEndValue;
				float temp_output_335_0_g73696 = saturate( ( ( ( distance( _WorldSpaceCameraPos , ObjectPosition4223_g73696 ) * ( 1.0 / TVE_DistanceFadeBias ) ) - temp_output_7_0_g73931 ) / ( _SizeFadeStartValue - temp_output_7_0_g73931 ) ) );
				float3 appendResult3482_g73696 = (float3(temp_output_335_0_g73696 , temp_output_335_0_g73696 , temp_output_335_0_g73696));
				float3 lerpResult5065_g73696 = lerp( _Vector5 , appendResult3482_g73696 , _SizeFadeMode);
				half3 ObjectData20_g73958 = lerpResult5065_g73696;
				half3 WorldData19_g73958 = _Vector5;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73958 = WorldData19_g73958;
				#else
				float3 staticSwitch14_g73958 = ObjectData20_g73958;
				#endif
				float3 Vertex_SizeFade1740_g73696 = staticSwitch14_g73958;
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - ase_worldPos );
				ase_worldViewDir = normalize(ase_worldViewDir);
				float3 normalizeResult2696_g73696 = normalize( ase_worldViewDir );
				float3 break2709_g73696 = cross( normalizeResult2696_g73696 , half3(0,1,0) );
				float3 appendResult2710_g73696 = (float3(-break2709_g73696.z , 0.0 , break2709_g73696.x));
				float3 appendResult2667_g73696 = (float3(Global_MeshVariation5104_g73696 , 0.5 , Global_MeshVariation5104_g73696));
				float3 normalizeResult2169_g73696 = normalize( ase_worldViewDir );
				float3 ViewDir_Normalized3963_g73696 = normalizeResult2169_g73696;
				float dotResult2212_g73696 = dot( ViewDir_Normalized3963_g73696 , float3(0,1,0) );
				half Mask_HView2656_g73696 = dotResult2212_g73696;
				float saferPower2652_g73696 = abs( Mask_HView2656_g73696 );
				half3 Grass_Perspective2661_g73696 = ( ( ( mul( GetWorldToObjectMatrix(), float4( appendResult2710_g73696 , 0.0 ) ).xyz * _PerspectivePushValue ) + ( (appendResult2667_g73696*2.0 + -1.0) * _PerspectiveNoiseValue ) ) * Mesh_Height1524_g73696 * pow( saferPower2652_g73696 , _PerspectiveAngleValue ) );
				float3 lerpResult16_g74000 = lerp( VertexPosition3588_g73696 , ( ( staticSwitch4976_g73696 * Vertex_Size1741_g73696 * Vertex_SizeFade1740_g73696 ) + Mesh_PivotsOS2291_g73696 + Grass_Perspective2661_g73696 ) , TVE_Enabled);
				float3 Final_VertexPosition890_g73696 = ( lerpResult16_g74000 + _DisableSRPBatcher );
				
				o.ase_texcoord1.xyz = vertexToFrag3890_g73696;
				o.ase_texcoord2.xyz = vertexToFrag4224_g73696;
				
				o.ase_texcoord = v.ase_texcoord;
				o.ase_color = v.ase_color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.w = 0;
				o.ase_texcoord2.w = 0;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif

				float3 vertexValue = Final_VertexPosition890_g73696;

				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				o.clipPos = TransformWorldToHClip(positionWS);

				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord3 = v.ase_texcoord3;
				o.ase_color = v.ase_color;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
				return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord3 = patch[0].ase_texcoord3 * bary.x + patch[1].ase_texcoord3 * bary.y + patch[2].ase_texcoord3 * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN ) : SV_TARGET
			{
				SurfaceDescription surfaceDescription = (SurfaceDescription)0;

				float localCustomAlphaClip19_g74020 = ( 0.0 );
				half2 Main_UVs15_g73696 = ( ( IN.ase_texcoord.xy * (_MainUVs).xy ) + (_MainUVs).zw );
				float4 tex2DNode29_g73696 = tex2D( _MainAlbedoTex, Main_UVs15_g73696 );
				float Main_Alpha316_g73696 = ( _MainColor.a * tex2DNode29_g73696.a );
				float4 temp_output_93_19_g73986 = TVE_ExtrasCoords;
				float3 vertexToFrag3890_g73696 = IN.ase_texcoord1.xyz;
				float3 WorldPosition3905_g73696 = vertexToFrag3890_g73696;
				float3 vertexToFrag4224_g73696 = IN.ase_texcoord2.xyz;
				half3 ObjectData20_g73953 = vertexToFrag4224_g73696;
				half3 WorldData19_g73953 = vertexToFrag3890_g73696;
				#ifdef TVE_FEATURE_BATCHING
				float3 staticSwitch14_g73953 = WorldData19_g73953;
				#else
				float3 staticSwitch14_g73953 = ObjectData20_g73953;
				#endif
				float3 ObjectPosition4223_g73696 = staticSwitch14_g73953;
				float3 lerpResult4827_g73696 = lerp( WorldPosition3905_g73696 , ObjectPosition4223_g73696 , _ExtrasPositionMode);
				half2 UV96_g73986 = ( (temp_output_93_19_g73986).zw + ( (temp_output_93_19_g73986).xy * (lerpResult4827_g73696).xz ) );
				float temp_output_84_0_g73986 = _LayerExtrasValue;
				float4 lerpResult109_g73986 = lerp( TVE_ExtrasParams , SAMPLE_TEXTURE2D_ARRAY_LOD( TVE_ExtrasTex, sampler_linear_clamp, UV96_g73986,temp_output_84_0_g73986, 0.0 ) , TVE_ExtrasUsage[(int)temp_output_84_0_g73986]);
				float4 break89_g73986 = lerpResult109_g73986;
				half Global_Extras_Alpha1033_g73696 = saturate( break89_g73986.a );
				float3 break111_g73994 = ObjectPosition4223_g73696;
				half Global_DynamicMode5112_g73696 = _VertexDynamicMode;
				half Input_DynamicMode120_g73994 = Global_DynamicMode5112_g73696;
				float Mesh_Variation16_g73696 = IN.ase_color.r;
				half Input_Variation124_g73994 = Mesh_Variation16_g73696;
				half ObjectData20_g73995 = frac( ( ( ( break111_g73994.x + break111_g73994.y + break111_g73994.z + 0.001275 ) * ( 1.0 - Input_DynamicMode120_g73994 ) ) + Input_Variation124_g73994 ) );
				half WorldData19_g73995 = Input_Variation124_g73994;
				#ifdef TVE_FEATURE_BATCHING
				float staticSwitch14_g73995 = WorldData19_g73995;
				#else
				float staticSwitch14_g73995 = ObjectData20_g73995;
				#endif
				float clampResult129_g73994 = clamp( staticSwitch14_g73995 , 0.01 , 0.99 );
				half Global_MeshVariation5104_g73696 = clampResult129_g73994;
				float lerpResult5154_g73696 = lerp( 0.0 , Global_MeshVariation5104_g73696 , _AlphaVariationValue);
				half Global_Alpha_Variation5158_g73696 = lerpResult5154_g73696;
				half AlphaTreshold2132_g73696 = _AlphaClipValue;
				half Global_Alpha_Mask4546_g73696 = 1.0;
				float lerpResult5203_g73696 = lerp( 1.0 , ( ( Global_Extras_Alpha1033_g73696 - Global_Alpha_Variation5158_g73696 ) + AlphaTreshold2132_g73696 + ( Global_Extras_Alpha1033_g73696 * 0.5 ) ) , ( Global_Alpha_Mask4546_g73696 * _GlobalAlpha ));
				float lerpResult16_g74016 = lerp( 1.0 , lerpResult5203_g73696 , TVE_Enabled);
				half Global_Alpha315_g73696 = lerpResult16_g74016;
				half Fade_Alpha3727_g73696 = 1.0;
				float temp_output_661_0_g73696 = ( Main_Alpha316_g73696 * Global_Alpha315_g73696 * Fade_Alpha3727_g73696 );
				half Alpha34_g74021 = temp_output_661_0_g73696;
				half Offest27_g74021 = AlphaTreshold2132_g73696;
				half AlphaFeather5305_g73696 = _AlphaFeatherValue;
				half Feather30_g74021 = AlphaFeather5305_g73696;
				float temp_output_25_0_g74021 = ( ( ( Alpha34_g74021 - Offest27_g74021 ) / ( max( fwidth( Alpha34_g74021 ) , 0.001 ) + Feather30_g74021 ) ) + Offest27_g74021 );
				float temp_output_3_0_g74020 = temp_output_25_0_g74021;
				float Alpha19_g74020 = temp_output_3_0_g74020;
				float temp_output_15_0_g74020 = AlphaTreshold2132_g73696;
				float Treshold19_g74020 = temp_output_15_0_g74020;
				{
				#if defined (TVE_FEATURE_CLIP)
				#if defined (TVE_IS_HD_PIPELINE)
				#if !defined (SHADERPASS_FORWARD_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#if !defined (SHADERPASS_GBUFFER_BYPASS_ALPHA_TEST)
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#else
				clip(Alpha19_g74020 - Treshold19_g74020);
				#endif
				#endif
				}
				half Final_Alpha914_g73696 = saturate( Alpha19_g74020 );
				

				surfaceDescription.Alpha = Final_Alpha914_g73696;
				surfaceDescription.AlphaClipThreshold = 0.5;

				#if _ALPHATEST_ON
					float alphaClipThreshold = 0.01f;
					#if ALPHA_CLIP_THRESHOLD
						alphaClipThreshold = surfaceDescription.AlphaClipThreshold;
					#endif
						clip(surfaceDescription.Alpha - alphaClipThreshold);
				#endif

				half4 outColor = 0;

				#ifdef SCENESELECTIONPASS
					outColor = half4(_ObjectId, _PassValue, 1.0, 1.0);
				#elif defined(SCENEPICKINGPASS)
					outColor = _SelectionID;
				#endif

				return outColor;
			}
        
			ENDHLSL
		}
	//-------------------------------------------------------------------------------------
	// END_PASS SCENEPICKINGPASS
	//-------------------------------------------------------------------------------------
		
	}
	
	CustomEditor "TVEShaderCoreGUI"
	Fallback "Hidden/BOXOPHOBIC/The Vegetation Engine/Fallback"
	
}
/*ASEBEGIN
Version=18935
1920;6;1920;1023;2655.666;932.516;1;True;False
Node;AmplifyShaderEditor.FunctionNode;1041;-1344,-896;Inherit;False;Compile All Shaders;-1;;63677;e67c8238031dbf04ab79a5d4d63d1b4f;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1047;-1408,-768;Half;False;Property;_render_coverage;_render_coverage;202;1;[HideInInspector];Create;True;0;2;Opaque;0;Transparent;1;0;True;0;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;713;-2176,384;Inherit;False;Define Shader Grass;-1;;74026;921559c53826c0142ba6e27dd03eaef2;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-2176,-768;Half;False;Property;_render_cull;_render_cull;200;1;[HideInInspector];Create;True;0;3;Both;0;Back;1;Front;2;0;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;1042;-1536,-896;Inherit;False;Compile Core;-1;;74024;634b02fd1f32e6a4c875d8fc2c450956;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-1792,-768;Half;False;Property;_render_dst;_render_dst;201;1;[HideInInspector];Create;True;0;2;Opaque;0;Transparent;1;0;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;1040;-1920,384;Inherit;False;Define Pipeline Universal;-1;;74025;71dc7add32e5f6247b1fb74ecceddd3e;0;0;1;FLOAT;529
Node;AmplifyShaderEditor.RangedFloatNode;17;-1600,-768;Half;False;Property;_render_zw;_render_zw;203;1;[HideInInspector];Create;True;0;2;Opaque;0;Transparent;1;0;True;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;168;-2176,-896;Half;False;Property;_IsGrassShader;_IsGrassShader;197;1;[HideInInspector];Create;True;0;0;0;True;0;False;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;1025;-2176,-384;Inherit;False;Base Shader;0;;73696;856f7164d1c579d43a5cf4968a75ca43;84,3880,1,4029,1,4028,1,3903,1,3904,1,3900,1,4204,1,3908,1,4172,1,1300,1,1298,1,4995,1,4179,1,3586,0,4499,1,3658,1,1708,1,3509,1,5151,0,3873,1,893,1,5196,0,5128,1,5156,1,5345,0,1718,1,1715,1,1714,1,1717,1,5075,1,916,0,1763,0,1762,0,3568,1,5118,0,1776,1,3475,1,4210,1,1745,1,3479,0,3501,1,5152,0,1646,0,1271,1,3889,0,2807,1,3886,0,4999,0,3887,0,3957,0,5357,0,2172,0,3883,0,3728,0,3949,0,5147,0,5146,1,5350,0,2658,1,1742,1,3484,0,4837,0,5079,0,1736,0,1735,0,1737,0,1734,0,1733,0,1550,0,878,0,860,1,2261,1,2260,1,2054,0,2032,0,5258,0,2039,1,2062,1,3243,1,5220,0,4217,1,5090,1,4242,1,5339,0;6;5115;FLOAT;1;False;5127;FLOAT;1;False;5143;FLOAT;1;False;5119;FLOAT;1;False;5117;FLOAT;1;False;5340;FLOAT3;0,0,0;False;23;FLOAT3;0;FLOAT3;528;FLOAT3;2489;FLOAT;531;FLOAT;4842;FLOAT;529;FLOAT;3678;FLOAT;530;FLOAT;4122;FLOAT;4134;FLOAT;1235;FLOAT;532;FLOAT;5389;FLOAT;721;FLOAT3;1230;FLOAT;5296;FLOAT;1461;FLOAT;1290;FLOAT;629;FLOAT3;534;FLOAT;4867;FLOAT4;5246;FLOAT4;4841
Node;AmplifyShaderEditor.RangedFloatNode;1010;-1984,-896;Half;False;Property;_IsSubsurfaceShader;_IsSubsurfaceShader;198;1;[HideInInspector];Create;True;0;0;0;True;0;False;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-1984,-768;Half;False;Property;_render_src;_render_src;199;1;[HideInInspector];Create;True;0;0;0;True;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1037;-1376,-384;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;28cd5599e02859647ae1798e4fcaef6c;True;Universal2D;0;5;Universal2D;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;True;17;d3d9;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;xboxseries;ps4;playstation;psp2;n3ds;wiiu;switch;nomrt;0;False;True;1;1;True;20;0;True;7;1;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;False;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;LightMode=Universal2D;False;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1044;-1376,-384;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;28cd5599e02859647ae1798e4fcaef6c;True;GBuffer;0;7;GBuffer;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;True;17;d3d9;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;xboxseries;ps4;playstation;psp2;n3ds;wiiu;switch;nomrt;0;False;True;1;1;True;20;0;True;7;1;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;LightMode=UniversalGBuffer;False;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1045;-1376,-304;Float;False;False;-1;2;UnityEditor.ShaderGraphLitGUI;0;1;New Amplify Shader;28cd5599e02859647ae1798e4fcaef6c;True;SceneSelectionPass;0;8;SceneSelectionPass;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;2;True;17;d3d9;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;xboxseries;ps4;playstation;psp2;n3ds;wiiu;switch;nomrt;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=SceneSelectionPass;False;True;4;d3d11;glcore;gles;gles3;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1033;-1376,-384;Float;False;True;-1;2;TVEShaderCoreGUI;0;20;GPUInstancer/BOXOPHOBIC/The Vegetation Engine/Default/Grass Subsurface Lit;28cd5599e02859647ae1798e4fcaef6c;True;Forward;0;1;Forward;19;False;False;False;False;False;False;False;False;False;False;False;False;True;0;True;1047;True;True;2;True;10;False;False;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;True;1;True;17;True;0;False;-1;True;False;0;False;-1;0;False;-1;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;4;True;17;d3d9;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;xboxseries;ps4;playstation;psp2;n3ds;wiiu;switch;nomrt;0;True;True;1;1;True;20;0;True;7;1;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;LightMode=UniversalForwardOnly;False;False;0;Hidden/BOXOPHOBIC/The Vegetation Engine/Fallback;0;0;Standard;41;Workflow;0;0;Surface;0;0;  Refraction Model;0;0;  Blend;0;0;Two Sided;0;0;Fragment Normal Space,InvertActionOnDeselection;0;0;Forward Rendering;0;0;Transmission;0;637860535030774692;  Transmission Shadow;0.5,True,1013;0;Translucency;1;637860535037399992;  Translucency Strength;1,False,-1;0;  Normal Distortion;0.5,False,-1;0;  Scattering;2,False,-1;0;  Direct;0.9,False,-1;0;  Ambient;0.1,False,-1;0;  Shadow;0.5,False,-1;0;Cast Shadows;1;0;  Use Shadow Threshold;0;0;Receive Shadows;1;0;GPU Instancing;1;0;LOD CrossFade;1;0;Built-in Fog;1;0;_FinalColorxAlpha;0;0;Meta Pass;1;0;Override Baked GI;0;0;Extra Pre Pass;0;0;DOTS Instancing;1;0;Tessellation;0;0;  Phong;0;0;  Strength;0.5,False,-1;0;  Type;0;0;  Tess;16,False,-1;0;  Min;10,False,-1;0;  Max;25,False,-1;0;  Edge Length;16,False,-1;0;  Max Displacement;25,False,-1;0;Write Depth;0;0;  Early Z;0;0;Vertex Position,InvertActionOnDeselection;0;0;Debug Display;0;0;Clear Coat;0;0;0;10;False;True;True;True;True;True;True;False;True;True;False;;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1043;-1376,-23;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;28cd5599e02859647ae1798e4fcaef6c;True;DepthNormals;0;6;DepthNormals;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;True;17;d3d9;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;xboxseries;ps4;playstation;psp2;n3ds;wiiu;switch;nomrt;0;False;True;1;1;False;-1;0;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;False;-1;True;3;False;-1;False;True;1;LightMode=DepthNormalsOnly;False;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1034;-1376,-384;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;28cd5599e02859647ae1798e4fcaef6c;True;ShadowCaster;0;2;ShadowCaster;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;True;17;d3d9;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;xboxseries;ps4;playstation;psp2;n3ds;wiiu;switch;nomrt;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;True;False;False;False;False;0;False;-1;False;False;False;False;False;False;False;False;False;True;1;False;-1;True;3;False;-1;False;True;1;LightMode=ShadowCaster;False;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1046;-1376,-304;Float;False;False;-1;2;UnityEditor.ShaderGraphLitGUI;0;1;New Amplify Shader;28cd5599e02859647ae1798e4fcaef6c;True;ScenePickingPass;0;9;ScenePickingPass;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;2;True;17;d3d9;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;xboxseries;ps4;playstation;psp2;n3ds;wiiu;switch;nomrt;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=Picking;False;True;4;d3d11;glcore;gles;gles3;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1035;-1376,-384;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;28cd5599e02859647ae1798e4fcaef6c;True;DepthOnly;0;3;DepthOnly;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;True;17;d3d9;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;xboxseries;ps4;playstation;psp2;n3ds;wiiu;switch;nomrt;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;True;False;False;False;False;0;False;-1;False;False;False;False;False;False;False;False;False;True;1;False;-1;False;False;True;1;LightMode=DepthOnly;False;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1036;-1376,-384;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;28cd5599e02859647ae1798e4fcaef6c;True;Meta;0;4;Meta;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;True;17;d3d9;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;xboxseries;ps4;playstation;psp2;n3ds;wiiu;switch;nomrt;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=Meta;False;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1032;-1376,-384;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;28cd5599e02859647ae1798e4fcaef6c;True;ExtraPrePass;0;0;ExtraPrePass;5;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;True;17;d3d9;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;xboxseries;ps4;playstation;psp2;n3ds;wiiu;switch;nomrt;0;False;True;1;1;False;-1;0;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;0;False;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.CommentaryNode;37;-2176,-1024;Inherit;False;1023.392;100;Internal;0;;1,0.252,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;33;-2176,-512;Inherit;False;1024.392;100;Final;0;;0,1,0.5,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;408;-2176,256;Inherit;False;1026.438;100;Features;0;;0,1,0.5,1;0;0
WireConnection;1033;0;1025;0
WireConnection;1033;1;1025;528
WireConnection;1033;2;1025;2489
WireConnection;1033;9;1025;3678
WireConnection;1033;4;1025;530
WireConnection;1033;5;1025;531
WireConnection;1033;6;1025;532
WireConnection;1033;15;1025;1230
WireConnection;1033;8;1025;534
ASEEND*/
//CHKSM=F99B99B3811BB2D3E6337AA2DE3600C245D0AF58
