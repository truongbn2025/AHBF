using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public GameObject explosionPrefab;
    public List<GameObject> positions;

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            foreach (GameObject pos in positions)
            {
                Instantiate(explosionPrefab, pos.transform.position, Quaternion.identity);
            }
        }
    }
}
