using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestUIComponent : MonoBehaviour
{
    public Camera mainCamera;

    RaycastHit2D hit;
    private void Awake()
    {
        mainCamera = FindObjectOfType<Camera>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Click");
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (hit = Physics2D.Raycast(ray.origin, new Vector2(0, 0)))
            {
                Debug.Log(hit.collider.name);
            }
        }
    }
}
