using EasyCharacterMovement;
using EasyCharacterMovement.Examples.Gameplay.PlanetWalkExample;
using FishNet;
using FishNet.Component.ColliderRollback;
using FishNet.Editing;
using FishNet.Managing.Timing;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using static UnityEngine.UI.GridLayoutGroup;

public enum ENEMY_BOT_STATE
{
    IDLE,
    AGRRO,
    AIMING,
    RUNNING,
    DEAD
}

public class EnemyBotBehaviour : NetworkBehaviour
{
    public LayerMask shootingLayer;
    private Animator animator;
    private float fireDelay;
    private float nextFire;
    ENEMY_BOT_STATE state;
    GameObject target;
    Weapon currentWeapon;
    public Transform exitPoint;

    public GameObject hitIndicator;
    public GameObject muzzleEffect;
    public GameObject bulletTrail;
    private void Awake()
    {
        fireDelay = 5f;
        nextFire = Time.time + fireDelay;
        animator = GetComponent<Animator>();
        animator.SetBool("Grounded", true);
        
    }

    private void Update()
    {
        switch (state)
        {
            case ENEMY_BOT_STATE.IDLE:

                break;
            case ENEMY_BOT_STATE.AGRRO:
                if (target != null)
                {

                    
                    transform.LookAt(target.transform.position);
                    if (Time.time >= nextFire)
                    {
                        FireHitScanWeapon(exitPoint.position, (target.transform.position - exitPoint.position));
                    }
                }

                break;
            case ENEMY_BOT_STATE.RUNNING:

                break;
            case ENEMY_BOT_STATE.DEAD:

                break;
        }
    }

    private void UpdateTimers(float deltaTime)
    {
        
    }
    public void TransitionToState(ENEMY_BOT_STATE newState)
    {
        state = newState;
        switch (state)
        {
            case ENEMY_BOT_STATE.IDLE:

                animator.SetBool("AimingLeft", false);
                break;
            case ENEMY_BOT_STATE.AGRRO:
                animator.SetBool("AimingLeft", true);
                transform.LookAt(target.transform.position);

                //if (currentWeapon.currentClip <= 0) return;
                //Vector3 aimPoint = GetAimPoint();
                //Vector3 forward = AddSpreadHitScan((aimPoint.position - weapon.ExitPoint.position).normalized, weapon.consecutiveShot);
                //FireHitScanWeapon( exitPoint.position, (target.transform.position - exitPoint.position));
                break;
            case ENEMY_BOT_STATE.RUNNING:

                break;
            case ENEMY_BOT_STATE.DEAD:
                animator.SetBool("isDead", true);
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //Debug.Log("Hello");
            target = other.gameObject;
            TransitionToState(ENEMY_BOT_STATE.AGRRO);
        }
    }


    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.CompareTag("Player"))
    //    {
    //        //TransitionToState(ENEMY_BOT_STATE.AGRRO);
    //        if(target != null)
    //            transform.LookAt(target.transform.position);
    //    }
    //}

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            target = null;
            TransitionToState(ENEMY_BOT_STATE.IDLE);
            //transform.LookAt(target.transform.position);

        }
    }
    private void StartFiring()
    {
        
    }

    #region HITSCAN WEAPON

    [ServerRpc(RunLocally = true)]
    private void FireHitScanWeapon( Vector3 position, Vector3 forward)
    {
        Debug.Log("5");
        Ray shootingRay = new Ray(position, forward);
        nextFire = Time.time + fireDelay;
        if (Physics.Raycast(shootingRay, out RaycastHit hit, float.PositiveInfinity, shootingLayer))
        {
            if (hit.transform.CompareTag("Player"))
            {
                ShootingEffectHitScan(position, hit.point, hit.normal, HitTarget.ENEMY);
                ShootClientHitScan(position, hit.point, hit.normal, HitTarget.ENEMY);

            }
            else if (hit.transform.CompareTag("InvisibleBorder"))
            {
                ShootingEffectHitScan(position, hit.point, hit.normal, HitTarget.NONE);
                ShootClientHitScan(position, hit.point, hit.normal, HitTarget.NONE);
            }
            else
            {
                ShootingEffectHitScan(position, hit.point, hit.normal, HitTarget.SURROUNDING);
                ShootClientHitScan(position, hit.point, hit.normal, HitTarget.SURROUNDING);
            }

            //Apply damage and other server things.
            if (base.IsServer)
            {
                //hit.transform.GetComponent<HurtBox>().HitByBot(10, "ThisBotName", "Pistol");
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    hit.collider.gameObject.GetComponent<HurtBox>().HitByBot(10, "ThisBotName", "Pistol");
                    //hit.collider.gameObject.GetComponent<HurtBox>().Hit(currentWeapon.Damage, GetComponent<NetworkCharacter>(), currentWeapon.WeaponName);
                }

            }
        }

        
    }



    [ObserversRpc]
    private void ShootClientHitScan(Vector3 ShootPosition, Vector3 hitPosition, Vector3 hitNormal, HitTarget target)
    {
        if (!base.IsOwner && !IsServer)
        {
            ShootingEffectHitScan(ShootPosition, hitPosition, hitNormal, target);
        }
    }


    private void ShootingEffectHitScan(Vector3 ShootPosition, Vector3 hitPosition, Vector3 hitNormal, HitTarget _target)
    {
        if (base.IsServerOnly) return;
        //recoilLeft.OnRecoil();
        //var index = UnityEngine.Random.Range(0, currentWeapon.shootSound.Length);
        //AudioSource.PlayClipAtPoint(currentWeapon.shootSound[index], ShootPosition);
        GameObject r = Instantiate(muzzleEffect, exitPoint.position, exitPoint.rotation);
        r.transform.parent = exitPoint;

        var trail = Instantiate(bulletTrail, ShootPosition, Quaternion.LookRotation(hitPosition - exitPoint.position));
        trail.GetComponent<HitscanTrail>().SetEndPos(hitPosition, _target, hitNormal, target.transform);
        
    }

    #endregion

}
