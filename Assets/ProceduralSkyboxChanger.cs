
using UnityEngine;

public class ProceduralSkyboxChanger : MonoBehaviour
{
    public Material skyboxMaterial;
    public Color color1;    //color of map 1
    public Color color2;
    public Color color3;

    //Center position of each island
    public Transform end1;
    public Transform end2;
    public Transform end3; //havent had map3 yet

    [SerializeField]
    private Transform TransitionPoint1to2;
    [SerializeField]
    private Transform TransitionPoint2to1;

    private int currentIslandIndex; 
    private Transform thisEnd;
    private int ending;

    private bool doneChanging;
    
    //void Update()
    //{
    //    Color result = color1 + (color2 - color1) * offset;
    //    skyboxMaterial.SetColor("_SkyTint", result);

    //}

    public void StartChanging(Vector3 characterPosition)
    {
        if(Vector3.Distance(end1.position, characterPosition) > Vector3.Distance(end2.position, characterPosition))
        {
            thisEnd = end1;
            ending = 1;
        }
        else
        {
            thisEnd = end2;
            ending = 2;
        }
        //Debug.Log(ending);
    }

    public void ChangeSkyboxColor(Vector3 characterPosition)
    {
        //float offset = Vector3.Distance(thisEnd.position, characterPosition) / Vector3.Distance(end1.position, end2.position);
        ////Debug.Log(offset);
        //if(ending == 1)
        //{
        //    Color result = color1 + (color2 - color1) * offset;
        //    skyboxMaterial.SetColor("_Tint", result);
        //}
        //else if(ending == 2)
        //{
        //    Color result = color2 + (color1 - color2) * offset;
        //    skyboxMaterial.SetColor("_Tint", result);
        //}
        float offset;
        Color result;
        switch (currentIslandIndex)
        {
            case 0:
                //1-2 or 1-3
                //1-2
                offset = Mathf.Abs(characterPosition.x - TransitionPoint2to1.position.x) / Mathf.Abs(TransitionPoint1to2.position.x - TransitionPoint2to1.position.x);
                result = color2 + (color1 - color2) * offset;
               
                skyboxMaterial.SetColor("_Tint", result);
                break;
            case 1:
                //2-1 or 2-3
                //2-1
                offset = Mathf.Abs(characterPosition.x - TransitionPoint1to2.position.x) / Mathf.Abs(TransitionPoint1to2.position.x - TransitionPoint2to1.position.x);
                result = color1 + (color2 - color1) * offset;
                skyboxMaterial.SetColor("_Tint", result);
                break;
            case 2:
                //Havent had map 3 yet
                break;

        }

    }

    public void InitialSkybox(Vector3 characterPosition)
    {
        float[] distance = new float[3];
        distance[0] = Vector3.Distance(end1.position, characterPosition);
        distance[1] = Vector3.Distance(end2.position, characterPosition);
        distance[2] = Vector3.Distance(end3.position, characterPosition);
       
        switch (IndexOfMin(distance))
        {
            case 0:
                skyboxMaterial.SetColor("_Tint", color1);
                currentIslandIndex = 0;
                break;
            case 1:
                skyboxMaterial.SetColor("_Tint", color2);
                currentIslandIndex = 1;
                break;
            case 2:
                //Havent had map 3 yet
                break;

        }
        
    }

    public static int IndexOfMin(float[] self)
    {


        float min = self[0];
        int minIndex = 0;

        for (int i = 0; i < self.Length; i++)
        {
            if (self[i] < min)
            {
                min = self[i];
                minIndex = i;
            }
        }
        
        return minIndex;
    }
}
