using EasyCharacterMovement;
using FishNet;
using FishNet.Managing.Timing;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zipline : NetworkBehaviour
{
    [SerializeField] private Transform startPoint;
    [SerializeField] private Transform endPoint;

    private bool transporting = false;

    private NetworkCharacter character;

    private void Awake()
    {
        InstanceFinder.TimeManager.OnTick += OnTick;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!InstanceFinder.IsServer) return;

        if (transporting) return;

        if (other.CompareTag("Player"))
        {
            print("detected");
            character = other.GetComponent<NetworkCharacter>();
            if (character == null)
                character = other.GetComponentInParent<NetworkCharacter>();
            StartZipline(character);
        }

    }

    [ObserversRpc(RunLocally = true)]
    private void StartZipline(NetworkCharacter activateCharacter)
    {
        character = activateCharacter;
        transporting = true;
        character.canEverMove = false;
        character.GetAnimator().SetBool("Ziplining", true);
        character.SetPosition(startPoint.position);
        character.GetComponent<ZiplineManager>().EnableMotor(true);
        character.RotateCharacter(endPoint.position - startPoint.position);
    }

    private void OnTick()
    {
        if (transporting)
        {
            if (Vector3.Distance(character.transform.position, endPoint.position) > 0.2f)
            {
                Vector3 newPos = character.transform.position;
                newPos = Vector3.MoveTowards(newPos, endPoint.position, 10f * (float)InstanceFinder.TimeManager.TickDelta);
                character.SetPosition(newPos);
            }
            else
            {
                character.canEverMove = true;
                transporting = false;
                character.GetAnimator().SetBool("Ziplining", false);
                character.GetComponent<ZiplineManager>().EnableMotor(false);
            }
        }
    }
}
