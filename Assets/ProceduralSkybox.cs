using UnityEngine;

public class ProceduralSkybox : MonoBehaviour
{
    private Vector3 initialCharacterPosition;
    private void OnEnable()
    {
        initialCharacterPosition = transform.GetChild(1).position;
        GameManager.Instance.ProceduralSkyboxChanger.InitialSkybox(initialCharacterPosition);
        //Debug.Log("call PS here");
    }

    private void Start()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Skybox"))
        {
            other.GetComponent<ProceduralSkyboxChanger>().StartChanging(transform.GetChild(1).position);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Skybox"))
        {
            other.GetComponent<ProceduralSkyboxChanger>().ChangeSkyboxColor(transform.GetChild(1).position);
        }
    }
}
