using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;

public class TestGrenade : NetworkBehaviour
{
    [SerializeField]
    private string grenadeName = "Astro fucking grenade";

    [Range(0.0f, 1.0f)]
    public float velocityLossOnCollision = 0.5f;

    [Range(0.0f, 20f)]
    public float explosionDelay = 2f;

    [Range(0.0f, 20f)]
    public float explosionSize = 2f;

    [SerializeField]
    private float damage = 70f;

    [SerializeField]
    private float explodeVelocity = 8f;

    [SerializeField]
    private ParticleSystem explodeEffect;

    [SerializeField]
    private AudioClip explodeSound;

    [SerializeField]
    private AudioClip collideSound;

    [SerializeField]
    private LayerMask damageableLayer;

    [SerializeField]
    private GameObject graphic;

    [SerializeField]
    private bool delayGraphic = true;

    public NetworkCharacter owner;

    private bool exploding = false;
    private bool ignorePlayer = true;

    public Vector3 force;
    public Vector3 angulatVelocity;

    public void ApplyThrowImpulse(Vector3 force, Vector3 angulatVelocity)
    {
        this.force = force;
        this.angulatVelocity = angulatVelocity;
    }

    public override void OnStartServer()
    {
        base.OnStartServer();

        GetComponent<Rigidbody>().AddForce(force);
        GetComponent<Rigidbody>().angularVelocity = angulatVelocity;
        
        Invoke("ActivatePlayerCollision", 0.3f);

    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        if(delayGraphic)
        {
            graphic.SetActive(false);
            Invoke("EnableGraphic", 0.1f);
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player") && ignorePlayer) return;

        if(!IsServerOnly)
            AudioSource.PlayClipAtPoint(collideSound, transform.position);

        if (!IsServer) return;

        
            

        GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity * velocityLossOnCollision;
        

        if (!exploding)
        {
            Invoke("Explode", explosionDelay);
            exploding = true;
        }
    }

    private void ActivatePlayerCollision()
    {
        ignorePlayer = false;
        graphic.SetActive(true);
    }

    private void EnableGraphic()
    {
        graphic.SetActive(true);
    }

    private void Explode()
    {
        ExplodeEffect(transform.position);

        var damageableHit = Physics.OverlapSphere(transform.position, explosionSize, damageableLayer);
        foreach(var hit in damageableHit)
        {
            Health health = hit.GetComponentInParent<Health>();
            if (health != null)
            {
                health.ReduceHealth(damage, owner, grenadeName);
                NetworkCharacter hitCharacter = hit.GetComponentInParent<NetworkCharacter>();
                Vector3 addVelocity;
                if (hitCharacter.characterMovement.isGrounded)
                    addVelocity = -(transform.position - hitCharacter.transform.position).normalized * explodeVelocity + transform.up * 5f;
                else
                    addVelocity = -(transform.position - hitCharacter.transform.position).normalized * explodeVelocity;

                hitCharacter.AddVelocity(addVelocity);
            }
        }

        Despawn(DespawnType.Destroy);
    }

    [ObserversRpc]
    private void ExplodeEffect(Vector3 position)
    {
        var explosion = Instantiate(explodeEffect, position, Quaternion.identity);
        explosion.transform.localScale = Vector3.one * explosionSize;

        AudioSource.PlayClipAtPoint(explodeSound, position);
    }
}
