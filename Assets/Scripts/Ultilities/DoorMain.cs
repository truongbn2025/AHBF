﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

// v1.0 RASKALOF INITIAL DEVELOPMENT
// This is the main door script which handles all doors things together

public class DoorMain : MonoBehaviour {
    [SerializeField] LOCKED_STATUS locked = LOCKED_STATUS.LOCKED; // Lock status of door, defines: can door be opened?
    [SerializeField] DOOR_STATE door_state = DOOR_STATE.CLOSED; // Door state
    [SerializeField] DOOR_CLOSE_TYPE closing_type; // Closing type: for different purposes
    [SerializeField] float time_to_timed_close = 5f; // If timed close selected, this is time after which door will begin closing
    [SerializeField] protected FlapClass[] door_flaps; // All door flaps

    [SerializeField] UnityEvent OnUnlock;
    [SerializeField] UnityEvent OnLock;
    [SerializeField] UnityEvent OnOpenDenial;
    [SerializeField] UnityEvent OnFirstTimeOpened;

    bool timed_close_in_action; // For system purposes
    bool first_time_opened = false;

    public LOCKED_STATUS GetLockedStatus() { // Used for getting door lock status
        return locked;
    }

    public bool GetFirstTimeOpened() { // Used for getting door first time opened
        return first_time_opened;
    }

    public void SetFirstTimeOpened(bool state) { // Used for setting first time opened status
        if(OnFirstTimeOpened != null) OnFirstTimeOpened.Invoke();
        first_time_opened = state;
    }

    public void SetFirstTimeNoInvoke(bool state) { //Use this for loading
        first_time_opened = state;
    }

    public void SetLocked(LOCKED_STATUS status) { // Used for setting door lock status (Cant be used from UnityEvents)
        locked = status;
        if (status == LOCKED_STATUS.UNLOCKED) {
            OnUnlock?.Invoke();
        }
        if (status == LOCKED_STATUS.LOCKED) {
            OnLock?.Invoke();
        }
    }

    public void SetLockedNoInvoke(bool lockedst) { // For some reasons maybe needed just to set locked status but without invoking events
        if (lockedst) locked = LOCKED_STATUS.LOCKED; else locked = LOCKED_STATUS.UNLOCKED;
    }

    public void UnlockDoorNoInvoke() { // Used for forcibly unlock door without invoking events
        locked = LOCKED_STATUS.UNLOCKED;
    }

    public void UnlockDoor() { // Used to unlock door (Can be used from UnityEvents)
        if(GetLockedStatus() == LOCKED_STATUS.LOCKED) SetLocked(LOCKED_STATUS.UNLOCKED);
    }

    public DOOR_STATE GetDoorState() { // Used for getting door state (opened/closed, inaction)
        return door_state;
    }

    public void SetDoorState(DOOR_STATE state) { // Used for setting door state
        if (GetDoorState() != state) {
            door_state = state;
        }
    }

    public void CheckDoorStatus() { // Used for checking all flaps statusses to define is door reaches target state and set this status in good case
        if (door_flaps.Any(itm => itm.GetFlapStatus() == DOOR_STATE.INACTION_OPEN)) {
            SetDoorState(DOOR_STATE.INACTION_OPEN);
        }
        if (door_flaps.Any(itm => itm.GetFlapStatus() == DOOR_STATE.INACTION_CLOSE)) {
            SetDoorState(DOOR_STATE.INACTION_CLOSE);
        }
        if (door_flaps.All(itm => itm.GetFlapStatus() == DOOR_STATE.OPENED)) {
            SetDoorState(DOOR_STATE.OPENED);
        }
        if (door_flaps.All(itm => itm.GetFlapStatus() == DOOR_STATE.CLOSED)) {
            SetDoorState(DOOR_STATE.CLOSED);
        }
        
    }

    public void UEI_TryToOpen(int direction) { // Try to open the door, this is not forcibly! (Used from UnityEvents)
        TryToOpen((OPEN_DIRECTION) direction);
    }

    public void TryToOpen(OPEN_DIRECTION direction = OPEN_DIRECTION.DIRECT) { // Try to open the door, this is not forcibly! (Not used from UnityEvents, use UEI_TryToOpen)
        if (GetLockedStatus() == LOCKED_STATUS.UNLOCKED) {
            if ((GetDoorState() == DOOR_STATE.CLOSED || GetDoorState() == DOOR_STATE.INACTION_CLOSE)) {
                StopAllCoroutines();
                timed_close_in_action = false;
                if (!GetFirstTimeOpened()) {
                    SetFirstTimeOpened(true);
                }
                foreach (FlapClass flap in door_flaps) {
                    flap.SetCaller(this);
                    flap.StartOpen(direction);
                }
                if (closing_type == DOOR_CLOSE_TYPE.TIMED_CLOSE && !timed_close_in_action) StartCoroutine("DoorCloseTimerStart");
            }
        } else {
            if (OnOpenDenial != null) OnOpenDenial.Invoke();
        }
    }

    public void TryToClose() { // Try to close the door, this is not forcibly! (Not used from UnityEvents)
        if (closing_type == DOOR_CLOSE_TYPE.AUTO_CLOSE || closing_type == DOOR_CLOSE_TYPE.TIMED_CLOSE || closing_type != DOOR_CLOSE_TYPE.DONT_CLOSE || closing_type == DOOR_CLOSE_TYPE.MANUAL_CLOSE) {
            if (GetDoorState() == DOOR_STATE.OPENED || GetDoorState() == DOOR_STATE.INACTION_OPEN) {
                foreach (FlapClass flap in door_flaps) {
                    flap.SetCaller(this);
                    flap.StartClose();
                }
            }
        }
    }

    public void SwitchDoorState() { // For situation when you dont know in which state door is and you just want to switch state opened <=> closed
        if (GetDoorState() == DOOR_STATE.OPENED || GetDoorState() == DOOR_STATE.INACTION_OPEN) {
            foreach (FlapClass flap in door_flaps) {
                flap.SetCaller(this);
                flap.StartClose();
            }
        }else
        if (GetLockedStatus() == LOCKED_STATUS.UNLOCKED && (GetDoorState() == DOOR_STATE.CLOSED || GetDoorState() == DOOR_STATE.INACTION_CLOSE)) {
            StopAllCoroutines();
            timed_close_in_action = false;
            foreach (FlapClass flap in door_flaps) {
                flap.SetCaller(this);
                flap.StartOpen(OPEN_DIRECTION.DIRECT);
            }
            if (closing_type == DOOR_CLOSE_TYPE.TIMED_CLOSE && !timed_close_in_action) StartCoroutine("DoorCloseTimerStart");
        }
    }

    public void HardCloseDoor() { // Forcibly close door (no animations etc, just set door state as its already passed all things) with UnityEvents invoking
        foreach (FlapClass flap in door_flaps) {
            flap.HardSetState(DOOR_STATE.CLOSED);
        }
    }
    public void HardCloseDoorNoInvoke() { // Forcibly close door (no animations etc, just set door state as its already passed all things) without UnityEvents invoking
        foreach (FlapClass flap in door_flaps) {
            flap.HardSetState(DOOR_STATE.CLOSED, false);
        }
    }

    public void HardOpenDoor() { // Forcibly open door (no animations etc, just set door state as its already passed all things) with UnityEvents invoking
        foreach (FlapClass flap in door_flaps) {
            flap.HardSetState(DOOR_STATE.OPENED);
        }
    }
    public void HardOpenDoorNoInvoke() { // Forcibly open door (no animations etc, just set door state as its already passed all things) without UnityEvents invoking
        foreach (FlapClass flap in door_flaps) {
            flap.HardSetState(DOOR_STATE.OPENED, false);
        }
    }

    public void SetDoorClosingType(DOOR_CLOSE_TYPE closing_door_type) { // If you want to change door closing type on the go
        closing_type = closing_door_type;
    }

    public DOOR_CLOSE_TYPE GetDoorClosingType() { // Get door closing type for scripting purposes
        return closing_type;
    }

    IEnumerator DoorCloseTimerStart() { // Used for timed close, to try to close door after setted amount of time
        timed_close_in_action = true;
        yield return new WaitForSeconds(time_to_timed_close);
        timed_close_in_action = false;
        TryToClose();
    }

}
