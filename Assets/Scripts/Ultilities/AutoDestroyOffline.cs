using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyOffline : MonoBehaviour
{
    public float time = 0.5f;

    private void Start()
    {
        StartCoroutine(AutoDestroyCoroutine(time));
    }

    IEnumerator AutoDestroyCoroutine(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
