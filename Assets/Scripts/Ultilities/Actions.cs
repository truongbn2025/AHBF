using System;


public static class Actions
{
    public static Action<WEAPON_TYPE, int> OnWeaponSwitch;
    public static Action<WEAPON_TYPE> OnWeaponDrop;
    public static Action<int> OnPrimaryAmmoChange;
    public static Action<int> OnSecondaryAmmoChange;
    public static Action<int> OnGrenadeNumberChange;
    public static Action<int> OnReloadPrimaryAmmo;
    public static Action<int> OnReloadSecondaryAmmo;
}
