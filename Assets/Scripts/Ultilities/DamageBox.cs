using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageBox : MonoBehaviour
{
    public float damage;

    public event Action<Collider, float> Hit;

    private void OnTriggerEnter(Collider other)
    {
        Hit?.Invoke(other, damage);
    }

}
