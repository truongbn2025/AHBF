using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class ToolTip : MonoBehaviour
{
    public static ToolTip Instance;
    public CanvasGroup tooltips;

    [SerializeField] private Image icon;
    [SerializeField] private TextMeshProUGUI name;
    [SerializeField] private TextMeshProUGUI type;
    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] private Vector3 offset;
    [SerializeField] private List<GameObject> rarity;

    private RectTransform _rectTransform;
    private Sprite _icon;
    private string _name = "Medkit";
    private string _type = "Healing";
    private string _description = "Heal";
    private int _levelRarity = 0;

    private void ChangePivot(SCREEN_POSITION position)
    {
        switch (position)
        {
            case SCREEN_POSITION.BOTTOM_LEFT:
                _rectTransform.pivot = new Vector2(0, 0);
                break;
            case SCREEN_POSITION.TOP_LEFT:
                _rectTransform.pivot = new Vector2(0, 1);
                break;
            case SCREEN_POSITION.BOTTOM_RIGHT:
                _rectTransform.pivot = new Vector2(1, 0);
                break;
            case SCREEN_POSITION.TOP_RIGHT:
                _rectTransform.pivot = new Vector2(1, 1);
                break;
        }
    }
    public void ShowToolTip(Vector3 position, SCREEN_POSITION localPosition)
    {
        ChangePivot(localPosition);
        transform.position = position;
        gameObject.SetActive(true);
        name.text = _name;
        type.text = _type;
        description.text = _description;
        icon.sprite = _icon;
        DisplayRarity(_levelRarity);
        transform.DOMove(position, 0.25f);
        //transform.position = position;
        gameObject.SetActive(true);
        tooltips.alpha = 0;
        tooltips.DOFade(1, 0.5f);
        transform.localScale = new Vector3(0, 0, 0);
        transform.DOScale(1, 0.5f);
    }
    public void ChangeTooltipsProperties(string newName, string newType, string newDescription, int newRarity, Sprite newIcon)
    {

        _name = newName;
        _type = newType;
        _description = newDescription;
        _levelRarity = newRarity;
        _icon = newIcon;

    }
    public void HideTooltip()
    {
        tooltips.DOFade(0, 0.25f);
        transform.DOScale(0, 0.25f);
        gameObject.SetActive(false);
    }
    private void DisplayRarity(int levelRarity)
    {
        
        foreach (GameObject star in rarity)
            star.SetActive(false);
        for (int i = 0; i< levelRarity; i++)
        {
            rarity[i].SetActive(true);
        }
    }

    #region MonoBehavior
    private void Awake()
    {
        Instance = this;
        _rectTransform = GetComponent<RectTransform>();
        gameObject.SetActive(false);

    }
    #endregion
}
