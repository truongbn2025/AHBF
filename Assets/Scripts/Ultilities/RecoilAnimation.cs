using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class RecoilAnimation : NetworkBehaviour
{
    [SerializeField]
    private MultiAimConstraint lowerArmContrain;
    [SerializeField]
    private Vector3 defaultLowerArmOffset;
    [SerializeField]
    private MultiAimConstraint handContrain;
    [SerializeField]
    private Vector3 defaultHandOffset;
    [SerializeField]
    private float recoilAmount = -30f;

    [SerializeField]
    private float reloadAmount = -30f;

    private bool updateOffset = true;

    public void OnRecoil()
    {
        lowerArmContrain.data.offset = new Vector3(defaultLowerArmOffset.x, defaultLowerArmOffset.y, defaultLowerArmOffset.z + recoilAmount);
    }

    public void OnReload()
    {
        lowerArmContrain.data.offset = new Vector3(defaultLowerArmOffset.x, defaultLowerArmOffset.y, defaultLowerArmOffset.z + reloadAmount);
        handContrain.data.offset = new Vector3(defaultHandOffset.x, defaultHandOffset.y, defaultHandOffset.z + reloadAmount);
        updateOffset = false;
        if (IsServer) OnReloadObserver();
    }

    public void OnReloadCompleted()
    {
        updateOffset = true;
        if (IsServer) OnReloadCompletedObserver();
    }

    [ObserversRpc(ExcludeOwner = true)]
    public void OnReloadObserver()
    {
        if (IsServer) return;
        lowerArmContrain.data.offset = new Vector3(defaultLowerArmOffset.x, defaultLowerArmOffset.y, defaultLowerArmOffset.z + reloadAmount);
        handContrain.data.offset = new Vector3(defaultHandOffset.x, defaultHandOffset.y, defaultHandOffset.z + reloadAmount);
        updateOffset = false;
    }

    [ObserversRpc(ExcludeOwner = true)]
    public void OnReloadCompletedObserver()
    {
        if (IsServer) return;
        updateOffset = true;
    }

    private void Update()
    {
        if(updateOffset)
        {
            lowerArmContrain.data.offset = Vector3.Lerp(lowerArmContrain.data.offset, defaultLowerArmOffset, Time.deltaTime * 20f);
            handContrain.data.offset = Vector3.Lerp(handContrain.data.offset, defaultHandOffset, Time.deltaTime * 20f);
        }
    }
}
