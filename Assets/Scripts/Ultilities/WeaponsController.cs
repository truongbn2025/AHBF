using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;
using FishNet.Component.Animating;
using TMPro;
using FishNet.Managing.Timing;
using UnityEngine.Animations.Rigging;
using FishNet.Connection;
using System;
using Random = UnityEngine.Random;
using FishNet;
using UnityEngine.UI;

public enum CombatState
{
    Normal,
    LeftAiming,
    RightAiming,
    Throwing,
    Meleeing,
    CombatLock
}

public enum TargetLeftGun
{
    Main,
    SideArm,
}

public class WeaponsController : NetworkBehaviour
{
    public CombatState combatState;
    public LayerMask shootingLayer;

    public Weapon currentWeapon;

    public GameObject[] UIWeapon;
    public GameObject[] HandWeapons;
    public RightHandWeapon rightHandWeapon;
    public TextMeshProUGUI ClipNormal;
    public TextMeshProUGUI ClipSideArm;
    public TextMeshProUGUI ClipSpecial;
    public GameObject switchUI;

    public int GunIndex;
    public int LeftMainIndex;
    public int LeftSideArmIndex;
    public int MeleeIndex;
    public int ThrowableIndex;
   

    public bool GunEquiped;
    public bool MeleeEquiped;
    public bool ThrowEquiped;
    public bool CanAim;
    public bool UsingMainLeftWeapon;

    public float UnAimTime = 0.5f;
    public float EquipFireDelay = 0.1f;

    public int CurrentAmmo = 1000;
    public event Action<int> AmmoChange;
    public int CurrentSpecialAmmo = 1000;
    public int CurrentGrenades = 100;

    [SerializeField]
    private Transform grenadeThrowPoint;

    [SerializeField]
    private TestGrenade grenadePrefab;

    [SerializeField]
    private TestGrenadeDummy grenadeDummyPrefab;

    [SerializeField]
    private Transform aimThrowPoint;
    private Vector3 aimThrowPos;

    [SerializeField]
    private AudioClip throwSound;

    [SerializeField]
    private AudioClip axeSound;

    public float forwardForce;
    public float upwardForce;

    public RecoilAnimation recoilLeft;
    public RecoilAnimation recoilRight;

    public GameObject shields;

    private Animator animator;
    private NetworkAnimator networkAnimator;
    private NetworkCharacter networkCharacter;
    private InputManager inputManager;
    private BodyPartsManager bodyPartsManager;
    private CameraController cameraController;
    public RigBuilder rigBuilder;
    private float unAimTimer;
    private Weapon leftMainWeapon;
    private Weapon leftSideArmWeapon;
    private Weapon rightWeapon;
    private int weaponIndex;
    private Health health;
    private Camera mainCamera;
    private Transform hitTarget;
    //Events

    public event Action<Vector3> OnAxeAttack1;
    public event Action<Vector3> OnSwordAttack1;

    private float shieldCooldown;

    private bool clientDoneCalled = false;

    private int randomNum;

    private void Awake()
    {
        print(rigBuilder);
        cacheComponents();
        print(rigBuilder);
        combatState = CombatState.Normal;
        bodyPartsManager.LoadingPartReady += OnLoadingCompleted;
        InstanceFinder.TimeManager.OnTick += OnTick;
        print(rigBuilder);
        randomNum = Random.Range(0, 100000);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (!gameObject.activeInHierarchy) return;
        print("called " + randomNum + gameObject.GetInstanceID());
        ClientDoneSpawning();

    }

    [ServerRpc(RunLocally = true)]
    private void ClientDoneSpawning()
    {
        print(clientDoneCalled + "_" + randomNum + "");

        if(clientDoneCalled) return;

        clientDoneCalled = true;

        var inventory = GetComponent<PlayerInventory>();
        Actions.OnWeaponSwitch += OnWeaponSwitch;
        Actions.OnWeaponDrop += OnWeaponDrop;
        Actions.OnPrimaryAmmoChange += OnAmmoChange;
        Actions.OnSecondaryAmmoChange += OnSpecialAmmoChange;
        Actions.OnGrenadeNumberChange += OnGrenadeChange;

        rigBuilder.enabled = true;
        CanAim = true;
        ClipNormal.text = "0/" + CurrentAmmo;
        ClipSideArm.text = "0/" + CurrentAmmo;
        ClipSpecial.text = "0/" + CurrentSpecialAmmo;
    }

    private void Update()
    {
        if (!IsOwner) return;

        if (networkCharacter.characterState != CharacterState.Playing || !CanAim)
        {
            inputManager.aim = false;
            inputManager.specialAction = false;
            OnLeftAim(false);
            OnRightAim(false);
            return;
        }

        UpdateWeaponState();
        HandleCombatAction();
        UpdateWeaponUI();


        if (inputManager.skill_1)
        {
            inputManager.aim = false;
            inputManager.specialAction = false;
            OnLeftAim(false);
            OnRightAim(false);
            SwitchToMainWeapon(!UsingMainLeftWeapon);
            inputManager.skill_1 = false;
        }

        if (bodyPartsManager.shields[bodyPartsManager.partID.shield].name != "None")
        {
            if (inputManager.skill_2 && !shields.activeInHierarchy && shieldCooldown <= 0f)
            {
                inputManager.aim = false;
                inputManager.specialAction = false;
                OnLeftAim(false);
                OnRightAim(false);
                OnShieldUp();
                inputManager.skill_1 = false;
            }
            else if (inputManager.skill_2 && shields.activeInHierarchy && shieldCooldown <= 0f)
            {
                OnShieldDown();
                inputManager.skill_1 = false;
            }
        }

        inputManager.throwAction = false;
        inputManager.reload = false;
    }

    [ServerRpc(RunLocally = true)]
    private void OnShieldUp()
    {
        networkCharacter.canEverMove = false;
        shields.SetActive(true);
        ShieldEffect(true);
        shieldCooldown = 1f;
        animator.SetBool("Shielding", true);
    }

    [ServerRpc(RunLocally = true)]
    private void OnShieldDown()
    {
        networkCharacter.canEverMove = true;
        shields.SetActive(false);
        ShieldEffect(false);
        shieldCooldown = 1f;
        animator.SetBool("Shielding", false);
    }

    [ObserversRpc(ExcludeOwner = true)]
    private void ShieldEffect(bool isOn)
    {
        shields.SetActive(isOn);
    }

    private void cacheComponents()
    {
        animator = GetComponent<Animator>();
        networkAnimator = GetComponent<NetworkAnimator>();
        networkCharacter = GetComponent<NetworkCharacter>();
        inputManager = FindObjectOfType<InputManager>();
        bodyPartsManager = GetComponentInChildren<BodyPartsManager>();
        rigBuilder = GetComponent<RigBuilder>();
        health = GetComponent<Health>();
        mainCamera = Camera.main;
        cameraController = GetComponent<CameraController>();
    }

    #region RPCs

    

    [ServerRpc(RunLocally = true)]
    private void SwitchToMainWeapon(bool main)
    {
        if(leftMainWeapon)
            if(leftMainWeapon.reloading)
            {
                leftMainWeapon.reloading = false;
                leftMainWeapon.currentClip = 0;
                ClipNormal.text = "0/" + CurrentAmmo;
            }

        if(leftSideArmWeapon)
            if(leftSideArmWeapon.reloading)
            {
                leftSideArmWeapon.reloading = false;
                leftSideArmWeapon.currentClip = 0;
                ClipSideArm.text = "0/" + CurrentAmmo;
            }
        
        UsingMainLeftWeapon = main;

        recoilLeft.OnReloadCompleted();

        if (UsingMainLeftWeapon)
        {
            if (leftMainWeapon == null) 
                GunEquiped = false;
            else
            {
                GunEquiped = true;
                GunIndex = LeftMainIndex;
                UpdateGunIndex(GunIndex);
            }
        }
        else
        {
            if (leftSideArmWeapon == null) 
                GunEquiped = false;
            else
            {
                GunEquiped = true;
                GunIndex = LeftSideArmIndex;
                UpdateGunIndex(GunIndex);
            }
        }
    }

    private void UpdateWeaponUI()
    {
        if(UsingMainLeftWeapon)
        {
            if(leftMainWeapon)
                UIWeapon[LeftMainIndex].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            if(leftSideArmWeapon)
                UIWeapon[LeftSideArmIndex].GetComponent<Image>().color = new Color32(123, 123, 123, 255);
        }
        else
        {
            if (leftSideArmWeapon)
                UIWeapon[LeftSideArmIndex].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            if (leftMainWeapon)
                UIWeapon[LeftMainIndex].GetComponent<Image>().color = new Color32(123, 123, 123, 255);
        }
    }

    [ObserversRpc(ExcludeOwner = true)]
    private void UpdateGunIndex(int index)
    {
        GunIndex = index;
    }

    [ServerRpc(RunLocally = true)]
    public void SetCanAimFlag(bool flag)
    {
        CanAim = flag;
    }

    [ObserversRpc(RunLocally = true)]
    private void SwitchWeapon(WEAPON_TYPE type, int weaponIndex)
    {
        switch (type)
        {
            case WEAPON_TYPE.MAIN_GUN:
                if (weaponIndex < 0)
                {
                    CurrentAmmo += leftMainWeapon.currentClip;
                    leftMainWeapon.reloading = false;
                    leftMainWeapon.currentClip = 0;

                    UIWeapon[LeftMainIndex].SetActive(false);
                    ClipNormal.text = "0/" + CurrentAmmo;

                    leftMainWeapon = null;
                    LeftMainIndex = weaponIndex;
                }
                else
                {
                    if (leftMainWeapon == null && leftSideArmWeapon == null) UsingMainLeftWeapon = true;
                    leftMainWeapon = HandWeapons[weaponIndex].GetComponent<Weapon>();

                    //UIWeapon[LeftMainIndex].SetActive(false);

                    LeftMainIndex = weaponIndex;

                    UIWeapon[LeftMainIndex].SetActive(true);

                    leftMainWeapon.reloading = false;
                    leftMainWeapon.currentClip = 0;
                }
                SwitchToMainWeapon(UsingMainLeftWeapon);
                break;
            case WEAPON_TYPE.SIZE_ARM:
                if (weaponIndex < 0)
                {
                    CurrentAmmo += leftSideArmWeapon.currentClip;
                    leftSideArmWeapon.reloading = false;
                    leftSideArmWeapon.currentClip = 0;

                    UIWeapon[LeftSideArmIndex].SetActive(false);
                    ClipSideArm.text = "0/" + CurrentAmmo;

                    leftSideArmWeapon = null;
                    LeftSideArmIndex = weaponIndex;
                }
                else
                {
                    if (leftMainWeapon == null && leftSideArmWeapon == null) UsingMainLeftWeapon = false;
                    leftSideArmWeapon = HandWeapons[weaponIndex].GetComponent<Weapon>();
                    LeftSideArmIndex = weaponIndex;
                    UIWeapon[LeftSideArmIndex].SetActive(true);
                    leftSideArmWeapon.reloading = false;
                    leftSideArmWeapon.currentClip = 0;
                }
                SwitchToMainWeapon(UsingMainLeftWeapon);
                break;
            case WEAPON_TYPE.MELEE:
                if (weaponIndex < 0)
                {
                    MeleeEquiped = false;
                    UIWeapon[MeleeIndex].SetActive(false);
                    MeleeIndex = weaponIndex;
                }
                else
                {
                    MeleeEquiped = true;
                    MeleeIndex = weaponIndex;
                    UIWeapon[MeleeIndex].SetActive(true);
                }
                break;
            case WEAPON_TYPE.THROWABLE:
                break;
        }

        if (leftMainWeapon != null && leftSideArmWeapon != null)
            switchUI.SetActive(true);
        else
            switchUI.SetActive(false);

        if (leftMainWeapon)
            ClipNormal.gameObject.SetActive(true);
        else
            ClipNormal.gameObject.SetActive(false);

        if (leftSideArmWeapon)
            ClipSideArm.gameObject.SetActive(true);
        else
            ClipSideArm.gameObject.SetActive(false);
    }

    public void OnWeaponSwitch(WEAPON_TYPE type, int weaponIndex)
    {
        SwitchWeapon(type, weaponIndex);
    }
    public void OnWeaponDrop(WEAPON_TYPE type)
    {
        SwitchWeapon(type, -1);
    }

    [ObserversRpc(RunLocally = true)]
    public void OnAmmoChange(int ammountLeft)
    {
        Debug.Log(ammountLeft);
        CurrentAmmo = ammountLeft;
        UpdateLeftClip();
    }

    [ObserversRpc(RunLocally = true)]
    public void OnSpecialAmmoChange(int ammountLeft)
    {
        CurrentSpecialAmmo = ammountLeft;
        if (rightWeapon)
            ClipSpecial.text = rightWeapon.currentClip + "/" + CurrentSpecialAmmo;
        else
            ClipSpecial.text = "0/" + CurrentSpecialAmmo;
    }

    [ObserversRpc(RunLocally = true)]
    public void OnGrenadeChange(int ammountLeft)
    {
        CurrentGrenades = ammountLeft;
    }

    [ServerRpc(RunLocally = true)]
    private void OnLeftAim(bool isAim)
    {
        if (isAim)
        {
            HandWeapons[GunIndex].SetActive(true);
            OnLeftAimObserver(true);
            combatState = CombatState.LeftAiming;
            SetCurrentWeapon(UsingMainLeftWeapon ? leftMainWeapon : leftSideArmWeapon);
            SetCurrentWeaponObserver(true, UsingMainLeftWeapon);
            animator.SetBool("AimingLeft", true);
            SetRigActiveState("left");
            currentWeapon.fireDelayTimer = EquipFireDelay;
        }
        else
        {
            if (GunEquiped)
            {
                HandWeapons[GunIndex].SetActive(false);
                OnLeftAimObserver(false);
            }
            combatState = CombatState.Normal;
            animator.SetBool("AimingLeft", false);
            SetRigActiveState("none");
        }
    }

    [ObserversRpc(ExcludeOwner = true)]
    private void OnLeftAimObserver(bool isAim)
    {
        HandWeapons[GunIndex].SetActive(isAim);
    }

    [ServerRpc(RunLocally = true)]
    private void OnRightAim(bool isAim)
    {
        if (isAim)
        {
            SetCurrentWeapon(rightWeapon);
            SetCurrentWeaponObserver(false);
            combatState = CombatState.RightAiming;
            //animator.SetLayerWeight(animator.GetLayerIndex("Aim"), 1);
            animator.SetBool("AimingRight", true);
            SetRigActiveState("right");
            rightWeapon.fireDelayTimer = EquipFireDelay;
        }
        else
        {
            combatState = CombatState.Normal;
            //animator.SetLayerWeight(animator.GetLayerIndex("Aim"), 0);
            animator.SetBool("AimingRight", false);
            SetRigActiveState("none");
        }
    }

    [ObserversRpc]
    private void SetCurrentWeaponObserver(bool isLeftWeapon, bool isMainWeapon = true)
    {
        if (isLeftWeapon)
            SetCurrentWeapon(isMainWeapon ? leftMainWeapon : leftSideArmWeapon);
        else
            SetCurrentWeapon(rightWeapon);
    }

    [ObserversRpc]
    private void SetRigActiveState(string layer, bool interpolate = false)
    {
        if(interpolate)
        {
            if (layer == "none")
                DisableAllRig();
            if (layer == "left")
            {
                DisableAllRig();
                StartCoroutine(LerpWeight(rigBuilder.layers[0].rig, 1f));
            }
            if (layer == "right")
            {
                DisableAllRig();
                StartCoroutine(LerpWeight(rigBuilder.layers[1].rig, 1f));
            }
            if (layer == "grenade")
            {
                DisableAllRig();
                StartCoroutine(LerpWeight(rigBuilder.layers[2].rig, 1f));
            }
        }
        else
        {
            if (layer == "none")
                foreach (RigLayer rigLayer in rigBuilder.layers)
                    rigLayer.rig.weight = 0f;
            if (layer == "left")
            {
                foreach (RigLayer rigLayer in rigBuilder.layers)
                    rigLayer.rig.weight = 0f;
                rigBuilder.layers[0].rig.weight = 1;
            }
            if (layer == "right")
            {
                foreach (RigLayer rigLayer in rigBuilder.layers)
                    rigLayer.rig.weight = 0f;
                rigBuilder.layers[1].rig.weight = 1;
            }
            if (layer == "grenade")
            {
                foreach (RigLayer rigLayer in rigBuilder.layers)
                    rigLayer.rig.weight = 0f;
                rigBuilder.layers[2].rig.weight = 1;
            }
        }
    }
        

    private void DisableAllRig()
    {
        foreach (RigLayer rigLayer in rigBuilder.layers)
            StartCoroutine(LerpWeight(rigLayer.rig, 0f, false));
    }

    IEnumerator LerpWeight(Rig targetRig, float targetWeight, bool increase = true)
    {
        if(increase)
        {
            for (float weight = targetRig.weight; weight <= targetWeight; weight += 0.1f)
            {
                targetRig.weight = weight;
                yield return null;
            }
        }
        else
        {
            for (float weight = targetRig.weight; weight >= targetWeight; weight -= 0.1f)
            {
                targetRig.weight = weight;
                yield return null;
            }
        }
        

    }

    [ServerRpc]
    private void RequestReload(bool isLeftWeapon)
    {
        ReloadWeapon(Owner, isLeftWeapon, UsingMainLeftWeapon);
    }

    [TargetRpc(RunLocally = true)]
    private void ReloadWeapon(NetworkConnection connection, bool isLeftWeapon, bool isLeftMainWeapon = true)
    {
        Weapon currentWeapon;

        if (isLeftWeapon)
        {
            currentWeapon = isLeftMainWeapon ? leftMainWeapon : leftSideArmWeapon;
        }
        else
            currentWeapon = rightWeapon;

        if (CurrentAmmo <= 0 && isLeftWeapon || CurrentSpecialAmmo <= 0 && !isLeftWeapon) return;

        currentWeapon.fireDelayTimer = currentWeapon.BaseReloadDelay;
        currentWeapon.reloading = true;
        currentWeapon.consecutiveShot = 0;
        AudioSource.PlayClipAtPoint(currentWeapon.reloadSound, currentWeapon.gameObject.transform.position);
    }

    [TargetRpc(RunLocally = true)]
    private void SetClip(NetworkConnection connection, int amount, bool isLeftWeapon, bool isLeftMainWeapon = true)
    {
        if (isLeftWeapon)
        {
            if (isLeftMainWeapon)
            {
                leftMainWeapon.currentClip = amount;
                ClipNormal.text = currentWeapon.currentClip + "/" + CurrentAmmo;
            }
            else
            {
                leftSideArmWeapon.currentClip = amount;
                ClipSideArm.text = currentWeapon.currentClip + "/" + CurrentAmmo;
            }

        }
        else
        {
            rightWeapon.currentClip = amount;
            ClipSpecial.text = currentWeapon.currentClip + "/" + CurrentSpecialAmmo;
        }
    }

    #endregion

    private void UpdateWeaponState()
    {
        if (!GunEquiped) inputManager.aim = false;

        if (GunEquiped && combatState == CombatState.Normal && inputManager.aim && unAimTimer <= 0f)
        {
            inputManager.aim = false;
            inputManager.specialAction = false;
            OnLeftAim(true);
        }
        else if (combatState == CombatState.LeftAiming && (inputManager.aim || inputManager.specialAction || inputManager.sprint))
        {
            inputManager.aim = false;
            OnLeftAim(false);
        }

        if (combatState == CombatState.Normal && inputManager.specialAction && unAimTimer <= 0f)
        {
            inputManager.aim = false;
            inputManager.specialAction = false;

            if (rightHandWeapon.handType == RightHandType.Gun)
            {
                OnRightAim(true);
            }

            if(rightHandWeapon.handType == RightHandType.Melee)
            {
                CallRightMeleeAttack();
            }
        }
        else if (combatState == CombatState.RightAiming && (inputManager.aim || inputManager.specialAction || inputManager.sprint))
        {
            inputManager.specialAction = false;
            OnRightAim(false);
        }
    }

    [ServerRpc(RunLocally = true)]
    private void CallRightMeleeAttack()
    {
        RightWPMelee weapon = rightHandWeapon.GetComponent<RightWPMelee>();
        if (weapon == null) return;

        networkAnimator.SetTrigger("Attack");
        animator.SetInteger("AttackIndex", 0);
        animator.SetInteger("Weapon", 2);
        animator.SetFloat("Speed", 0);
        combatState = CombatState.Meleeing;
        networkCharacter.canEverMove = false;
        if (IsServer)
            networkCharacter.RotateCharacter(mainCamera.transform.forward);
        EquipWeapon();

        PlayAxeEffect();
    }

    private void UpdateTimers(float deltaTime)
    {
        shieldCooldown -= deltaTime;

        if (combatState == CombatState.LeftAiming || combatState == CombatState.RightAiming)
            unAimTimer = UnAimTime;
        else
            unAimTimer -= deltaTime;

        var leftCurrentWeapon = UsingMainLeftWeapon ? leftMainWeapon : leftSideArmWeapon;
        var leftCurrentClipText = UsingMainLeftWeapon ? ClipNormal : ClipSideArm;

        if (leftMainWeapon && UsingMainLeftWeapon || leftSideArmWeapon && !UsingMainLeftWeapon)
        {
            leftCurrentWeapon.fireDelayTimer -= deltaTime;
            if (leftCurrentWeapon.reloading)
            {
                leftCurrentClipText.text = "Reloading";
                recoilLeft.OnReload();
            }
            if (leftCurrentWeapon.reloading && leftCurrentWeapon.fireDelayTimer <= 0f)
            {
                int reloadAmount;

                int ammoNeeded = leftCurrentWeapon.Clip - leftCurrentWeapon.currentClip;

                if (ammoNeeded > CurrentAmmo)
                    reloadAmount = CurrentAmmo;
                else
                    reloadAmount = ammoNeeded;

                leftCurrentWeapon.currentClip += reloadAmount;
                CurrentAmmo -= reloadAmount;
                //inventory.AddPrimaryAmmo(item, -amount);
                Actions.OnReloadPrimaryAmmo?.Invoke(reloadAmount);
                //AmmoChange?.Invoke(CurrentAmmo);

                UpdateLeftClip();

                leftCurrentWeapon.reloading = false;
                recoilLeft.OnReloadCompleted();
            }
        }

        if (rightWeapon)
        {
            rightWeapon.fireDelayTimer -= deltaTime;
            if (rightWeapon.reloading)
            {
                ClipSpecial.text = "Reloading";
                recoilRight.OnReload();
            }
            if (rightWeapon.reloading && rightWeapon.fireDelayTimer <= 0f)
            {
                int reloadAmount;

                int ammoNeeded = rightWeapon.Clip - rightWeapon.currentClip;

                if (ammoNeeded > CurrentSpecialAmmo)
                    reloadAmount = CurrentSpecialAmmo;
                else
                    reloadAmount = ammoNeeded;

                rightWeapon.currentClip += reloadAmount;
                CurrentSpecialAmmo -= reloadAmount;
                //Actions.OnSecondaryAmmoChange?.Invoke(CurrentSpecialAmmo);
                Actions.OnReloadSecondaryAmmo?.Invoke(reloadAmount);
                //AmmoChange?.Invoke(CurrentSpecialAmmo);

                ClipSpecial.text = rightWeapon.currentClip + "/" + CurrentSpecialAmmo;
                rightWeapon.reloading = false;
                recoilRight.OnReloadCompleted();
            }
        }
    }

    private void UpdateLeftClip()
    {
        if (leftMainWeapon)
            ClipNormal.text = leftMainWeapon.currentClip + "/" + CurrentAmmo;
        else
            ClipNormal.text = "0/" + CurrentAmmo;
        if (leftSideArmWeapon)
            ClipSideArm.text = leftSideArmWeapon.currentClip + "/" + CurrentAmmo;
        else
            ClipSideArm.text = "0/" + CurrentAmmo;
    }

    private void HandleCombatAction()
    {
        switch (combatState)
        {
            case CombatState.LeftAiming:
                AimingLeft();
                break;
            case CombatState.RightAiming:
                AimingRight();
                break;
            case CombatState.Normal:
                MeleeReady();
                break;
        }


    }

    private void AimingLeft()
    {
        var leftCurrentWeapon = UsingMainLeftWeapon ? leftMainWeapon : leftSideArmWeapon;

        if (leftCurrentWeapon.reloading) return;

        if (inputManager.mainAction)
        {
            if (leftCurrentWeapon.fireDelayTimer <= 0f && (leftCurrentWeapon.isAutomatic || !leftCurrentWeapon.firedThisInput))
            {
                FireWeapon(leftCurrentWeapon);
            }
        }
        else
        {
            leftCurrentWeapon.firedThisInput = false;
            leftCurrentWeapon.consecutiveShot = 0;
            if (leftCurrentWeapon.shootingLastFrame && leftCurrentWeapon.isAutomatic)
            {
                leftCurrentWeapon.fireDelayTimer = leftCurrentWeapon.BaseSingleShootDelay;
                leftCurrentWeapon.shootingLastFrame = false;
            }
        }

        if (inputManager.reload && !leftCurrentWeapon.reloading && leftCurrentWeapon.currentClip < leftCurrentWeapon.Clip)
        {
            RequestReload(true);
        }

        if (leftCurrentWeapon.currentClip <= 0)
        {
            RequestReload(true);
        }

    }

    private void AimingRight()
    {
        if (rightWeapon.reloading) return;

        if (inputManager.mainAction)
        {
            if (rightWeapon.fireDelayTimer <= 0f && (rightWeapon.isAutomatic || !rightWeapon.firedThisInput))
            {
                FireWeapon(rightWeapon);
            }
        }
        else
        {
            rightWeapon.firedThisInput = false;
            rightWeapon.consecutiveShot = 0;
            if (rightWeapon.shootingLastFrame && rightWeapon.isAutomatic)
            {
                rightWeapon.fireDelayTimer = rightWeapon.BaseSingleShootDelay;
                rightWeapon.shootingLastFrame = false;
            }
            if(rightWeapon.Type == WeaponType.Lazer)
            {
                if (rightWeapon.lazerLine != null)
                    HideLazerWeapon();
            }
        }

        if (inputManager.reload && !rightWeapon.reloading && rightWeapon.currentClip < rightWeapon.Clip)
        {
            RequestReload(false);
        }

        if (rightWeapon.currentClip <= 0)
        {
            RequestReload(false);
        }
    }

    private void MeleeReady()
    {
        if (leftMainWeapon)
        {
            leftMainWeapon.consecutiveShot = 0;
            leftMainWeapon.shootingLastFrame = false;
        }

        if (leftSideArmWeapon)
        {
            leftSideArmWeapon.consecutiveShot = 0;
            leftSideArmWeapon.shootingLastFrame = false;
        }

        if (rightWeapon)
        {
            rightWeapon.consecutiveShot = 0;
            rightWeapon.shootingLastFrame = false;
        }

        if (inputManager.mainAction && networkCharacter.characterMovement.isGrounded && MeleeEquiped)
        {
            switch (MeleeIndex)
            {
                case 3:
                    CallAttack(mainCamera.transform.forward, 0);
                    break;
                case 4:
                    CallAttack(mainCamera.transform.forward, 1);
                    break;
                case 6:
                    CallAttack(mainCamera.transform.forward, 1);
                    break;
                case 8:
                    CallAttack(mainCamera.transform.forward, 1);
                    break;
                case 9:
                    CallAttack(mainCamera.transform.forward, 1);
                    break;
                case 10:
                    CallAttack(mainCamera.transform.forward, 1);
                    break;
                case 11:
                    CallAttack(mainCamera.transform.forward, 1);
                    break;
                case 12:
                    CallAttack(mainCamera.transform.forward, 1);
                    break;
                case 13:
                    CallAttack(mainCamera.transform.forward, 1);
                    break;
                case 14:
                    CallAttack(mainCamera.transform.forward, 1);
                    break;
            }
            
        }

        if(inputManager.throwAction && networkCharacter.characterMovement.isGrounded)
        {
            CallThrowAttack(GetAimPoint(), mainCamera.transform.forward);

        }
    }

    private void OnLoadingCompleted()
    {
        //print("Called");
        rightHandWeapon = bodyPartsManager.rArms[bodyPartsManager.partID.rArm].GetComponent<RightHandWeapon>();
        //print(rightHandWeapon);
        rightWeapon = rightHandWeapon.gameObject.GetComponent<Weapon>();
        if (rightWeapon)
            ClipSpecial.gameObject.SetActive(true);
        else
            ClipSpecial.gameObject.SetActive(false);
    }

    private void OnTick()
    {
        if (!(IsOwner || IsServer)) return;

        UpdateTimers((float)TimeManager.TickDelta);
    }

    private Vector3 GetAimPoint()
    {
        Vector2 centerScreenPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        Ray ray = mainCamera.ScreenPointToRay(centerScreenPoint);
        if (Physics.Raycast(ray, out RaycastHit raycastHit, float.PositiveInfinity, shootingLayer))
        {
            return raycastHit.point;
        }
        else
        {
            return mainCamera.transform.position + mainCamera.transform.forward * 200f;
        }
    }

    private void SetCurrentWeapon(Weapon weapon)
    {
        currentWeapon = weapon;
    }

    private void FireWeapon(Weapon weapon)
    {
        if (currentWeapon.Type == WeaponType.Hitscan)
        {
            if (currentWeapon.currentClip <= 0) return;
            Vector3 aimPoint = GetAimPoint();
            Vector3 forward = AddSpreadHitScan((aimPoint - weapon.ExitPoint.position).normalized, weapon.consecutiveShot);
            FireHitScanWeapon(base.TimeManager.GetPreciseTick(TickType.Tick), weapon.ExitPoint.position, forward);
        }

        if (currentWeapon.Type == WeaponType.ShotgunHitscan)
        {
            if (currentWeapon.currentClip <= 0) return;
            Vector3 aimPoint = GetAimPoint();
            List<Vector3> forwardShells = AddSpreadShotgunHitScan((aimPoint - weapon.ExitPoint.position).normalized);
            FireShotgunHitScanWeapon(base.TimeManager.GetPreciseTick(TickType.Tick), weapon.ExitPoint.position, forwardShells);
        }

        if(currentWeapon.Type == WeaponType.GrenadeLauncher)
        {
            if (currentWeapon.currentClip <= 0) return;
            FireWeaponGrenadeLauncher((GetAimPoint() - weapon.ExitPoint.position).normalized * 1000f, Vector3.zero, weapon.ExitPoint.position, weapon.ExitPoint.rotation);
        }

        if(currentWeapon.Type == WeaponType.Lazer)
        {
            if (currentWeapon.currentClip <= 0)
            {
                HideLazerWeapon();
                return;
            }
            Vector3 aimPoint = GetAimPoint();
            FireLazerWeapon(base.TimeManager.GetPreciseTick(TickType.Tick), weapon.ExitPoint.position, aimPoint);
        }
    }

    private float hitcheckCooldown;

    [ServerRpc(RunLocally = true)]
    private void FireLazerWeapon(PreciseTick pt, Vector3 position, Vector3 aimPoint)
    {
        if(currentWeapon.lazerLine == null)
        {
            currentWeapon.lazerLine = currentWeapon.gameObject.AddComponent<LineRenderer>();
            currentWeapon.lazerLine.startWidth = 0.1f;
            currentWeapon.lazerLine.endWidth = 0.1f;
            hitcheckCooldown = 0;
        }

        currentWeapon.lazerLine.SetPosition(0, position);
        currentWeapon.lazerLine.SetPosition(1, aimPoint);

        if (!IsServer) return; 

        hitcheckCooldown -= (float)TimeManager.TickDelta;

        if(hitcheckCooldown < 0)
        {
            currentWeapon.currentClip--;
            SetClip(Owner, currentWeapon.currentClip, (currentWeapon == leftMainWeapon || currentWeapon == leftSideArmWeapon), UsingMainLeftWeapon);

            RollbackManager.Rollback(pt, FishNet.Component.ColliderRollback.RollbackManager.PhysicsType.ThreeDimensional);
            health.SetIgnoreRaycast(true);
            Ray shootingRay = new Ray(position, aimPoint - position);

            if (Physics.Raycast(shootingRay, out RaycastHit hit, 500, shootingLayer))
            {
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    ShootingEffectHitScan(position, hit.point, hit.normal, HitTarget.ENEMY);
                    ShootClientHitScan(position, hit.point, hit.normal, HitTarget.ENEMY);
                    hitTarget = hit.transform;
                    hit.collider.gameObject.GetComponent<HurtBox>().Hit(currentWeapon.Damage, GetComponent<NetworkCharacter>(), currentWeapon.WeaponName);
                }
            }

            health.SetIgnoreRaycast(false);
            RollbackManager.Return();
            hitcheckCooldown = 0.25f;
        }

        FireLazerWeaponObserver(position, aimPoint);
    }

    [ObserversRpc(ExcludeOwner = true)]
    private void FireLazerWeaponObserver(Vector3 position, Vector3 aimPoint)
    {
        if (currentWeapon.lazerLine == null)
        {
            currentWeapon.lazerLine = currentWeapon.gameObject.AddComponent<LineRenderer>();
            currentWeapon.lazerLine.startWidth = 0.1f;
            currentWeapon.lazerLine.endWidth = 0.1f;
        }

        currentWeapon.lazerLine.SetPosition(0, position);
        currentWeapon.lazerLine.SetPosition(1, aimPoint);
    }

    [ServerRpc(RunLocally = true)]
    private void HideLazerWeapon()
    {
        if(currentWeapon.lazerLine != null)
            Destroy(currentWeapon.lazerLine);
    }

    #region HITSCAN WEAPON

    [ServerRpc(RunLocally = true)]
    private void FireHitScanWeapon(PreciseTick pt, Vector3 position, Vector3 forward)
    {
        if (currentWeapon.fireDelayTimer > 0f) return;
        currentWeapon.fireDelayTimer = currentWeapon.BaseAutoShootDelay;
        currentWeapon.shootingLastFrame = true;
        currentWeapon.consecutiveShot++;
        currentWeapon.firedThisInput = true;

        if (IsServer)
        {
            currentWeapon.currentClip--;
            SetClip(Owner, currentWeapon.currentClip, (currentWeapon == leftMainWeapon || currentWeapon == leftSideArmWeapon), UsingMainLeftWeapon);
        }




        Ray shootingRay = new Ray(position, forward);
        bool enableRollback = base.IsServer;

        if (enableRollback)
            RollbackManager.Rollback(pt, FishNet.Component.ColliderRollback.RollbackManager.PhysicsType.ThreeDimensional);

        health.SetIgnoreRaycast(true);


        if (Physics.Raycast(shootingRay, out RaycastHit hit, float.PositiveInfinity, shootingLayer))
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                ShootingEffectHitScan(position, hit.point, hit.normal, HitTarget.ENEMY);
                ShootClientHitScan(position, hit.point, hit.normal, HitTarget.ENEMY);
                hitTarget = hit.transform;

            }
            else if (hit.collider.gameObject.CompareTag("InvisibleBorder"))
            {
                ShootingEffectHitScan(position, hit.point, hit.normal, HitTarget.NONE);
                ShootClientHitScan(position, hit.point, hit.normal, HitTarget.NONE);
            }
            else
            {
                ShootingEffectHitScan(position, hit.point, hit.normal, HitTarget.SURROUNDING);
                ShootClientHitScan(position, hit.point, hit.normal, HitTarget.SURROUNDING);
            }

            //Apply damage and other server things.
            if (base.IsServer)
            {
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    hit.collider.gameObject.GetComponent<HurtBox>().Hit(currentWeapon.Damage, GetComponent<NetworkCharacter>(), currentWeapon.WeaponName);
                }
            }
        }

        health.SetIgnoreRaycast(false);

        if (enableRollback)
            RollbackManager.Return();


    }

    [ServerRpc]
    private void FireWeaponGrenadeLauncher(Vector3 forwardForce, Vector3 upwardForce, Vector3 exitpoint, Quaternion exitRot)
    {
        if (currentWeapon.fireDelayTimer > 0f) return;
        currentWeapon.fireDelayTimer = currentWeapon.BaseAutoShootDelay;
        currentWeapon.shootingLastFrame = true;
        currentWeapon.consecutiveShot++;
        currentWeapon.firedThisInput = true;

        if (IsServer)
        {
            currentWeapon.currentClip--;
            SetClip(Owner, currentWeapon.currentClip, (currentWeapon == leftMainWeapon || currentWeapon == leftSideArmWeapon), UsingMainLeftWeapon);
        }

        SpawnGrenadeServer(forwardForce, upwardForce, exitpoint, exitRot);
    }

    private Vector3 AddSpreadHitScan(Vector3 aimPoint, int consecutiveShot)
    {
        if (!currentWeapon.isAutomatic) cameraController.AddCameraPitch(-currentWeapon.verticalRecoil);

        if (consecutiveShot < 2)
        {
            cameraController.AddCameraPitch(0.1f > currentWeapon.verticalRecoil / 2 ? -0.1f : -currentWeapon.verticalRecoil / 2);
            return aimPoint;
        }

        float spreadValue = currentWeapon.SpreadValue * (consecutiveShot < 10 ? consecutiveShot : 10);

        if (consecutiveShot < 10)
            cameraController.AddCameraPitch(-currentWeapon.verticalRecoil);
        else if (consecutiveShot < 20)
            cameraController.AddCameraPitch(-currentWeapon.verticalRecoil - 0.1f);
        else
            cameraController.AddCameraPitch(-currentWeapon.verticalRecoil - 0.2f);

        Vector3 newAimPoint = new Vector3(
            Random.Range(aimPoint.x - 0, aimPoint.x + 0),
            Random.Range(aimPoint.y - spreadValue, aimPoint.y + spreadValue),
            Random.Range(aimPoint.z - spreadValue, aimPoint.z + spreadValue));
        return newAimPoint;
    }

    [ObserversRpc]
    private void ShootClientHitScan(Vector3 ShootPosition, Vector3 hitPosition, Vector3 hitNormal, HitTarget target)
    {
        if (!base.IsOwner && !IsServer)
        {
            ShootingEffectHitScan(ShootPosition, hitPosition, hitNormal, target);
        }
    }


    private void ShootingEffectHitScan(Vector3 ShootPosition, Vector3 hitPosition, Vector3 hitNormal, HitTarget target)
    {
        if (base.IsServerOnly) return;
        recoilLeft.OnRecoil();
        var index = UnityEngine.Random.Range(0, currentWeapon.shootSound.Length);
        AudioSource.PlayClipAtPoint(currentWeapon.shootSound[index], ShootPosition);
        GameObject r = Instantiate(currentWeapon.muzzleEffect, currentWeapon.ExitPoint.position, currentWeapon.ExitPoint.rotation);
        r.transform.parent = currentWeapon.ExitPoint;

        var trail = Instantiate(currentWeapon.bulletTrail, ShootPosition, Quaternion.LookRotation(hitPosition - currentWeapon.ExitPoint.position));
        trail.GetComponent<HitscanTrail>().SetEndPos(hitPosition, target, hitNormal);
        if (target == HitTarget.ENEMY)
        {
            trail.GetComponent<HitscanTrail>().target = hitTarget;
        }
    }

    #endregion

    #region BOXCAST WEAPON
    #endregion

    #region HITSCAN_SHOTGUN WEAPON

    [ServerRpc(RunLocally = true)]
    private void FireShotgunHitScanWeapon(PreciseTick pt, Vector3 position, List<Vector3> forwardShells)
    {
        if (currentWeapon.fireDelayTimer > 0f) return;
        currentWeapon.fireDelayTimer = currentWeapon.BaseAutoShootDelay;
        currentWeapon.shootingLastFrame = true;
        currentWeapon.consecutiveShot++;
        currentWeapon.firedThisInput = true;

        if (IsServer)
        {
            currentWeapon.currentClip--;
            SetClip(Owner, currentWeapon.currentClip, (currentWeapon == leftMainWeapon || currentWeapon == leftSideArmWeapon), UsingMainLeftWeapon);
        }

        bool enableRollback = base.IsServer;

        if (enableRollback)
            RollbackManager.Rollback(pt, FishNet.Component.ColliderRollback.RollbackManager.PhysicsType.ThreeDimensional);

        health.SetIgnoreRaycast(true);

        foreach (Vector3 forward in forwardShells)
        {
            Ray shootingRay = new Ray(position, forward);

            if (Physics.Raycast(shootingRay, out RaycastHit hit, float.PositiveInfinity, shootingLayer))
            {
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    ShootingEffectShotgunHitScan(position, hit.point, hit.normal, HitTarget.ENEMY);
                    ShootClientHitScan(position, hit.point, hit.normal, HitTarget.ENEMY);

                }
                else if (hit.collider.gameObject.CompareTag("InvisibleBorder"))
                {
                    ShootingEffectShotgunHitScan(position, hit.point, hit.normal, HitTarget.NONE);
                    ShootClientShotgunHitScan(position, hit.point, hit.normal, HitTarget.NONE);
                }
                else
                {
                    ShootingEffectShotgunHitScan(position, hit.point, hit.normal, HitTarget.SURROUNDING);
                    ShootClientShotgunHitScan(position, hit.point, hit.normal, HitTarget.SURROUNDING);
                }

                //Apply damage and other server things.
                if (base.IsServer)
                {
                    if (hit.collider.gameObject.CompareTag("Player"))
                    {
                        hit.collider.gameObject.GetComponent<HurtBox>().Hit(currentWeapon.Damage, GetComponent<NetworkCharacter>(), currentWeapon.WeaponName);
                    }
                }
            }
            else
            {
                ShootingEffectShotgunHitScan(position, position + forward * currentWeapon.range, Vector3.zero, HitTarget.NONE);
                ShootClientShotgunHitScan(position, position + forward * currentWeapon.range, Vector3.zero, HitTarget.NONE);
            }
        }

        health.SetIgnoreRaycast(false);

        if (enableRollback)
            RollbackManager.Return();
    }

    private List<Vector3> AddSpreadShotgunHitScan(Vector3 aimPoint)
    {
        float spreadValue = currentWeapon.SpreadValue;

        cameraController.AddCameraPitch(-currentWeapon.verticalRecoil);

        List<Vector3> aimShells = new List<Vector3>();

        for(int i = 0; i < currentWeapon.shells; i++)
        {
            Vector3 newAimPoint = new Vector3(
            Random.Range(aimPoint.x - spreadValue, aimPoint.x + spreadValue),
            Random.Range(aimPoint.y - spreadValue, aimPoint.y + spreadValue),
            Random.Range(aimPoint.z - spreadValue, aimPoint.z + spreadValue));

            aimShells.Add(newAimPoint);
        }

        return aimShells;
    }

    [ObserversRpc]
    private void ShootClientShotgunHitScan(Vector3 ShootPosition, Vector3 hitPosition, Vector3 hitNormal, HitTarget target)
    {
        if (!base.IsOwner && !IsServer)
        {
            ShootingEffectShotgunHitScan(ShootPosition, hitPosition, hitNormal, target);
        }
    }


    private void ShootingEffectShotgunHitScan(Vector3 ShootPosition, Vector3 hitPosition, Vector3 hitNormal, HitTarget target)
    {
        if (base.IsServerOnly) return;
        recoilLeft.OnRecoil();
        recoilRight.OnRecoil();
        var index = UnityEngine.Random.Range(0, currentWeapon.shootSound.Length);
        AudioSource.PlayClipAtPoint(currentWeapon.shootSound[index], ShootPosition);
        GameObject r = Instantiate(currentWeapon.muzzleEffect, currentWeapon.ExitPoint.position, currentWeapon.ExitPoint.rotation);
        r.transform.parent = currentWeapon.ExitPoint;

        var trail = Instantiate(currentWeapon.bulletTrail, ShootPosition, Quaternion.LookRotation(hitPosition - currentWeapon.ExitPoint.position));
        trail.GetComponent<HitscanTrail>().SetEndPos(hitPosition, target, hitNormal);
        if(target == HitTarget.ENEMY)
        {
            trail.GetComponent<HitscanTrail>().target = hitTarget;
        }
    }

    #endregion

    #region MELEE

    [SerializeField]
    private Transform meleBoxcastCenter;

    [SerializeField]
    private LayerMask meleeHitBoxLayer;

    [SerializeField]
    private GameObject hitEffect;

    public void FinishAttack()
    {
        if (IsOwner || IsServer)
        {
            UnequipWeapon();
            networkCharacter.canEverMove = true;
        }


        if (base.IsServer)
            ResetAttack(Owner);
    }

    public void HitRight()
    {
        OnAxeAttack1?.Invoke(transform.position + transform.forward);

        if (!base.IsServer) return;

        Collider[] hit = Physics.OverlapBox(meleBoxcastCenter.position, new Vector3(0.75f, 0.5f, 0.4f), meleBoxcastCenter.rotation, meleeHitBoxLayer, QueryTriggerInteraction.Collide);


        foreach (var colider in hit)
        {
            var hitNetworkObject = colider.GetComponentInParent<NetworkObject>();
            if (hitNetworkObject)
            {
                if (hitNetworkObject.Owner != Owner)
                {
                    var victim = colider.GetComponentInParent<Health>();
                    if (victim)
                        victim.ReduceHealth(20, GetComponent<NetworkCharacter>(), "Neon Blade");
                }
            }
            Debug.Log("hit");
        }
    }

    public void Hit()
    {
        switch (MeleeIndex)
        {
            case 3:
                OnAxeAttack1?.Invoke(transform.position + transform.forward);
                break;
            case 4:
                OnSwordAttack1?.Invoke(transform.position + transform.forward);
                break;
        }

        if (!base.IsServer) return;

        Collider[] hit = Physics.OverlapBox(meleBoxcastCenter.position, new Vector3(0.75f, 0.5f, 0.4f), meleBoxcastCenter.rotation, meleeHitBoxLayer, QueryTriggerInteraction.Collide);


        foreach (var colider in hit)
        {
            var hitNetworkObject = colider.GetComponentInParent<NetworkObject>();
            if (hitNetworkObject)
            {
                if (hitNetworkObject.Owner != Owner)
                {
                    var victim = colider.GetComponentInParent<Health>();
                    if (victim)
                        victim.ReduceHealth(20, GetComponent<NetworkCharacter>(), "Neon Blade");
                }
            }
            Debug.Log("hit");
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(meleBoxcastCenter.position, new Vector3(1f, 1, 0.8f));
    }

    //weapon 0 = sword
    //1 = axe
    //here
    [ServerRpc(RunLocally = true)]
    private void CallAttack(Vector3 direction)
    {
        networkAnimator.SetTrigger("Attack");
        animator.SetInteger("AttackIndex", 0);

        animator.SetInteger("Weapon", 0);
        animator.SetFloat("Speed", 0);
        combatState = CombatState.Meleeing;
        networkCharacter.canEverMove = false;
        if (IsServer)
            networkCharacter.RotateCharacter(direction);
        EquipWeapon();

        PlayAxeEffect();
    }

    [ServerRpc(RunLocally = true)]
    private void CallAttack(Vector3 direction, int type)
    {
        switch (type)
        {
            //axe
            case 0:
                networkAnimator.SetTrigger("Attack");
                animator.SetInteger("AttackIndex", 0);
                animator.SetInteger("Weapon", 1);
                animator.SetFloat("Speed", 0);
                combatState = CombatState.Meleeing;
                networkCharacter.canEverMove = false;
                if (IsServer)
                    networkCharacter.RotateCharacter(direction);
                EquipWeapon();

                PlayAxeEffect();
                break;
            //sword
            case 1:
                networkAnimator.SetTrigger("Attack");
                animator.SetInteger("AttackIndex", 0);
                animator.SetInteger("Weapon", 0);
                animator.SetFloat("Speed", 0);
                combatState = CombatState.Meleeing;
                networkCharacter.canEverMove = false;
                if (IsServer)
                    networkCharacter.RotateCharacter(direction);
                EquipWeapon();

                PlaySwordEffect();
                break;
            //spear
            case 2:
                break;
        }

        
    }

    [ObserversRpc(RunLocally = true, ExcludeOwner = true)]
    private void PlayAxeEffect()
    {
        AudioSource.PlayClipAtPoint(axeSound, transform.position);
    }

    [ObserversRpc(RunLocally = true, ExcludeOwner = true)]
    private void PlaySwordEffect()
    {
        AudioSource.PlayClipAtPoint(axeSound, transform.position);
    }

    [TargetRpc(RunLocally = true)]
    private void ResetAttack(NetworkConnection conn)
    {
        combatState = CombatState.Normal;
    }


    //RPCS
    [ServerRpc(RunLocally = true)]
    public void EquipWeapon()
    {
        HandWeapons[MeleeIndex].SetActive(true);
        EquipWeapon_Observer();
    }

    [ServerRpc(RunLocally = true)]
    public void UnequipWeapon()
    {
        HandWeapons[MeleeIndex].SetActive(false);
        UnequipWeapon_Observer();
    }


    [ObserversRpc]
    public void EquipWeapon_Observer()
    {
        if (IsOwner) return;
        HandWeapons[MeleeIndex].SetActive(true);
    }

    [ObserversRpc]
    public void UnequipWeapon_Observer()
    {
        if (IsOwner) return;
        HandWeapons[MeleeIndex].SetActive(false);
    }

    #endregion

    #region THROWABLE

    private Vector3 aimPoint_throw;

    [ServerRpc(RunLocally = true)]
    private void CallThrowAttack(Vector3 aimPoint, Vector3 faceDirection)
    {
        if (CurrentGrenades <= 0) return;

        CurrentGrenades--;

        if(IsServer)
        {
            networkCharacter.RotateCharacter(faceDirection);
            SetAimThrowPoint(aimPoint);
        }

        aimPoint_throw = aimPoint;
        networkCharacter.canEverMove = false;
        combatState = CombatState.Throwing;
        networkAnimator.SetTrigger("Throw");
        Invoke("PlayThrowSound", 0.5f);
        //SetRigActiveState("grenade", true);
    }

    [ObserversRpc(RunLocally = true, ExcludeOwner = true)]
    private void PlayThrowSound()
    {
        AudioSource.PlayClipAtPoint(throwSound, transform.position);
    }

    public void Throw()
    {
        if(IsOwner)
        {
            Vector3 forwardActualForce = (aimPoint_throw - grenadeThrowPoint.position).normalized * forwardForce;

            float cameraPitchRange = cameraController.TopClamp - cameraController.BottomClamp;
            float pitchDelta = cameraController.cinemachineTargetPitch - cameraController.BottomClamp; //How much pitch is above BottomPitch;

            float cameraPitchMultiplier = ((cameraPitchRange - pitchDelta) / cameraPitchRange) * 1.2f;

            Vector3 upwardActualForce = upwardForce * Vector3.up * cameraPitchMultiplier;

            SpawnGrenade(forwardActualForce, upwardActualForce, grenadeThrowPoint.position, grenadeThrowPoint.rotation);
        }
    }

    public void FinishThrow()
    {
        if (IsOwner || IsServer)
        {
            UnequipWeapon();
            networkCharacter.canEverMove = true;
            //SetRigActiveState("none", true);
        }


        if (base.IsServer)
            ResetThrow(Owner);
    }

    [TargetRpc(RunLocally = true)]
    private void ResetThrow(NetworkConnection conn)
    {
        combatState = CombatState.Normal;
    }

    [ServerRpc]
    private void SpawnGrenade(Vector3 forwardActualForce, Vector3 upwardActualForce, Vector3 spawnPoint, Quaternion spawnRot)
    {
        
        var grenade = Instantiate(grenadePrefab, spawnPoint, spawnRot);
        grenade.ApplyThrowImpulse(forwardActualForce + upwardActualForce, transform.right * 20f + transform.up * 5f);
        grenade.owner = networkCharacter;
        base.Spawn(grenade.gameObject);
    }

    [Server]
    private void SpawnGrenadeServer(Vector3 forwardActualForce, Vector3 upwardActualForce, Vector3 spawnPoint, Quaternion spawnRot)
    {

        var grenade = Instantiate(currentWeapon.projectile, spawnPoint, Random.rotationUniform).GetComponent<TestGrenade>();
        grenade.ApplyThrowImpulse(forwardActualForce + upwardActualForce, transform.right * 20f + transform.up * 5f);
        grenade.owner = networkCharacter;
        base.Spawn(grenade.gameObject);
    }

    [ObserversRpc]
    private void SetAimThrowPoint(Vector3 aimPoint)
    {
        aimThrowPoint.position = aimPoint;
    }

    #endregion
}
