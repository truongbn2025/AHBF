using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;

public class GroundItem : NetworkBehaviour
{
    public ItemObject item;
    public int amount;
    
    [ObserversRpc (RunLocally = true)]
    public void SetAmount(int _amount)
    {
        amount = _amount;
        Debug.Log("Set amount in GroundItem: " + amount);
    }
}
