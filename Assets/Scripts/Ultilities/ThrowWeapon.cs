using FishNet.Component.Animating;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowWeapon : NetworkBehaviour
{
    [SerializeField]
    private float ThrowCooldown;

    [SerializeField]
    private float ThrowCount;

    private InputManager inputManager;
    private Animator animator;
    private NetworkAnimator networkAnimator;
    private PlayerStates playerStates;

    private bool throwableEquiped;
    private float throwCooldownTimer;
    


    private void Awake()
    {
        inputManager = FindObjectOfType<InputManager>();
        animator = GetComponent<Animator>();
        networkAnimator = GetComponent<NetworkAnimator>();
        playerStates = GetComponent<PlayerStates>();
        throwableEquiped = true;
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        TimeManager.OnTick += OnTick;
    }


    private void OnTick()
    {
        throwCooldownTimer -= (float)TimeManager.TickDelta;
    }

    private void Update()
    {
        if (!IsOwner) return;

        if(throwableEquiped && playerStates.CanThrow() && throwCooldownTimer < 0 && ThrowCount > 0)
        {
            Throw();
        }
    }

    private void Throw()
    {
        throwCooldownTimer = ThrowCooldown;
        ThrowCount--;
        //Throw

    }
}
