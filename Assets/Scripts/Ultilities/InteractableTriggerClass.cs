using UnityEngine;
using FishNet.Object;
public class InteractableTriggerClass : NetworkBehaviour
{
    [SerializeField] DoorMain my_door; 
    [SerializeField] OPEN_DIRECTION direction = OPEN_DIRECTION.DIRECT; 

    private void Process()
    {
        if (base.IsServer)
        {
            //Open();
            OpenOnClient();
        }
        
    }

    #region MonoBehavior
    private void Awake()
    {
        if (my_door == null) my_door = gameObject.GetComponentInParent(typeof(DoorMain)) as DoorMain;
    }
    #endregion

    #region RPCs
    [ServerRpc(RequireOwnership = false)]
    public void RequestOpen()
    {
        Process();

    }
    [ObserversRpc(RunLocally = true)]
    public void OpenOnClient()
    {

        if (my_door.GetDoorState() == DOOR_STATE.CLOSED || my_door.GetDoorState() == DOOR_STATE.INACTION_CLOSE)
        {
            my_door.TryToOpen(direction);
        }
        else if (my_door.GetDoorState() == DOOR_STATE.OPENED || my_door.GetDoorState() == DOOR_STATE.INACTION_OPEN)
        {
            my_door.TryToClose();
        }
    }
    #endregion

}
