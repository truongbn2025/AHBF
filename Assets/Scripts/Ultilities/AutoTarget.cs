using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoTarget : NetworkBehaviour
{
    [SerializeField]
    private LayerMask aimLayer = new LayerMask();

    [SerializeField]
    private GameObject AimHitIndicator;

    private Camera mainCamera;

    [SerializeField]
    private WeaponsController weaponController;
    

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        if (!base.IsOwner)
        {
            AimHitIndicator.SetActive(false);
            return;
        }

        if(!(weaponController.combatState == CombatState.LeftAiming || weaponController.combatState == CombatState.RightAiming))
        {
            AimHitIndicator.SetActive(false);
            return;
        }

        Vector2 centerScreenPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        Ray ray = mainCamera.ScreenPointToRay(centerScreenPoint);
        RaycastHit raycastHit;
        if(Physics.Raycast(ray, out raycastHit, float.PositiveInfinity, aimLayer))
        {
            transform.position = raycastHit.point;
        }
        else
        {
            transform.position = mainCamera.transform.position + mainCamera.transform.forward * 200f;
        }

        if (weaponController.currentWeapon == null) return;

        Ray aimingRay = new Ray(weaponController.currentWeapon.ExitPoint.position, raycastHit.point - weaponController.currentWeapon.ExitPoint.position);

        if(Physics.Raycast(aimingRay, out RaycastHit aimHit, float.PositiveInfinity, aimLayer))
        {
            if (Vector3.Distance(raycastHit.point, aimHit.point) > 0.5f)
            {
                AimHitIndicator.SetActive(true);
                AimHitIndicator.transform.position = aimHit.point;
            }
            else
                AimHitIndicator.SetActive(false);
        }
    }
}
