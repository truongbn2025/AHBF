﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

// v1.0 RASKALOF INITIAL DEVELOPMENT
// This is used for controlling door's flap, this script vars handled by FlapEditor.cs in Editor folder

[RequireComponent(typeof(AudioSource))]
public class FlapClass : MonoBehaviour {
    [SerializeField] FLAP_MOOVEMENT_TYPE moovement_type;
    
    // START Block for LERP moovement type
    [SerializeField] Vector3 open_position, close_position;
    [SerializeField] float open_position_speed = 5f, close_position_speed = 5f;
    [SerializeField] float open_rotation_speed = 5f, close_rotation_speed = 5f;
    [SerializeField] Quaternion open_rotation_quaternion = new Quaternion(), close_rotation_quaternion = new Quaternion();
    [SerializeField] Quaternion open_rotation_quaternion_rev = new Quaternion();
    // END Block for LERP moovement type

    // START Block for CURVES moovement type
    [SerializeField] Animation my_animation;
    [SerializeField] AnimationClip opening_animation, opening_animation_rev, closing_animation, closing_animation_rev;
    
    // END BLOCK for CURVES type
    [SerializeField] UnityEvent OnOpeningStarts;
    [SerializeField] UnityEvent OnOpeningEnds;
    [SerializeField] UnityEvent OnClosingStarts;
    [SerializeField] UnityEvent OnClosingEnds;

    bool lerpmove_isdone, lerprotaion_isdone;

    DOOR_STATE my_state;
    OPEN_DIRECTION _direction;
    float time_pass;
    DoorMain return_result_state_to;

    private void Start() {
        if(my_animation == null) my_animation = GetComponent<Animation>();
    }

    public void SetCaller(DoorMain door) { // Used to get info about door and call methods on this door which send trigger to this flap
        return_result_state_to = door;
    }

    public void StartOpen(OPEN_DIRECTION direction = OPEN_DIRECTION.DIRECT) { // This is method what should always be called when trying to open flap
        StopAllCoroutines();
        _direction = direction;
        my_state = DOOR_STATE.INACTION_OPEN;
        if (moovement_type == FLAP_MOOVEMENT_TYPE.START_END_LERP) {
            if(isPositionValid(transform.position, open_position)) StartCoroutine("LerpDoorMoovement", DOOR_STATE.OPENED); else WaitForLerpEnds(1, DOOR_STATE.OPENED);
            if (_direction == OPEN_DIRECTION.DIRECT)
                if (isQuaternionValid(open_rotation_quaternion)) StartCoroutine("LerpDoorRotation", DOOR_STATE.OPENED); else WaitForLerpEnds(2, DOOR_STATE.OPENED);
            if (_direction == OPEN_DIRECTION.REVERSED)
                if (isQuaternionValid(open_rotation_quaternion_rev)) StartCoroutine("LerpDoorRotation", DOOR_STATE.OPENED); else WaitForLerpEnds(2, DOOR_STATE.OPENED);
        }

        if (moovement_type == FLAP_MOOVEMENT_TYPE.ANIMATION_CURVESS) {
            StartCoroutine("CurvesDoorMoovement", DOOR_STATE.OPENED);
        }

        return_result_state_to.CheckDoorStatus(); // Sending info about what we are done opening
        OnOpeningStarts?.Invoke();
    }

    public void StartClose() {
        StopAllCoroutines();
        my_state = DOOR_STATE.INACTION_CLOSE;
        if (moovement_type == FLAP_MOOVEMENT_TYPE.START_END_LERP) {
            if (isPositionValid(transform.position, close_position)) StartCoroutine("LerpDoorMoovement", DOOR_STATE.CLOSED); else WaitForLerpEnds(1, DOOR_STATE.CLOSED);
            if (isQuaternionValid(close_rotation_quaternion)) StartCoroutine("LerpDoorRotation", DOOR_STATE.CLOSED); else WaitForLerpEnds(2, DOOR_STATE.CLOSED);
        }

        if (moovement_type == FLAP_MOOVEMENT_TYPE.ANIMATION_CURVESS) {
            StartCoroutine("CurvesDoorMoovement", DOOR_STATE.CLOSED);
        }

        return_result_state_to.CheckDoorStatus(); // Sending info about what we are done closing
        OnClosingStarts?.Invoke();
    }

    IEnumerator LerpDoorMoovement(DOOR_STATE state) {
        Vector3 new_position = transform.localPosition; // Save position
        float t = 0f;
        //if(close_position_speed != open_position_speed)
        while (t < 1) // During time left
        {
            if (state == DOOR_STATE.CLOSED) {
                t += Time.unscaledDeltaTime / close_position_speed; // Increase timer
                transform.localPosition = Vector3.Lerp(new_position, close_position, t); // During period of time lerp to target position
            } else {
                t += Time.unscaledDeltaTime / open_position_speed; // Increase timer
                transform.localPosition = Vector3.Lerp(new_position, open_position, t); // During period of time lerp to target position
            }
            yield return null; // End Yield
        }
        WaitForLerpEnds(1, state);
    }

    IEnumerator LerpDoorRotation(DOOR_STATE state) {
        Quaternion new_rotation = transform.localRotation; // Save position
        float t_r = 0f;
        //if(close_rotation_quaternion != open_rotation_quaternion)
        while (t_r < 1) // During time left
        {
            if (state == DOOR_STATE.CLOSED) {
                t_r += Time.unscaledDeltaTime / close_rotation_speed; // Increase timer
                transform.localRotation = Quaternion.Lerp(new_rotation, close_rotation_quaternion, t_r);
            }
            else {
                t_r += Time.unscaledDeltaTime / open_rotation_speed; // Increase timer
                if (_direction == OPEN_DIRECTION.DIRECT) {
                    transform.localRotation = Quaternion.Lerp(new_rotation, open_rotation_quaternion, t_r);
                }
                else {
                    transform.localRotation = Quaternion.Lerp(new_rotation, open_rotation_quaternion_rev, t_r);
                }
            }
            yield return null; // End Yield
        }
        WaitForLerpEnds(2, state);
    }

    void WaitForLerpEnds(int operation, DOOR_STATE state) { // Cos rotation and moovement can be performed with different speed, this will controll it and wait for both
        if (operation == 1) {
            lerpmove_isdone = true;
        }
        if (operation == 2) {
            lerprotaion_isdone = true;
        }
        if (lerpmove_isdone && lerprotaion_isdone) {
            HardSetState(state);
            lerprotaion_isdone = lerpmove_isdone = false;
        }
        
    }

    IEnumerator CurvesDoorMoovement(DOOR_STATE state) {
        AnimationClip current_clip = null;

        if(state == DOOR_STATE.OPENED) {
            if (_direction == OPEN_DIRECTION.DIRECT) current_clip = opening_animation; else current_clip = opening_animation_rev;
        }
        if(state == DOOR_STATE.CLOSED) {
            if (_direction == OPEN_DIRECTION.DIRECT) current_clip = closing_animation; else current_clip = closing_animation_rev;
        }

        my_animation.clip = current_clip;
        my_animation.CrossFade(my_animation.clip.name);
        yield return new WaitForSeconds(current_clip.length);
        HardSetState(state);
    }

    public void HardSetState(DOOR_STATE state, bool should_invoke = true) { // This is interface for HardSetClosed and HardSetOpen, better use this instead of them
        if (state == DOOR_STATE.CLOSED) {
            HardSetClosed();
            if(should_invoke) OnClosingEnds?.Invoke();
        } else {
            HardSetOpen();
            if(should_invoke) OnOpeningEnds?.Invoke();
        }
    }

    void HardSetClosed() { // This method set flap to closed state (can be used for game loading to load door state)
        StopAllCoroutines();
        if (moovement_type == FLAP_MOOVEMENT_TYPE.START_END_LERP) {
            transform.localPosition = close_position;
            transform.localRotation = close_rotation_quaternion;
        }
        if (moovement_type == FLAP_MOOVEMENT_TYPE.ANIMATION_CURVESS) {
            if (_direction == OPEN_DIRECTION.DIRECT) {
                my_animation[closing_animation.name].time = my_animation[closing_animation.name].length;
                my_animation.Play(closing_animation.name);
            }
            else {
                my_animation[closing_animation_rev.name].time = my_animation[closing_animation_rev.name].length;
                my_animation.Play(closing_animation_rev.name);
            }
        }

        my_state = DOOR_STATE.CLOSED;
        if (return_result_state_to == null) return_result_state_to = transform.parent.GetComponent<DoorMain>();
        return_result_state_to?.CheckDoorStatus();
    }

    void HardSetOpen() { // This method set flap to opened state (can be used for game loading to load door state)
        StopAllCoroutines();
        if (moovement_type == FLAP_MOOVEMENT_TYPE.START_END_LERP) {
            transform.localPosition = open_position;
            if (_direction == OPEN_DIRECTION.DIRECT) transform.localRotation = open_rotation_quaternion;
            else transform.localRotation = open_rotation_quaternion_rev;
        }
        if (moovement_type == FLAP_MOOVEMENT_TYPE.ANIMATION_CURVESS) {
            if (_direction == OPEN_DIRECTION.DIRECT) {
                my_animation[opening_animation.name].time = my_animation[opening_animation.name].length;
                my_animation.Play(opening_animation.name);
            }
            else {
                my_animation[opening_animation_rev.name].time = my_animation[opening_animation_rev.name].length;
                my_animation.Play(opening_animation_rev.name);
            }
        }

        my_state = DOOR_STATE.OPENED;
        if (return_result_state_to == null) return_result_state_to = transform.parent.GetComponent<DoorMain>();
        return_result_state_to?.CheckDoorStatus();
    }

    public DOOR_STATE GetFlapStatus() {
        return my_state;
    }

    public void EDITOR_SetOpenCoords() {
        transform.localPosition = open_position;
    }
    public void EDITOR_GetOpenCoords() {
        open_position = transform.localPosition;
    }
    public void EDITOR_SetCloseCoords() {
        transform.localPosition = close_position;
    }
    public void EDITOR_GetCloseCoords() {
        close_position = transform.localPosition;
    }
    public void EDITOR_GetOpenQuaternion() {
        open_rotation_quaternion = transform.localRotation;
    }
    public void EDITOR_SetOpenQuaternion() {
        transform.localRotation = open_rotation_quaternion;
    }
    public void EDITOR_GetOpenQuaternionRev() {
        open_rotation_quaternion_rev = transform.localRotation;
    }
    public void EDITOR_SetOpenQuaternionRev() {
        transform.localRotation = open_rotation_quaternion_rev;
    }
    public void EDITOR_GetCloseQuaternion() {
        close_rotation_quaternion = transform.localRotation;
    }
    public void EDITOR_SetCloseQuaternion() {
        transform.localRotation = close_rotation_quaternion;
    }

    public bool isQuaternionValid(Quaternion quaternion) {
        bool isNaN = float.IsNaN(quaternion.x + quaternion.y + quaternion.z + quaternion.w);
        bool isZero = quaternion.x == 0 && quaternion.y == 0 && quaternion.z == 0 && quaternion.w == 0;
        return !(isNaN || isZero);
    }

    public bool isPositionValid(Vector3 positio1, Vector3 positio2) {
        if (positio1.x == positio2.x && positio1.y == positio2.y && positio1.z == positio2.z) return false;
        return true;
    }

}
