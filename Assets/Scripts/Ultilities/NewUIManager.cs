using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NewUIManager : NetworkBehaviour
{
    [SerializeField] Health health;
    [SerializeField] private TextMeshProUGUI healthText;
    [SerializeField] private Image heathBar;
    [SerializeField] private PlayerStatsManager playerStatsManager;

    [SerializeField] private TextMeshProUGUI shieldText;
    [SerializeField] private Image shieldBar;
    public override void OnStartClient()
    {
        base.OnStartClient();
        if (!IsOwner) gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        health.HealthUpdate += OnHealthChanged;
        health.ShieldUpdate += OnShieldChanged;
    }

    private void OnDisable()
    {
        health.HealthUpdate -= OnHealthChanged;
        health.ShieldUpdate -= OnShieldChanged;
    }

    public void OnHealthChanged(float health)
    {
        var maxHealth = playerStatsManager.GetMaxHealth();
        healthText.text = health.ToString("0") + "/" + maxHealth;
        heathBar.fillAmount = health / maxHealth;
    }
    public void OnShieldChanged(float shield)
    {
        var maxShield = playerStatsManager.GetMaxShield();
        shieldText.text = shield.ToString("0") + "/" + maxShield;
        shieldBar.fillAmount = shield / maxShield;
    }
}
