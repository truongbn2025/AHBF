using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType
{
    Hitscan,
    ShotgunHitscan,
    BoxCast,
    GrenadeLauncher,
    Rocket,
    Projectile,
    Special,
    Lazer
}

public class Weapon : MonoBehaviour
{
    public string WeaponName;
    public float Damage;
    public int Clip;
    public WeaponType Type;
    public Transform ExitPoint;
    public float SpreadValue;
    public int shells; // Shotgun only
    public float range; //Shotgun only
    public float BaseSingleShootDelay;
    public float BaseAutoShootDelay;
    public float BaseReloadDelay;
    public GameObject hitIndicator;
    public GameObject muzzleEffect;
    public GameObject bulletTrail;
    public GameObject projectile;
    public bool reloading { get; set; }
    public int currentClip { get; set; }
    public float fireDelayTimer { get; set; }
    public int consecutiveShot { get; set; }
    public bool shootingLastFrame { get; set; }
    public AudioClip[] shootSound;
    public AudioClip reloadSound;
    public float verticalRecoil;
    public bool isAutomatic;
    public bool firedThisInput { get; set; }
    public LineRenderer lazerLine;

    private void Awake()
    {
        currentClip = 0;
        firedThisInput = false;
        consecutiveShot = 0;
        fireDelayTimer = 0;
        reloading = false;
        shootingLastFrame = false;
    }
}
