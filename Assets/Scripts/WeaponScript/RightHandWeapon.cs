using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RightHandType
{
    Melee,
    Gun,
    None,
}
public class RightHandWeapon : MonoBehaviour
{
    public RightHandType handType = RightHandType.None;
}
