using FishNet;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;
public class SwordAttack : MonoBehaviour
{
    [SerializeField] ParticleSystem particleSystemm;
    [SerializeField] DamageBox damageBox;
    Particle[] paticles = new Particle[1000];
    bool called = false;
    bool registeringHitWindow = false;

    public NetworkCharacter Caster;

    private void Awake()
    {
        damageBox.Hit += Hit;
        registeringHitWindow = true;
    }

    private void Update()
    {
        int count = particleSystemm.GetParticles(paticles);

        if (count == 0) return;

        damageBox.gameObject.transform.position = paticles[0].position;

        if (!called)
        {
            Invoke("StopRegistering", 0.2f);
            called = true;
        }

    }

    private void StopRegistering()
    {
        registeringHitWindow = false;
    }

    private void Hit(Collider other, float damage)
    {
        if (!InstanceFinder.NetworkManager.IsServer) return;

        if (registeringHitWindow && other.gameObject.layer == LayerMask.NameToLayer("PlayerMeleeHitboxLayer"))
        {
            var hitNetworkObject = other.GetComponentInParent<NetworkObject>();
            if (hitNetworkObject)
            {
                if (hitNetworkObject.Owner != Caster.Owner)
                {
                    var victim = other.GetComponentInParent<Health>();
                    if (victim)
                        victim.ReduceHealth(damageBox.damage, Caster, "Urgly Axe's Rising Rock");
                }
            }
            //Debug.Log("hit " + other.name);
        }
    }
}
