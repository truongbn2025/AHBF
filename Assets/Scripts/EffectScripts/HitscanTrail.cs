using UnityEngine;

public class HitscanTrail : MonoBehaviour
{
    public Transform target;
    private Vector3 endPos;  
    private Vector3 hitNormal;
    private HitTarget hitTarget;
    [SerializeField] private float speed = 400f;
    [SerializeField] private GameObject bulletImpactSurrounding;
    [SerializeField] private GameObject bulletImpactPlayer;
    [SerializeField] private GameObject bulletImpactGlass;
    [SerializeField] private AudioClip impactSound;

    public void SetEndPos(Vector3 pos, HitTarget target ,Vector3 thisHitNormal)
    {
        endPos = pos;
        hitNormal = thisHitNormal;
        hitTarget = target;
        
    }

    public void SetEndPos(Vector3 pos, HitTarget _hitTarget, Vector3 thisHitNormal, Transform thisTarget)
    {
        endPos = pos;
        hitNormal = thisHitNormal;
        hitTarget = _hitTarget;
        target = thisTarget;

    }


    #region MonoBehavior
    private void Update()
    {
        if (Vector3.Distance(transform.position, endPos) > 0.01f)
            transform.position = Vector3.MoveTowards(transform.position, endPos, speed * Time.deltaTime);
        else
        {
            if (hitTarget == HitTarget.SURROUNDING)
            {
                GameObject r = Instantiate(bulletImpactSurrounding, endPos, Quaternion.LookRotation(hitNormal, Vector3.up) * bulletImpactSurrounding.transform.rotation);
                AudioSource.PlayClipAtPoint(impactSound, endPos, 0.2f);
                //bulletImpactSurrounding.GetComponent<ECparticleColorChangerMaster>().SetHitPlayer(HitTarget.SURROUNDING);
                r.transform.SetParent(target);
                Debug.Log(target);
                Destroy(gameObject);
            }
            else if (hitTarget == HitTarget.ENEMY)
            {
                GameObject r = Instantiate(bulletImpactPlayer, endPos, Quaternion.LookRotation(hitNormal, Vector3.up) * bulletImpactPlayer.transform.rotation);
                r.transform.SetParent(target);
                Debug.Log(target);
                //bulletImpactPlayer.GetComponent<ECparticleColorChangerMaster>().SetHitPlayer(HitTarget.ENEMY);

                Destroy(gameObject);
            }

        }

    }
    #endregion

}
