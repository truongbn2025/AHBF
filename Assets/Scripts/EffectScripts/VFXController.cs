using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXController : NetworkBehaviour
{
    [SerializeField] private ParticleSystem landingEffect;
    [SerializeField] private ParticleSystem axe1Effect;
    [SerializeField] private ParticleSystem sword1Effect;
    [SerializeField] private Transform[] frontCastPoint;
    [SerializeField] private LayerMask scanGroundLayer;


    private void Awake()
    {
        GetComponent<NetworkCharacter>().OnLand += OnLand;
        GetComponent<WeaponsController>().OnAxeAttack1 += OnAxeAttack1;
        GetComponent<WeaponsController>().OnSwordAttack1 += OnSwordAttack1;
    }

    private void OnDestroy()
    {
        GetComponent<NetworkCharacter>().OnLand -= OnLand;
        GetComponent<WeaponsController>().OnAxeAttack1 -= OnAxeAttack1;
        GetComponent<WeaponsController>().OnSwordAttack1 -= OnSwordAttack1;
    }

    [ObserversRpc(ExcludeOwner = true, RunLocally = true)]
    private void OnLand(Vector3 spawnPos)
    {
        if (IsServerOnly) return;
        StartCoroutine(SpawnVFX(landingEffect, spawnPos, transform.rotation, 0.2f));
    }

    [ObserversRpc(ExcludeOwner = true, RunLocally = true)]
    private void OnAxeAttack1(Vector3 spawnPos)
    {
        int haveGround;

        for(haveGround = 0; haveGround < 5; haveGround++)
        {
            if (!Physics.CheckSphere(frontCastPoint[haveGround].position, 0.5f, scanGroundLayer))
                break;
        }

        var particleSetting = axe1Effect.main;
        particleSetting.startSpeed = (haveGround + 1) * 3.5f;

        axe1Effect.gameObject.GetComponent<AxeAttack1>().Caster = GetComponent<NetworkCharacter>();
        StartCoroutine(SpawnVFX(axe1Effect, spawnPos, transform.rotation, 0));
    }

    [ObserversRpc(ExcludeOwner = true, RunLocally = true)]
    private void OnSwordAttack1(Vector3 spawnPos)
    {

        int haveGround;

        for (haveGround = 0; haveGround < 5; haveGround++)
        {
            if (!Physics.CheckSphere(frontCastPoint[haveGround].position, 0.5f, scanGroundLayer))
                break;
        }

        var particleSetting = sword1Effect.main;
        particleSetting.startSpeed = (haveGround + 1) * 3.5f;

        sword1Effect.gameObject.GetComponent<SwordAttack>().Caster = GetComponent<NetworkCharacter>();
        StartCoroutine(SpawnVFX(sword1Effect, spawnPos, transform.rotation, 0));
    }

    private IEnumerator SpawnVFX(ParticleSystem vfx, Vector3 pos, Quaternion rot, float delay)
    {
        yield return new WaitForSeconds(delay);
        Instantiate(vfx, pos, rot);
    }
}
