using FishNet.Transporting.Tugboat;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalClientStarter : MonoBehaviour
{
    [SerializeField] private GameObject playfabClientPrefab;
    private Tugboat tugboat;
    private NetworkHudCanvases networkHudCanvas;


    private void Awake()
    {
        tugboat = FindObjectOfType<Tugboat>();
        networkHudCanvas = FindObjectOfType<NetworkHudCanvases>();
        
        if (PlayerLobbyData.Instance == null)
        {
            Destroy(gameObject);
            return;
        }

        if (PlayerLobbyData.Instance.startType == LOBBY_START_TYPE.CREATE)
        {
            Instantiate(playfabClientPrefab);
            networkHudCanvas.ChangeAutoStart(AutoStartType.Host);
        }

        if (PlayerLobbyData.Instance.startType == LOBBY_START_TYPE.JOIN)
        {
            tugboat.SetClientAddress(PlayerLobbyData.Instance.ServerIP);
            tugboat.SetPort(PlayerLobbyData.Instance.port);
            networkHudCanvas.ChangeAutoStart(AutoStartType.Client);
            //networkHudCanvas.OnClick_Client();
        }

        if (PlayerLobbyData.Instance.startType == LOBBY_START_TYPE.LOCAL_DEBUG)
        {
            networkHudCanvas.ChangeAutoStart(AutoStartType.Client);
        }
    }
}
