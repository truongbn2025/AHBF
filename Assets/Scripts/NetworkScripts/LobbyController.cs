using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;



public class LobbyController : MonoBehaviour
{

    //public LOBBY_START_TYPE playOption { get; set; }
    [SerializeField] private TMP_InputField playerNameBox;
    [SerializeField] private TMP_InputField serverIpBox;
    [SerializeField] private TMP_InputField portBox;
    [SerializeField] private TMP_InputField idCodeBox;
    [SerializeField] private TextMeshProUGUI errorMessage;
    [SerializeField] private GameObject errorBox;

    public void Play()
    {
        switch (PlayerLobbyData.Instance.startType)
        {
            case LOBBY_START_TYPE.CREATE:
                CreateMatch();
                break;
            case LOBBY_START_TYPE.JOIN:
                JoinMatch();
                break;
            case LOBBY_START_TYPE.LOCAL_DEBUG:
                PlayLocal();
                break;
        }
    }

    private void CreateMatch()
    {
        if (playerNameBox.text == "")
        {
            ShowError("Please enter player name!");
            return;
        }

        PlayerLobbyData.Instance.startType = LOBBY_START_TYPE.CREATE;
        PlayerLobbyData.Instance.playerName = playerNameBox.text;
        SceneManager.LoadScene(1);
    }

    private void JoinMatch()
    {
        if (playerNameBox.text == "")
        {
            ShowError("Please enter player name!");
            return;
        }

        if (serverIpBox.text == "")
        {
            ShowError("Please enter Server IP Address!");
            return;
        }

        if (portBox.text == "")
        {
            ShowError("Please enter Server's port!");
            return;
        }

        PlayerLobbyData.Instance.startType = LOBBY_START_TYPE.JOIN;
        PlayerLobbyData.Instance.playerName = playerNameBox.text;
        PlayerLobbyData.Instance.ServerIP = serverIpBox.text;
        PlayerLobbyData.Instance.port = (ushort)Int32.Parse(portBox.text);
        SceneManager.LoadScene(1);
    }

    private void PlayLocal()
    {
        if (playerNameBox.text == "")
        {
            ShowError("Please enter player name!");
            return;
        }

        PlayerLobbyData.Instance.startType = LOBBY_START_TYPE.LOCAL_DEBUG;
        PlayerLobbyData.Instance.playerName = playerNameBox.text;
        PlayerLobbyData.Instance.characterCode = idCodeBox.text;
        SceneManager.LoadScene(1);
    }

    private void ShowError(string message)
    {
        errorMessage.text = message;
        errorBox.SetActive(true);
    }
}
