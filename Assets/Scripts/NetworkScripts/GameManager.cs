using FishNet.Connection;
using FishNet.Object;
using FishNet.Transporting;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using FishNet.Object.Synchronizing;

public class GameManager : NetworkBehaviour
{
    public static GameManager Instance;
    public bool AllowConnect= false;
    public int MaxPlayer = 1;
    public int CurrentPlayer = 0;
    public event Action LastPlayerLeaves;
    public ProceduralSkyboxChanger ProceduralSkyboxChanger;
    [SerializeField]
    private MatchManager matchManager;

    private void Awake()
    {
        Instance = this;
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        base.ServerManager.OnRemoteConnectionState += OnClientEvent;
        //matchManager = FindObjectOfType<MatchManager>();
    }

    private void OnClientEvent(NetworkConnection conn, RemoteConnectionStateArgs stateArg)
    {
        if (!base.IsServer) return;

        if (!AllowConnect) conn.Disconnect(true);

        if (stateArg.ConnectionState == RemoteConnectionState.Started)
        {
            if (CurrentPlayer == MaxPlayer) conn.Disconnect(true);
            else CurrentPlayer++;
        }
        else
        {
            CurrentPlayer--;
            if (matchManager.MatchStarted)
                matchManager.AlivePlayers--;

            if (CurrentPlayer == 0)
            {
                LastPlayerLeaves?.Invoke();
                Debug.Log("Last player leaves");
            }
        }
    }
}
