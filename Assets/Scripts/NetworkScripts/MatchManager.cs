using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MatchManager : NetworkBehaviour
{
    [SerializeField] GameObject DropShip;
    [SerializeField] Transform DropShipSpawnPoint;
    [SerializeField] GameObject SafeZone;
    public GameObject LocalInstance;
    public bool MatchStarted = false;
    
    public bool MatchEnded = false;
    public TextMeshProUGUI gameNotification;
    public TextMeshProUGUI smallNotification;
    public TextMeshProUGUI aliveCount;
    public TextMeshProUGUI killedCount;
    public KillFeedManager killFeedManager;
    public bool OnlyStartMatchWhenFull;
    public int CustomStartMatchPlayerCount;

    [SyncVar(OnChange = nameof(OnTimerChange))]
    public float StartMatchTimer = 5f;
    [SyncVar(OnChange = nameof(OnAlivePlayersChange))]
    public int AlivePlayers;
    [SyncVar(OnChange = nameof(OnJoinedPlayersChange))]
    public int JoinedPlayer;

    private bool notificationState = false;
    private bool isDoneCounting = false;

    public bool isBotGame;

    private void Update()
    {
        if (!base.IsServer) return;

        if (GameManager.Instance.CurrentPlayer != JoinedPlayer && !MatchStarted)
            JoinedPlayer = GameManager.Instance.CurrentPlayer;

        if (OnlyStartMatchWhenFull)
        {
            if (GameManager.Instance.CurrentPlayer == GameManager.Instance.MaxPlayer && !MatchStarted)
            {
                GameManager.Instance.AllowConnect = false;
                SetNotificationLabel(true);
                StartCoroutine(StartMatchAfterDelay(StartMatchTimer + 1f));
                MatchStarted = true;
            }
        }
        else
        {
            if (GameManager.Instance.CurrentPlayer >= CustomStartMatchPlayerCount && !MatchStarted)
            {
                GameManager.Instance.AllowConnect = false;
                SetNotificationLabel(true);
                StartCoroutine(StartMatchAfterDelay(StartMatchTimer + 1f));
                MatchStarted = true;
                
            }
        }
        

        if (MatchStarted && StartMatchTimer >= 0)
        {
            StartMatchTimer -= Time.deltaTime;
        }

        if (MatchStarted && StartMatchTimer < 0 && !isDoneCounting)
        {
            SetNotificationLabel(false);
            isDoneCounting = true;
        }
    }

    public void CallStartMatch()
    {
        if (GameManager.Instance.CurrentPlayer >= 1 && !MatchStarted)
        {
            GameManager.Instance.AllowConnect = false;
            SetNotificationLabel(true);
            StartCoroutine(StartMatchAfterDelay(StartMatchTimer + 1f));
            MatchStarted = true;
        }
    }

    public void StartMatch()
    {
        if (!base.IsServer) return;

        AlivePlayers = 0;

        foreach(var player in FindObjectsOfType<NetworkCharacter>())
        {
            AlivePlayers++;
            player.gameObject.SetActive(false);
        }

        GameObject dropShip = Instantiate(DropShip, DropShipSpawnPoint.position, DropShipSpawnPoint.rotation);
        base.Spawn(dropShip);
        base.Spawn(SafeZone);
        //FindObjectOfType<VirtualCameraManager>().dropShipCamera.SetActive(true);
        //EnableSafeZone();
        StartMatchClient();
        SetKilledCountLabel(true);
    }
    [ObserversRpc (RunLocally =true)]
    private void EnableSafeZone()
    {
        SafeZone.gameObject.SetActive(true);
    }

    [ObserversRpc]
    private void StartMatchClient()
    {
        foreach (var player in FindObjectsOfType<NetworkCharacter>())
        {
            player.gameObject.SetActive(false);
        }
        FindObjectOfType<VirtualCameraManager>().dropShipCamera.SetActive(true);
    }

    [ObserversRpc(BufferLast = true)]
    private void SetNotificationLabel(bool state)
    {
        gameNotification.transform.parent.gameObject.SetActive(state);
    }

    [ObserversRpc(BufferLast = true)]
    private void SetKilledCountLabel(bool state)
    {
        killedCount.gameObject.SetActive(state);
    }

    private void OnTimerChange(float prev, float next, bool asServer)
    {
        float timer = next;
        if (timer < 0) timer = 0;
        gameNotification.text = "Match start in " + next.ToString("0");
    }

    private void OnAlivePlayersChange(int prev, int next, bool asServer)
    {
        aliveCount.text = next + "";

        if (next == 1 && prev > 1 && asServer)
        {
            MatchEnded = true;
            EndGame();
        }
    }

    private void OnJoinedPlayersChange(int prev, int next, bool asServer)
    {
        aliveCount.text = next + "";
    }

    public void UpdateKilledCount(int kill)
    {
        killedCount.text = kill + "";
        
        //Only work in offline mode not network mode
        if(kill >= 5 && isBotGame)
        {
            EndGame();
        }
    }

    IEnumerator StartMatchAfterDelay(float timer)
    {
        yield return new WaitForSeconds(timer);
        StartMatch();
    }

    private void EndGame()
    {
        //Winner winner chicken dinner!!
        foreach (var player in FindObjectsOfType<NetworkCharacter>())
        {
            var heathComp = player.GetComponent<Health>();
            if(heathComp.isAlive)
            {
                heathComp.isInvincible = true;
                SendWinnerMessage(player.Owner);
            }
        }
    }

    [TargetRpc]
    private void SendWinnerMessage(NetworkConnection conn)
    {
        gameNotification.transform.parent.gameObject.SetActive(true);
        gameNotification.text = "Winner winner token dinner!!!!!";
    }

    [TargetRpc]
    public void SendKillMessage(NetworkConnection conn, string victimName, string weaponName)
    {
        StartCoroutine(ShowSmallNotification("You killed " + victimName + " with " + weaponName + "!", 3f));
    }

    [TargetRpc]
    public void SendDieMessage(NetworkConnection conn, string killerName, string weaponName)
    {
        StartCoroutine(ShowSmallNotification(killerName + " killed you with " + weaponName + "!", 3f));
    }

    public void OnPlayerDie(NetworkCharacter killer, NetworkCharacter victim, string weaponName)
    {
        AlivePlayers--;
        SendKillMessage(killer.Owner, victim.playerName, weaponName);
        SendDieMessage(victim.Owner, killer.playerName, weaponName);
        UpdateKillFeed(killer, victim, weaponName);
    }

    [ObserversRpc]
    private void UpdateKillFeed(NetworkCharacter killer, NetworkCharacter victim, string weaponName)
    {
        bool isMyKill = LocalInstance.GetComponent<NetworkObject>().Owner == killer.Owner;
        bool isAlliesKill = false;
        killFeedManager.CreateBanner(killer.playerName, victim.playerName, weaponName, isMyKill, isAlliesKill);
    }

    IEnumerator ShowSmallNotification(string message, float timer)
    {
        smallNotification.text = message;
        smallNotification.transform.parent.gameObject.SetActive(true);

        yield return new WaitForSeconds(timer);
        smallNotification.transform.parent.gameObject.SetActive(false);
    }
}
