using Cinemachine;
using FishNet;
using FishNet.Object;
using FishNet.Object.Prediction;
using FishNet.Object.Synchronizing;
using FishNet.Transporting;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Animations.Rigging;
using FishNet.Component.Animating;
using System;
using FishNet.Serializing.Helping;
using System.Collections.Generic;
using System.Collections;

public class VehicleDemoController : NetworkBehaviour
{

    public struct InputData
    {
        public Vector2 Move;
        public bool Jump;
        public bool Sprint;
    }

    private GameObject mainCamera;
    private InputManager localInput;
    [SerializeField] GameObject CinemachineCameraTarget;

    private InputData inputData;

    private float _cinemachineTargetYaw;
    private float _cinemachineTargetPitch;
    private float _sensitivity = 1f;
    private bool LockCameraPosition = false;
    public float TopClamp = 70.0f;
    public float BottomClamp = -30.0f;
    public float CameraAngleOverride = 0.0f;
    private const float _threshold = 0.01f;

    [SerializeField] List<GameObject> Springs = new List<GameObject>();
    [SerializeField] GameObject prop;
    [SerializeField] GameObject CM;
    [SerializeField] Rigidbody rigidbody;


    private void Awake()
    {
        InstanceFinder.TimeManager.OnTick += TimeManager_OnTick;
        InstanceFinder.TimeManager.OnUpdate += TimeManager_OnUpdate;
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        localInput = FindObjectOfType<InputManager>();
    }

    private void OnDestroy()
    {
        if (InstanceFinder.TimeManager != null)
        {
            InstanceFinder.TimeManager.OnTick -= TimeManager_OnTick;
            InstanceFinder.TimeManager.OnUpdate -= TimeManager_OnUpdate;
        }
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        if (base.IsOwner)
        {
            GameObject.FindGameObjectWithTag("PlayerFollowCamera").GetComponent<CinemachineVirtualCamera>().Follow = CinemachineCameraTarget.transform;
        }

        if(base.IsClientOnly)
        {
            DestroyImmediate(GetComponent<Rigidbody>());
        }
    }

    public override void OnStartNetwork()
    {
        base.OnStartNetwork();

        _cinemachineTargetYaw = CinemachineCameraTarget.transform.rotation.eulerAngles.y;
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        rigidbody.centerOfMass = CM.transform.localPosition;
    }



    private void TimeManager_OnTick()
    {
        if (base.IsServer)
        {
            rigidbody.AddForceAtPosition((float)base.TimeManager.TickDelta * transform.TransformDirection(Vector3.forward) * inputData.Move.y * 400f, prop.transform.position);
            rigidbody.AddTorque((float)base.TimeManager.TickDelta * transform.TransformDirection(Vector3.up) * inputData.Move.x * 300f);
            foreach (GameObject spring in Springs)
            {
                RaycastHit hit;
                if (Physics.Raycast(spring.transform.position, transform.TransformDirection(Vector3.down), out hit, 3f))
                {
                    rigidbody.AddForceAtPosition((float)base.TimeManager.TickDelta * transform.TransformDirection(Vector3.up) * Mathf.Pow(3f - hit.distance, 2) / 3f * 250f, spring.transform.position);
                }
            }
            rigidbody.AddForce(-(float)base.TimeManager.TickDelta * transform.TransformVector(Vector3.right) * transform.InverseTransformVector(rigidbody.velocity).x * 5f);
        }
    }
    private void TimeManager_OnUpdate()
    {
        if (base.IsOwner)
        {
            inputData.Move = localInput.move;
            inputData.Sprint = localInput.sprint;
            inputData.Jump = localInput.jump;

            SyncData(inputData);
        }
    }

    [ServerRpc]
    private void SyncData(InputData input)
    {
        inputData = input;
    }

    private void LateUpdate()
    {
        if (base.IsOwner)
            CameraRotation();
    }

    private void CameraRotation()
    {
        if (Cursor.lockState == CursorLockMode.None) LockCameraPosition = true;
        else LockCameraPosition = false;
        if (localInput.look.sqrMagnitude >= _threshold && !LockCameraPosition)
        {

            _cinemachineTargetYaw += localInput.look.x;
            _cinemachineTargetPitch += localInput.look.y;
        }

        _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
        _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

        CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
            _cinemachineTargetYaw, 0.0f);
    }

    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }

}
