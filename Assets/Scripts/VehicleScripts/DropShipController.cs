using Cinemachine;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropShipController : NetworkBehaviour
{
    public GameObject CinemachineCameraTarget;
    private GameObject _mainCamera;
    public InputManager _localInput;
    private const float _threshold = 0.01f;
    public bool LockCameraPosition = false;
    private float _cinemachineTargetYaw;
    private float _cinemachineTargetPitch;
    private float _sensitivity = 1f;
    public float TopClamp = 70.0f;
    public float BottomClamp = -30.0f;
    public float CameraAngleOverride = 0.0f;
    public GameObject PlayerDropPoint;
    public bool dropped = false;
    public bool AllowJump = false;

    private void Awake()
    {
        _mainCamera = Camera.main.gameObject;
        _localInput = FindObjectOfType<InputManager>();
    }
    public override void OnStartClient()
    {
        base.OnStartClient();
        FindObjectOfType<VirtualCameraManager>().dropShipCamera.GetComponent<CinemachineVirtualCamera>().Follow = CinemachineCameraTarget.transform;
        AllowJump = true;
    }

    public void Update()
    {
        if(_localInput.mainAction && !dropped && AllowJump)
        {
            dropped = true;
            FindObjectOfType<VirtualCameraManager>().dropShipCamera.SetActive(false);
            ClientPlayerSpawner ClientInstance = FindObjectOfType<MatchManager>().LocalInstance.GetComponent<ClientPlayerSpawner>();
            ClientInstance.DropPlayer(ClientInstance.LocalPlayer);
        }
    }

    

    private void LateUpdate()
    {
        if(base.IsClient)
            CameraRotation();
    }

    private void CameraRotation()
    {
        // if there is an input and camera position is not fixed
        if (_localInput.look.sqrMagnitude >= _threshold && !LockCameraPosition)
        {
            //Don't multiply mouse input by Time.deltaTime;
            float deltaTimeMultiplier = 1.0f;

            _cinemachineTargetYaw += _localInput.look.x * deltaTimeMultiplier;
            _cinemachineTargetPitch += _localInput.look.y * deltaTimeMultiplier;
        }

        // clamp our rotations so our values are limited 360 degrees
        _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
        _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

        // Cinemachine will follow this target
        CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
            _cinemachineTargetYaw, 0.0f);
    }
    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }

}
