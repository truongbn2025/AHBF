using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;



public class TestPlayButtonBehavior : MonoBehaviour, IPointerUpHandler
{


    //[SerializeField] private GameObject inputPanel;
    public Transform targetButtonPanel;
    public Transform hidingButtonPanel;

    private Transform playButton;
    private Transform joinButton;
    private Transform playOffline;

    private Vector3 hidingPositionPlayButton;
    private Vector3 hidingPositionJoinButton;
    private Vector3 hidingPositionPlayOfflineButton;

    private Vector3 targetPositionPlayButton;
    private Vector3 targetPositionJoinButton;
    private Vector3 targetPositionPlayOfflineButton;




    [SerializeField] private Ease easeTypeIn;
    [SerializeField] private Ease easeTypeOut;

    public bool displayed;

    private void Start()
    {

        

        playButton = targetButtonPanel.GetChild(0);
        joinButton = targetButtonPanel.GetChild(1);
        playOffline = targetButtonPanel.GetChild(2);

        //assign target pos
        targetPositionPlayButton = playButton.GetComponent<RectTransform>().position;
        targetPositionJoinButton = joinButton.GetComponent<RectTransform>().position;
        targetPositionPlayOfflineButton = playOffline.GetComponent<RectTransform>().position;

        //hiding pos
        hidingPositionPlayButton = hidingButtonPanel.GetChild(0).GetComponent<RectTransform>().position;
        hidingPositionJoinButton = hidingButtonPanel.GetChild(1).GetComponent<RectTransform>().position;
        hidingPositionPlayOfflineButton = hidingButtonPanel.GetChild(2).GetComponent<RectTransform>().position;

        playButton.GetComponent<RectTransform>().position = hidingPositionPlayButton;
        joinButton.GetComponent<RectTransform>().position = hidingPositionJoinButton;
        playOffline.GetComponent<RectTransform>().position = hidingPositionPlayOfflineButton;
        displayed = false;
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!displayed)
        {
            DisplayPlayOptionButton();
            displayed = true;
        }
        else
        {
            HidePlayOptionButton();
            displayed = false;
        }
            
        
    }

    private void DisplayPlayOptionButton()
    {

        playButton.DOMove(targetPositionPlayButton, 0.5f).SetEase(easeTypeIn);
        joinButton.DOMove(targetPositionJoinButton, 0.5f).SetEase(easeTypeIn);
        playOffline.DOMove(targetPositionPlayOfflineButton, 0.5f).SetEase(easeTypeIn);


    }

    private void HidePlayOptionButton()
    {
        playButton.DOMove(hidingPositionPlayButton, 0.5f).SetEase(easeTypeOut);
        joinButton.DOMove(hidingPositionJoinButton, 0.5f).SetEase(easeTypeOut);
        playOffline.DOMove(hidingPositionPlayOfflineButton, 0.5f).SetEase(easeTypeOut);
        
    }


   
    
    public void ShowJoinButton()
    {
        playButton.DOMove(hidingPositionPlayButton, 0.5f).SetEase(easeTypeIn);
        joinButton.DOMove(targetPositionPlayButton, 0.5f).SetEase(easeTypeIn);
        playOffline.DOMove(hidingPositionPlayOfflineButton, 0.5f).SetEase(easeTypeIn);
    }
    public void ShowPlayOfflineButton()
    {
        playButton.DOMove(hidingPositionPlayButton, 0.5f).SetEase(easeTypeIn);
        joinButton.DOMove(hidingPositionJoinButton, 0.5f).SetEase(easeTypeIn);
        playOffline.DOMove(targetPositionPlayButton, 0.5f).SetEase(easeTypeIn);
    }


}
