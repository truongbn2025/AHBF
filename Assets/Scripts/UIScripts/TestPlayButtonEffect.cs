using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
public class TestPlayButtonEffect : MonoBehaviour, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
{

    [SerializeField] private Ease easeType;
    private RectTransform targetIcon;


    private void Start()
    {
        targetIcon = transform.GetChild(0).GetComponent<RectTransform>();
    }

    private void ButtonEffect()
    {
        targetIcon.DOScale(1.3f, 0.25f).SetEase(easeType);
    }
    private void ExitButtonEffect()
    {
        targetIcon.DOScale(1f, 0.25f).SetEase(easeType);
    }


    public void OnPointerUp(PointerEventData eventData)
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ButtonEffect();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ExitButtonEffect();
    }
}
