using FishNet.Object;
using FishNet.Object.Synchronizing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatsManager : NetworkBehaviour
{
    public BodyPartsManager bodyPartsManager;

    [SyncVar]
    public int Strength;
    [SyncVar]
    public int Agility;
    [SyncVar]
    public int Intelligent;

    public bool loaded;

    public override void OnStartServer()
    {
        base.OnStartServer();

        loaded = false;
        if (IsServer)
            bodyPartsManager.LoadingPartReady += OnLoadingCompleted;
    }


    private void OnLoadingCompleted()
    {
        GetTheStats();
    }    

    private void GetTheStats()
    {
        loaded = true;
        Strength = bodyPartsManager.GetStrength();
        Agility = bodyPartsManager.GetAgility();
        Intelligent = bodyPartsManager.GetInteligence();
    }


    public int GetMaxHealth()
    {
        return Strength;
    }

    public int GetMaxShield()
    {
        return Intelligent;
    }

    public float GetSpeed()
    {
        return 4 * (Agility / 100f);
    }

    public float GetResistance()
    {
        return 2 * Strength / 100f;
    }
}
