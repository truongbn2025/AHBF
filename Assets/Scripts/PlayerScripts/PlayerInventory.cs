using UnityEngine;
using FishNet.Object;
using FishNet.Connection;
using TMPro;
using UnityEngine.UI;
using System;

public class PlayerInventory : NetworkBehaviour
{
    [Header("Inventory")]
    //public List<InventoryObject> inventoryObjects = new List<InventoryObject>();
    //public List<SurroundingObject> surroundingObjects = new List<SurroundingObject>();
    private InventoryObject inventory;
    [SerializeField] private Transform DropPosition;
    [SerializeField] private BodyPartsManager bodyPartsManager;
   
    [Header("Starter Items")]
    private ItemObject PrimaryAmmoObject;
    [SerializeField] GameObject PrimaryAmmoPrefab;
    private ItemObject SecondaryAmmoObject;
    [SerializeField] GameObject SecondaryAmmoPrefab;
    [SerializeField] private ItemObject[] StarterObject;
    //[SerializeField] private ItemObject SecondaryGun;

    [Header("Inventory Components")]
    [SerializeField] private GameObject invPanel;
    [SerializeField] private Transform invObjectHolder;
    [SerializeField] private Transform primaryGunHolder;
    [SerializeField] private Transform secondaryGunHolder;
    [SerializeField] private Transform meleeHolder;
    [SerializeField] private Transform grenadeHolder;
    [SerializeField] private GameObject primaryAmmoHolder;
    [SerializeField] private GameObject secondaryAmmoHolder;

    [Header("Component Display In Inventory")]
    [SerializeField] private GameObject invCanvasObject;

    [Header("Drop Canvas")]
    [SerializeField] private GameObject dropCanvas;
    [SerializeField] private TextMeshProUGUI playerName;
    [SerializeField] private TextMeshProUGUI strenght;
    [SerializeField] private TextMeshProUGUI agility;
    [SerializeField] private TextMeshProUGUI intel;

    //[Header("Pickup")]
    //[SerializeField] LayerMask pickupLayer;
    //[SerializeField] float pickupDistance;
    //[SerializeField] KeyCode pickupButton = KeyCode.E;

    private Camera cam;
    private Transform worldObjectHolder;
    private NetworkCharacter networkCharacter;
    private WeaponsController weaponsController;
    private Health healthManager;
    public void Pickup(RaycastHit hit)
    {
        if (networkCharacter.characterState != CharacterState.Playing || !networkCharacter.characterMovement.isGrounded || weaponsController.combatState != CombatState.Normal)
            return;

        var item = hit.transform.GetComponent<GroundItem>();
        if (item)
        {
            RequestPickup(this.transform, hit.transform, base.Owner);
        }
    }

    public void ClassifyInInventory(ItemObject item, int amount)
    {
        switch (item.type)
        {
            case ITEM_TYPE.WEAPON_MELEE:
                for (int i = 0; i < inventory.MeleeWeapon.Count; i++)
                {
                    ItemObject tempItem = inventory.MeleeWeapon[i].item;
                    DropItem(tempItem);
                }

                
                inventory.AddMeleeWeapon(item);
                if (IsServer)
                    Actions.OnWeaponSwitch(WEAPON_TYPE.MELEE, item.weaponIndex);
                break;
            case ITEM_TYPE.WEAPON_GUN_PRIMARY:
                for (int i = 0; i < inventory.PrimaryGun.Count; i++)
                {
                    ItemObject tempItem = inventory.PrimaryGun[i].item;
                    DropItem(tempItem);
                }
                inventory.AddPrimaryGun(item);
                if (IsServer)
                    Actions.OnWeaponSwitch(WEAPON_TYPE.MAIN_GUN, item.weaponIndex);
                break;
            case ITEM_TYPE.WEAPON_GUN_SECONDARY:
                for (int i = 0; i < inventory.SecondaryGun.Count; i++)
                {
                    ItemObject tempItem = inventory.SecondaryGun[i].item;
                    DropItem(tempItem);


                }
                inventory.AddSecondaryGun(item);
                if (IsServer)
                    Actions.OnWeaponSwitch(WEAPON_TYPE.SIZE_ARM, item.weaponIndex);
                break;
            case ITEM_TYPE.AMMO_PRIMARY:
                inventory.AddPrimaryAmmo(PrimaryAmmoObject, amount);
                Actions.OnPrimaryAmmoChange?.Invoke(inventory.PrimaryAmmo[0].amount);
                Debug.Log(inventory.PrimaryAmmo[0].amount);
                break;
            case ITEM_TYPE.AMMO_SECONDARY:

                inventory.AddSecondaryAmmo(SecondaryAmmoObject, amount);
                Actions.OnSecondaryAmmoChange?.Invoke(inventory.SecondaryAmmo[0].amount);
                break;
            case ITEM_TYPE.HEALING:
                inventory.AddItem(item, 1);
                break;
            case ITEM_TYPE.SHIELD:
                inventory.AddItem(item, 1);
                break;
            case ITEM_TYPE.CURRENCY:
                inventory.AddItem(item, 1);
                break;
            case ITEM_TYPE.GRENADE:
                inventory.AddGrenade(item, 1);
                Actions.OnGrenadeNumberChange?.Invoke(inventory.Grenade[0].amount);
                break;
        }
    }
    public void ToggleInventory(bool toggle)
    {
        if (!toggle)
        {
            //turn off
            dropCanvas.SetActive(false);
            weaponsController.SetCanAimFlag(true);
            ToolTip.Instance.HideTooltip();
            invPanel.SetActive(false);

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            weaponsController.SetCanAimFlag(false);

            UpdateInvUI();

            invPanel.SetActive(true);

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void ToggleInventory()
    {
        if (invPanel.activeSelf)
        {
            dropCanvas.SetActive(false);
            weaponsController.SetCanAimFlag(true);
            ToolTip.Instance.HideTooltip();
            invPanel.SetActive(false);

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (!invPanel.activeSelf)
        {
            weaponsController.SetCanAimFlag(false);

            UpdateInvUI();

            invPanel.SetActive(true);

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    private void UpdateInvUI()
    {
        foreach (Transform child in invObjectHolder)
            Destroy(child.gameObject);
        foreach (Transform child in meleeHolder)
            Destroy(child.gameObject);
        foreach (Transform child in primaryGunHolder)
            Destroy(child.gameObject);
        foreach (Transform child in secondaryGunHolder)
            Destroy(child.gameObject);
        foreach (Transform child in grenadeHolder)
            Destroy(child.gameObject);


        for (int i = 0; i < inventory.Container.Count; i++)
        {
            GameObject obj = Instantiate(invCanvasObject, invObjectHolder);
            obj.GetComponent<InventoryItem>()._item = inventory.Container[i].item;
            obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = inventory.Container[i].amount.ToString();
            obj.transform.GetComponent<Button>().image.sprite = inventory.Container[i].item.icon;
            ItemObject tempItem = inventory.Container[i].item;
            obj.GetComponent<Button>().onClick.AddListener(delegate { UseItem(tempItem); });
            

        }
        for (int i = 0; i < inventory.MeleeWeapon.Count; i++)
        {
            GameObject obj = Instantiate(invCanvasObject, meleeHolder);
            obj.GetComponent<InventoryItem>()._item = inventory.MeleeWeapon[i].item;
            ItemObject tempItem = inventory.MeleeWeapon[0].item;
            obj.transform.GetComponent<Button>().image.sprite = inventory.MeleeWeapon[i].item.icon;

            obj.GetComponent<Button>().onClick.AddListener(delegate { DropItem(tempItem); });
        }
        for (int i = 0; i < inventory.PrimaryGun.Count; i++)
        {
            GameObject obj = Instantiate(invCanvasObject, primaryGunHolder);
            obj.GetComponent<InventoryItem>()._item = inventory.PrimaryGun[i].item;
            ItemObject tempItem = inventory.PrimaryGun[0].item;
            obj.transform.GetComponent<Button>().image.sprite = inventory.PrimaryGun[i].item.icon;

            obj.GetComponent<Button>().onClick.AddListener(delegate { DropItem(tempItem); });
        }
        for (int i = 0; i < inventory.SecondaryGun.Count; i++)
        {
            GameObject obj = Instantiate(invCanvasObject, secondaryGunHolder);
            obj.GetComponent<InventoryItem>()._item = inventory.SecondaryGun[i].item;
            ItemObject tempItem = inventory.SecondaryGun[0].item;
            obj.transform.GetComponent<Button>().image.sprite = inventory.SecondaryGun[i].item.icon;

            obj.GetComponent<Button>().onClick.AddListener(delegate { DropItem(tempItem); });
        }
        for (int i = 0; i < inventory.Grenade.Count; i++)
        {
            GameObject obj = Instantiate(invCanvasObject, grenadeHolder);
            obj.GetComponent<InventoryItem>()._item = inventory.Grenade[i].item;
            obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = inventory.Grenade[i].amount.ToString();
            obj.transform.GetComponent<Button>().image.sprite = inventory.Grenade[i].item.icon;
            ItemObject tempItem = inventory.Grenade[0].item;
            obj.GetComponent<Button>().onClick.AddListener(delegate { DropItem(tempItem); });
        }
        for (int i = 0; i < inventory.PrimaryAmmo.Count; i++)
        {
            primaryAmmoHolder.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = inventory.PrimaryAmmo[i].amount.ToString();
            ItemObject tempItem = inventory.PrimaryAmmo[0].item;
            int recentAmount = inventory.PrimaryAmmo[i].amount;
            primaryAmmoHolder.GetComponent<Button>().onClick.AddListener(delegate { DropAmountPanel(tempItem, recentAmount); });
        }
        for (int i = 0; i < inventory.SecondaryAmmo.Count; i++)
        {
            secondaryAmmoHolder.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = inventory.SecondaryAmmo[i].amount.ToString();
            ItemObject tempItem = inventory.SecondaryAmmo[0].item;
            int recentAmount = inventory.SecondaryAmmo[i].amount;
            secondaryAmmoHolder.GetComponent<Button>().onClick.AddListener(delegate { DropAmountPanel(tempItem, recentAmount); });
        }


    }

    private void DropAmountPanel(ItemObject item, int recentAmount)
    {
        switch (item.type)
        {
            case ITEM_TYPE.AMMO_PRIMARY:
                dropCanvas.SetActive(true);
                dropCanvas.GetComponent<DropItemUI>().item = item;
                dropCanvas.GetComponent<DropItemUI>().maxAmount.text = recentAmount.ToString();
                break;
            case ITEM_TYPE.AMMO_SECONDARY:
                dropCanvas.SetActive(true);
                dropCanvas.GetComponent<DropItemUI>().item = item;
                dropCanvas.GetComponent<DropItemUI>().maxAmount.text = recentAmount.ToString();
                break;
        }
    }

    public void UseItem(ItemObject item)
    {
        switch(item.type)
        {
            case ITEM_TYPE.HEALING:
                healthManager.RegenHealth(item.restoreHealthValue);
                for (int i = 0; i < inventory.Container.Count; i++)
                {
                    if (inventory.Container[i].item != item)
                        continue;

                    if (inventory.Container[i].amount > 1)
                    {
                        inventory.Container[i].amount--;

                        //DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                    if (inventory.Container[i].amount <= 1)
                    {
                        inventory.Container.Remove(inventory.Container[i]);

                        //DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                }
                break;
            case ITEM_TYPE.SHIELD:
                healthManager.RegenShield(item.restoreHealthValue);
                for (int i = 0; i < inventory.Container.Count; i++)
                {
                    if (inventory.Container[i].item != item)
                        continue;

                    if (inventory.Container[i].amount > 1)
                    {
                        inventory.Container[i].amount--;

                        //DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                    if (inventory.Container[i].amount <= 1)
                    {
                        inventory.Container.Remove(inventory.Container[i]);

                        //DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                }
                break;
        }
    }

    public void DeadDrop()
    {
        Debug.Log("called");
        for (int i = 0; i < inventory.PrimaryGun.Count; i++)
        {
            ItemObject tempItem = inventory.PrimaryGun[i].item;
            DropItem(tempItem);
            Debug.Log(tempItem.name);
        }
        for (int i = 0; i < inventory.SecondaryGun.Count; i++)
        {
            ItemObject tempItem = inventory.SecondaryGun[i].item;
            DropItem(tempItem);
            Debug.Log(tempItem.name);
        }
        for (int i = 0; i < inventory.MeleeWeapon.Count; i++)
        {
            ItemObject tempItem = inventory.MeleeWeapon[i].item;
            DropItem(tempItem);
            Debug.Log(tempItem.name);
        }
        for (int i = 0; i < inventory.PrimaryAmmo.Count; i++)
        {
            ItemObject tempItem = inventory.PrimaryAmmo[i].item;
            Debug.Log(tempItem.name);
            DropItem(tempItem, inventory.PrimaryAmmo[i].amount);
        }
        for (int i = 0; i < inventory.SecondaryAmmo.Count; i++)
        {
            ItemObject tempItem = inventory.SecondaryAmmo[i].item;
            Debug.Log(tempItem.name);
            DropItem(tempItem, inventory.SecondaryAmmo[i].amount);
        }
        for (int i = 0; i < inventory.Container.Count; i++)
        {
            ItemObject tempItem = inventory.Container[i].item;
            Debug.Log(tempItem.name);
            DropItem(tempItem, inventory.Container[i].amount);
        }

    }

    public void DropItem(ItemObject item, int amount)
    {
        switch (item.type)
        {
            case ITEM_TYPE.AMMO_PRIMARY:
                if (amount > 0)
                {
                    inventory.PrimaryAmmo[0].amount -= amount;
                    
                    Actions.OnPrimaryAmmoChange?.Invoke(inventory.PrimaryAmmo[0].amount);
                   
                    DropItemRPC(PrimaryAmmoPrefab, DropPosition.position, amount);
                    UpdateInvUI();
                }
                break;
            case ITEM_TYPE.AMMO_SECONDARY:
                if (amount > 0)
                {
                    inventory.SecondaryAmmo[0].amount -= amount;
                    
                    Actions.OnSecondaryAmmoChange?.Invoke(inventory.SecondaryAmmo[0].amount);
                  
                    DropItemRPC(SecondaryAmmoPrefab, DropPosition.position, amount);
                    UpdateInvUI();
                }

                break;
            
        }

    }


    public void DropItem(ItemObject item)
    {
        //Debug.Log("drop"+item.type);
        GameObject dropItem = item.prefab;
        switch (item.type)
        {
            case ITEM_TYPE.WEAPON_MELEE:
                for (int i = 0; i < inventory.MeleeWeapon.Count; i++)
                {
                    inventory.MeleeWeapon.Remove(inventory.MeleeWeapon[i]);
                    
                    DropItemRPC(item.prefab, DropPosition.position);
                    UpdateInvUI();
                    if (IsServer)
                        Actions.OnWeaponDrop?.Invoke(WEAPON_TYPE.MELEE);
                    return;
                }
                break;
            case ITEM_TYPE.WEAPON_GUN_PRIMARY:
                for (int i = 0; i < inventory.PrimaryGun.Count; i++)
                {
                    inventory.PrimaryGun.Remove(inventory.PrimaryGun[i]);
                    
                    DropItemRPC(item.prefab, DropPosition.position);
                    
                    UpdateInvUI();
                    if (IsServer)
                        Actions.OnWeaponDrop?.Invoke(WEAPON_TYPE.MAIN_GUN);
                    return;
                }
                break;
            case ITEM_TYPE.WEAPON_GUN_SECONDARY:
                for (int i = 0; i < inventory.SecondaryGun.Count; i++)
                {
                    inventory.SecondaryGun.Remove(inventory.SecondaryGun[i]);
                    
                    DropItemRPC(item.prefab, DropPosition.position);
                    
                    UpdateInvUI();
                    if (IsServer)
                        Actions.OnWeaponDrop?.Invoke(WEAPON_TYPE.SIZE_ARM);
                    return;
                }
                break;
            case ITEM_TYPE.AMMO_PRIMARY:

                break;
            case ITEM_TYPE.AMMO_SECONDARY:

                break;
            case ITEM_TYPE.HEALING:
                for (int i = 0; i < inventory.Container.Count; i++)
                {
                    if (inventory.Container[i].item != item)
                        continue;

                    if (inventory.Container[i].amount > 1)
                    {
                        inventory.Container[i].amount--;

                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                    if (inventory.Container[i].amount <= 1)
                    {
                        inventory.Container.Remove(inventory.Container[i]);

                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                }
                break;
            case ITEM_TYPE.SHIELD:
                for (int i = 0; i < inventory.Container.Count; i++)
                {
                    if (inventory.Container[i].item != item)
                        continue;

                    if (inventory.Container[i].amount > 1)
                    {
                        inventory.Container[i].amount--;

                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                    if (inventory.Container[i].amount <= 1)
                    {
                        inventory.Container.Remove(inventory.Container[i]);

                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                }
                break;
            case ITEM_TYPE.GRENADE:
                for (int i = 0; i < inventory.Grenade.Count; i++)
                {
                    if (inventory.Grenade[i].item != item)
                        continue;

                    if (inventory.Grenade[i].amount > 1)
                    {
                        inventory.Grenade[i].amount--;
                        Actions.OnGrenadeNumberChange?.Invoke(inventory.Grenade[i].amount);
                       
                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                    if (inventory.Grenade[i].amount <= 1)
                    {
                        inventory.Grenade.Remove(inventory.Grenade[i]);

                        Actions.OnGrenadeNumberChange?.Invoke(0);
                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                }
                break;
        }
    }

    public void SetPlayerName(string name)
    {
        playerName.text = name;
    }

    public void SetStarterWeapon()
    {
        
        switch(bodyPartsManager.partID.weapon)
        {
            case 1:
                break;
            case 2:
                break;
            //Light Machine gun
            case 3:

                inventory.AddPrimaryGun(StarterObject[0]);
                Actions.OnWeaponSwitch(WEAPON_TYPE.MAIN_GUN, StarterObject[0].weaponIndex);
                break;
            case 4:
                inventory.AddMeleeWeapon(StarterObject[1]);
                Actions.OnWeaponSwitch(WEAPON_TYPE.MELEE, StarterObject[1].weaponIndex);
                
                break;
            //Shot gun
            case 5:
                inventory.AddPrimaryGun(StarterObject[2]);
                Actions.OnWeaponSwitch(WEAPON_TYPE.MAIN_GUN, StarterObject[2].weaponIndex);
                break;
            case 6:
                inventory.AddMeleeWeapon(StarterObject[3]);
                Actions.OnWeaponSwitch(WEAPON_TYPE.MELEE, StarterObject[3].weaponIndex);
                break;
            case 7:
                inventory.AddMeleeWeapon(StarterObject[4]);
                Actions.OnWeaponSwitch(WEAPON_TYPE.MELEE, StarterObject[4].weaponIndex);
                break;
            case 10:
                inventory.AddMeleeWeapon(StarterObject[5]);
                Actions.OnWeaponSwitch(WEAPON_TYPE.MELEE, StarterObject[5].weaponIndex);
                break;
            case 12:
                inventory.AddMeleeWeapon(StarterObject[6]);
                Actions.OnWeaponSwitch(WEAPON_TYPE.MELEE, StarterObject[6].weaponIndex);
                break;
            case 13:
                inventory.AddMeleeWeapon(StarterObject[7]);
                Actions.OnWeaponSwitch(WEAPON_TYPE.MELEE, StarterObject[7].weaponIndex);
                break;
            case 15:
                inventory.AddMeleeWeapon(StarterObject[8]);
                Actions.OnWeaponSwitch(WEAPON_TYPE.MELEE, StarterObject[8].weaponIndex);
                break;
            case 16:
                inventory.AddMeleeWeapon(StarterObject[9]);
                Actions.OnWeaponSwitch(WEAPON_TYPE.MELEE, StarterObject[9].weaponIndex);
                break;
        }
    }

    #region MonoBehavior
    private void Awake()
    {
        healthManager = GetComponent<Health>();
        inventory = new InventoryObject();
        PrimaryAmmoObject = new PrimaryAmmoItem();
        SecondaryAmmoObject = new SecondaryAmmoItem();
        networkCharacter = GetComponent<NetworkCharacter>();
        weaponsController = GetComponent<WeaponsController>();
        //bodyPartsManager = GetComponentInChildren<BodyPartsManager>();

       
        //inventory.AddPrimaryGun();

    }
   
    #endregion
                
    #region NetworkBehavior
    public override void OnStartClient()
    {
        base.OnStartClient();
        
       
        Debug.Log(bodyPartsManager.partID.weapon);
        SetStarterWeapon();
        //this is where you assign initial item
        inventory.AddPrimaryAmmo(PrimaryAmmoObject, 30);
        Actions.OnPrimaryAmmoChange?.Invoke(30);
        inventory.AddSecondaryAmmo(SecondaryAmmoObject, 15);
        Actions.OnSecondaryAmmoChange?.Invoke(15);
        ClientDoneSpawning();
        if (!base.IsOwner)
        {
            enabled = false;
            return;
        }

        cam = Camera.main;

        worldObjectHolder = GameObject.FindGameObjectWithTag("WorldObjects").transform;

        if (base.IsClient)
        {
            ToggleInventory(false);
        }

    }

    private void Update()
    {
        if(IsClient)
        {
            strenght.text = GetComponent<PlayerStatsManager>().Strength + "";
            agility.text = GetComponent<PlayerStatsManager>().Agility + "";
            intel.text = GetComponent<PlayerStatsManager>().Intelligent + "";
        }
    }

    public void ReloadPrimaryAmmo(int amount)
    {
        inventory.PrimaryAmmo[0].amount -= amount;
        Actions.OnPrimaryAmmoChange?.Invoke(inventory.PrimaryAmmo[0].amount);
    }

    public void ReloadSecondaryAmmo(int amount)
    {
        inventory.SecondaryAmmo[0].amount -= amount;
        Actions.OnPrimaryAmmoChange?.Invoke(inventory.SecondaryAmmo[0].amount);
    }
    #endregion

    #region RPCs



    [ServerRpc(RunLocally = true)]
    private void ClientDoneSpawning()
    {
        Actions.OnReloadPrimaryAmmo += ReloadPrimaryAmmo;
        Actions.OnReloadSecondaryAmmo += ReloadSecondaryAmmo;
    }

    [ServerRpc]
    void DespawnObject(GameObject objToDespawn)
    {
        ServerManager.Despawn(objToDespawn);
    }

    [ServerRpc(RequireOwnership = false)]
    void DropItemRPC(GameObject prefab, Vector3 position)
    {
        //DropItemObserver(prefab, position);
        GameObject drop = Instantiate(prefab, position, Quaternion.identity, worldObjectHolder);
        Spawn(drop);
    }

    [ServerRpc(RequireOwnership = false)]
    void DropItemRPC(GameObject prefab, Vector3 position, int amount)
    {

        //DropItemObserver(prefab, position);
        
        GameObject drop = Instantiate(prefab, position, Quaternion.identity, worldObjectHolder);
        
       
        Spawn(drop);
        drop.GetComponent<GroundItem>().SetAmount(amount);

    }

    [ServerRpc]
    public void RequestPickup(Transform player, Transform item, NetworkConnection connection)
    {
        float distance = 3f;
        if (Vector3.Distance(player.position, item.position) <= distance)
        {
            //Debug.Log("Allowed");
            AllowPickup(connection, item);
        }
    }

    [TargetRpc(RunLocally = true)]
    public void AllowPickup(NetworkConnection conn, Transform item)
    {
        //Debug.Log("Allow Pick up amount: " + item.GetComponent<GroundItem>().amount);
        ClassifyInInventory(item.GetComponent<GroundItem>().item, item.GetComponent<GroundItem>().amount);
        DespawnObject(item.gameObject);
    }

    #endregion

}