using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LOBBY_START_TYPE
{
    CREATE,
    JOIN,
    LOCAL_DEBUG
}

public enum PrototypeAstroCharacter
{
    Blue
}

public class PlayerLobbyData : MonoBehaviour
{
    public static PlayerLobbyData Instance;
    public LobbyController lobbyController;
    private void Awake()
    {
        lobbyController = GetComponent<LobbyController>();
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public string playerName;
    public string characterCode;
    public string ServerIP;
    public ushort port;
    public LOBBY_START_TYPE startType { get; set; }
    public PrototypeAstroCharacter character;
}
