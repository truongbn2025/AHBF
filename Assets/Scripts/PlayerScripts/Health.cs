using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using System;
using FishNet.Connection;

public class Health : NetworkBehaviour
{
    [SyncVar(OnChange = nameof(OnHealthChanged))]
    private float health;
    [SyncVar(OnChange = nameof(OnShieldChanged))]
    private float shield;

    //[SyncVar]
    public bool isInvincible = true;
    public bool isAlive = true;
    public bool hasShield = true;

    public bool canHealth = false;
    public bool canShield = false;
    public float timer = 1f;

    public event Action<float> HealthUpdate;
    public event Action<float> ShieldUpdate;

    public event Action<NetworkCharacter, string> Die;
    private HurtBox[] hurtBoxes;
    private PlayerStatsManager statsManager;
    private PlayerInventory inventory;

    //ontick damage
    public bool isBurning = false;
    private float damageTimer = 1f;
    private float baseDamageTimer = 1f;
    private float burnDamage = 1f;

    private bool loaded;


    private void Awake()
    {
        hurtBoxes = GetComponentsInChildren<HurtBox>();
        statsManager = GetComponent<PlayerStatsManager>();
        inventory = GetComponent<PlayerInventory>();
        canHealth = false;
        canShield = false;
        loaded = false;
    }


    private void OnTick()
    {
        if (isBurning)
        {
            if (damageTimer > 0)
                damageTimer -= (float)TimeManager.TickDelta;
            else
            {
                damageTimer = baseDamageTimer;
                ReduceOnTick(burnDamage, null, null);
                Debug.Log("Burning hot");
            }

        }
        
    }
    public override void OnStartServer()
    {
        base.OnStartServer();
        TimeManager.OnTick += OnTick;
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            hurtBox.hit += ReduceHealth;
            hurtBox.hitByBot += ReduceHealth;
        }
    }
    private void Update()
    {
        if(!loaded && statsManager.loaded)
        {
            health = statsManager.GetMaxHealth();
            shield = statsManager.GetMaxShield();
            //health = 1f;
        }

        if (!loaded) return;


        //if (shield > 0)
        //{
        //    hasShield = true;
        //}
        //else
        //{
        //    hasShield = false;
        //}

        //if (canShield && timer <= 0)
        //{
        //    RegenShield(20f);
        //    timer = 1f;
        //}
        //else
        //{
        //    timer -= Time.deltaTime;

        //}

        //if (isBurning)
        //{
        //    if (damageTimer > 0)
        //        damageTimer -= Time.deltaTime;
        //    else
        //    {
        //        damageTimer = baseDamageTimer;
        //        ReduceOnTick(0.2f, null, null);
        //    }

        //}

    }
    public void OnDestroy()
    {
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            hurtBox.hit -= ReduceHealth;
            hurtBox.hitByBot -= ReduceHealth;
        }
    }
    public void SetBurnState(bool state, float damage)
    {
        isBurning = state;

    }
    public void ReduceOnTick(float amount, NetworkCharacter source, string weaponName)
    {
        if (isInvincible || !isAlive) return;
        //if (hasShield)
        //{
        //    shield -= amount;
        //    if (shield <= 0)
        //        hasShield = false;
        //}
        //else
        //{
        //    health -= amount;
        //}
        if (shield > 0)
        {
            shield -= amount;
            hasShield = true;

        }
        else
        {
            health -= amount;
            hasShield = false;
        }
        //health -= Mathf.Round((amount - statsManager.GetResistance()));

        if (health <= 0)
        {
            inventory.DeadDrop();
            health = 0;
            isAlive = false;
            Die?.Invoke(source, weaponName);
            source.KillCount++;
            
            GetComponent<NetworkCharacter>().characterState = CharacterState.Dead;
        }
    }


    public void ReduceHealth(float amount, NetworkCharacter source, string weaponName)
    {
        if (isInvincible || !isAlive) return;

        //if (hasShield)
        //{
        //    shield -= Mathf.Round((amount - statsManager.GetResistance()));
        //    if (shield <= 0)
        //        hasShield = false;
        //}
        //else
        //{
        //    health -= Mathf.Round((amount - statsManager.GetResistance()));
        //}

        if (shield > 0)
        {
            shield -= Mathf.Round((amount - statsManager.GetResistance()));
            hasShield = true;

        }
        else
        {
            //Debug.Log(health);
            health -= Mathf.Round((amount - statsManager.GetResistance()));
            hasShield = false;

        }

        if (health <= 0)
        {
            inventory.DeadDrop();
            health = 0;
            isAlive = false;
            Die?.Invoke(source, weaponName);
            source.KillCount++;
            
            GetComponent<NetworkCharacter>().characterState = CharacterState.Dead;
        }
    }
    public void ReduceHealth(float amount, string source, string weaponName)
    {
        Debug.Log("1");
        if (isInvincible || !isAlive) return;

        if (shield > 0)
        {
            shield -= Mathf.Round((amount - statsManager.GetResistance()));
            hasShield = true;

        }
        else
        {
            //Debug.Log(health);
            health -= Mathf.Round((amount - statsManager.GetResistance()));
            hasShield = false;

        }

        if (health <= 0)
        {
            inventory.DeadDrop();
            health = 0;
            isAlive = false;
            //Die?.Invoke(source, weaponName);
            //source.KillCount++;

            GetComponent<NetworkCharacter>().characterState = CharacterState.Dead;
        }
    }
    public void RegenHealth(float ammount)
    {
        Debug.Log(ammount);
        if ((health + ammount) < statsManager.GetMaxHealth())
            health += ammount;
        else
        {
            health = statsManager.GetMaxHealth();
        }

    }

    public void RegenShield(float ammount)
    {
        if((shield+ ammount) < statsManager.GetMaxShield())
            shield += ammount;
        else
        {
            shield = statsManager.GetMaxShield();
        }
    }


    private void OnHealthChanged(float prev, float next, bool asServer)
    {
        if (asServer) return;
        HealthUpdate?.Invoke(next);
        if (health <= 0)
        {
            //GetComponent<NetworkCharacter>().OnDie(); 
            isAlive = false;
        }
    }

    private void OnShieldChanged(float prev, float next, bool asServer)
    {
        if (asServer) return;
        ShieldUpdate?.Invoke(next);
        if (shield <= 0)
        {
            //GetComponent<NetworkCharacter>().OnDie(); 
            hasShield = false;
        }
    }

    public void SetIgnoreRaycast(bool isIgnore)
    {
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            if (isIgnore) hurtBox.gameObject.layer = 2;
            else hurtBox.gameObject.layer = 3;
        }
    }
}
