using UnityEngine;
using FishNet.Object;
using FishNet.Connection;
using TMPro;
using UnityEngine.UI;
using System;


public class PlayerBody : NetworkBehaviour
{
    [SerializeField] private BodyPartManager bodyPartManager;

    //create ui object
    [SerializeField] private GameObject bodypartCanvasObject;

    [Header("Body Parts")]
    [SerializeField] private Transform headHolder;
    [SerializeField] private Transform hipHolder;
    [SerializeField] private Transform leftArmHolder;
    [SerializeField] private Transform rightArmHolder;
    [SerializeField] private Transform leftLegHolder;
    [SerializeField] private Transform rightLegHolder;

    private void UpdateBodyPartUI()
    {
        foreach (Transform child in headHolder)
            Destroy(child.gameObject);
        foreach (Transform child in hipHolder)
            Destroy(child.gameObject);
        foreach (Transform child in leftArmHolder)
            Destroy(child.gameObject);
        foreach (Transform child in rightArmHolder)
            Destroy(child.gameObject);
        foreach (Transform child in leftLegHolder)
            Destroy(child.gameObject);
        foreach (Transform child in rightLegHolder)
            Destroy(child.gameObject);


        for(int i = 0; i< bodyPartManager._head.Count; i++)
        {
            GameObject obj = Instantiate(bodypartCanvasObject, headHolder);
            obj.GetComponent<EquippedBodypart>()._bodyPart = bodyPartManager._head[i].playerHead;
            BodyPart tempItem = bodyPartManager._head[0].playerHead;
            obj.transform.GetComponent<Button>().image.sprite = bodyPartManager._head[i].playerHead.GetICon();
        }
    }
}
