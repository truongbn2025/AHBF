using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalCanvasControl : MonoBehaviour
{
    private Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }
    void Start()
    {
        
    }

    
    void LateUpdate()
    {
        transform.LookAt(transform.position + mainCamera.transform.rotation * Vector3.forward, mainCamera.transform.rotation * Vector3.up);
    }
}
