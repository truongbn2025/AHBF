using EasyCharacterMovement;
using FishNet;
using FishNet.Object;
using FishNet.Object.Prediction;
using FishNet.Object.Synchronizing;
using FishNet.Transporting;
using System;
using TMPro;
using UnityEngine;

public struct InputDataNew : IReplicateData
{
    public Vector2 MoveDirection;
    public bool Jump;
    public bool Sprint;
    public bool Aim;
    public bool SpecialAim;
    public Vector3 cameraForward;
    public Vector3 cameraUp;

    private uint _tick;
    public void Dispose() { }
    public uint GetTick() => _tick;
    public void SetTick(uint value) => _tick = value;
}

public struct CharacterMovementReconcileData : IReconcileData
{
    public Vector3 position;
    public Quaternion rotation;

    public Vector3 velocity;

    public bool isConstrainedToGround;
    public float unconstrainedTimer;

    public bool hitGround;
    public bool isWalkable;

    public Vector3 groundNormal;

    private uint _tick;
    public void Dispose() { }
    public uint GetTick() => _tick;
    public void SetTick(uint value) => _tick = value;
}

public enum MovementMode
{
    Walking,
    Aiming,
    Flying,
    Diving,
}

public enum CharacterState
{
    Waiting,
    StartingMatch,
    Droping,
    Playing,
    Dead,
}

public class NetworkCharacter : NetworkBehaviour
{
    #region STRUCTS



    #endregion

    #region EDITOR EXPOSED FIELDS
    public MovementMode movementMode;

    [SyncVar(Channel = Channel.Reliable)]
    public CharacterState characterState;

    [SyncVar(Channel = Channel.Reliable, OnChange = nameof(OnKilledCountUpdate))]
    public int KillCount = 0;

    [SyncVar(Channel = Channel.Reliable, OnChange = nameof(OnPlayerSetName))]
    public string playerName;

    [SyncVar(Channel = Channel.Reliable, OnChange = nameof(OnPlayerSetCode))]
    public string characterCode;

    public float rotationRate = 540.0f;

    public bool canEverMove = true;
    public float maxSpeed = 5;

    public bool canEverSprint = true;
    public float sprintSpeedMultiplier = 2f;

    public float acceleration = 20.0f;
    public float deceleration = 20.0f;

    public float groundFriction = 8.0f;
    public float airFriction = 0.5f;

    public bool canEverJump = true;
    public float jumpImpulse = 6.5f;
    public float jumpTimeout = 0.3f;

    public bool canEverAim = true;
    public float aimSpeedMultiplier = 0.5f;


    [Range(0.0f, 1.0f)]
    public float airControl = 0.3f;

    public Vector3 gravity = Vector3.down * 9.81f;

    public TextMeshProUGUI playerNameUI;

    #endregion

    #region PROPERTIES

    public CharacterMovement characterMovement { get; private set; }

    public Vector2 moveInput { get; set; }
    public Vector3 moveDirection { get; set; }

    public bool jump { get; set; }

    public bool sprint { get; set; }

    public bool aim { get; set; }

    public bool specialAim { get; set; }

    #endregion

    #region PRIVATE FIELDS

    private Transform currentCamera;
    private InputManager inputManager;
    private Animator animator;
    private WeaponsController weaponsController;
    private Health health;

    private Vector3 cameraForward;
    private Vector3 cameraUp;

    private float jumpTimeoutTimer;
    private bool ungroundedLastFrame;

    private bool skipGroundCheckThisUpdate;

    private bool disableInput = false;
    private BodyPartsManager bodyPartsManager;
    //Animator triggers

    private bool landing;

    //State bool

    private bool landed = false;

    #endregion

    //Events

    public event Action<Vector3> OnLand;

    #region METHODS

    /// <summary>
    /// Cache components.
    /// </summary>

    private void CacheComponents()
    {
        characterMovement = GetComponent<CharacterMovement>();
        currentCamera = Camera.main.transform;
        inputManager = FindObjectOfType<InputManager>();
        animator = GetComponent<Animator>();
        weaponsController = GetComponent<WeaponsController>();
        health = GetComponent<Health>();
    }

    private void OnPlayerSetName(string prev, string next, bool asServer)
    {
        playerNameUI.text = next;
        GetComponent<PlayerInventory>().SetPlayerName(next);
    }
     
    private void OnPlayerSetCode(string prev, string next, bool asServer)
    {
        //playerNameUI.text = next;
        //GetComponent<PlayerInventory>().SetPlayerName(next);
    }

    [ServerRpc]
    private void SetPlayerName(string name)
    {
        playerName = name;
    }

    private void SetCharacterCode(string code)
    {
        bodyPartsManager.NFTid = code;
    }

    private void OnKilledCountUpdate(int prev, int next, bool asServer)
    {
        if (base.IsOwner)
            FindObjectOfType<MatchManager>().UpdateKilledCount(next);
    }

    /// <summary>
    /// Read player inputs.
    /// </summary>

    private void ReadInput(out InputDataNew inputData)
    {
        inputData = default;

        if (disableInput) return;

        inputData.MoveDirection = inputManager.move;

        if (characterState == CharacterState.Droping)
        {
            inputData.MoveDirection.x = 0;
            if (inputData.MoveDirection.y < 0) inputData.MoveDirection.y = 0;
        }
        inputData.cameraForward = currentCamera.transform.forward;
        inputData.cameraUp = currentCamera.transform.up;

        if (jumpTimeoutTimer <= 0)
            inputData.Jump = inputManager.jump;
        else
            inputData.Jump = false;

        inputData.Sprint = inputManager.sprint;
        inputData.Aim = weaponsController.combatState == CombatState.LeftAiming;
        inputData.SpecialAim = weaponsController.combatState == CombatState.RightAiming;

        inputManager.jump = false;
    }

    /// <summary>
    /// Handle player input.
    /// </summary>

    private void HandleInput(InputDataNew inputData)
    {
        //print(inputData);
        // Movement input
        moveInput = inputData.MoveDirection;

        moveDirection = Vector3.zero;
        moveDirection += Vector3.right * inputData.MoveDirection.x;
        moveDirection += Vector3.forward * inputData.MoveDirection.y;

        moveDirection = moveDirection.relativeTo(cameraForward, cameraUp);

        // Jump input

        jump = inputData.Jump;

        sprint = inputData.Sprint;

        aim = inputData.Aim;

        specialAim = inputData.SpecialAim;

        cameraForward = inputData.cameraForward;
        cameraUp = inputData.cameraUp;
    }

    public float GetMaxSpeed()
    {
        if ((aim || specialAim) && canEverAim)
            return maxSpeed * aimSpeedMultiplier;
        else if (sprint && canEverSprint)
            return maxSpeed * sprintSpeedMultiplier;
        else
            return maxSpeed;
    }

    public void SetPosition(Vector3 position)
    {
        characterMovement.SetPosition(position);
    }

    [ObserversRpc(RunLocally = true)]
    public void DisableInput(bool value)
    {
        disableInput = value;
    }

    public Animator GetAnimator()
    {
        return animator;
    }


    public bool IsSprinting()
    {
        return sprint && canEverSprint && (characterMovement.speed > maxSpeed);
    }

    /// <summary>
    /// Update character's rotation.
    /// </summary>

    public void RotateCharacter(Vector3 direction)
    {
        direction = direction.projectedOnPlane(transform.up);

        characterMovement.rotation = Quaternion.LookRotation(direction, transform.up);
    }

    private void UpdateRotation()
    {
        // Rotate towards movement direction

        if (!canEverMove) return;

        float deltaTime = (float)TimeManager.TickDelta;

        Vector3 rotationDirection = (aim || specialAim || characterState == CharacterState.Droping) ? cameraForward : moveDirection;

        float currentRotationrate = characterState == CharacterState.Droping ? 50f : rotationRate;

        characterMovement.RotateTowards(rotationDirection, currentRotationrate * deltaTime);
    }

    /// <summary>
    /// Update character's movement.
    /// </summary>
    /// 
    private void EndDropping()
    {
        characterState = CharacterState.Playing;
        canEverSprint = true;
        canEverMove = true;
    }

    private void UpdateMovement()
    {
        if (characterState == CharacterState.Droping && characterMovement.isGrounded && !skipGroundCheckThisUpdate && !landed)
        {
            Invoke("EndDropping", 0.8f);
            OnLand.Invoke(transform.position);
            canEverMove = false;
            landed = true;
        }
        else
            skipGroundCheckThisUpdate = false;

        // Jumping

        if (jump && characterMovement.isGrounded)
        {
            characterMovement.PauseGroundConstraint();
            characterMovement.LaunchCharacter(Vector3.up * jumpImpulse, true);
            animator.SetBool("Jump", true);
        }

        // Do move
        if (!canEverMove)
        {
            characterMovement.velocity = Vector3.zero;
            return;
        }

        if (characterState == CharacterState.Droping)
        {
            float droppingMaxSpeed = GetMaxSpeed() * (sprint ? 40f : 300f);

            Vector3 desiredVelocity = moveDirection * droppingMaxSpeed;

            if (sprint)
            {
                characterMovement.velocity = new Vector3(characterMovement.velocity.x, -100, characterMovement.velocity.z);
            }
            else
            {
                characterMovement.velocity = new Vector3(characterMovement.velocity.x, -50, characterMovement.velocity.z);
            }

            Ray findGroundRay = new Ray(transform.position, Vector3.down);
            if (Physics.Raycast(findGroundRay, out RaycastHit hit, 20f, characterMovement.collisionLayers))
            {
                if (hit.collider.CompareTag("Untagged") || hit.collider.CompareTag("Ground"))
                {
                    characterMovement.velocity = new Vector3(characterMovement.velocity.x, -300, characterMovement.velocity.z);
                    landing = true;
                }
            }

            float actualAcceleration = 300;
            float actualDeceleration = 300;

            float actualFriction = airFriction;

            float deltaTime = (float)TimeManager.TickDelta;

            characterMovement.SimpleMove(desiredVelocity, droppingMaxSpeed, actualAcceleration, actualDeceleration,
                actualFriction, actualFriction, gravity, true, deltaTime);
        }
        else
        {
            Vector3 desiredVelocity = moveDirection * GetMaxSpeed();

            float actualAcceleration = characterMovement.isGrounded ? acceleration : acceleration * airControl;
            float actualDeceleration = characterMovement.isGrounded ? deceleration : 0.0f;

            float actualFriction = characterMovement.isGrounded ? groundFriction : airFriction;

            float deltaTime = (float)TimeManager.TickDelta;

            if (disableInput) return;
            characterMovement.SimpleMove(desiredVelocity, GetMaxSpeed(), actualAcceleration, actualDeceleration,
                actualFriction, actualFriction, gravity, true, deltaTime);
        }

    }

    public void AddVelocity(Vector3 velocity)
    {
        characterMovement.velocity = velocity;
    }

    private void UpdateTimers()
    {
        if (!characterMovement.isGrounded)
            ungroundedLastFrame = true;
        else if (ungroundedLastFrame)
        {
            jumpTimeoutTimer = jumpTimeout;
            ungroundedLastFrame = false;
        }

        jumpTimeoutTimer -= (float)TimeManager.TickDelta;
    }

    /// <summary>
    /// Simulate character using given input data.
    /// </summary>

    [Replicate]
    private void Simulate(InputDataNew inputData, bool asServer, Channel channel = Channel.Unreliable, bool replying = false)
    {
        if (characterState == CharacterState.Dead) return;

        HandleInput(inputData);
        UpdateRotation();
        UpdateMovement();
    }

    [Reconcile]
    private void Reconciliation(CharacterMovementReconcileData reconcileData, bool asServer, Channel channel = Channel.Unreliable)
    {
        CharacterMovement.State characterState = new CharacterMovement.State(
            reconcileData.position,
            reconcileData.rotation,
            reconcileData.velocity,
            reconcileData.isConstrainedToGround,
            reconcileData.unconstrainedTimer,
            reconcileData.hitGround,
            reconcileData.isWalkable,
            reconcileData.groundNormal);

        characterMovement.SetState(in characterState);
    }

    #endregion

    #region NETWORKBEHAVIOUR

    public override void OnStartClient()
    {
        base.OnStartClient();
        print("AAAA");
        ClientDoneSpawning();
    }

    [ServerRpc(RunLocally = true)]
    private void ClientDoneSpawning()
    {
        // Subscribe to TimeManager events

        //TimeManager.OnTick += OnTick;

        if (IsServer)
        {
            health.isInvincible = true;
            characterState = CharacterState.Waiting;
        }

        // Take control of player camera

        if (!IsOwner && !IsServer)
        {
            Destroy(characterMovement);
            Destroy(GetComponent<Rigidbody>());
            return;
        }

        if (IsOwner)
        {
            Cursor.lockState = CursorLockMode.Locked;

            if (PlayerLobbyData.Instance != null)
            {
                SetPlayerName(PlayerLobbyData.Instance.playerName);
                SetCharacterCode(PlayerLobbyData.Instance.characterCode);
            }


            else
            {
                SetPlayerName("Player " + UnityEngine.Random.Range(1000, 10000));
                SetCharacterCode("AH 2921");
            }
                
        }
    }

    public override void OnStopClient()
    {
        base.OnStopClient();

        // Unsubscribe from TimeManager events

        TimeManager.OnTick -= OnTick;
    }

    private void OnTick()
    {
        if (IsOwner)
        {
            Reconciliation(default, false);
            ReadInput(out InputDataNew inputData);
            //print(inputData);
            Simulate(inputData, false);
            UpdateTimers();
        }

        if (IsServer)
        {
            Simulate(default, true);

            CharacterMovement.State characterState = characterMovement.GetState();
            CharacterMovementReconcileData reconcileData = new CharacterMovementReconcileData
            {
                position = characterState.position,
                rotation = characterState.rotation,

                velocity = characterState.velocity,

                isConstrainedToGround = characterState.isConstrainedToGround,
                unconstrainedTimer = characterState.unconstrainedTimer,

                isWalkable = characterState.isWalkable,
                hitGround = characterState.hitGround,

                groundNormal = characterState.groundNormal
            };

            Reconciliation(reconcileData, true);
        }
    }

    #endregion

    #region MONOBEHAVIOUR

    private void Awake()
    {
        CacheComponents();
        InstanceFinder.TimeManager.OnTick += OnTick;
        bodyPartsManager = GetComponentInChildren<BodyPartsManager>();
    }

    private void Update()
    {
        Animate();
        SimulateSound();
    }

    private void OnEnable()
    {
        skipGroundCheckThisUpdate = true;
    }

    private void Animate()
    {
        if (IsOwner || IsServer)
        {
            float speedBlend = animator.GetFloat("Speed");
            speedBlend = Mathf.Lerp(speedBlend, characterMovement.speed, Time.deltaTime * 10f);
            animator.SetFloat("Speed", speedBlend);

            animator.SetFloat("DirectionX", Mathf.Lerp(animator.GetFloat("DirectionX"), moveInput.x, Time.deltaTime * 10f));
            animator.SetFloat("DirectionZ", Mathf.Lerp(animator.GetFloat("DirectionZ"), moveInput.y, Time.deltaTime * 10f));

            animator.SetBool("isSkyDiving", characterState == CharacterState.Droping);
            animator.SetBool("QuickDive", sprint && characterState == CharacterState.Droping);

            animator.SetBool("Grounded", characterMovement.isGrounded);

            if (characterMovement.isGrounded) animator.SetBool("Jump", false);

            animator.SetBool("isDead", !health.isAlive);

            if (characterState == CharacterState.Droping)
            {
                animator.SetBool("Landing", landing);
            }
        }
    }

    private bool isFalling = true;
    private bool isDroping = false;
    private bool dropped = false;
    [SerializeField]
    private AudioClip landingSound;
    [SerializeField]
    private AudioClip superLandingSound;

    private void SimulateSound()
    {
        if (animator.GetBool("Grounded") && isFalling && !isDroping)
        {
            AudioSource.PlayClipAtPoint(landingSound, transform.position, FootstepAudioVolume);
            isFalling = false;
        }

        if (animator.GetBool("Grounded") && isDroping)
        {
            AudioSource.PlayClipAtPoint(superLandingSound, transform.position, 1);
            isDroping = false;
            dropped = true;
        }

        if (characterState == CharacterState.Droping && !dropped) isDroping = true;

        if (!animator.GetBool("Grounded")) isFalling = true;
    }

    [SerializeField]
    private AudioClip[] FootstepAudioClips;

    [SerializeField]
    [Range(0, 1)]
    private float FootstepAudioVolume;

    private void FootL(AnimationEvent animationEvent)
    {
        if (animationEvent.animatorClipInfo.weight > 0.5f)
            if (FootstepAudioClips.Length > 0)
            {
                var index = UnityEngine.Random.Range(0, FootstepAudioClips.Length);
                AudioSource.PlayClipAtPoint(FootstepAudioClips[index], transform.position, FootstepAudioVolume);
            }
    }

    private void FootR(AnimationEvent animationEvent)
    {
        if (animationEvent.animatorClipInfo.weight > 0.5f)
            if (FootstepAudioClips.Length > 0)
            {
                var index = UnityEngine.Random.Range(0, FootstepAudioClips.Length);
                AudioSource.PlayClipAtPoint(FootstepAudioClips[index], transform.position, FootstepAudioVolume);
            }
    }


    #endregion
}