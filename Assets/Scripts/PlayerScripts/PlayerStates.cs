using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
    Normal,
    Aiming,
    Casting,
    SkyDiving,
    Dying,
    Freezing,
    Teleporting,
    Attacking,
    Uncontrollable
}

public class PlayerStates : MonoBehaviour
{
    

    public bool isInvincible;

    public State state;

    private void Start()
    {
        state = State.Normal;
        isInvincible = true;
    }

    public bool CanJump()
    {
        if (state == State.Normal || state == State.Aiming) return true;
        else return false;
    }

    public bool CanCast()
    {
        if (state == State.Normal || state == State.Aiming || state == State.Casting) return true;
        else return false;
    }

    public bool CanAim()
    {
        if (state == State.Normal || state == State.Aiming) return true;
        else return false;
    }

    public bool CanMove()
    {
        if (state == State.Normal || state == State.Aiming || state == State.SkyDiving) return true;
        else return false;
    }

    public bool CanAttack()
    {
        if (state == State.Normal) return true;
        else return false;
    }

    public bool CanThrow()
    {
        if (state == State.Normal) return true;
        else return false;
    }
}
