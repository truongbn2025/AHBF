using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using FishNet.Transporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public struct PartID
{
    public int back;
    public int hat;
    public int head;
    public int lArm;
    public int rArm;
    public int shoe;
    public int suit;
    public int trunk;
    public int shield;
    public int weapon;
}

public class BodyPartsManager : NetworkBehaviour
{
    public List<GameObject> backs;
    public List<GameObject> hats;
    public List<GameObject> heads;
    public List<GameObject> lArms;
    public List<GameObject> rArms;
    public List<GameObject> shoes;
    public List<GameObject> suits;
    public List<GameObject> trunks;
    public List<GameObject> shields;
    public List<GameObject> weapons;

    [SyncVar(Channel = Channel.Reliable, OnChange = nameof(OnIDChanged))]
    public string NFTid;

    public event Action LoadingPartReady;

    [SyncVar(Channel = Channel.Reliable, OnChange = nameof(OnPartIDChanged))]
    public PartID partID = new PartID();

    public override void OnStartServer()
    {
        base.OnStartServer();

        GetID(NFTid);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    //[ServerRpc]
    private void GetID(string NFTid)
    {
        NFTDictionary dictionary = new NFTDictionary();
        dictionary.AssignDictionary();

        PartID newPart = new PartID();
        newPart.back = dictionary.myNFTDictionary[NFTid].back;
        newPart.hat = dictionary.myNFTDictionary[NFTid].hat;
        newPart.head = dictionary.myNFTDictionary[NFTid].head;
        newPart.lArm = dictionary.myNFTDictionary[NFTid].lArm;
        
        newPart.rArm = dictionary.myNFTDictionary[NFTid].rArm;

        newPart.shoe = dictionary.myNFTDictionary[NFTid].shoe;
        newPart.trunk = dictionary.myNFTDictionary[NFTid].trunk;
        newPart.suit = dictionary.myNFTDictionary[NFTid].suit;
        newPart.shield = dictionary.myNFTDictionary[NFTid].weaponShield;
        newPart.weapon = 1;
        //newPart.weapon = dictionary.myNFTDictionary[NFTid].weapon;
        
        partID = newPart;
      
    }


    [ServerRpc]
    private void GenerateRandomPartID()
    {
        //print("Calledd");
        PartID newPart = new PartID();
        newPart.back = UnityEngine.Random.Range(0, backs.Count);
        newPart.hat = UnityEngine.Random.Range(0, hats.Count);
        newPart.head = UnityEngine.Random.Range(0, heads.Count);
        newPart.lArm = UnityEngine.Random.Range(0, lArms.Count);
        //newPart.rArm = UnityEngine.Random.Range(0, rArms.Count);
        newPart.rArm = 12;
        newPart.shoe = UnityEngine.Random.Range(0, shoes.Count);
        newPart.trunk = UnityEngine.Random.Range(0, trunks.Count);
        newPart.suit = UnityEngine.Random.Range(0, suits.Count);
        newPart.shield = UnityEngine.Random.Range(0, shields.Count);

        partID = newPart;
    }

    //this where you get the body part
    [ServerRpc]
    private void GenerateRandomSetID()
    {
        //PartID newPart = new PartID();
        ////int idPart = UnityEngine.Random.Range(0, 4);
        //int idPart = 0;
        //newPart.Head = idPart;
        //newPart.Body = idPart;
        //newPart.Thight = idPart;
        //newPart.LArm = idPart;
        //newPart.RArm = idPart;
        //newPart.LLeg = idPart;
        //newPart.RLeg = idPart;

        //partID = newPart;
    }


    private void DisableAllPart()
    {
        foreach (var skin in backs)
            skin.SetActive(false);
        foreach (var skin in hats)
            skin.SetActive(false);
        foreach (var skin in heads)
            skin.SetActive(false);
        foreach (var skin in lArms)
            skin.SetActive(false);
        foreach (var skin in rArms)
            skin.SetActive(false);
        foreach (var skin in shoes)
            skin.SetActive(false);
        foreach (var skin in trunks)
            skin.SetActive(false);
        foreach (var skin in suits)
            skin.SetActive(false);
        foreach (var skin in shields)
            skin.SetActive(false);
    }
    private void OnPartIDChanged(PartID prev, PartID next, bool asServer)
    {
        LoadBodyParts(next);
    }

    private void OnIDChanged(string prev, string next, bool asServer)
    {
        GetID(NFTid);
    }

    private void LoadBodyParts(PartID partID)
    {
        DisableAllPart();
       
        backs[partID.back].SetActive(true);
        hats[partID.hat].SetActive(true);
        heads[partID.head].SetActive(true);
        lArms[partID.lArm].SetActive(true);
        rArms[partID.rArm].SetActive(true);
        shoes[partID.shoe].SetActive(true);
        trunks[partID.trunk].SetActive(true);
        suits[partID.suit].SetActive(true);
        //shields[partID.shield].SetActive(true);


        //GetBodyPartsObject();
        //Debug.Log(partObject.Head.partName);
        //Debug.Log(partObject.Head.partName);
        LoadingPartReady?.Invoke();
    }

    public int GetStrength()
    {
        int stat = 0;
        stat += backs[partID.back].GetComponent<PartStats>().Strength;
        stat += hats[partID.hat].GetComponent<PartStats>().Strength;
        stat += heads[partID.head].GetComponent<PartStats>().Strength;
        stat += lArms[partID.lArm].GetComponent<PartStats>().Strength;
        stat += rArms[partID.rArm].GetComponent<PartStats>().Strength;
        stat += shoes[partID.shoe].GetComponent<PartStats>().Strength;
        stat += trunks[partID.trunk].GetComponent<PartStats>().Strength;
        stat += suits[partID.suit].GetComponent<PartStats>().Strength;

        return stat;

    }

    public int GetAgility()
    {
        int stat = 0;
        stat += backs[partID.back].GetComponent<PartStats>().Agility;
        stat += hats[partID.hat].GetComponent<PartStats>().Agility;
        stat += heads[partID.head].GetComponent<PartStats>().Agility;
        stat += lArms[partID.lArm].GetComponent<PartStats>().Agility;
        stat += rArms[partID.rArm].GetComponent<PartStats>().Agility;
        stat += shoes[partID.shoe].GetComponent<PartStats>().Agility;
        stat += trunks[partID.trunk].GetComponent<PartStats>().Agility;
        stat += suits[partID.suit].GetComponent<PartStats>().Agility;

        return stat;

    }

    public int GetInteligence()
    {
        int stat = 0;
        stat += backs[partID.back].GetComponent<PartStats>().Intelligent;
        stat += hats[partID.hat].GetComponent<PartStats>().Intelligent;
        stat += heads[partID.head].GetComponent<PartStats>().Intelligent;
        stat += lArms[partID.lArm].GetComponent<PartStats>().Intelligent;
        stat += rArms[partID.rArm].GetComponent<PartStats>().Intelligent;
        stat += shoes[partID.shoe].GetComponent<PartStats>().Intelligent;
        stat += trunks[partID.trunk].GetComponent<PartStats>().Intelligent;
        stat += suits[partID.suit].GetComponent<PartStats>().Intelligent;

        return stat;

    }
}
