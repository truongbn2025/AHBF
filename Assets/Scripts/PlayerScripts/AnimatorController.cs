using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    public bool AnimatorReady;
    private Animator animator;

    private int _animIDSpeed;
    private int _animIDGrounded;
    private int _animIDJump;
    private int _animIDFreeFall;
    private int _animIDMotionSpeed;
    private int _animIDDirectionX;
    private int _animIDDirectionZ;
    private int _animIDisDead;
    private int _animIDskyFalling;
    private int _animIDskyDiving;
    private int _animIDquickDiving;

    private void AssignAnimationIDs()
    {
        _animIDSpeed = Animator.StringToHash("Speed");
        _animIDGrounded = Animator.StringToHash("Grounded");
        _animIDJump = Animator.StringToHash("Jump");
        _animIDFreeFall = Animator.StringToHash("FreeFall");
        _animIDMotionSpeed = Animator.StringToHash("MotionSpeed");
        _animIDDirectionX = Animator.StringToHash("DirectionX");
        _animIDDirectionZ = Animator.StringToHash("DirectionZ");
        _animIDisDead = Animator.StringToHash("isDead");
        _animIDskyDiving = Animator.StringToHash("isSkyDiving");
        _animIDquickDiving = Animator.StringToHash("QuickDive");
        _animIDskyFalling = Animator.StringToHash("SkyFalling");
    }

    private void Awake()
    {
        animator = GetComponent<Animator>();
        if (animator != null)
            AnimatorReady = true;
        else
            AnimatorReady = false;
    }

    public void SetMovementAnimation(float speed, Vector2 direction)
    {
        animator.SetFloat(_animIDSpeed, speed);
        animator.SetFloat(_animIDDirectionX, Mathf.Lerp(animator.GetFloat(_animIDDirectionX), direction.x, Time.deltaTime * 10f));
        animator.SetFloat(_animIDDirectionZ, Mathf.Lerp(animator.GetFloat(_animIDDirectionZ), direction.y, Time.deltaTime * 10f));
    }

   

}
