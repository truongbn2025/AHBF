using FishNet.Object;
using UnityEngine;

public class PlayerControlInventory : NetworkBehaviour
{
    //avoid multiple input
    private float InteractTimeoutDelta;
    private float InventoryTimeoutDelta;
    private float Timeout = 0.2f;

    private InputManager _localInput;
    public override void OnStartClient()
    {
        base.OnStartClient();

        TimeManager.OnTick += OnTick;
    }


    private void OnTick()
    {
        InventoryTimeoutDelta -= (float)TimeManager.TickDelta;
        InteractTimeoutDelta -= (float)TimeManager.TickDelta;
    }
    private void ToggleInventory(bool inputData)
    {
        if (inputData && InventoryTimeoutDelta <= 0.0f)
        {


            GetComponent<PlayerInventory>().ToggleInventory();
            _localInput.toggleInventory = false;
            InventoryTimeoutDelta = Timeout;
        }
        //if (InventoryTimeoutDelta > 0.0f)
        //{
        //    InventoryTimeoutDelta -= Time.deltaTime;
        //}

    }
    private void InteractItem(bool inputData)
    {

        if (inputData && InteractTimeoutDelta <= 0.0f)
        {

            GetComponentInChildren<PlayerRaycast>().InteractKeyPressed();
            _localInput.interactItem = false;
            InteractTimeoutDelta = Timeout;
        }

        //if (InteractTimeoutDelta > 0.0f)
        //{
        //    InteractTimeoutDelta -= Time.deltaTime;
        //}
    }

    #region MonoBehavior
    private void Awake()
    {
        _localInput = FindObjectOfType<InputManager>();
    }

    private void Update()
    {
        if (!IsOwner) return;

        ToggleInventory(_localInput.toggleInventory);
        InteractItem(_localInput.interactItem);
    }

    #endregion


}
