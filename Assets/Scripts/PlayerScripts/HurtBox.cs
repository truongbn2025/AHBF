using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtBox : MonoBehaviour
{
    public float Multiplier;
    public event Action<float, NetworkCharacter, string> hit;
    public event Action<float, string, string> hitByBot;


    public void Hit(float weaponDamage, NetworkCharacter source, string weaponName)
    {
        hit?.Invoke(Multiplier * weaponDamage, source, weaponName);
    }
    public void HitByBot(float weaponDamage, string sourceBot, string weaponName)
    {
        Debug.Log("2");
        hitByBot?.Invoke(Multiplier * weaponDamage, sourceBot, weaponName);
    }
}
