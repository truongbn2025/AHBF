using UnityEngine;
using TMPro;
using HighlightPlus;
using UnityEngine.InputSystem;
//using FishNet.Object;
public class PlayerRaycast : MonoBehaviour
{
    [SerializeField] float check_distance = 2f; 
    [SerializeField] private LayerMask what_to_hit;
    [SerializeField] private GameObject helper;
    [SerializeField] private PlayerInventory inventory;
    [SerializeField] private InputActionAsset _inputAction;
    private bool interactKeyPressed;
    private Camera Cam;
    private float interactDistance = 3f;
    private Vector3 size = new Vector3 (0.5f,0.5f,0.5f);
    private float distance = 1;
    private RaycastHit boxCast;
    private string[] keyBindings;
    private HighlightEffect hb;
    

    //Use for debugging
    private bool isHit;

    private void ProcessRaycastHit(string tag, RaycastHit hit)
    {
        switch (tag)
        {
            case "Door":
                helper.SetActive(true);
                helper.GetComponent<TextMeshProUGUI>().text = "Press " + keyBindings[1].ToUpper() + " to Open.";
                if (interactKeyPressed)
                {
                    hit.transform.GetComponent<InteractableTriggerClass>().RequestOpen();
                }
                break;
            case "Item":
                helper.SetActive(true);
                helper.GetComponent<TextMeshProUGUI>().text = "Press " + keyBindings[1].ToUpper() + " to pick up " + hit.transform.GetComponent<GroundItem>().item.name + ".";
                hb = hit.transform.GetComponentInChildren<HighlightEffect>();
                SetHighlighted(true);
                if (interactKeyPressed)
                {
                    inventory.Pickup(hit);
                }
                break;
            case "LootBox":
                helper.SetActive(true);
                helper.GetComponent<TextMeshProUGUI>().text = "Press " + keyBindings[1].ToUpper() + " to Open.";
                if (interactKeyPressed)
                {
                    hit.transform.GetComponent<LootBox>().RequestOpen();
                }
                break;
        }
    }
    public void InteractKeyPressed()
    {
        interactKeyPressed = true;
    }

    #region Monobehavior
    private void Start()
    {
        Cam = Camera.main;
        interactKeyPressed = false;
        //Get key binded to interact action => keyBindings[1]
        keyBindings = _inputAction.actionMaps[0].actions[15].bindings[0].path.Split("/");

        //inventory = GetComponent<PlayerInventory>();
    }
    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(Cam.transform.position, Cam.transform.forward, out hit, float.PositiveInfinity, what_to_hit))
        {

            if (hit.transform.tag == "Door" || hit.transform.tag == "LootBox" || hit.transform.tag == "Item")
            {


                if (Vector3.Distance(transform.position, hit.transform.position) <= interactDistance)
                {

                    ProcessRaycastHit(hit.transform.tag, hit);
                }
                else
                {
                    helper.SetActive(false);
                }

                
            }
            else
            {
                helper.SetActive(false);
            }

        }
        else
        {

            //isHit = Physics.BoxCast(Cam.transform.position, size, Cam.transform.forward, out boxCast, transform.rotation, float.PositiveInfinity, what_to_hit);
            //if (isHit)
            //{
            //    if (boxCast.transform.tag == "Door" || boxCast.transform.tag == "LootBox" || boxCast.transform.tag == "Item")
            //    {
            //        if (Vector3.Distance(transform.position, boxCast.transform.position) <= interactDistance)
            //        {
            //            ProcessRaycastHit(boxCast.transform.tag, boxCast.transform);
            //        }
            //    }
            //    else
            //    {
            //        helper.SetActive(false);
            //    }
            //}
            //else
            //{
            //    helper.SetActive(false);
            //}
            //Debug.DrawRay(transform.position, Cam.transform.forward * check_distance, Color.gray);
            helper.SetActive(false);
            SetHighlighted(false);
            hb = null;
        }

        interactKeyPressed = false;
    }
    #endregion

    #region Effect
    public void SetHighlighted(bool state)
    {
        if(hb != null)
        {
            if (state)
                hb.overlay = 1f;
            else
                hb.overlay = 0f;
        } 
            
        
    }
    #endregion


}
