using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZiplineManager : MonoBehaviour
{
    [SerializeField] private Transform ziplineMotor;

    public void EnableMotor(bool isEnable)
    {
        ziplineMotor.gameObject.SetActive(isEnable);
    }
}
