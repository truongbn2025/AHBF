using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorRotation : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StopAllCoroutines(); //to prevent overlapping
            StartCoroutine(Rotate());
        }
    }

    
    IEnumerator Rotate()
    {
        Quaternion startRotation = transform.rotation;
        float endZRot = 405f;
        float duration = 1f;
        float t = 0;

        while (t < 1f)
        {
            t = Mathf.Min(1f, t + Time.deltaTime / duration);
            Vector3 newEulerOffset = Vector3.forward * (endZRot * t);
            // global z rotation
            transform.rotation = Quaternion.Euler(newEulerOffset) * startRotation;
            // local z rotation
            // transform.rotation = startRotation * Quaternion.Euler(newEulerOffset);
            yield return null;
        }
    }
}
