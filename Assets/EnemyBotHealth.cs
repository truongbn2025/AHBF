using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using System;
using FishNet.Connection;

public class EnemyBotHealth : NetworkBehaviour
{
    [SyncVar]
    public float health;
    [SyncVar]
    public float shield;

    //[SyncVar]
    public bool isInvincible = true;
    public bool isAlive = true;

    Animator animator;

    //public event Action<NetworkCharacter, string> Die;
    private HurtBox[] hurtBoxes;

    private void Awake()
    {
        hurtBoxes = GetComponentsInChildren<HurtBox>();
        animator = GetComponent<Animator>();

    }

    public override void OnStartNetwork()
    {
        base.OnStartNetwork();
        if (base.IsServer)
        {
            health = 100;
            shield = 100;

        }

        foreach (HurtBox hurtBox in hurtBoxes)
        {
            hurtBox.hit += ReduceHealth;
        }
    }

    
    public void OnDestroy()
    {
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            hurtBox.hit -= ReduceHealth;
        }
    }


    public void ReduceHealth(float amount, NetworkCharacter source, string weaponName)
    {
        if (isInvincible || !isAlive) return;



        if (shield > 0)
        {
            shield -= amount;
   

        }
        else
        {
            //Debug.Log(health);
            health -= amount;
    

        }

        if (health <= 0)
        {
            
            health = 0;
            isAlive = false;
            
            animator.SetBool("isDead", true);
            source.KillCount++;
            GetComponent<EnemyBotBehaviour>().enabled = false;
            
        }
    }

    public void SetIgnoreRaycast(bool isIgnore)
    {
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            if (isIgnore) hurtBox.gameObject.layer = 2;
            else hurtBox.gameObject.layer = 3;
        }
    }
}
