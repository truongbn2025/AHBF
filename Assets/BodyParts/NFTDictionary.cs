using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using System.IO;
public struct Bodypart
{
    public int back;
    public int hat;
    public int head;
    public int lArm;
    public int rArm;
    public int shoe;
    public int suit;
    public int trunk;
    public int weapon;
    public int weaponShield;

    public Bodypart(int thisBack, int thisHat, int thisHead,int thisLArm, int thisRArm,
        int thisShoe, int thisSuit, int thisTrunk, int thisWeapon, int thisShield)
    {
        back = thisBack;
        hat = thisHat;
        head = thisHead;
        lArm = thisLArm;
        rArm = thisRArm;
        shoe = thisShoe;
        suit = thisSuit;
        trunk = thisTrunk;
        weapon = thisWeapon;
        weaponShield = thisShield;
    }
}

public class NFTDictionary
{
    [System.Serializable]
    public class NFTid
    {
        public string Name;
        public int Back;
        public int HAT;
        public int Head;
        public int Left;
        public int Right;
        public int Shoes;
        public int Suit;
        public int Trunk;
        public int Weapon;
        public int Shield;
    }

    [System.Serializable]
    public class ListNFTid
    {
        public NFTid[] List;
    }

    public ListNFTid myList;
    //public TextAsset json;
    public Dictionary<string, Bodypart> myNFTDictionary = new Dictionary<string, Bodypart>();
    public void AssignDictionary()
    {
        var path = Application.dataPath + "/StreamingAssets/NFT_id_export.json";
        var json = File.ReadAllText(path);
        myList = JsonUtility.FromJson<ListNFTid>(json);
        foreach (NFTid id in myList.List)
        {
            myNFTDictionary.Add(id.Name, new Bodypart(id.Back, id.HAT, id.Head, id.Left, id.Right, id.Shoes, id.Suit, id.Trunk, id.Weapon, id.Shield));
        }
    }

}


