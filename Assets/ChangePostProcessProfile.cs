using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.Rendering;
using UnityEngine.Profiling;
using UnityEngine.Rendering.Universal;

public enum PROFILE_STATE
{
    DEFAULT,
    OUTSIDE
}

public class ChangePostProcessProfile : MonoBehaviour
{
    public Volume postProcessVolume; 
    
    public VolumeProfile profileDefault; 
    public VolumeProfile profileOutsideZone;
    void Start()
    {
        postProcessVolume = GetComponent<Volume>();
        postProcessVolume.sharedProfile = profileDefault;
    }

    public void ChangeProfile(PROFILE_STATE state)
    {
        switch (state)
        {
            case PROFILE_STATE.DEFAULT:
                postProcessVolume.sharedProfile = profileDefault;
                break; 
            case PROFILE_STATE.OUTSIDE:
                postProcessVolume.sharedProfile = profileOutsideZone;
                break;
        }
    }
    
    
}
